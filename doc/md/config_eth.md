### Introduction

This document does explane how to config the device with software through the ETH interfaces.

For more details check https://techdock.ch/de/downloads/documents

[TOC]

#### Protocoll

- ASCII based commuication over TCP/IP socket at port 10000
- Startsign: @
- Stopsign: \r\n (carrier return line feed)

#### ETH Interface

- all three interfaces are usable for protocoll
- client initiated socket will be automatically closed from server on device after 5 sec. without commuication to prevent socket to be occupied.

#### Parameter

Config device with USB-storage is depending of the follow parameters.

- min: smallest possible string
- std: value when delivered or after use function code FC32 on device
- max: biggest possible string
- PA: Parameter
- TP: Text-Parameter for events and emails

**ETH0**

| PA| Function | min | std | max |
|--------|--------|--------|--------|--------|
| 101 | IPv4  | 0.0.0.0 | 192.168.19.77  | 255.255.255.255 |
| 102 | Subnet | 0.0.0.0  | 192.168.19.1  | 255.255.255.255 |
| 111 | Application Port (0 = off) | 0 | 10000 | 65535 |
| 112 | Deactivate (0), activate PA reading (1) and writing (2) | 0 | 2 | 2 |
| 113 | Deactivate (0), activate TP reading (1) and writing (2) | 0 | 2 | 2 |

Settings of ETH1 and -2 can be found at [Homepage](https://techdock.ch/de/downloads/documents)

#### Usage

As soon a valid ASCII string is send to the ETH, the server will send back the value of the request after the delimiter.

**Example**

| send | answer |
|--------|--------|
| @PA51\r\n | @PA51.control4log\r\n |

For more details about parameters, download document [control4log Software Functional Description](https://techdock.ch/en/downloads/documents/control4log/id:d63541099c362b2aa877d87ac786e159) from [Homepage](https://techdock.ch).

#### Communication software

For start download software [Any Where Promo](https://techdock.ch/en/downloads/software/id:c79b9baff5007c41976f6c4ec665b3ff)from [Homepage](https://techdock.ch).


#### Request single PA

![single.png](./pic_batch.png)


#### Request a batch of PA's

![batch.png](./pic_batch.png)


#### Info

- Downlaod [here](https://techdock.ch/en/downloads/documents/control4log/id:a7fe72f59c53309675dd0bf6f786af0c) a set of batch files which can be used with software **Any Where Promo**
- For more options check documentaation from device at [Homepage](https://techdock.ch/de/downloads/documents)