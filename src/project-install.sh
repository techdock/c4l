#!/bin/bash

# file		project-install.sh
# brief		script for project control4log
# details	copy all needed project files to install-directory
# date		19.07.2021
# author	Marcel Ming
# copyright	TRiMADA AG	CH-5610 Wohlen

# version	19.07.21 mm	- creating
#		18.08.21 mm	- adapt to new directory structure
#				- add srcdir and bindir as variable
#		20.08.21 mm	- add dhcpcd.conf and rt_tables
#		23.08.21 mm	- add configntp
#		12.11.21 mm	- add usbevent
#		25.04.23 mimi	- make script dynamic
#				- rename from "install-11007.sh" to "project-install.sh"
#

# declare script variables and dierctories
project="control4log"
name="c4l"
filename=$(basename $0)

printf "\n### Running ${filename} to install project ${project}-files and symlinks\n"

# create instdir = /opt/<name>/
instdir="/opt/${name}"
printf "Install directory is:	${instdir}\n"

# create srcdir = /home/<user>/projects/<project>/src/
srcdir="$(cd -P -- "$(dirname -- "$0")" && pwd -P)"
printf "Source directory is:	${srcdir}\n"

# create bindir = /home/<user>/projects/<project>/opt/<name>/
cd srcdir > /dev/null 2>&1 # redirect output for make cmd silent
cd ../ > /dev/null 2>&1 # redirect output for make cmd silent

# bindir = /home/<user>/projects/<project>/opt/<name>/
bindir="$(cd -P -- "$(dirname -- "$0")" && pwd -P)${instdir}"
printf "Binary directory is:	${bindir}\n"

# create instdir
sudo mkdir $instdir > /dev/null 2>&1 # redirect output for make cmd silent if directory allready exisitng


# copy project binaries and create symlinks for services

## eth
printf "\n# handle config of eth installation\n"

echo "copy ${bindir}/configeths to ${instdir}/configeths"
sudo cp -f $bindir/configeths $instdir/configeths

echo "copy ${bindir}/interfaces-template to ${instdir}/interfaces-template"
sudo cp -f $bindir/interfaces-template $instdir/interfaces-template

echo "copy ${bindir}/dhcpcd.conf to /etc/dhcpcd.conf"
sudo cp -f $bindir/dhcpcd.conf /etc/dhcpcd.conf

echo "copy ${bindir}/rt_tables to /etc/iproute2/rt_tables"
sudo cp -f $bindir/rt_tables /etc/iproute2/rt_tables

echo "create softlink /etc/systemd/system/configeths.service and enable service"
sudo ln -s -f $srcdir/tools/configeths/configeths.service /etc/systemd/system/configeths.service
sudo systemctl enable configeths.service

## database
printf "\n# handle database installation\n"

echo "copy ${bindir}/dbcreate to ${instdir}/create"
sudo cp -f $bindir/dbcreate $instdir/dbcreate

echo "copy ${bindir}/dbdelete to ${instdir}/dbdelete"
sudo cp -f $bindir/dbdelete $instdir/dbdelete

echo "copy ${bindir}/11007-PA-default.csv to ${instdir}/11007-PA-default.csv"
sudo cp -f $bindir/11007-PA-default.csv $instdir/11007-PA-default.csv

echo "copy ${bindir}/11007-TP-default.csv to ${instdir}/11007-TP-default.csv"
sudo cp -f $bindir/11007-TP-default.csv $instdir/11007-TP-default.csv

## logger
echo "copy ${bindir}/ioLog to ${instdir}/ioLog"
sudo cp -f $bindir/ioLog $instdir/ioLog

echo "create softlink /etc/systemd/system/ioLog.service and enable service"
sudo ln -s -f $srcdir/iolog/ioLog.service /etc/systemd/system/ioLog.service
sudo systemctl enable ioLog.service

## ntp
printf "\n# handle config of ntp installation\n"

echo "copy ${bindir}/configntp to ${instdir}/configntp"
sudo cp -f $bindir/configntp $instdir/configntp

echo "copy ${bindir}/interfaces-template to ${instdir}/interfaces-template"
sudo cp -f $bindir/timesyncd-template.conf $instdir/timesyncd-template.conf

echo "create softlink /etc/systemd/system/configntp.service and enable service"
sudo ln -s -f $srcdir/tools/configntp/configntp.service /etc/systemd/system/configntp.service
sudo systemctl enable configntp.service

## socket server
printf "\n# handle tcp socket server installation\n"

echo "copy ${bindir}/tcpss to ${instdir}/tcpss"
sudo cp -f $bindir/tcpss $instdir/tcpss

echo "copy ${srcdir}/tcpss-start.sh to ${instdir}/tcpss-start.sh"
sudo cp -f $srcdir/tcpss-start.sh $instdir/tcpss-start.sh

echo "create softlink /etc/systemd/system/tcpss.service and enable service"
sudo ln -s -f $srcdir/tcpss/tcpss.service /etc/systemd/system/tcpss.service
sudo systemctl enable tcpss.service

## usb
printf "\n# handle USB config installation\n"

echo "copy ${bindir}/usbhello to ${instdir}/usbhello"
sudo cp -f $bindir/usbhello $instdir/usbhello

echo "create softlink /etc/udev/rules.d/custom_usb.rules"
sudo ln -s -f $srcdir/tools/usbhello/custom_usb.rules /etc/udev/rules.d/custom_usb.rules

echo "copy ${bindir}/usbevent to ${instdir}/usbevent"
sudo cp -f $bindir/usbevent $instdir/usbevent

echo "create softlink /etc/systemd/system/usbevent.service and enable service"
sudo ln -s -f $srcdir/tools/usbevent/usbevent.service /etc/systemd/system/usbevent.service
sudo systemctl enable usbevent.service


# build db new with binaries in instdir

## delete first db and then create again the db
sudo $instdir/dbdelete
sudo $instdir/dbcreate


# end skript
printf "### end ${filename}\n\n"
