#!/bin/bash

# file		tcpss-start.sh
# brief		start script for tcpss from project control4log
# details	strating socket server for each eth interface as own entity
# date		23.11.2021
# author	Marcel Ming
# copyright	TRiMADA AG	CH-5610 Wohlen

# version	23.11.21 mm	- creating
#		25.04.23 mimi	- make skript dynamic
#

# declare script variables and dierctories
project="control4log"
name="c4l"
filename=$(basename $0)

printf "\n### Running ${filename} to start service *tcpss* eth0, -1 & -2 for project ${project} services\n"

# create instdir = /opt/<name>/
instdir="/opt/${name}"
printf "Install directory is:   ${instdir}\n"


# starting process
printf "starting tcpss for eth 0 for ${name} of project ${project}"

$instdir/tcpss -0 &
$instdir/tcpss -1 &
$instdir/tcpss -2 &


# end skript
printf "### end ${filename}\n\n"
