/**************************************************************************//**
 *
 *	\file		db.c
 *
 *	\brief		Stellt allgemeine mySQL Datenbank Funktionen zur Verfügung.
 *
 *	\details	https://zetcode.com/db/mysqlc/
 *
 *	\date		23.02.2021
 *	\author		Marcel Ming
 *	\copyright	TRiMADA AG   CH-5610 Wohlen
 *	\remark		Compiler- / IDE-Version
 *
 *	\version	23.02.21 mm
 *					- Erstellt.
 *	\version	19.03.21 mm
 *					- Tabelle PA mit checkRange erweitert um zu definieren, ob
 *					  ein Tabelleneintrag auf Min- / Maximum überprüft werden
 *					  kann. 
 *	\version	26.03.21 mm
 *					- Anpassungen für die Änderungen in der Struktur ts_db_tp
 *					  implementiert.
 *	\version	06.05.21 mm
 *					- Anpassungen für die Integrierung der EV, PA, TP und VA
 *					  Tabellen implementiert.
 *	\version	07.05.21 mm
 *					- Anpassungen an Pflichtenheft Revision 4.
 *	\version	20.05.21 mm
 *					- Anpassungen an die Änderungen der Tabellenstrukturelemente
 *					  in ts_db_xy.
 *	\version	31.05.21 mm
 *					- Funktion db_tableCreateAllDatalists implementiert.
 *	\version	10.06.21 mm
 *					- Funktion db_csvWriteDataLists und db_rowCount implementiert.
 * 	\version	13.07.21 ch
 * 					- Funktion db_PArowRangeCheck ergänzt
 *	\version	03.08.21 mm
 *					- Debugmeldungen angepasst.
 * 	\version	04.08.21 ch
 * 					- db_rowUpdate so ergänzt, dass erkannt werden kann, ob die
 * 					  ID vorhanden ist oder nicht.
 *	\version	06.08.21 ch
 * 					- add rowDelete Funktion
 * 	\version	09.08.21 ch
 * 					- db_datalistCutToLimitation() und db_datalistsCut() programmiert
 * 					- ID wird bei db_copyRowResultToTableStruct() neu auch zurück
 * 					  kopiert.
 * 	\version	12.08.21 mm
 *					- Funktion db_stripText implementiert.
 * 	\version	19.08.21 ch
 * 					- db_PArowRangeCheck optimiert mit isdigit() funktion
 * 	\version	20.08.21 mm
 *					- Funktion db_tableDelete implementiert.
 *	\version	24.08.21 ch
 * 					- db_PArowRangeCheck optimiert: negative Werte blieben an 
 * 					  isdigit() hängen und wurden als ungültig deklariert.
 *					- db_vaUpdateTime wird bei leerer Tabelle durch add ersetzt.
 * 	\version	26.10.21 mm
 *					- csv Exporte in der Funktion db_csvWriteDataLists mit
 *					  mySQL Befehlen realisiert, da sich das Auslesen von 'Hand'
 *					  über mehrere Stunden zieht.
 *					- Funktionen db_tableExport und db_buildQueryStringExport
 *					  implementiert.
 * 	\version	12.11.21 mm
 * 					- Funktionen db_flagClear, db_flagSet und db_flagGet
 *					  implementiert. 
 * 	\version	25.11.21 mm
 *					- Funktion db_tablePermission implementiert.
 * 	\version	29.11.21 mm
 *					- in Funktion db_tablePermission Permissions für Tabellentypen
 *					  FC und SC ergänzt.
 *					- db_rowGet wurde falsch mit tableType anstelle von E_db_table-
 *					  TypePA aufgerufen. 
 * 	\version	14.12.21 mm
 *					- Logging mit Beachtung des Sommerzeitflags (PA7) implementiert.
 * 	\version	17.12.21 mm
 *					- Anpassungen für Strukturerweiterung von ts_db_flag um
 *					  strDescription.
 *	\version	18.02.22 mm
 *					- Kommentar ergänzt / korrigiert.
 * 	\version	29.09.23 mm
 *					- Funktion db_vaNumberToFlagIndex implementiert.
 * 	\version	19.10.23 mm
 *					- Funktion db_vaNumberToFlagIndex gelöscht und als
 *					  va_numberToFlagIndex in va.c implementiert um Linker-
 *					  abhängigkeiten zu reduzieren.
 *
 *
 ******************************************************************************/


/******************************************************************************
 * Modules                                                                    *
 *                                                                            *
 ******************************************************************************/
#include <errno.h>
#include <mysql/mysql.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>

#include "csv.h"
#include "db.h"


/******************************************************************************
 * Definitions and Macros                                                     *
 *                                                                            *
 ******************************************************************************/
const char *C_db_tableCreateStr [E_db_tableTypeCount] = {
	"CREATE TABLE AI (id INT PRIMARY KEY, value VARCHAR(255))",
	"CREATE TABLE AO (id INT PRIMARY KEY, value VARCHAR(255))",
	"CREATE TABLE DI (id INT PRIMARY KEY, value VARCHAR(255))",
	"CREATE TABLE DO (id INT PRIMARY KEY, value VARCHAR(255))",
	"CREATE TABLE DL (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, value VARCHAR(255), dateTime DATETIME)",
	"CREATE TABLE EV (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, value VARCHAR(255), dateTime DATETIME)",
	"CREATE TABLE FLAG (id INT PRIMARY KEY, value TINYINT, description VARCHAR(255))",
	"CREATE TABLE PA (id INT PRIMARY KEY, min VARCHAR(255), value VARCHAR(255), max VARCHAR(255), scale VARCHAR(255), checkRange INT)",
	"CREATE TABLE TP (id INT PRIMARY KEY, text VARCHAR(255))",
	"CREATE TABLE VA (id INT PRIMARY KEY, description VARCHAR(255))",
};

const char *C_db_tableDeleteStr [E_db_tableTypeCount] = {
	"DROP TABLE AI",
	"DROP TABLE AO",
	"DROP TABLE DI",
	"DROP TABLE DO",
	"DROP TABLE DL",
	"DROP TABLE EV",
	"DROP TABLE FLAG",
	"DROP TABLE PA",
	"DROP TABLE TP",
	"DROP TABLE VA",
};

// Textdefinitionen zu te_db_tableType;
const char *C_db_tableTypeStr [E_db_tableTypeCount] = {
	"AI",
	"AO",
	"DI",
	"DO",
	"DL",
	"EV",
	"FLAG",
	"PA",
	"TP",
	"VA",
};


// Row Count definitionen zu te_db_tableType;
const char C_db_tableTypeRowCount [E_db_tableTypeCount] = {
	2,
	2,
	2,
	2,
	3,
	3,
	2,
	6,
	2,
	2,
};


// Textdefinitionen zu te_db_listType;
const char *C_db_listTypeStr [E_db_listTypeCount] = {
	"",
	"AI",
	"AO",
	"DI",
	"DO",
	"VA",
};


const char C_db_listTypeChannelCount [E_db_listTypeCount] = {
	0,
	2,		//!< ai analog Eingänge
	2,		//!< ao analog Ausgänge
	4,		//!< di digital Eingänge
	4,		//!< do digital Ausgänge
	4,		//!< va analoge Ein-/Ausgänge skaliert
};


/******************************************************************************
 * Typedefs and Structures                                                    *
 *                                                                            *
 ******************************************************************************/
 
/******************************************************************************
 * External Variables                                                         *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Global Variables                                                           *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Local Functions                                                            *
 *                                                                            *
 ******************************************************************************/

/**************************************************************************//**
 *
 *	\brief		db_buildQueryStringCount
 *
 *	\details	fügt den Abfragestring für einen mySQL COUNT (*) Befehl zusammen.
 *
 *	\param		*pQueryString	Pointer auf Abfragestring
 *	\param		tableType		Tabellentyp gemäss Enum te_db_tableType 
 *	\param		listType		Listentyp gemäss Enum te_db_listType
 *	\param		channel			Kannalnummer des Listentyps [1..4]
 *
 *	\return		Abfragestring
 *
 ******************************************************************************/
static char *db_buildQueryStringCount (char				*pQueryString,
									   te_db_tableType	tableType,
									   te_db_listType	listType,
									   int				channel)
{
	if ((tableType < E_db_tableTypeCount) && (listType < E_db_listTypeCount))
	{
		if (listType == E_db_listTypeNone)
		{
			snprintf (pQueryString, C_db_queryStringSize, "SELECT COUNT(*) FROM %s",
									C_db_tableTypeStr [tableType]);
		}
		else
		{
			snprintf (pQueryString, C_db_queryStringSize, "SELECT COUNT(*) FROM %s%s%d",
									C_db_tableTypeStr [tableType], C_db_listTypeStr [listType], channel);
		} // else if (listType == E_db_listTypeNone)
	}
	else
	{
		pQueryString = "";
	} // else if ((tableType < E_db_tableTypeCount) && (listType < E_db_listTypeCount))

	#ifdef __db_showQueryStrings
		fprintf (stderr, "db_buildQueryStringCount: %s\n", pQueryString);
	#endif

	return (pQueryString);
} /* db_buildQueryStringCount */


/**************************************************************************//**
 *
 *	\brief		db_buildQueryStringDelete
 *
 *	\details	fügt den Abfragestring für einen mySQL Delete Befehl zusammen.
 *
 *	\param		*pQueryString	Pointer auf Abfragestring
 *	\param		tableType		Tabellentyp gemäss Enum te_db_tableType 
 *	\param		listType		Listentyp gemäss Enum te_db_listType
 *	\param		channel			Kannalnummer des Listentyps [1..4]
 *	\param		startID			Start ID vom zu löschenden Bereich
 *	\param		endID			End ID vom zu löschenden Bereich
 *
 *	\return		Abfragestring
 *
 ******************************************************************************/
static char *db_buildQueryStringDelete (char			*pQueryString,
										te_db_tableType	tableType,
										te_db_listType	listType,
										int				channel,
										int				startID,
										int				endID)
{
	if ((tableType < E_db_tableTypeCount) && (listType < E_db_listTypeCount))
	{
		if (listType == E_db_listTypeNone)
		{
			snprintf (pQueryString, C_db_queryStringSize, "DELETE FROM %s WHERE ID BETWEEN %d AND %d",
									C_db_tableTypeStr [tableType], startID, endID);
		}
		else
		{
			snprintf (pQueryString, C_db_queryStringSize, "DELETE FROM %s%s%d WHERE ID BETWEEN %d AND %d",
									C_db_tableTypeStr [tableType], C_db_listTypeStr [listType], channel,
									startID, endID);
		} // else if (listType == E_db_listTypeNone)
	}
	else
	{
		fprintf (stderr, "db_buildQueryStringDelete: error: tabletype or listetype out of range\n");
		pQueryString = "";
	} // else if ((tableType < E_db_tableTypeCount) && (listType < E_db_listTypeCount))

	#ifdef __db_showQueryStrings
		fprintf (stderr, "db_buildQueryStringDelete: %s\n", pQueryString);
	#endif

	return (pQueryString);
} /* db_buildQueryStringDelete */


/**************************************************************************//**
 *
 *	\brief		db_buildQueryStringInsert
 *
 *	\details	fügt den Abfragestring für einen mySQL INSERT Befehl zusammen.
 *
 *	\param		*pQueryString	Pointer auf Abfragestring
 *	\param		tableType		Tabellentyp gemäss Enum te_db_tableType 
 *	\param		listType		Listentyp gemäss Enum te_db_listType
 *	\param		channel			Kannalnummer des Listentyps [1..4]
 *	\param		*pTable			Pointer auf Datensatz Struktur
 *
 *	\return		Abfragestring
 *
 ******************************************************************************/
static char *db_buildQueryStringInsert (char			*pQueryString,
										te_db_tableType	tableType,
										te_db_listType	listType,
										int				channel,
										void			*pTable)
{
	switch (tableType)
	{
		case E_db_tableTypeAI:
		{
			ts_db_ai *pDbAI = pTable;
			snprintf (pQueryString, C_db_queryStringSize,
			          "INSERT INTO AI (id, value) VALUES(%d,'%s')",
			          pDbAI->id, pDbAI->strValue);
		}
		break;

		case E_db_tableTypeAO:
		{
			ts_db_ao *pDbAO = pTable;
			snprintf (pQueryString, C_db_queryStringSize,
			          "INSERT INTO AO (id, value) VALUES(%d,'%s')",
			          pDbAO->id, pDbAO->strValue);
		}
		break;

		case E_db_tableTypeDI:
		{
			ts_db_di *pDbDI = pTable;
			snprintf (pQueryString, C_db_queryStringSize,
			          "INSERT INTO DI (id, value) VALUES(%d,'%s')",
			          pDbDI->id, pDbDI->strValue);
		}
		break;

		case E_db_tableTypeDO:
		{
			ts_db_do *pDbDO = pTable;
			snprintf (pQueryString, C_db_queryStringSize,
			          "INSERT INTO DO (id, value) VALUES(%d,'%s')",
			          pDbDO->id, pDbDO->strValue);
		}
		break;

		case E_db_tableTypeDL:
		{
 			ts_db_dl *pDbDL = pTable;
			// Spalte id wird von mySql automatisch eingetragen, da AUTO_INCREMENT
			snprintf (pQueryString, C_db_queryStringSize,
			          "INSERT INTO DL%s%d (value, datetime) VALUES('%s', CURRENT_TIMESTAMP)",
			           C_db_listTypeStr [listType], channel, pDbDL->strValue);
		}
		break;

		case E_db_tableTypeEV:
		{
			ts_db_ev *pDbEV = pTable;
			// Spalte id wird von mySql automatisch eingetragen, da AUTO_INCREMENT
			snprintf (pQueryString, C_db_queryStringSize,
					  "INSERT INTO EV (value, datetime) VALUES('%s', CURRENT_TIMESTAMP)",
					  pDbEV->strValue);
		}
		break;

		case E_db_tableTypeFLAG:
		{
			ts_db_flag *pDbFlag = pTable;
			snprintf (pQueryString, C_db_queryStringSize,
			          "INSERT INTO FLAG (id, value, description) VALUES(%d, %d, '%s')",
			          pDbFlag->id, pDbFlag->value, pDbFlag->strDescription);
		}
		break;

		case E_db_tableTypePA:
		{
			ts_db_pa *pDbPA = pTable;
			snprintf (pQueryString, C_db_queryStringSize,
			          "INSERT INTO PA (id, min, value, max, scale, checkRange) VALUES(%d,'%s','%s','%s','%s', %d)",
			          pDbPA->id, pDbPA->strMin, pDbPA->strValue, pDbPA->strMax, pDbPA->strScale, pDbPA->checkRange);
		}
		break;

		case E_db_tableTypeTP:
		{
			ts_db_tp *pDbTP = pTable;
			snprintf (pQueryString, C_db_queryStringSize,
			          "INSERT INTO TP (id, text) VALUES(%d,'%s')",
			          pDbTP->id, pDbTP->strText);
		}
		break;

		case E_db_tableTypeVA:
		{
			ts_db_va *pDbVA = pTable;
			snprintf (pQueryString, C_db_queryStringSize,
			          "INSERT INTO VA (id, description) VALUES(%d,'%s')",
			          pDbVA->id, pDbVA->strDescription);
		}
		break;

		default:
			fprintf (stderr, "db_buildQueryStringInsert: error: tabletype not supported\n");
			pQueryString = "";
			break;

	} // switch (tableType)

	#ifdef __db_showQueryStrings
		fprintf (stderr, "db_buildQuerystringInsert: %s\n", pQueryString);
	#endif

	return (pQueryString);
} /* db_buildQueryStringInsert */


/**************************************************************************//**
 *
 *	\brief		db_buildQueryStringUpdate
 *
 *	\details	fügt den Abfragestring für einen mySQL UPDATE Befehl zusammen.
 *
 *	\param		*pQueryString	Pointer auf Abfragestring
 *	\param		tableType		Tabellentyp gemäss Enum te_db_tableType 
 *	\param		*pTable			Pointer auf Datensatz Struktur
 *
 *	\return		Abfragestring
 *
 ******************************************************************************/
static char *db_buildQueryStringUpdate (char			*pQueryString,
										te_db_tableType	tableType,
										void			*pTable)
{
	switch (tableType)
	{
		case E_db_tableTypeAI:
		{
			ts_db_ai *pDbAI = pTable;
			snprintf (pQueryString, C_db_queryStringSize,
			          "UPDATE AI SET value='%s' WHERE id=%d",
			          pDbAI->strValue, pDbAI->id);
		}
		break;

		case E_db_tableTypeAO:
		{
			ts_db_ao *pDbAO = pTable;
			snprintf (pQueryString, C_db_queryStringSize,
			          "UPDATE AO SET value='%s' WHERE id=%d",
			          pDbAO->strValue, pDbAO->id);
		}
		break;

		case E_db_tableTypeDI:
		{
			ts_db_di *pDbDI = pTable;
			snprintf (pQueryString, C_db_queryStringSize,
			          "UPDATE DI SET value='%s' WHERE id=%d",
			          pDbDI->strValue, pDbDI->id);
		}
		break;

		case E_db_tableTypeDO:
		{
			ts_db_do *pDbDO = pTable;
			snprintf (pQueryString, C_db_queryStringSize,
			          "UPDATE DO SET value='%s' WHERE id=%d",
			          pDbDO->strValue, pDbDO->id);
		}
		break;
			
		case E_db_tableTypeDL:
		{
			// ist gibt keinen Update einer Datenliste
		}
		break;

		case E_db_tableTypeEV:
		{
			// ist gibt keinen Update eines Events
		}
		break;

		case E_db_tableTypeFLAG:
		{
			ts_db_flag *pDbFlag = pTable;
			snprintf (pQueryString, C_db_queryStringSize,
			          "UPDATE FLAG SET value=%d WHERE id=%d",
			          pDbFlag->value, pDbFlag->id);
		}
		break;

		case E_db_tableTypePA:
		{
			ts_db_pa *pDbPA = pTable;
			snprintf (pQueryString, C_db_queryStringSize,
			          "UPDATE PA SET min='%s', value='%s', max='%s', scale='%s', checkRange='%d' WHERE id=%d",
			          pDbPA->strMin, pDbPA->strValue, pDbPA->strMax, pDbPA->strScale, pDbPA->checkRange, pDbPA->id);
		}
		break;

		case E_db_tableTypeTP:
		{
			ts_db_tp *pDbTP = pTable;
			snprintf (pQueryString, C_db_queryStringSize,
			          "UPDATE TP SET text='%s' WHERE id=%d",
			          pDbTP->strText, pDbTP->id);
		}
		break;

		case E_db_tableTypeVA:
		{
			ts_db_va *pDbVA = pTable;
			snprintf (pQueryString, C_db_queryStringSize,
			          "UPDATE VA SET description='%s' WHERE id=%d",
			          pDbVA->strDescription, pDbVA->id);
		}
		break;

		default:
			fprintf (stderr, "db_buildQueryStringUpdate: error: tabletype not supported\n");
			pQueryString = "";
			break;

	} // switch (tableType)

	#ifdef __db_showQueryStrings
		fprintf (stderr, "db_buildQuerystringUpdate: %s\n", pQueryString);
	#endif

	return (pQueryString);
} /* db_buildQueryStringUpdate */


/**************************************************************************//**
 *
 *	\brief		db_buildQueryStringSelectAll
 *
 *	\details	fügt den Abfragestring für einen mySQL SELECT * Befehl zusammen.
 *
 *	\param		*pQueryString	Pointer auf Abfragestring
 *	\param		tableType		Tabellentyp gemäss Enum te_db_tableType 
 *	\param		listType		Listentyp gemäss Enum te_db_listType
 *	\param		channel			Kannalnummer des Listentyps [1..4]
 *	\param		*pTable			Pointer auf Datensatz Struktur
 *
 *	\return		Abfragestring
 *
 ******************************************************************************/
static char *db_buildQueryStringSelectAll (char				*pQueryString,
										   te_db_tableType	tableType,
										   te_db_listType	listType,
										   int				channel,
										   void				*pTable)
{
	switch (tableType)
	{
		case E_db_tableTypeAI:
		{
			ts_db_ai *pDbAI = pTable;
			snprintf (pQueryString, C_db_queryStringSize,
			          "SELECT * FROM AI WHERE id=%d", channel);
		}
		break;

		case E_db_tableTypeAO:
		{
			ts_db_ao *pDbAO = pTable;
			snprintf (pQueryString, C_db_queryStringSize,
			          "SELECT * FROM AO WHERE id=%d", channel);
		}
		break;

		case E_db_tableTypeDI:
		{
			ts_db_di *pDbDI = pTable;
			snprintf (pQueryString, C_db_queryStringSize,
			          "SELECT * FROM DI WHERE id=%d", channel);
		}
		break;

		case E_db_tableTypeDO:
		{
			ts_db_do *pDbDO = pTable;
			snprintf (pQueryString, C_db_queryStringSize,
			          "SELECT * FROM DO WHERE id=%d", channel);
		}
		break;

		case E_db_tableTypeDL:
		{
 			ts_db_dl *pDbDL = pTable;
			snprintf (pQueryString, C_db_queryStringSize,
			          "SELECT * FROM DL%s%d WHERE id=%d",
			           C_db_listTypeStr [listType], channel, pDbDL->id);
		}
		break;

		case E_db_tableTypeEV:
		{
			ts_db_ev *pDbEV = pTable;
			snprintf (pQueryString, C_db_queryStringSize,
			          "SELECT * FROM EV WHERE id=%d", pDbEV->id);
		}
		break;

		case E_db_tableTypeFLAG:
		{
			ts_db_flag *pDbFLAG = pTable;
			snprintf (pQueryString, C_db_queryStringSize,
			          "SELECT * FROM FLAG WHERE id=%d", pDbFLAG->id);
		}
		break;

		case E_db_tableTypePA:
		{
			ts_db_pa *pDbPA = pTable;
			snprintf (pQueryString, C_db_queryStringSize,
			          "SELECT * FROM PA WHERE id=%d", pDbPA->id);
		}
		break;

		case E_db_tableTypeTP:
		{
			ts_db_tp *pDbTP = pTable;
			snprintf (pQueryString, C_db_queryStringSize,
			          "SELECT * FROM TP WHERE id=%d", pDbTP->id);
		}
		break;

		case E_db_tableTypeVA:
		{
			ts_db_va *pDbVA = pTable;
			snprintf (pQueryString, C_db_queryStringSize,
			          "SELECT * FROM VA WHERE id=%d", pDbVA->id);
		}
		break;

		default:
			fprintf (stderr, "db_buildQuerystringSelectAll: error: tabletype not supported\n");
			pQueryString = "";
			break;

	} // switch (tableType)

	#ifdef __db_showQueryStrings
		fprintf (stderr, "db_buildQuerystringSelectAll: %s\n", pQueryString);
	#endif

	return (pQueryString);
} /* db_buildQueryStringSelectAll */


/**************************************************************************//**
 *
 *	\brief		db_buildQueryStringSelectSorted
 *
 *	\details	fügt den Abfragestring für einen mySQL SELECT * ORDER BY
 *				Befehl dateTime LIMIT x, 1 zusammen.
 *
 *	\param		*pQueryString	Pointer auf Abfragestring
 *	\param		tableType		Tabellentyp gemäss Enum te_db_tableType 
 *	\param		listType		Listentyp gemäss Enum te_db_listType
 *	\param		channel			Kannalnummer des Listentyps [1..4]
 *	\param		*pTable			Pointer auf Datensatz Struktur
 *
 *	\return		Abfragestring
 *
 ******************************************************************************/
static char *db_buildQueryStringSelectSorted (char				*pQueryString,
											  te_db_tableType	tableType,
											  te_db_listType	listType,
											  int				channel,
											  void				*pTable)
{
	switch (tableType)
	{
		case E_db_tableTypeDL:
		{
 			ts_db_dl *pDbDL = pTable;
			snprintf (pQueryString, C_db_queryStringSize,
					  "SELECT * FROM DL%s%d ORDER BY dateTime DESC LIMIT %d, 1",
					  C_db_listTypeStr [listType], channel, pDbDL->id);
		}
		break;

		case E_db_tableTypeEV:
		{
			ts_db_ev *pDbEV = pTable;
			snprintf (pQueryString, C_db_queryStringSize,
					  "SELECT * FROM EV ORDER BY dateTime DESC LIMIT %d, 1", pDbEV->id);
		}
		break;

		default:
			fprintf (stderr, "db_buildQuerystringSelectSorted: error: tabletype not supported\n");
			pQueryString = "";
			break;

	} // switch (tableType)

	#ifdef __db_showQueryStrings
		fprintf (stderr, "db_buildQuerystringSelectSorted: %s\n", pQueryString);
	#endif

	return (pQueryString);
} /* db_buildQueryStringSelectSorted */


/**************************************************************************//**
 *
 *	\brief		db_buildQueryStringExport
 *
 *	\details	fügt den Abfragestring für einen mySQL SELECT * FROM DLxyz ORDER
 *				BY dateTime DESC INTO OUTFILE 'xyz.csv' FIELDS TERMINATED BY ';'
 *				OPTIONALLY ENCLOSED BY '"' ESCAPED BY '' LINES TERMINATED BY '\n'
 *				zusammen.
 *
 *	\param		*pQueryString	Pointer auf Abfragestring
 *	\param		tableType		Tabellentyp gemäss Enum te_db_tableType 
 *	\param		listType		Listentyp gemäss Enum te_db_listType
 *	\param		channel			Kannalnummer des Listentyps [1..4]
 *	\param		pFilename		Filename (inlusive Pfad) für den Export
 *
 *	\return		Abfragestring
 *
 ******************************************************************************/
static char *db_buildQueryStringExport (char			*pQueryString,
										te_db_tableType	tableType,
										te_db_listType	listType,
										int				channel,
										char			*pFilename)
{
	switch (tableType)
	{
		case E_db_tableTypeDL:
		{
			snprintf (pQueryString, C_db_queryStringSize,
					  "SELECT * FROM DL%s%d ORDER BY dateTime DESC INTO OUTFILE '%s' FIELDS TERMINATED BY ';' OPTIONALLY ENCLOSED BY '\"' ESCAPED BY '' LINES TERMINATED BY '\n'",
					  C_db_listTypeStr [listType], channel, pFilename);
		}
		break;

		case E_db_tableTypeEV:
		{
			snprintf (pQueryString, C_db_queryStringSize,
					  "SELECT * FROM EV ORDER BY dateTime DESC INTO OUTFILE '%s' FIELDS TERMINATED BY ';' OPTIONALLY ENCLOSED BY '\"' ESCAPED BY '' LINES TERMINATED BY '\n'",
					  pFilename);
		}
		break;

		default:
			fprintf (stderr, "db_buildQuerystringExport: error: tabletype not supported\n");
			pQueryString = "";
			break;

	} // switch (tableType)

	#ifdef __db_showQueryStrings
		fprintf (stderr, "db_buildQuerystringExport: %s\n", pQueryString);
	#endif

	return (pQueryString);
} /* db_buildQueryStringExport */


/**************************************************************************//**
 *
 *  \brief		db_stripText
 *
 *  \details	löscht alle " aus einem String.
 *
 *	\param		*pText		Pointer auf Text
 *
 *	\return		-
 *
 ******************************************************************************/
static void db_stripText (char *pText)
{
	char	*ptr;


	while (*pText)
	{
		while (*pText == '"')
		{
			for (ptr = pText; *ptr; ptr++)
			{
				*ptr = *(ptr + 1);
			} // for (ptr = pText; *ptr; ptr++)
		} // while (*pText == '"')

		pText++;
	} // while (*pText)
} /* db_stripText */


/**************************************************************************//**
 *
 *  \brief		db_copyRowResultToTableStruct
 *
 *  \details	kopiert das Resultat in row gemäss tableType in die Tabellen-
 *				struktur pTable.
 *
 *	\param		tableType		Tabellentyp gemäss Enum te_db_tableType 
 *	\param		*pTable			Pointer auf Datensatz Struktur
 *	\param		row				Datenreihe
 *
 *	\return		mysql_errno
 *
 ******************************************************************************/
static void db_copyRowResultToTableStruct (te_db_tableType	tableType,
										   void				*pTable,
										   MYSQL_ROW		row)
{
	int	i;


	// Alle " aus den Suchresultaten entfernen
	for (i = 0; i < C_db_tableTypeRowCount [tableType]; i++)
	{
		db_stripText (row [i]);
	}

	switch (tableType)
	{
		case E_db_tableTypeAI:
		{
			ts_db_ai *pDbAI = pTable;
			pDbAI->id = atoi (row [0]);
			snprintf (pDbAI->strValue,	C_db_dataStringSize, "%s", row [1]);
		}
		break;

		case E_db_tableTypeAO:
		{
			ts_db_ao *pDbAO = pTable;
			pDbAO->id = atoi (row [0]);
			snprintf (pDbAO->strValue,	C_db_dataStringSize, "%s", row [1]);
		}
		break;

		case E_db_tableTypeDI:
		{
			ts_db_di *pDbDI = pTable;
			pDbDI->id = atoi (row [0]);
			snprintf (pDbDI->strValue,	C_db_dataStringSize, "%s", row [1]);
		}
		break;

		case E_db_tableTypeDO:
		{
			ts_db_do *pDbDO = pTable;
			pDbDO->id = atoi (row [0]);
			snprintf (pDbDO->strValue,	C_db_dataStringSize, "%s", row [1]);
		}
		break;

		case E_db_tableTypeDL:
		{
 			ts_db_dl *pDbDL = pTable;
			pDbDL->id = atoi (row [0]);
			snprintf (pDbDL->strValue, C_db_dataStringSize, "%s", row [1]);
			snprintf (pDbDL->strDateTime, C_db_dataStringSize, "%s",  row [2]);
		}
		break;

		case E_db_tableTypeEV:
		{
			ts_db_ev *pDbEV = pTable;
			pDbEV->id = atoi (row [0]);
			snprintf (pDbEV->strValue, C_db_dataStringSize, "%s", row [1]);
			snprintf (pDbEV->strDateTime, C_db_dataStringSize, "%s",  row [2]);
		}
		break;

		case E_db_tableTypeFLAG:
		{
			ts_db_flag *pDbFLAG = pTable;
			pDbFLAG->id = atoi (row [0]);
			pDbFLAG->value = atoi (row [1]);
			snprintf (pDbFLAG->strDescription, C_db_dataStringSize, "%s",  row [2]);
		}
		break;

		case E_db_tableTypePA:
		{
			ts_db_pa *pDbPA = pTable;
			pDbPA->id = atoi (row [0]);
			snprintf (pDbPA->strMin,	C_db_dataStringSize, "%s", row [1]);
			snprintf (pDbPA->strValue,	C_db_dataStringSize, "%s", row [2]);
			snprintf (pDbPA->strMax,	C_db_dataStringSize, "%s", row [3]);
			snprintf (pDbPA->strScale,	C_db_dataStringSize, "%s", row [4]);

			pDbPA->checkRange = atoi (row [5]);
		}
		break;

		case E_db_tableTypeTP:
		{
			ts_db_tp *pDbTP = pTable;
			pDbTP->id = atoi (row [0]); 
			snprintf (pDbTP->strText, C_db_dataStringSize, "%s", row [1]);
		}
		break;

		case E_db_tableTypeVA:
		{
			ts_db_va *pDbVA = pTable;
			pDbVA->id = atoi (row [0]);
			snprintf (pDbVA->strDescription, C_db_dataStringSize, "%s", row [1]);
		}
		break;


		default:
			fprintf (stderr, "db_copyRowResultToTableStruct: error: tabletype not supported\n");
			break;

	} // switch (tableType)
} /* db_copyRowResultToTableStruct */


/******************************************************************************
 * Exported Functions                                                         *
 *                                                                            *
 ******************************************************************************/


/**************************************************************************//**
 *
 *	\brief		db_connectInit
 *
 *	\details	initialisiert die mysql Bibliothek und die Zugriffsstruktur auf
 *				die Datenbank.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		*pName		Pointer auf String Name
 *	\param		*pHost		Pointer auf String Host
 *	\param		*pUser		Pointer auf String User
 *	\param		*pPassword	Pointer auf String Password
 *
 *	\return		mysql_errno
 *
 ******************************************************************************/
int db_connectInit (ts_db_connect	*pConnect,
					char			*pName,
					char			*pHost,
					char			*pUser,
					char			*pPassword)
{
	snprintf (pConnect->strDatabase, C_db_connectStringSize, "%s", pName);
	snprintf (pConnect->strHost,	 C_db_connectStringSize, "%s", pHost);
	snprintf (pConnect->strUser,	 C_db_connectStringSize, "%s", pUser);
	snprintf (pConnect->strPassword, C_db_connectStringSize, "%s", pPassword);

	return (mysql_library_init (0, NULL, NULL));
} /* db_connectInit */


/**************************************************************************//**
 *
 *	\brief		db_connectDebug
 *
 *	\details	schreibt die Werte der übergebenen ts_db_connect Struktur auf
 *				stderr.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *
 *	\return		-
 *
 ******************************************************************************/
void db_connectDebug (ts_db_connect *pConnect)
{
	fprintf (stderr, "\033[1mdb_connectDebug\033[0m\n");
	fprintf (stderr, "host    : %s\n", pConnect->strHost);
	fprintf (stderr, "user    : %s\n", pConnect->strUser);
	fprintf (stderr, "password: %s\n", pConnect->strPassword);
	fprintf (stderr, "database: %s\n", pConnect->strDatabase);
} /* db_connectDebug */


/**************************************************************************//**
 *
 *  \brief		db_create
 *
 *  \details	erstellt eine mySQL Datenbank gemäss dem Inhalt der ts_db_connect
 *				Struktur.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *
 *	\return		mysql_errno
 *
 ******************************************************************************/
int db_create (ts_db_connect *pConnect)
{
	char	queryString [C_db_queryStringSize] = "";
	MYSQL	*pConnection = mysql_init (NULL);
	int		retValue = 0;


	if (pConnection == NULL)
	{
		retValue = mysql_errno (pConnection);
	}
	else
	{
		if (mysql_real_connect (pConnection,			// connection handler
								pConnect->strHost,		// hostname
								pConnect->strUser,		// user name
								pConnect->strPassword,	// password
								NULL,					// database name
								0,						// port number
								NULL,					// unix socket
								0) == NULL)				// client flag
		{
			retValue = mysql_errno (pConnection);
		}
		else
		{
			mysql_set_character_set (pConnection, "utf8");
//			mysql_query (pConnection, "SET NAMES 'utf8'");

			snprintf (queryString, C_db_queryStringSize, "CREATE DATABASE %s", pConnect->strDatabase);

			if (mysql_query (pConnection, queryString))
			{
				retValue = mysql_errno (pConnection);
			}
		} // else if (mysql_real_connect (pConnection, ...

		mysql_close (pConnection);
		} // else if (pConnection != NULL)

	return (retValue);
} /* db_create */


/**************************************************************************//**
 *
 *  \brief		db_delete
 *
 *  \details	löscht eine mySQL Datenbank gemäss dem Inhalt der ts_db_connect
 *				Struktur.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *
 *	\return		mysql_errno
 *
 ******************************************************************************/
int db_delete (ts_db_connect *pConnect)
{
	char	queryString [C_db_queryStringSize] = "";
	MYSQL	*pConnection = mysql_init (NULL);
	int		retValue = 0;


	if (pConnection == NULL)
	{
		retValue = mysql_errno (pConnection);
	}
	else
	{
		if (mysql_real_connect (pConnection,			// connection handler
								pConnect->strHost,		// hostname
								pConnect->strUser,		// user name
								pConnect->strPassword,	// password
								pConnect->strDatabase,	// database name
								0,						// port number
								NULL,					// unix socket
								0) == NULL)				// client flag
		{
			retValue = mysql_errno (pConnection);
		}
		else
		{
			snprintf (queryString, C_db_queryStringSize, "DROP DATABASE %s", pConnect->strDatabase);

			if (mysql_query (pConnection, queryString))
			{
				retValue = mysql_errno (pConnection);
			}
		} // else if (mysql_real_connect (pConnection, ...

		mysql_close (pConnection);
		} // else if (pConnection != NULL)

	return (retValue);
} /* db_delete */


/**************************************************************************//**
 *
 *  \brief		db_PArowRangeCheck
 *
 *  \details	überprüft ob der Wert in der übergebenen ts_dbPA Struktur sich
 *				innerhalb von Min und Max befindet.
 *
 *	\param		*pDbPA	Pointer auf Struktur ts_dbPA
 *
 *	\return		- E_dbPA_rangeCheckOk
 * 				- E_dbPA_rangeCheckNok
 * 				- E_dbPA_rangeCheckFail
 *
 ******************************************************************************/
te_db_PArangeCheck db_PArowRangeCheck ( ts_db_pa	*pDbPA)
{
	int	value;
	te_db_PArangeCheck	retValue = E_db_PArangeCheckFail;
	
	if (pDbPA->checkRange)
	{
		if ((isdigit(*pDbPA->strValue)) || (*pDbPA->strValue == '-'))
		{
			value = atoi (pDbPA->strValue);

			if ((value < atoi (pDbPA->strMin)) || (value > atoi (pDbPA->strMax)))
			{
				retValue = E_db_PArangeCheckNok;
			}
			else
			{
				retValue = E_db_PArangeCheckOk;
			}
		} // if ((isdigit(*pDbPA->strValue)) || (*pDbPA->strValue == '-'))
		else
		{
			retValue = E_db_PArangeCheckNok;
		} // else: if ((isdigit(*pDbPA->strValue)) || (*pDbPA->strValue == '-'))
	}
	else
	{
		retValue = E_db_PArangeCheckOk;
	} // else if (pDbPA.checkRange)

	return (retValue);
} /* db_PArowRangeCheck */


/**************************************************************************//**
 *
 *  \brief		db_tableCreate
 *
 *  \details	erstellt eine Tabelle in der mySQL Datenbank gemäss dem Inhalt
 *				der ts_db_connect Struktur und dem te_db_tableType Enum.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		tableType	Tabellentyp gemäss Enum te_db_tableType 
 *
 *	\return		mysql_errno
 *
 ******************************************************************************/
int db_tableCreate (ts_db_connect	*pConnect,
					te_db_tableType	tableType)
{
	MYSQL	*pConnection = mysql_init (NULL);
	int		retValue = 0;


	if (pConnection == NULL)
	{
		retValue = mysql_errno (pConnection);
	}
	else
	{
		if (mysql_real_connect (pConnection,			// connection handler
								pConnect->strHost,		// hostname
								pConnect->strUser,		// user name
								pConnect->strPassword,	// password
								pConnect->strDatabase,	// database name
								0,						// port number
								NULL,					// unix socket
								0) == NULL)				// client flag
		{
			retValue = mysql_errno (pConnection);
		}
		else
		{
			mysql_set_character_set (pConnection, "utf8");
//			mysql_query (pConnection, "SET NAMES 'utf8'");

			if (mysql_query (pConnection, C_db_tableCreateStr [tableType]))
			{
				retValue = mysql_errno (pConnection);
			}
		} // else if (mysql_real_connect (pConnection, ...

		mysql_close (pConnection);
	} // else if (pConnection != NULL)

	return (retValue);
} /* db_tableCreate */


/**************************************************************************//**
 *
 *  \brief		db_tableDelete
 *
 *  \details	löscht eine Tabelle in der mySQL Datenbank gemäss dem Inhalt
 *				der ts_db_connect Struktur und dem te_db_tableType Enum.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		tableType	Tabellentyp gemäss Enum te_db_tableType 
 *
 *	\return		mysql_errno
 *
 ******************************************************************************/
int db_tableDelete (ts_db_connect	*pConnect,
					te_db_tableType	tableType)
{
	MYSQL	*pConnection = mysql_init (NULL);
	int		retValue = 0;


	if (pConnection == NULL)
	{
		retValue = mysql_errno (pConnection);
	}
	else
	{
		if (mysql_real_connect (pConnection,			// connection handler
								pConnect->strHost,		// hostname
								pConnect->strUser,		// user name
								pConnect->strPassword,	// password
								pConnect->strDatabase,	// database name
								0,						// port number
								NULL,					// unix socket
								0) == NULL)				// client flag
		{
			retValue = mysql_errno (pConnection);
		}
		else
		{
			if (mysql_query (pConnection, C_db_tableDeleteStr [tableType]))
			{
				retValue = mysql_errno (pConnection);
			}
		} // else if (mysql_real_connect (pConnection, ...

		mysql_close (pConnection);
	} // else if (pConnection != NULL)

	return (retValue);
} /* db_tableDelete */


/**************************************************************************//**
 *
 *  \brief		db_tablePermission
 *
 *  \details	gibt die Schreib- und Leserechte der Tabelle in der mySQL
 *				Datenbank gemäss dem Inhalt	der ts_db_connect Struktur und dem
 *				te_db_tableType Enum zurück.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		tableType	Tabellentyp gemäss Enum te_db_tableType
 *	\param		ethNumber	Ethernet Schnittstellen Nummer (0..2)
 *
 *	\return		E_db_tablePermissionNone		kein Zugriff
 *				E_db_tablePermissionRead		Zugriff lesen
 *				E_db_tablePermissionReadWrite	Zugriff lesen und schreiben
 *
 ******************************************************************************/
te_db_tablePermission db_tablePermission (ts_db_connect	  *pConnect,
										  te_db_tableType tableType,
										  int			  ethNumber)
{
	te_db_tablePermission retValue = E_db_tablePermissionNone;
	ts_db_pa			  dbPA;


	#ifdef __db_showTablePermission
		fprintf(stderr, "db_tablePermission: eth%d, tableType:", ethNumber);
	#endif

	dbPA.id = 0;

	switch (tableType)
	{
		case E_db_tableTypeAI:
			switch (ethNumber)
			{
				case 0:
					dbPA.id = C_db_paBaseRights0 + E_db_PAoRightsAI;
					break;

				case 1:
					dbPA.id = C_db_paBaseRights1 + E_db_PAoRightsAI;
					break;

				case 2:
					dbPA.id = C_db_paBaseRights2 + E_db_PAoRightsAI;
					break;

				default:
					break;
			} // switch (ethNumber)

			#ifdef __db_showTablePermission
				fprintf(stderr, "AI, PA%d", dbPA.id);
			#endif

			break;

		case E_db_tableTypeAO:
			switch (ethNumber)
			{
				case 0:
					dbPA.id = C_db_paBaseRights0 + E_db_PAoRightsAO;
					break;

				case 1:
					dbPA.id = C_db_paBaseRights1 + E_db_PAoRightsAO;
					break;

				case 2:
					dbPA.id = C_db_paBaseRights2 + E_db_PAoRightsAO;
					break;

				default:
					break;
			} // switch (ethNumber)

			#ifdef __db_showTablePermission
				fprintf(stderr, "AO, PA%d", dbPA.id);
			#endif

			break;

		case E_db_tableTypeDI:
			switch (ethNumber)
			{
				case 0:
					dbPA.id = C_db_paBaseRights0 + E_db_PAoRightsDI;
					break;

				case 1:
					dbPA.id = C_db_paBaseRights1 + E_db_PAoRightsDI;
					break;

				case 2:
					dbPA.id = C_db_paBaseRights2 + E_db_PAoRightsDI;
					break;

				default:
					break;
			} // switch (ethNumber)

			#ifdef __db_showTablePermission
				fprintf(stderr, "DI, PA%d", dbPA.id);
			#endif

			break;

		case E_db_tableTypeDO:
			switch (ethNumber)
			{
				case 0:
					dbPA.id = C_db_paBaseRights0 + E_db_PAoRightsDO;
					break;

				case 1:
					dbPA.id = C_db_paBaseRights1 + E_db_PAoRightsDO;
					break;

				case 2:
					dbPA.id = C_db_paBaseRights2 + E_db_PAoRightsDO;
					break;

				default:
					break;
			} // switch (ethNumber)

			#ifdef __db_showTablePermission
				fprintf(stderr, "DO, PA%d", dbPA.id);
			#endif

			break;

		case E_db_tableTypeDL:
			switch (ethNumber)
			{
				case 0:
					dbPA.id = C_db_paBaseRights0 + E_db_PAoRightsDL;
					break;

				case 1:
					dbPA.id = C_db_paBaseRights1 + E_db_PAoRightsDL;
					break;

				case 2:
					dbPA.id = C_db_paBaseRights2 + E_db_PAoRightsDL;
					break;

				default:
					break;
			} // switch (ethNumber)

			#ifdef __db_showTablePermission
				fprintf(stderr, "DL, PA%d", dbPA.id);
			#endif

			break;

		case E_db_tableTypeEV:
			switch (ethNumber)
			{
				case 0:
					dbPA.id = C_db_paBaseRights0 + E_db_PAoRightsEV;
					break;

				case 1:
					dbPA.id = C_db_paBaseRights1 + E_db_PAoRightsEV;
					break;

				case 2:
					dbPA.id = C_db_paBaseRights2 + E_db_PAoRightsEV;
					break;

				default:
					break;
			} // switch (ethNumber)

			#ifdef __db_showTablePermission
				fprintf(stderr, "EV, PA%d", dbPA.id);
			#endif

			break;

		case E_db_tableTypeFLAG:
			// Tabelle nur für interne Zwecke
			break;

		case E_db_tableTypePA:
			switch (ethNumber)
			{
				case 0:
					dbPA.id = C_db_paBaseRights0 + E_db_PAoRightsPA;
					break;

				case 1:
					dbPA.id = C_db_paBaseRights1 + E_db_PAoRightsPA;
					break;

				case 2:
					dbPA.id = C_db_paBaseRights2 + E_db_PAoRightsPA;
					break;

				default:
					break;
			} // switch (ethNumber)

			#ifdef __db_showTablePermission
				fprintf(stderr, "PA, PA%d", dbPA.id);
			#endif

			break;

		case E_db_tableTypeTP:
			switch (ethNumber)
			{
				case 0:
					dbPA.id = C_db_paBaseRights0 + E_db_PAoRightsTP;
					break;

				case 1:
					dbPA.id = C_db_paBaseRights1 + E_db_PAoRightsTP;
					break;

				case 2:
					dbPA.id = C_db_paBaseRights2 + E_db_PAoRightsTP;
					break;

				default:
					break;
			} // switch (ethNumber)

			#ifdef __db_showTablePermission
				fprintf(stderr, "TP, PA%d", dbPA.id);
			#endif

			break;

		case E_db_tableTypeVA:
			switch (ethNumber)
			{
				case 0:
					dbPA.id = C_db_paBaseRights0 + E_db_PAoRightsVA;
					break;

				case 1:
					dbPA.id = C_db_paBaseRights1 + E_db_PAoRightsVA;
					break;

				case 2:
					dbPA.id = C_db_paBaseRights2 + E_db_PAoRightsVA;
					break;

				default:
					break;
			} // switch (ethNumber)

			#ifdef __db_showTablePermission
				fprintf(stderr, "VA, PA%d", dbPA.id);
			#endif

			break;

		case E_db_tableTypeFC:
			switch (ethNumber)
			{
				case 0:
					dbPA.id = C_db_paBaseRights0 + E_db_PAoRightsFC;
					break;

				case 1:
					dbPA.id = C_db_paBaseRights1 + E_db_PAoRightsFC;
					break;

				case 2:
					dbPA.id = C_db_paBaseRights2 + E_db_PAoRightsFC;
					break;

				default:
					break;
			} // switch (ethNumber)

			#ifdef __db_showTablePermission
				fprintf(stderr, "FC, PA%d", dbPA.id);
			#endif

			break;

		case E_db_tableTypeSC:
			switch (ethNumber)
			{
				case 0:
					dbPA.id = C_db_paBaseRights0 + E_db_PAoRightsSC;
					break;

				case 1:
					dbPA.id = C_db_paBaseRights1 + E_db_PAoRightsSC;
					break;

				case 2:
					dbPA.id = C_db_paBaseRights2 + E_db_PAoRightsSC;
					break;

				default:
					break;
			} // switch (ethNumber)

			#ifdef __db_showTablePermission
				fprintf(stderr, "SC, PA%d", dbPA.id);
			#endif

			break;

		default:
			#ifdef __db_showTablePermission
				fprintf(stderr, " - ");
			#endif

			break;
	} // switch (tableType)

	if (dbPA.id != 0)
	{
		db_rowGet (pConnect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA);
		dbPA.id = atoi (dbPA.strValue);

		#ifdef __db_showTablePermission
			fprintf(stderr, ", value:%d\n", dbPA.id);
		#endif

		switch (dbPA.id)
		{
			case 0:
				retValue = E_db_tablePermissionNone;
				break;

			case 1:
				retValue = E_db_tablePermissionRead;
				break;

			case 2:
				retValue = E_db_tablePermissionReadWrite;
				break;

			default:
				retValue = E_db_tablePermissionNone;
				break;
		} // switch (dbPA.id)
	}
	else
	{
		#ifdef __db_showTablePermission
			fprintf(stderr, ", error\n");
		#endif
	} // if (dbPA.id != 0)

	return (retValue);
} /* db_tablePermission */
 
 
/**************************************************************************//**
 *
 *  \brief		db_tableExport
 *
 *  \details	exportiert eine Tabelle in der mySQL Datenbank gemäss dem Inhalt
 *				der ts_db_connect Struktur und dem te_db_tableType Enum.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		tableType	Tabellentyp gemäss Enum te_db_tableType 
 *	\param		listType	Listentyp gemäss Enum te_db_listType
 *	\param		channel		Kannalnummer des Listentyps [1..4]
 *	\param		*pFilename	Filename für den Datenexport
 *
 *	\return		mysql_errno
 *
 ******************************************************************************/
int db_tableExport (ts_db_connect	*pConnect,
					te_db_tableType	tableType,
					te_db_listType	listType,
					int				channel,
					char			*pFilename)
{
	char		queryString [C_db_queryStringSize];
	MYSQL		*pConnection = mysql_init (NULL);
	int			retValue = 0;


	fprintf(stderr, "db_tableExport\n");

	if (pConnection != NULL)
	{
		if (mysql_real_connect (pConnection,			// connection handler
								pConnect->strHost,		// hostname
								pConnect->strUser,		// user name
								pConnect->strPassword,	// password
								pConnect->strDatabase,	// database name
								0,						// port number
								NULL,					// unix socket
								0) == NULL)				// client flag
		{
			retValue = mysql_errno (pConnection);
		}
		else
		{
			if (mysql_query (pConnection,
							 db_buildQueryStringExport (queryString,
														tableType,
														listType,
														channel,
														pFilename)))
			{
				retValue = mysql_errno (pConnection);
				fprintf (stderr, "db_tableExport: mysql_query(%s) error:%d, state:%s\n", queryString, retValue, mysql_sqlstate (pConnection));
			} // if (mysql_query (pConnection, ...
		} // else if (mysql_real_connect (pConnection, ...

		mysql_close (pConnection);
	} // else if (pConnection != NULL)

	return (retValue);
} /* db_tableExport */


/**************************************************************************//**
 *
 *	\brief		db_tableCreateAllDatalists
 *
 *	\details	erstellt alle Datenlisten Tabelle in der mySQL Datenbank gemäss
 *				dem Inhalt der ts_db_connect Struktur und dem te_db_tableType Enum.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *
 *	\return		mysql_errno
 *
 ******************************************************************************/
int db_tableCreateAllDatalists (ts_db_connect *pConnect)
{
	te_db_listType	listType;
	int				channel;
	char			queryString [C_db_queryStringSize];
	MYSQL			*pConnection = mysql_init (NULL);
	int				retValue = 0;


	if (pConnection == NULL)
	{
		retValue = mysql_errno (pConnection);
	}
	else
	{
		if (mysql_real_connect (pConnection,			// connection handler
								pConnect->strHost,		// hostname
								pConnect->strUser,		// user name
								pConnect->strPassword,	// password
								pConnect->strDatabase,	// database name
								0,						// port number
								NULL,					// unix socket
								0) == NULL)				// client flag
		{
			retValue = mysql_errno (pConnection);
		}
		else
		{
			mysql_set_character_set (pConnection, "utf8");
//			mysql_query (pConnection, "SET NAMES 'utf8'");

			for (listType = E_db_listTypeAI; listType < E_db_listTypeCount; listType++)
			{
				for (channel = 1; channel <= C_db_listTypeChannelCount [listType]; channel++)
				{
					snprintf (queryString, C_db_queryStringSize,
							  "CREATE TABLE DL%s%d (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, value VARCHAR(255), dateTime DATETIME)",
							  C_db_listTypeStr [listType], channel);
					fprintf (stderr, "- create table DL%s%d - ", C_db_listTypeStr [listType], channel);
					mysql_query (pConnection, queryString);

					retValue = mysql_errno (pConnection);

					if (retValue)
					{
						fprintf (stderr, "failed, error %d\n", retValue);
					}
					else
					{
						fprintf (stderr, "ok\n");
					} // else if (retValue)
				} // for (channel = 1; channel <= C_db_channelCount listType]; channel++)
			} // for (listType = E_db_listTypeAI; listType < E_db_listTypeCount; listType++);
		} // else if (mysql_real_connect (pConnection, ...

		mysql_close (pConnection);
	} // if (pConnection == NULL)

	return (retValue);
} /* db_tableCreateAllDatalists */


/**************************************************************************//**
 *
 *  \brief		db_datalistCutToLimitation
 *
 *  \details	Überprüft die Anzahl Einträge einer Datenliste der mySQL 
 * 				Datenbank und löscht falls nötig die Einträge, die das ange-
 * 				gebene Limit übersteigen.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		tableType	Tabellentyp gemäss Enum te_db_tableType 
 *	\param		listType	Listentyp gemäss Enum te_db_listType
 *	\param		channel		Kannalnummer des Listentyps [1..4]
 *	\param		limit		Anzahl Einträge die eine Datenliste haben darf
 *
 *	\return		mysql_errno
 *
 ******************************************************************************/
int db_datalistCutToLimitation (ts_db_connect	*pConnect,
								te_db_tableType	tableType,
								te_db_listType	listType,
								int				channel,
								int				limit)
{
	int			retValue = 0;
	int			rowCount = 0;
	int			delStartId = 0;
	int			delStopId = 0;
	ts_db_dl	dbDL;
	ts_db_ev	dbEV;


	fprintf(stderr, "db_datalistCutToLimitation\n");

	if ((rowCount = db_rowCount(pConnect, tableType, listType, channel)) > limit)
	{
		fprintf(stderr, "\t-Limit erreicht\n");

		if ((tableType < E_db_tableTypeCount) && (listType < E_db_listTypeCount))
		{
			if (listType == E_db_listTypeNone)
			{
				// Stop ID bestimmen
				dbEV.id = limit - 1;
				if ((retValue = db_rowGetSorted (pConnect, tableType, listType, channel, &dbEV)) == 0)
				{
					delStopId = dbEV.id;
					
					// Start ID bestimmen
					dbEV.id = rowCount -1;
					if ((retValue = db_rowGetSorted (pConnect, tableType, listType, channel, &dbEV)) == 0)
					{
						delStartId = dbEV.id;
						if (delStopId < delStartId)
						{
							retValue = db_rowDelete (pConnect, tableType, listType, channel, 0, delStopId);
							if (retValue == 0)
							{
								retValue = db_rowDelete (pConnect, tableType, listType, channel, delStartId, INT_MAX);
							} // if (retValue == 0)
						} // if (delStopId < delStartId)
						else
						{
							retValue = db_rowDelete (pConnect, tableType, listType, channel, delStartId, delStopId);
						} // else if (delStopId < delStartId)
					} // if ((retValue = db_rowGetSorted (pConnect, tableType, ...
				} // if ((retValue = db_rowGetSorted (pConnect, tableType...
			} // if (listType == E_db_listTypeNone)
			else
			{
				// Stop ID bestimmen
				dbDL.id = limit - 1;
				if ((retValue = db_rowGetSorted (pConnect, tableType, listType, channel, &dbDL)) == 0)
				{
					delStopId = dbDL.id;

					fprintf(stderr, "\t-stop id: %d\n", delStopId);

					// Start ID bestimmen
					dbDL.id = rowCount -1;

					if ((retValue = db_rowGetSorted (pConnect, tableType, listType, channel, &dbDL)) == 0)
					{
						delStartId = dbDL.id;

						fprintf(stderr, "\t-start id: %d\n", delStartId);

						if (delStopId < delStartId)
						{
							retValue = db_rowDelete (pConnect, tableType, listType, channel, 0, delStopId);
							if (retValue == 0)
							{
								retValue = db_rowDelete (pConnect, tableType, listType, channel, delStartId, INT_MAX);
							} // if (retValue == 0)
						} // if (delStopId < delStartId)
						else
						{
							retValue = db_rowDelete (pConnect, tableType, listType, channel, delStartId, delStopId);
						} // else if (delStopId < delStartId)
					} // if ((retValue = db_rowGetSorted (pConnect, tableType, ...
				} // if ((retValue = db_rowGetSorted (pConnect, tableType...
				
			} // else if (listType == E_db_listTypeNone)
		} // if ((tableType < E_db_tableTypeCount) && (listType < E_db_listTypeCount))
	} // if (db_rowCount(pConnect, tableType, listType, channel) > limit)
	return (retValue);
} /* db_datalistCutToLimitation */


/**************************************************************************//**
 *
 *  \brief		db_rowCount
 *
 *  \details	gibt die Anzahl Datensätze in die Tabelle in der mySQL Datenbank
 *				gemäss dem Inhalt der ts_db_connect Struktur und dem te_db_tableType
 *				zurück.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		tableType	Tabellentyp gemäss Enum te_db_tableType 
 *	\param		listType	Listentyp gemäss Enum te_db_listType
 *	\param		channel		Kannalnummer des Listentyps [1..4]
 *
 *	\return		Datensatzanzahl
 *
 ******************************************************************************/
int db_rowCount (ts_db_connect		*pConnect,
				 te_db_tableType	tableType,
				 te_db_listType		listType,
				 int				channel)
{
	char		queryString [C_db_queryStringSize];
	MYSQL		*pConnection = mysql_init (NULL);
	MYSQL_RES	*pResult;
	MYSQL_ROW	row;
	int			retValue = 0;


	if (pConnection != NULL)
	{
		if (mysql_real_connect (pConnection,			// connection handler
								pConnect->strHost,		// hostname
								pConnect->strUser,		// user name
								pConnect->strPassword,	// password
								pConnect->strDatabase,	// database name
								0,						// port number
								NULL,					// unix socket
								0))						// client flag
		{
			if (mysql_query (pConnection,
							 db_buildQueryStringCount (queryString,
													   tableType,
													   listType,
													   channel)) == 0)
			{
				pResult = mysql_store_result (pConnection);

				if (pResult)
				{
					row = mysql_fetch_row (pResult);

					if (row)
					{
						retValue = atoi (row [0]);
					} // if (row)
					
					mysql_free_result (pResult);
				} // if (pResult)
			} // if (mysql_query (pConnection, queryString))
		} // if (mysql_real_connect (pConnection, ...

		mysql_close (pConnection);
	} // else if (pConnection != NULL)

	return (retValue);
} /* db_rowCount */


/**************************************************************************//**
 *
 *  \brief		db_rowDelete
 *
 *  \details	Löscht einen Bereich einer Datenliste der mySQL Datenbank.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		tableType	Tabellentyp gemäss Enum te_db_tableType 
 *	\param		listType	Listentyp gemäss Enum te_db_listType
 *	\param		channel		Kannalnummer des Listentyps [1..4]
 *	\param		startID		Start ID des zu löschenden Bereichs
 *	\param		endID		End ID des zu löschenden Bereichs
 *
 *	\return		mysql_errno
 *
 ******************************************************************************/
int db_rowDelete (ts_db_connect		*pConnect,
				  te_db_tableType	tableType,
				  te_db_listType	listType,
				  int				channel,
				  int				startID,
				  int				endID)
{
	char	queryString [C_db_queryStringSize];
	MYSQL	*pConnection = mysql_init (NULL);
	int		retValue = 0;


	if (pConnection == NULL)
	{
		retValue = mysql_errno (pConnection);
	}
	else
	{
		if (mysql_real_connect (pConnection,			// connection handler
								pConnect->strHost,		// hostname
								pConnect->strUser,		// user name
								pConnect->strPassword,	// password
								pConnect->strDatabase,	// database name
								0,						// port number
								NULL,					// unix socket
								0) == NULL)				// client flag
		{
			retValue = mysql_errno (pConnection);
		}
		else
		{
			if (mysql_query (pConnection,
							 db_buildQueryStringDelete (queryString,
														tableType,
														listType,
														channel,
														startID,
														endID)))
			{
				retValue = mysql_errno (pConnection);
			}
		} // else if (mysql_real_connect (pConnection, ...

		mysql_close (pConnection);
	} // else if (pConnection != NULL)

	return (retValue);
} /* db_rowDelete */


/**************************************************************************//**
 *
 *  \brief		db_rowAdd
 *
 *  \details	fügt einen Datensatz in die Tabelle in der mySQL Datenbank gemäss
 *				dem Inhalt der ts_db_connect Struktur und dem te_db_tableType
 *				Enum ein.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		tableType	Tabellentyp gemäss Enum te_db_tableType 
 *	\param		listType	Listentyp gemäss Enum te_db_listType
 *	\param		channel		Kannalnummer des Listentyps [1..4]
 *	\param		*pTable		Pointer auf Datensatz Struktur
 *
 *	\return		mysql_errno
 *
 ******************************************************************************/
int db_rowAdd (ts_db_connect	*pConnect,
			  te_db_tableType	tableType,
			  te_db_listType	listType,
			  int				channel,
			  void				*pTable)
{
	char	queryString [C_db_queryStringSize];
	MYSQL	*pConnection = mysql_init (NULL);
	int		retValue = 0;


	if (pConnection == NULL)
	{
		retValue = mysql_errno (pConnection);
	}
	else
	{
		if (mysql_real_connect (pConnection,			// connection handler
								pConnect->strHost,		// hostname
								pConnect->strUser,		// user name
								pConnect->strPassword,	// password
								pConnect->strDatabase,	// database name
								0,						// port number
								NULL,					// unix socket
								0) == NULL)				// client flag
		{
			retValue = mysql_errno (pConnection);
		}
		else
		{
			mysql_set_character_set (pConnection, "utf8");
//			mysql_query (pConnection, "SET NAMES 'utf8'");

			if (mysql_query (pConnection,
							 db_buildQueryStringInsert (queryString,
														tableType,
														listType,
														channel,
														pTable)))
			{
				retValue = mysql_errno (pConnection);
			}
		} // else if (mysql_real_connect (pConnection, ...

		mysql_close (pConnection);
	} // else if (pConnection != NULL)

	return (retValue);
} /* db_rowAdd */


/**************************************************************************//**
 *
 *  \brief		db_rowUpdate
 *
 *  \details	ändert den Datensatz in der ts_db_connect Struktur aufgeführten
 *				mySQL Datenbank.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		tableType	Tabellentyp gemäss Enum te_db_tableType 
 *	\param		*pTable		Pointer auf Datensatz Struktur
 *
 *	\return		mysql_errno
 *
 ******************************************************************************/
int db_rowUpdate (ts_db_connect		*pConnect,
                  te_db_tableType	tableType,
                  void				*pTable)
{
	char		queryString [C_db_queryStringSize] = "";
	MYSQL		*pConnection = mysql_init (NULL);
	int			retValue = 0;
	u_int64_t	affectedRows = 0;


	if (pConnection == NULL)
	{
		retValue = mysql_errno (pConnection);
	}
	else
	{
		if (mysql_real_connect (pConnection,				// connection handler
								pConnect->strHost,			// hostname
								pConnect->strUser,			// user name
								pConnect->strPassword,		// password
								pConnect->strDatabase,		// database name
								0,							// port number
								NULL,						// unix socket
								CLIENT_FOUND_ROWS) == NULL) // client flag
		{
			retValue = mysql_errno (pConnection);
		}
		else
		{
			mysql_set_character_set (pConnection, "utf8");
//			mysql_query (pConnection, "SET NAMES 'utf8'");

			if (mysql_query (pConnection,
			                 db_buildQueryStringUpdate (queryString,
			                                            tableType,
			                                            pTable)))
			{
				retValue = mysql_errno (pConnection);
			}
			else
			{
				affectedRows = mysql_affected_rows(pConnection);
				if (affectedRows == 0)
				{
					retValue = 2000; //  Unknown MySQL error
				} // if (affectedRows == 0)
			} // else if (mysql_query (pConnection, ...
		} // else if (mysql_real_connect (pConnection, ...

		mysql_close (pConnection);
	} // else if (pConnection != NULL)

	return (retValue);
} /* db_rowUpdate */


/**************************************************************************//**
 *
 *  \brief		db_rowGet
 *
 *  \details	gibt den Datensatz in die Tabelle in der mySQL Datenbank gemäss
 *				dem Inhalt der ts_db_connect Struktur und dem te_db_tableType
 *				Enum zurück.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		tableType	Tabellentyp gemäss Enum te_db_tableType 
 *	\param		listType	Listentyp gemäss Enum te_db_listType
 *	\param		channel		Kannalnummer des Listentyps [1..4]
 *	\param		*pTable		Pointer auf Datensatz Struktur
 *
 *	\return		mysql_errno
 *
 ******************************************************************************/
int db_rowGet (ts_db_connect	*pConnect,
               te_db_tableType	tableType,
			   te_db_listType	listType,
			   int				channel,
               void				*pTable)
{
	char		queryString [C_db_queryStringSize] = "";
	MYSQL		*pConnection = mysql_init (NULL);
	MYSQL_RES	*pResult;
	MYSQL_ROW	row;
	int			retValue = 0;


	if (pConnection == NULL)
	{
		retValue = mysql_errno (pConnection);
	}
	else
	{
		if (mysql_real_connect (pConnection,			// connection handler
								pConnect->strHost,		// hostname
								pConnect->strUser,		// user name
								pConnect->strPassword,	// password
								pConnect->strDatabase,	// database name
								0,						// port number
								NULL,					// unix socket
								0) == NULL)				// client flag
		{
			retValue = mysql_errno (pConnection);
		}
		else
		{
			if (mysql_query (pConnection,
			                 db_buildQueryStringSelectAll (queryString,
			                                               tableType,
			                                               listType,
			                                               channel,
			                                               pTable)))
			{
				retValue = mysql_errno (pConnection);
			}
			else
			{
				pResult = mysql_store_result (pConnection);

				if (pResult == NULL)
				{
					retValue = mysql_errno (pConnection);
				}
				else
				{
					row = mysql_fetch_row (pResult);

					if (row == NULL)
					{
						retValue = -1;
					}
					else
					{
						db_copyRowResultToTableStruct (tableType, pTable, row);
					} // else if (row == NULL)
					
					mysql_free_result (pResult);
				} // else if (pResult == NULL)
			} // else if (mysql_query (pConnection, queryString))
		} // else if (mysql_real_connect (pConnection, ...

		mysql_close (pConnection);
	} // else if (pConnection != NULL)

	return (retValue);
} /* db_rowGet */


/**************************************************************************//**
 *
 *  \brief		db_rowGetSorted
 *
 *  \details	gibt den Datensatz an der Position .id in der sortierten Tabelle
 *				in der mySQL Datenbank gemäss dem te_db_tableType Enum zurück.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		tableType	Tabellentyp gemäss Enum te_db_tableType 
 *	\param		listType	Listentyp gemäss Enum te_db_listType
 *	\param		channel		Kannalnummer des Listentyps [1..4]
 *	\param		*pTable		Pointer auf Datensatz Struktur
 *
 *	\return		mysql_errno
 *
 ******************************************************************************/
int db_rowGetSorted (ts_db_connect	 *pConnect,
					 te_db_tableType tableType,
					 te_db_listType	 listType,
					 int			 channel,
					 void			 *pTable)
{
	char		queryString [C_db_queryStringSize] = "";
	MYSQL		*pConnection = mysql_init (NULL);
	MYSQL_RES	*pResult;
	MYSQL_ROW	row;
	int			retValue = 0;


	if (pConnection == NULL)
	{
		retValue = mysql_errno (pConnection);
	}
	else
	{
		if (mysql_real_connect (pConnection,			// connection handler
								pConnect->strHost,		// hostname
								pConnect->strUser,		// user name
								pConnect->strPassword,	// password
								pConnect->strDatabase,	// database name
								0,						// port number
								NULL,					// unix socket
								0) == NULL)				// client flag
		{
			retValue = mysql_errno (pConnection);
		}
		else
		{
			if (mysql_query (pConnection,
							 db_buildQueryStringSelectSorted (queryString,
															  tableType,
															  listType,
															  channel,
															  pTable)))
			{
				retValue = mysql_errno (pConnection);
				fprintf (stderr, "db_rowGetSorted: mysql_query error: %d\n", retValue); 
			}
			else
			{
				pResult = mysql_store_result (pConnection);

				if (pResult == NULL)
				{
					retValue = mysql_errno (pConnection);
					fprintf (stderr, "db_rowGetSorted: mysql_store_result error: %d\n", retValue); 
				}
				else
				{
					row = mysql_fetch_row (pResult);

					if (row == NULL)
					{
						retValue = -1;
						fprintf (stderr, "db_rowGetSorted: mysql_fetch_row error: %d\n", retValue); 
					}
					else
					{
						db_copyRowResultToTableStruct (tableType, pTable, row);
					} // else if (row == NULL)
					
					mysql_free_result (pResult);
				} // else if (pResult == NULL)
			} // else if (mysql_query (pConnection, queryString))
		} // else if (mysql_real_connect (pConnection, ...

		mysql_close (pConnection);
	} // else if (pConnection != NULL)

	return (retValue);
} /* db_rowGetSorted */


/**************************************************************************//**
 *
 *	\brief		db_rowDebug
 *
 *	\details	schreibt die Werte der übergebenen ts_dbXX Struktur gemäss dem
 *				te_db_tableType auf stderr.
 *
 *	\param		tableType	Tabellentyp gemäss Enum te_db_tableType 
 *	\param		*pTable		Pointer auf Datensatz Struktur
 *
 *	\return		-
 *
 ******************************************************************************/
void db_rowDebug (te_db_tableType	tableType,
				  void				*pTable)
{
	fprintf (stderr, "\033[1mdb_rowDebug\033[0m\n"); 

	switch (tableType)
	{
		case E_db_tableTypePA:
		{
			ts_db_pa *pDbPA = pTable;
			fprintf (stderr, "dbPA.id        : %d\n", pDbPA->id); 
			fprintf (stderr, "dbPA.min       : %s\n", pDbPA->strMin);
			fprintf (stderr, "dbPA.value     : %s\n", pDbPA->strValue);
			fprintf (stderr, "dbPA.max       : %s\n", pDbPA->strMax);
			fprintf (stderr, "dbPA.scale     : %s\n", pDbPA->strScale);
			fprintf (stderr, "dbPA.checkRange: %d\n", pDbPA->checkRange);
		}
		break;

		case E_db_tableTypeTP:
		{
			ts_db_tp *pDbTP = pTable;
			fprintf (stderr, "dbTP.id  : %d\n", pDbTP->id); 
			fprintf (stderr, "dbPA.text: %s\n", pDbTP->strText);
		}
		break;

		case E_db_tableTypeVA:
		{
			ts_db_va *pDbVA = pTable;
			fprintf (stderr, "dbVA.id         : %d\n", pDbVA->id); 
			fprintf (stderr, "dbVA.description: %s\n", pDbVA->strDescription); 
		}
		break;

		case E_db_tableTypeDI:
		{
			ts_db_di *pDbDI = pTable;
			fprintf (stderr, "dbDI.id   : %d\n", pDbDI->id); 
			fprintf (stderr, "dbDI.value: %s\n", pDbDI->strValue); 
		}
		break;

		case E_db_tableTypeDO:
		{
			ts_db_do *pDbDO = pTable;
			fprintf (stderr, "dbDO.id   : %d\n", pDbDO->id); 
			fprintf (stderr, "dbDO.value: %s\n", pDbDO->strValue); 
		}
		break;

		case E_db_tableTypeAI:
		{
			ts_db_ai *pDbAI = pTable;
			fprintf (stderr, "dbAI.id   : %d\n", pDbAI->id); 
			fprintf (stderr, "dbAI.value: %s\n", pDbAI->strValue); 
		}
		break;

		case E_db_tableTypeAO:
		{
			ts_db_ao *pDbAO = pTable;
			fprintf (stderr, "dbAO.id   : %d\n", pDbAO->id); 
			fprintf (stderr, "dbAO.value: %s\n", pDbAO->strValue); 
		}
		break;

		case E_db_tableTypeDL:
		{
		}
		break;

		case E_db_tableTypeEV:
		{
			ts_db_ev *pDbEV = pTable;
			fprintf (stderr, "dbEV.id      : %d\n", pDbEV->id); 
			fprintf (stderr, "dbEV.value   : %s\n", pDbEV->strValue);
			fprintf (stderr, "dbEV.dateTime: %s\n", pDbEV->strDateTime);
		}
		break;

		case E_db_tableTypeFLAG:
		{
			ts_db_flag *pDbFLAG = pTable;
			fprintf (stderr, "dbFLAG.id         : %d\n", pDbFLAG->id); 
			fprintf (stderr, "dbFLAG.value      : %d\n", pDbFLAG->value);
			fprintf (stderr, "dbFLAG.description: %s\n", pDbFLAG->strDescription);
		}
		break;

		default:
			break;

	} // switch (tableType)
} /* db_rowDebug */


/**************************************************************************//**
 *
 *  \brief		db_datalistsCut
 *
 *  \details	Überprüft die Länge aller Datenlisten und der Eventliste der 
 * 				mySQL Datenbank und kürzt diese auf das Limit falls nötig.
 *
 *	\param		*pConnect		Pointer auf Struktur ts_db_connect
 *
 *	\return		mysql_errno
 *
 ******************************************************************************/
int db_datalistsCut (ts_db_connect	*pConnect)
{
	te_db_listType	listType;
	int				channel;
	int		 		retValue = 0;


	for (listType = E_db_listTypeAI; listType < E_db_listTypeCount; listType++)
	{
		for (channel = 1; channel <= C_db_listTypeChannelCount [listType]; channel++)
		{
			retValue =  db_datalistCutToLimitation (pConnect, E_db_tableTypeDL, 
													listType, channel, C_db_maxDlSize);
			if (retValue != 0)
			{
				break;
			} // if (retValue != 0)
		} // for (channel = 1; channel <= C_db_listTypeChannelCount [listType]; channel++)
		if (retValue != 0)
		{
			break;
		} // if (retValue != 0)
	} // for (listType = E_db_listTypeAI; listType < E_db_listTypeCount; listType++)
	
	if (retValue == 0)
	{
		retValue =  db_datalistCutToLimitation (pConnect, E_db_tableTypeEV, E_db_listTypeNone, 
												0, C_db_maxEvSize);
	} // if (retValue == 0)
	
	return (retValue);
} /* db_datalistsCut */


/**************************************************************************//**
 *
 *  \brief		db_csvReadFile
 *
 *  \details	liest die Tabelle gemäss tableType aus dem File csvFile und
 *				schreibt die Daten in die aus der ts_db_connect Struktur
 *				aufgeführte mySQL Datenbank.
 *
 *	\param		*pConnect		Pointer auf Struktur ts_db_connect
 *	\param		tableType		Tabellentyp gemäss Enum te_db_tableType 
 *	\param		*pCsvFileName	Pointer auf den csv Filenamen
 *
 *	\return		0: ok, alle anderen Werte Fehler
 *
 ******************************************************************************/
int db_csvReadFile (ts_db_connect	*pConnect,
					te_db_tableType	tableType,
					char			*pCsvFileName)
{
	FILE	 *pFile;
	char	 csvString [C_db_csvStringSize];

	// notwendig für Funktion getline
	char	 *pCsvString = csvString;
	size_t	 csvStringSize = C_db_csvStringSize;
	int		 csvStringLen;

	ts_db_pa dbPA;
	ts_db_tp dbTP;
	void	 *pTable;
	int		 retValue = 0;


	pFile = fopen (pCsvFileName, "r");

	if (pFile == NULL)
	{
		retValue = errno;
	}
	else
	{
		// Tabellenheader lesen ohne in die Datenbanktabelle zu schreiben
		csvStringLen = getline (&pCsvString, &csvStringSize, pFile);	//!< ist aber in stdio.h vorhanden!

		switch (tableType)
		{
			case E_db_tableTypePA:
			{
				db_tableDelete (pConnect, tableType);
				db_tableCreate (pConnect, tableType);
				pTable = &dbPA;
			}
			break;

			case E_db_tableTypeTP:
			{
				db_tableDelete (pConnect, tableType);
				db_tableCreate (pConnect, tableType);
				pTable = &dbTP;
			}
			break;

			default:
				break;

		} // switch (tableType)

		do
		{
			csvStringLen = getline (&pCsvString, &csvStringSize, pFile);

			if (csvStringLen > 0)
			{
				csv_fillTable (csvString, csvStringLen, tableType, pTable);

				retValue = db_rowAdd (pConnect, tableType, E_db_listTypeNone, 0, pTable);
			} // if (cvsStringLen > 0)
		} while (csvStringLen > 0);

		fclose (pFile);
		retValue = 0;
	} // else if (pFile == NULL)

	return (retValue);
} /* db_csvReadFile */


/**************************************************************************//**
 *
 *  \brief		db_csvWriteFile
 *
 *  \details	schreibt die Tabelle gemäss tableType in der ts_db_connect
 *				Struktur aufgeführten mySQL Datenbank in das File csvFile.
 *
 *	\param		*pConnect		Pointer auf Struktur ts_db_connect
 *	\param		tableType		Tabellentyp gemäss Enum te_db_tableType 
 *	\param		*pCsvFileName	Pointer auf den csv Filenamen
 *
 *	\return		0: ok, alle anderen Werte Fehler
 *
 ******************************************************************************/
int db_csvWriteFile (ts_db_connect	 *pConnect,
					 te_db_tableType tableType,
					 char			 *pCsvFileName)
{
	FILE	*pFile;
	void	*pTable;
	int		retValue = 0;


	pFile = fopen (pCsvFileName, "w");

	if (pFile == NULL)
	{
		retValue = errno;
	}
	else
	{
		switch (tableType)
		{
			case E_db_tableTypePA:
			{
				ts_db_pa dbPA;
				pTable = &dbPA;

				// Tabellenheader schreiben
				fprintf (pFile, "%s\n", C_db_csvHeaderStringPa);

				for (dbPA.id = 0; dbPA.id < C_db_maxRowPa; dbPA.id++) 
				{
					if (db_rowGet (pConnect, tableType, E_db_listTypeNone, 0, &dbPA) == 0)
					{
						// gültige Daten ins File schreiben
						fprintf (pFile, "%d;%s;%s;%s;%s;%d\n",
								 dbPA.id, dbPA.strMin, dbPA.strValue, dbPA.strMax, dbPA.strScale, dbPA.checkRange);
					} // if (cvsBufferLen > 0)
				} // for (dbPA.id = 0; dbPA.id < C_dbPA_maxRow; dbPA.id++)
			}
			break;

			case E_db_tableTypeTP:
			{
				ts_db_tp dbTP;
				pTable = &dbTP;

				// Tabellenheader schreiben
				fprintf (pFile, "%s\n", C_db_csvHeaderStringTp);

				for (dbTP.id = 0; dbTP.id < C_db_maxRowTp; dbTP.id++) 
				{
					if (db_rowGet (pConnect, tableType, E_db_listTypeNone, 0, &dbTP) == 0)
					{
						// gültige Daten ins File schreiben
						fprintf (pFile, "%d;%s\n",
								 dbTP.id, dbTP.strText);
					} // if (cvsBufferLen > 0)
				} // for (dbTP.id = 0; dbTP.id < C_dbTP_maxRow; dbTP.id++)
			}
			break;

		} // switch (tableType)

		fclose (pFile);
		retValue = 0;
	} // else if (pFile == NULL)
		
	return (retValue);
} /* db_csvWriteFile */


/**************************************************************************//**
 *
 *  \brief		db_csvWriteDataLists
 *
 *  \details	schreibt alle Datenlisten in der ts_db_connect Struktur
 *				aufgeführten mySQL Datenbank nach Datum sortiert (neustes zuerst)
 *				in die Files pCsvFileBody + Listentyp + Kanalnummer.
 *
 *	\param		*pConnect		Pointer auf Struktur ts_db_connect
 *	\param		*pCsvFileBody	Pointer auf den csv Filenamenrumpf. Der Rumpf
 *								wird mit der Datenlistenbezeichnung und der
 *								Kanalnummer ergänzt. 
 *
 *	\return		0: ok, alle anderen Werte Fehler
 *
 ******************************************************************************/
int db_csvWriteDataLists (ts_db_connect	*pConnect,
						  char			*pCsvFileBody)
{
	char			filenameTmp [C_db_dataStringSize];
	char			filenameStick [C_db_dataStringSize];
	char			command [C_db_dataStringSize];
	te_db_listType	listType;
	int				channel;
	int		 		retValue = 0;


	for (listType = E_db_listTypeAI; listType < E_db_listTypeCount; listType++)
	{
		for (channel = 1; channel <= C_db_listTypeChannelCount [listType]; channel++)
		{
			snprintf (filenameTmp, C_db_dataStringSize, "/tmp/%s%d.csv",
					  C_db_listTypeStr [listType], channel);
			snprintf (filenameStick, C_db_dataStringSize, "%s%s%d.csv", pCsvFileBody,
					  C_db_listTypeStr [listType], channel);
			snprintf (command, C_db_dataStringSize, "sudo mv %s %s", filenameTmp, filenameStick);
			fprintf (stderr, "db_csvWriteDataLists: filename: %s\n", filenameStick);

			// db_tableExport kann bestehende Files nicht löschen
			remove (filenameTmp);
			db_tableExport (pConnect, E_db_tableTypeDL, listType, channel, filenameTmp);
			system (command);
		} // for (channel = 1; channel <= C_db_listTypeChannelCount [listType]; channel++)
	} // for (listType = E_db_listTypeAI; listType < E_db_listTypeCount; listType++)
		
	return (retValue);
} /* db_csvWriteDataLists */


/**************************************************************************//**
 *
 *	\brief		db_flagClear
 *
 *	\details	löscht das flag in der Datenbanktabelle FLAG.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		flag		gewünschtes Flag
 *
 ******************************************************************************/
void db_flagClear (ts_db_connect *pConnect,
				   te_db_flag	 flag)
{
	ts_db_flag	dbFlag;


	dbFlag.id	 = flag;
	dbFlag.value = 0;
	
	db_rowUpdate (pConnect, E_db_tableTypeFLAG, &dbFlag);
} /* db_flagClear */


/**************************************************************************//**
 *
 *	\brief		db_flagSet
 *
 *	\details	setzt das flag in der Datenbanktabelle FLAG.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		flag		gewünschtes Flag
 *
 ******************************************************************************/
void db_flagSet (ts_db_connect	*pConnect,
				te_db_flag		flag)
{
	ts_db_flag	dbFlag;


	dbFlag.id	 = flag;
	dbFlag.value = 1;
	
	db_rowUpdate (pConnect, E_db_tableTypeFLAG, &dbFlag);
} /* db_flagSet */


/**************************************************************************//**
 *
 *	\brief		db_flagGet
 *
 *	\details	liest den Status des flag in der Datenbanktabelle FLAG.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		flag		gewünschtes Flag
 *
 *	\return		flag, 0:off, 1:on
 *
 ******************************************************************************/
int db_flagGet (ts_db_connect	*pConnect,
				te_db_flag		flag)
{
	ts_db_flag	dbFlag;


	dbFlag.id = flag;
	db_rowGet (pConnect, E_db_tableTypeFLAG, E_db_listTypeNone, 0, &dbFlag);

	return (dbFlag.value);
} /* db_flagGet */


/**************************************************************************//**
 *
 *  \brief		db_vaNumberToFlagIndex
 *
 *  \details	schreibt die momentane Systemzeit in die entsprechenden VA
 *				Datensätze in der ts_db_connect Struktur aufgeführten mySQL
 *				Datenbank.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *
 *	\return		mysql_errno
 *
 ******************************************************************************
te_db_flag db_vaNumberToFlagIndex (ts_db_connect *pConnect,
								   int			 vaNumber)
{
	if ((vaNumber >= C_va_counterMinNumber) &&
		(vaNumber <= C_va_counterMaxNumber))
	{
		switch (vaNumber)
		{
			case C_va_counterMinNumber	  : return E_db_flagVa9writePermission;  break;
			case C_va_counterMinNumber + 1: return E_db_flagVa10writePermission; break;
			case C_va_counterMinNumber + 2: return E_db_flagVa11writePermission; break;
			case C_va_counterMinNumber + 3: return E_db_flagVa12writePermission; break;
			case C_va_counterMinNumber + 4: return E_db_flagVa13writePermission; break;
			case C_va_counterMinNumber + 5: return E_db_flagVa14writePermission; break;
			case C_va_counterMinNumber + 6: return E_db_flagVa15writePermission; break;
			case C_va_counterMinNumber + 7: return E_db_flagVa16writePermission; break;
		} // switch (vaNumber)
	} // if ((vaNumber >= C_va_counterMinNumber) && ...
} /* db_vaNumberToFlagIndex */


/**************************************************************************//**
 *
 *  \brief		db_vaUpdateTime
 *
 *  \details	schreibt die momentane Systemzeit in die entsprechenden VA
 *				Datensätze in der ts_db_connect Struktur aufgeführten mySQL
 *				Datenbank.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *
 *	\return		mysql_errno
 *
 ******************************************************************************/
int db_vaUpdateTime (ts_db_connect	*pConnect)
{
	ts_db_pa	dbPA;
	ts_db_va	dbVA;
	time_t		timer = time (NULL);
	struct tm	*dateTime; //  = localtime (&timer);
	char		timeStr [C_dbVA_timeStringLen3];
	int			retValue = 0;

	// Sommerzeitkorrektur aktiviert?
	dbPA.id = C_db_paBaseSystem + E_db_PAoAlgWinterSummerTime;
	db_rowGet (pConnect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA);
	
	if (atoi (dbPA.strValue) == 1)
	{
		timer = timer + 3600;	// 1h addieren
	} // if (atoi (&dbPA.strValue) == 1)

	dateTime = localtime (&timer);

	strftime (timeStr, C_dbVA_timeStringLen3, C_dbVA_timeFormatString1, dateTime);
	dbVA.id = E_db_VAidSystemTimeFormat1;
	snprintf (dbVA.strDescription, C_db_dataStringSize, "%s", timeStr);

	if (retValue = db_rowUpdate (pConnect, E_db_tableTypeVA, &dbVA))
	{
		// Eintrag nicht vorhanden -> add
		retValue = db_rowAdd (pConnect, E_db_tableTypeVA, E_db_listTypeNone, dbVA.id, &dbVA);
	} // if (retValue = db_rowUpdate (pConnect, E_db_tableTypeVA, &dbVA))

	if (retValue == 0)
	{
		strftime (timeStr, C_dbVA_timeStringLen3, C_dbVA_timeFormatString2, dateTime);
		dbVA.id = E_db_VAidSystemTimeFormat2;
		snprintf (dbVA.strDescription, C_db_dataStringSize, "%s", timeStr);

		if (retValue = db_rowUpdate (pConnect, E_db_tableTypeVA, &dbVA))
		{
			// Eintrag nicht vorhanden -> add
			retValue = db_rowAdd (pConnect, E_db_tableTypeVA, E_db_listTypeNone, dbVA.id, &dbVA);
		} // if (retValue = db_rowUpdate (pConnect, E_db_tableTypeVA, &dbVA))

		if (retValue == 0)
		{
			strftime (timeStr, C_dbVA_timeStringLen3, C_dbVA_timeFormatString3, dateTime);
			dbVA.id = E_db_VAidSystemTimeFormat3;
			snprintf (dbVA.strDescription, C_db_dataStringSize, "%s", timeStr);

			if (retValue = db_rowUpdate (pConnect, E_db_tableTypeVA, &dbVA))
			{
				// Eintrag nicht vorhanden -> add
				retValue = db_rowAdd (pConnect, E_db_tableTypeVA, E_db_listTypeNone, dbVA.id, &dbVA);
			} // if (retValue = db_rowUpdate (pConnect, E_db_tableTypeVA, &dbVA))
		} // if (retValue == 0)
	} // if (retValue == 0)
	
	return (retValue);
} /* db_vaUpdateTime */


/**************************************************************************//**
 *
 *  \brief		db_vaUpdateHiResTime
 *
 *  \details	schreibt die momentane Systemzeit in Nanosekundenauflösung in
 *				den entsprechenden VA Datensatz in der ts_db_connect Struktur
 *				aufgeführten mySQL Datenbank.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *
 *	\return		mysql_errno
 *
 ******************************************************************************/
int db_vaUpdateHiResTime (ts_db_connect	*pConnect)
{
	ts_db_va		dbVA;
	struct timespec	highResTimer;
	char			timeStr [C_dbVA_timeStringLen3];

 
	// funktioniert nur mit TIME_UTC
	timespec_get (&highResTimer, TIME_UTC); 	 
	snprintf (timeStr, C_dbVA_timeStringLen3, "%ld.%ld", highResTimer.tv_sec, highResTimer.tv_nsec); 

	dbVA.id = E_db_VAidSystemTimeNanoSec;
	snprintf (dbVA.strDescription, C_db_dataStringSize, "%s", timeStr);

	return (db_rowUpdate (pConnect, E_db_tableTypeVA, &dbVA));
} /* db_vaUpdateHiResTime */


/* db.c */

