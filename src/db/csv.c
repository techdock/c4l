/**************************************************************************//**
 *
 *	\file		csv.c
 *
 *	\brief		Stellt allgemeine csv Funktionen zur Verfügung.
 *
 *	\details	-
 *
 *	\date		01.03.2021
 *	\author		Marcel Ming
 *	\copyright	TRiMADA AG   CH-5610 Wohlen
 *	\remark		Compiler- / IDE-Version
 *
 *	\version	01.03.21 mm
 *					- Erstellt.
 *	\version	20.05.21 mm
 *					- Anpassungen an die Änderungen der Tabellenstrukturelemente
 *					  in ts_db_xy.
 *	\version	18.02.22 mm
 *					- Kommentar ergänzt / korrigiert.
 *
 *
 ******************************************************************************/


/******************************************************************************
 * Compilerswitches                                                           *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Modules                                                                    *
 *                                                                            *
 ******************************************************************************/
#include <stdio.h>
#include <stdlib.h>


#include "db.h"
#include "csv.h"


/******************************************************************************
 * Definitions and Macros                                                     *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Typedefs and Structures                                                    *
 *                                                                            *
 ******************************************************************************/
 
/******************************************************************************
 * Exi2cAddressed Variables                                                         *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Global Variables                                                           *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Local Functions                                                            *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Exported Functions                                                         *
 *                                                                            *
 ******************************************************************************/

/**************************************************************************//**
 *
 *  \brief		csv_fillTable
 *
 *  \details	sucht im String pSource ab startPos nach dem nächsten ';'. Die
 *				Zeichen zwischen startPos und ';' werden im String pDest zurück-
 *				gegeben.
 * 				Weiter wird ein eventuell vorhandenes \n durch \0 ersetzt.
 *
 *	\param		*string		Pointer auf den RückgabeString.
 *	\param		strLen		Länge des Strings.
 *	\param		tableType	Tabellentyp gemäss Enum te_db_tableType 
 *	\param		*pTable		Pointer auf Datensatz Struktur
 *
 *	\return		-
 *
 ******************************************************************************/
void csv_fillTable (char			*string,
					int				strLen,
					te_db_tableType	tableType,
					void			*pTable)
{
	int		 pos							= 0;
	int		 tableItem						= 0;
	int		 posTemp						= 0;
	char	 strTemp [C_db_dataStringSize]	= "\0";
	ts_db_pa *pDbPA 						= pTable;
	ts_db_tp *pDbTP 						= pTable;


	while (pos < strLen)
	{
		strTemp [posTemp] = string [pos];

		if ((string [pos] == ';') || (string [pos] == '\n') || (string [pos] == '\r'))
		{
			strTemp [posTemp] = '\0';

			switch (tableType)
			{
				case E_db_tableTypePA:
					switch (tableItem)
					{
						case 0:
							// beim 1. Element Struktur initialisieren
							pDbPA->id			= 0;
							pDbPA->strMin [0]	= 0;
							pDbPA->strValue [0]	= 0;
							pDbPA->strMax [0]	= 0;
							pDbPA->strScale [0]	= 0;
							pDbPA->checkRange	= 0;
							
							pDbPA->id = atoi (strTemp);
							break;

						case 1:
							snprintf (pDbPA->strMin, C_db_dataStringSize, "%s", strTemp);
							break;

						case 2:
							snprintf (pDbPA->strValue, C_db_dataStringSize, "%s", strTemp);
							break;

						case 3:
							snprintf (pDbPA->strMax, C_db_dataStringSize, "%s", strTemp);
							break;

						case 4:
							snprintf (pDbPA->strScale, C_db_dataStringSize, "%s", strTemp);
							break;

						case 5:
							pDbPA->checkRange = atoi (strTemp);
							pos = strLen;
							break;

						default:
							pos = strLen;
							break;

					} // switch (tableItem)
					break;

				case E_db_tableTypeTP:
					switch (tableItem)
					{
						case 0:
							// beim 1. Element Struktur initialisieren
							pDbTP->id			= 0;
							pDbTP->strText [0]	= 0;

							pDbTP->id = atoi (strTemp);
							break;

						case 1:
							snprintf (pDbTP->strText, C_db_dataStringSize, "%s", strTemp);
							pos = strLen;
							break;


						default:
							pos = strLen;
							break;

					} // switch (tableItem)
					break;
			} // switch (tableType)

			posTemp = -1;
			tableItem++;
		} // if ((string [pos] == '\n') || ...

		posTemp++;
		pos++;
	} // while (pos < strLen)

	// db_rowDebug (tableType, pTable);
} /* csv_fillTable */


/* csv.c */

