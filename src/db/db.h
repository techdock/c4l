/**************************************************************************//**
 *
 *	\file		db.h
 *
 *	\brief		Headerfile der Funktionen für den allgemeine Datenbank Zugriff.
 *
 *	\details	-
 *
 *	\date		23.02.2021
 *	\author		Marcel Ming
 *	\copyright	TRiMADA AG   CH-5610 Wohlen
 *	\remark		Compiler- / IDE-Version
 *
 *	\version	23.02.21 mm
 *					- Erstellt.
 *	\version	30.04.21 mm
 *					- dbAI, dbAO, dbDI und dbDO integriert, da dessen Funktionen
 *					  lediglich in db gemappt werden. (Reduziert die Anzahl der
 *					  Projektfiles und erhöht die Übersichtlichkeit)
 *	\version	06.05.21 mm
 *					- dbEV, dbPA, dbTP und dbVA integriert, da dessen Funktionen
 *					  lediglich in db gemappt werden. (Reduziert die Anzahl der
 *					  Projektfiles und erhöht die Übersichtlichkeit)
 *	\version	07.05.21 mm
 *					- Anpassungen an Pflichtenheft Revision 5.
 *	\version	20.05.21 mm
 *					- Tabellenstrukturelemente in ts_db_xy von * auf char []
 *					  geändert, damit jeweils Speicher nicht seperat angefordert
 *					  werden muss.
 * 	\version	13.07.21 ch
 * 					- Funktion db_PArowRangeCheck ergänzt
 * 	\version	15.07.21 ch
 * 					- Fehlermeldungen für Funktionen hinzugefügt
 * 	\version	06.08.21 ch
 * 					- add rowDelete Funktion
 * 	\version	11.08.21 mm
 * 					- Funktion db_datalistCutToLimitation und db_datalistsCut
 *					  in exported functions gelistet.
 * 	\version	12.08.21 mm
 *					- Funktion db_stripText definiert.
 * 	\version	16.08.21 mm
 *					- Funktion db_stripText gelöscht, wird direkt als lokale
 *					  Funktion in db_copyRowResultToTableStruct verwendet.
 * 	\version	20.08.21 mm
 *					- Funktion db_tableDelete definiert.
 * 	\version	23.08.21 mm
 *					- Enum te_db_flag um E_db_flagReconfigEthernet und
 *					  E_db_flagReconfigNTP erweitert.
 * 	\version	26.08.21 mm
 *					- Enum te_db_VAid um E_db_VAidSpare1 bis E_db_VAidSpare9
 *					  erweitert. (VA8 - VA16)
 * 	\version	03.09.21 mm
 *					- Enum te_db_flag um E_db_flagReboot erweitert.
 * 	\version	26.10.21 mm
 *					- Funktion db_tableExport definiert.
 * 	\version	11.11.21 mm
 *					- Enum te_db_flag um E_db_flagMemoryStickPlugged erweitert.
 * 					- Funktionen db_flagClear, db_flagSet und db_flagGet definiert. 
 * 	\version	24.11.21 mm
 *					- Enum te_db_tablePermission und te_db_PAidOffsetRights definiert.
 *					- Funktion db_tablePermission definiert.
 * 	\version	17.12.21 mm
 *					- Struktur ts_db_flag um strDescription erweitert.
 * 	\version	26.09.23 mm
 * 					- Erweiterungen für freie Betriebsparameter und Zähler.
 * 						- Konstanten C_db_paCounterBase, C_db_paCounterOffset
 * 						- Enums te_db_PAcounterType, te_db_PAsource,
 * 						  te_db_PAidOffsetVa, te_db_TPidOffsetVa
 * 	\version	28.09.23 mm
 *					- Enum te_db_flag um E_db_flagVa9writePermission bis
 *					  E_db_flagVa16writePermission erweitert.
 *					- Funktion db_vaNumberToFlagIndex definiert.
 * 	\version	19.10.23 mm
 *					- Funktion db_vaNumberToFlagIndex gelöscht und als
 *					  va_numberToFlagIndex in va.c implementiert um Linker-
 *					  abhängigkeiten zu reduzieren.
 *
 *
 ******************************************************************************/
#ifndef __db_H
    #define __db_H


/******************************************************************************
 * Compilerswitches                                                           *
 *                                                                            *
 ******************************************************************************/
//	#define	__db_accelerateTableCut
//	#define __db_showQueryStrings
	#define __db_showTablePermission
//	#define __db_showCsvStrings


/******************************************************************************
 * Modules                                                                    *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Definitions and Macros                                                     *
 *                                                                            *
 ******************************************************************************/
#define	C_db_dataBaseName		"testBase"
#define	C_db_dataBaseHost		"localhost"
#define	C_db_dataBaseUser		"trimada"
#define	C_db_dataBasePassword	"Master-5610"

#define	C_db_csvPaDefaultFile	"/opt/c4l/11007-PA-default.csv"
#define	C_db_csvTpDefaultFile	"/opt/c4l/11007-TP-default.csv"

#define C_db_dataStringSize		255
#define C_db_queryStringSize	255
#define C_db_debugStringSize	127
#define C_db_connectStringSize	63
#define C_db_intStringSize		15
#define	C_db_csvStringSize		1023

#ifdef __db_accelerateTableCut
	#define	C_db_maxDlSize			1000
	#define	C_db_maxEvSize			50
#else
	#define	C_db_maxDlSize			1000000
	#define	C_db_maxEvSize			500
#endif

#define	C_db_maxRowAi			2
#define	C_db_maxRowAo			2
#define	C_db_maxRowDi			4
#define	C_db_maxRowDo			4
#define	C_db_maxRowDl			1000000
#define	C_db_maxRowEv			800
#define	C_db_maxRowFlag			3
#define	C_db_maxRowPa			800
#define	C_db_maxRowTp			9999
#define	C_db_maxRowVa			8

#define	C_db_maxColumnAi		2
#define	C_db_maxColumnAo		2
#define	C_db_maxColumnDi		2
#define	C_db_maxColumnDo		2
#define	C_db_maxColumnDl		3
#define	C_db_maxColumnEv		2
#define	C_db_maxColumnFlag		2
#define	C_db_maxColumnPa		6
#define	C_db_maxColumnTp		2
#define	C_db_maxColumnVa		2

#define	C_db_csvHeaderStringAi		"id;value"
#define	C_db_csvHeaderStringAo		"id;value"
#define	C_db_csvHeaderStringDi		"id;description"
#define	C_db_csvHeaderStringDo		"id;description"
#define	C_db_csvHeaderStringDl		"id;value;dateTime"
#define	C_db_csvHeaderStringEv		"id;value;dateTime"
#define	C_db_csvHeaderStringFlag	"id;value"
#define	C_db_csvHeaderStringPa		"id;min;value;max;scale;checkRange"
#define	C_db_csvHeaderStringTp		"id;text"
#define	C_db_csvHeaderStringVa		"id;description"

#define	C_db_paBaseSystem		0
#define	C_db_paBaseUSB			51
#define	C_db_paBaseEth0			101
#define	C_db_paBaseApp0			111
#define	C_db_paBaseRights0		112
#define	C_db_paBaseSMTP			131
#define	C_db_paBaseNTP			145
#define	C_db_paBaseWebServer0	151
#define	C_db_paBaseEth1			201
#define	C_db_paBaseApp1			211
#define	C_db_paBaseRights1		212
#define	C_db_paBaseWebServer1	231
#define	C_db_paBaseModbusTCP1	241
#define	C_db_paBaseEth2			251
#define	C_db_paBaseApp2			261
#define	C_db_paBaseRights2		262
#define	C_db_paBaseWebServer2	281
#define	C_db_paBaseModbusTCP2	291
#define	C_db_paBaseMode2		300
#define	C_db_paChannelBaseAi	501
#define	C_db_paChannelOffsetAi	50
#define	C_db_paChannelBaseAo	601
#define	C_db_paChannelOffsetAo	50
#define	C_db_paChannelBaseDi	301
#define	C_db_paChannelOffsetDi	10
#define	C_db_paChannelBaseDo	401
#define	C_db_paChannelOffsetDo	10
#define	C_db_paCounterBase		701
#define	C_db_paCounterOffset	20

#define	C_db_tpChannelBaseAi	512
#define	C_db_tpChannelOffsetAi	50
#define	C_db_tpChannelBaseAo	613
#define	C_db_tpChannelOffsetAo	50
#define	C_db_tpChannelBaseDi	308
#define	C_db_tpChannelOffsetDi	10
#define	C_db_tpChannelBaseDo	407
#define	C_db_tpChannelOffsetDo	10
#define	C_db_tpCounterBase		709
#define	C_db_tpCounterOffset	20

#define	C_db_tpIdChgDeviceTime	4
#define	C_db_tpIdChgLanguage	6
#define	C_db_tpIdChgWntrSmrTime	8
#define	C_db_tpIdUsbPaUpload	53
#define	C_db_tpIdUsbPaDownload	54
#define	C_db_tpIdUsbTpUploadUSB	55
#define	C_db_tpIdUsbTpDownload	56
#define	C_db_tpIdLinkEth0		104
#define	C_db_tpIdSMTPeth0failed	132
#define	C_db_tpIdSMTPsubject	140
#define	C_db_tpIdNTPeth0failed	142
#define	C_db_tpIdLinkEth1		204
#define	C_db_tpIdLinkEth2		251

#define	C_dbVA_timeFormatString1	"%T"
#define	C_dbVA_timeFormatString2	"%d.%m.%Y"
#define	C_dbVA_timeFormatString3	"%T / %d.%m.%Y"
#define	C_dbVA_timeStringLen1		9					//!< hh:mm:ss + \n
#define	C_dbVA_timeStringLen2		11					//!< dd:mm:yyyy + \n
#define	C_dbVA_timeStringLen3		23					//!< hh:mm:ss / dd:mm:yyyy + \n


/******************************************************************************
 * Typedefs and Structures                                                    *
 *                                                                            *
 ******************************************************************************/
typedef enum {
	E_db_tableTypeAI,		//!< analoge Eingangswerte
	E_db_tableTypeAO,		//!< analoge Ausgangswerte
	E_db_tableTypeDI,		//!< digitale Eingangswerte
	E_db_tableTypeDO,		//!< digitale Ausgangswerte
	E_db_tableTypeDL,		//!< Datenliste
	E_db_tableTypeEV,		//!< Events
	E_db_tableTypeFLAG,		//!< Flags
	E_db_tableTypePA,		//!< Parameter
	E_db_tableTypeTP,		//!< Textparameter
	E_db_tableTypeVA,		//!< Prozessvariablen skaliert
	E_db_tableTypeCount,	//!< Anzahl effektiver Datentabellen
	E_db_tableTypeFC,		//!< Funktionscode
	E_db_tableTypeSC		//!< Set Clock
} te_db_tableType;


typedef enum {
	E_db_listTypeNone,		//!< keiner
	E_db_listTypeAI,		//!< analoge Eingangswerte
	E_db_listTypeAO,		//!< analoge Ausgangswerte
	E_db_listTypeDI,		//!< digitale Eingangswerte
	E_db_listTypeDO,		//!< digitale Ausgangswerte
	E_db_listTypeVA,		//!< Prozessvariablen skaliert
	E_db_listTypeCount
} te_db_listType;


typedef enum {
	E_db_tablePermissionNone,		//!< kein Listenzugriff
	E_db_tablePermissionRead,		//!< Listenzugriff lesen
	E_db_tablePermissionReadWrite,	//!< Listenzugriff lesen und schreiben
} te_db_tablePermission;


typedef struct {
	char strDatabase [C_db_connectStringSize];
	char strHost	 [C_db_connectStringSize];
	char strUser	 [C_db_connectStringSize];
	char strPassword [C_db_connectStringSize];
} ts_db_connect;


typedef struct {
	int		id;
	char	strValue [C_db_dataStringSize];
} ts_db_ai;


typedef struct {
	int		id;
	char	strValue [C_db_dataStringSize];
} ts_db_ao;


typedef struct {
	int		id;
	char	strValue [C_db_dataStringSize];
} ts_db_di;


typedef struct {
	int		id;
	char	strValue [C_db_dataStringSize];
} ts_db_do;


typedef struct {
	int		id;
	char	strValue [C_db_dataStringSize];
	char	strDateTime [C_db_dataStringSize];
} ts_db_dl;


typedef struct {
	int		id;
	char	strValue [C_db_dataStringSize];
	char	strDateTime [C_db_dataStringSize];
} ts_db_ev;


typedef struct {
	int		id;
	char	value;
	char	strDescription [C_db_dataStringSize];
} ts_db_flag;


// Definitionen für FLAG
typedef enum {
	E_db_flagServiceMode,					//!< (FC00 / FC01) Servicemode, 0:off, 1:on
	E_db_flagInitEventlist,					//!< (FC30) Eventliste initialisieren
	E_db_flagInitParameter,					//!< (FC32) PA und TP initialisieren
	E_db_flagReconfigEthernet,				//!< (FC33) Eine Neukonfiguration der Ethernetschnittstellen
	E_db_flagReconfigNTP,					//!< (FC34) Eine Neukonfiguration der Network Time Protcol Einstellungen
	E_db_flagReboot,						//!< (FC99) Rebooten des Systems angefordert.
	E_db_flagMemoryStickPlugged,			//!< Memorystick an USB Port erkannt
	E_db_flagVa9writePermission,			//!< VA9 darf beschrieben werden
	E_db_flagVa10writePermission,			//!< VA10 darf beschrieben werden
	E_db_flagVa11writePermission,			//!< VA11 darf beschrieben werden
	E_db_flagVa12writePermission,			//!< VA12 darf beschrieben werden
	E_db_flagVa13writePermission,			//!< VA13 darf beschrieben werden
	E_db_flagVa14writePermission,			//!< VA14 darf beschrieben werden
	E_db_flagVa15writePermission,			//!< VA15 darf beschrieben werden
	E_db_flagVa16writePermission,			//!< VA16 darf beschrieben werden
	E_db_flagCount							//!< Anzahl Flags
} te_db_flag;


// Definitionen für PA
typedef enum {
	E_db_PAoAlgSoftwareVersion,				//!< Software Version und Revision
	E_db_PAoAlgProjectNumber,				//!< Projektnummer
	E_db_PAoAlgCycleTime,					//!< Zykluszeit
	E_db_PAoAlgRTCsource,					//!< Gerätezeit 0:Betriebssystem, 1:NTP-Server
	E_db_PAoAlgMsgChangeRTCsource,			//!< SMTP aktivieren für Umstellen Systemzeitquelle
	E_db_PAoAlgLanguage,					//!< Sprache 1:TP1001..2000, 2:TP2001..3000, ...
	E_db_PAoAlgMsgChangeLanguage,			//!< SMTP aktivieren für Umstellen Sprache
	E_db_PAoAlgWinterSummerTime,			//!< Winter / Sommerzeit 0:MEZ, 1:MESZ
	E_db_PAoAlgMsgChangeWinterSummerTime,	//!< SMTP aktivieren für Umstellen Winter / Sommerzeit
	E_db_PAoAlgSerialnumber					//!< Seriennummer
} te_db_PAidOffsetAlg;


typedef enum {
	E_db_PAoUSBDirectory,		//!< Ordnernamen für USB
	E_db_PAoUSBPassword,		//!< Passwort für USB-Parameter upload
	E_db_PAoUSBSMTPuploadPA,
	E_db_PAoUSBSMTPdownloadPA,
	E_db_PAoUSBSMTPuploadTP,
	E_db_PAoUSBSMTPdownloadTP,
} te_db_PAidOffsetUSB;


typedef enum {
	E_db_PAoRightsPA,
	E_db_PAoRightsTP,
	E_db_PAoRightsVA,
	E_db_PAoRightsDI,
	E_db_PAoRightsDO,
	E_db_PAoRightsAI,
	E_db_PAoRightsAO,
	E_db_PAoRightsEV,
	E_db_PAoRightsDL,
	E_db_PAoRightsFC,
	E_db_PAoRightsSC
} te_db_PAidOffsetRights;


typedef enum {
	E_db_PAoSMTPPort,			//!< Port (0:off, >0:Portnummer)
	E_db_PAoSMTPMode,			//!< Mode (0:off, 1:on 1x, 2:on iteration x)
	E_db_PAoSMTPFrom,			//!< from Address
	E_db_PAoSMTPTo,				//!< to Address
	E_db_PAoSMTPServer,			//!< SMTP Server IP oder URL Adresse
	E_db_PAoSMTPIteration,		//!< Iterationsanzahl bei Sendefehler
	E_db_PAoSMTPUsername,		//!< 
	E_db_PAoSMTPPassword,		//!< 
	E_db_PAoSMTPEncryption,		//!< Encryption (0:off, 1:TLS, 2:SSL)
	E_db_PAoSMTPSubject,		//!< 
	E_db_PAoSMTPErrEvent,		//!< SMTP aktivieren für SMTP senden fehlgeschlagen
	E_db_PAoSMTPCount
} te_db_PAidOffsetSMTP;


typedef enum {
	E_db_PAoNTPPort,			//!< Port (0:off, >0:Portnummer)
	E_db_PAoNTPHost,			//!< NTP Host IP
	E_db_PAoNTPSMTP,			//!< SMTP aktivieren
	E_db_PAoNTPCount
} te_db_PAidOffsetNTP;


typedef enum {
	E_db_PAoAiEnableFunction,
	E_db_PAoAiEnableScaledLog,
	E_db_PAoAiLimitLo,
	E_db_PAoAiLimitHi,
	E_db_PAoAiUnit,
	E_db_PAoAiMeanSampleCount,
	E_db_PAoAiScaleName,		//!< String
	E_db_PAoAiScaleUnit,		//!< String
	E_db_PAoAiScaleElements,
	E_db_PAoAiScaleOffset,
	E_db_PAoAiStorageIntervall,
	E_db_PAoAiMsgTooLo,
	E_db_PAoAiMsgOperatorTooLo,
	E_db_PAoAiValueTooLo,
	E_db_PAoAiMsgLo,
	E_db_PAoAiMsgOperatorLo,
	E_db_PAoAiValueLo,
	E_db_PAoAiMsgHi,
	E_db_PAoAiMsgOperatorHi,
	E_db_PAoAiValueHi,
	E_db_PAoAiMsgTooHi,
	E_db_PAoAiMsgOperatorTooHi,
	E_db_PAoAiValueTooHi,
} te_db_PAidOffsetAi;


typedef enum {
	E_db_PAoAoFunction,
	E_db_PAoAoEnableScaledLog,
	E_db_PAoAoLimitLo,
	E_db_PAoAoLimitHi,
	E_db_PAoAoUnit,
	E_db_PAoAoMeanSampleCount,
	E_db_PAoAoScaleName,		//!< String
	E_db_PAoAoScaleUnit,		//!< String
	E_db_PAoAoScaleElements,
	E_db_PAoAoScaleOffset,
	E_db_PAoAoStorageIntervall,
	E_db_PAoAoDepency,
	E_db_PAoAoMsgTooLo,
	E_db_PAoAoMsgOperatorTooLo,
	E_db_PAoAoValueTooLo,
	E_db_PAoAoMsgLo,
	E_db_PAoAoMsgOperatorLo,
	E_db_PAoAoValueLo,
	E_db_PAoAoMsgHi,
	E_db_PAoAoMsgOperatorHi,
	E_db_PAoAoValueHi,
	E_db_PAoAoMsgTooHi,
	E_db_PAoAoMsgOperatorTooHi,
	E_db_PAoAoValueTooHi,
	E_db_PAoAo25,
	E_db_PAoAo26,
	E_db_PAoAo27,
	E_db_PAoAo28,
	E_db_PAoAo29,
	E_db_PAoAo30,
	E_db_PAoAoPiTr,
	E_db_PAoAoPiWsource,
	E_db_PAoAoPiFp,
	E_db_PAoAoPiFi,
	E_db_PAoAoPiInvert,
	E_db_PAoAoPiOutputMin,
	E_db_PAoAoPiOutputMax,
	E_db_PAoAoPiTimeout,
	E_db_PAoAoPiOutputDefault,
	E_db_PAoAoPiOutputRange,
	E_db_PAoAoPiMsgMin,
	E_db_PAoAoPiMsgOperatorMin,
	E_db_PAoAoPiValueMin,
	E_db_PAoAoPiMsgMax,
	E_db_PAoAoPiMsgOperatorMax,
	E_db_PAoAoPiValueMax,
} te_db_PAidOffsetAo;


typedef enum {
	E_db_PAoDiFunction,
	E_db_PAoDiLimitLoHi,
	E_db_PAoDiLimitHiLo,
	E_db_PAoDiEnableLog,
	E_db_PAoDiScaleName,		//!< String
	E_db_PAoDiScaleUnit,		//!< String
	E_db_PAoDiCountTime,
	E_db_PAoDiMsgLoHi,
	E_db_PAoDiMsgHiLo,
} te_db_PAidOffsetDi;


typedef enum {
	E_db_PAoDoFunction,
	E_db_PAoDoEnableLog,
	E_db_PAoDoName,				//!< String
	E_db_PAoDoUnit,				//!< String
	E_db_PAoDoActiveTime,
	E_db_PAoDoFollow,
	E_db_PAoDoMsgLoHi,
	E_db_PAoDoMsgHiLo,
} te_db_PAidOffsetDo;


typedef enum {
	E_db_PAoVaFunction,
	E_db_PAoVaType,
	E_db_PAoVaName,				//!< String
	E_db_PAoVaUnit,				//!< String
	E_db_PAoVaSource,
	E_db_PAoVaStartCondition,
	E_db_PAoVaValueCondition,
	E_db_PAoVaHysteresis,
	E_db_PAoVaMsgTooLo,
	E_db_PAoVaMsgOperatorTooLo,
	E_db_PAoVaValueTooLo,
	E_db_PAoVaMsgLo,
	E_db_PAoVaMsgOperatorLo,
	E_db_PAoVaValueLo,
	E_db_PAoVaMsgHi,
	E_db_PAoVaMsgOperatorHi,
	E_db_PAoVaValueHi,
	E_db_PAoVaMsgTooHi,
	E_db_PAoVaMsgOperatorTooHi,
	E_db_PAoVaValueTooHi,
} te_db_PAidOffsetVa;


typedef enum {
	E_db_PArangeCheckOk,		//!< Parameter
	E_db_PArangeCheckNok,		//!< Parameter
	E_db_PArangeCheckFail		//!< Parameter
} te_db_PArangeCheck;


typedef struct {
	int		id;									//!< Parameter Nr.
	int		checkRange;							//!< 0: keine Prüfung, 1: Wert wird auf min / max geprüft
	char	strMin [C_db_dataStringSize];		//!< Minimalwert
	char	strValue [C_db_dataStringSize];		//!< Wert
	char	strMax [C_db_dataStringSize];		//!< Maximalwert
	char	strScale [C_db_dataStringSize];		//!< Skalierung
} ts_db_pa;


// Definitionen für TP
typedef enum {
	E_db_TPoAiMsgTooLo,			//!< Indexoffset für AI in der TP Tabelle
	E_db_TPoAiFillerTooLo1,
	E_db_TPoAiFillerTooLo2,
	E_db_TPoAiMsgLo,
	E_db_TPoAiFillerLo1,
	E_db_TPoAiFillerLo2,
	E_db_TPoAiMsgHi,
	E_db_TPoAiFillerHi1,
	E_db_TPoAiFillerHi2,
	E_db_TPoAiMsgTooHi
} te_db_TPidOffsetAi;


typedef enum {
	E_db_TPoAoMsgTooLo,			//!< Indexoffset für AO in der TP Tabelle
	E_db_TPoAoFillerTooLo1,
	E_db_TPoAoFillerTooLo2,
	E_db_TPoAoMsgLo,
	E_db_TPoAoFillerLo1,
	E_db_TPoAoFillerLo2,
	E_db_TPoAoMsgHi,
	E_db_TPoAoFillerHi1,
	E_db_TPoAoFillerHi2,
	E_db_TPoAoMsgTooHi,
	E_db_TPoAoFillerTooHi1,
	E_db_TPoAoFillerTooHi2,
	E_db_TPoAoMsgPiLo,
	E_db_TPoAoFillerPiLo1,
	E_db_TPoAoFillerPiLo2,
	E_db_TPoAoMsgPiHi,
} te_db_TPidOffsetAo;


typedef enum {
	E_db_TPoVaMsgTooLo,			//!< Indexoffset für AI in der TP Tabelle
	E_db_TPoVAFillerTooLo1,
	E_db_TPoVAFillerTooLo2,
	E_db_TPoVaMsgLo,
	E_db_TPoVaFillerLo1,
	E_db_TPoVaFillerLo2,
	E_db_TPoVaMsgHi,
	E_db_TPoVaFillerHi1,
	E_db_TPoVaFillerHi2,
	E_db_TPoVaMsgTooHi
} te_db_TPidOffsetVa;


typedef enum {
	E_db_TPoDiMsgChangeFalseTrue,	//!< Indexoffset für DI in der TP Tabelle
	E_db_TPoDiMsgChangeTrueFalse
} te_db_TPidOffsetDi;


typedef enum {
	E_db_TPoDoMsgChangeFalseTrue,	//!< Indexoffset für DO in der TP Tabelle
	E_db_TPoDoMsgChangeTrueFalse
} te_db_TPidOffsetDo;


typedef enum {
	E_db_TPid0,						//!< Indexe für restliche Texte in der TP Tabelle
	E_db_TPid1,
	E_db_TPid2,
	E_db_TPid3,
	E_db_TPidChangeDeviceTime,
	E_db_TPidChangeLanguage,
	E_db_TPidChangeWinterSummerTime,

	E_db_TPidUsbUploadPa			= 53,
	E_db_TPidUsbDownloadPa,
	E_db_TPidUsbUploadTp,
	E_db_TPidUsbDownloadTp,
} te_db_TPid;


typedef struct {
	int		id;
	char	strText [C_db_dataStringSize];
} ts_db_tp;


// Definitionen für VA
typedef enum {
	E_db_VAidEmpty,				//!< leer (Index 0)
	E_db_VAidAi1Scaled,			//!< analog Eingang 1 skaliert
	E_db_VAidAi2Scaled,			//!< analog Eingang 2 skaliert
	E_db_VAidAo1Scaled,			//!< analog Ausgang 1 skaliert
	E_db_VAidAo2Scaled,			//!< analog Ausgang 2 skaliert
	E_db_VAidSystemTimeFormat1,	//!< hh:mm:ss
	E_db_VAidSystemTimeFormat2,	//!< dd.mm.yyyy
	E_db_VAidSystemTimeFormat3,	//!< hh:mm:ss / dd.mm.yyyy
	E_db_VA8,					//!< freier Betriebswert, Start oder Sundenzähler
	E_db_VA9,					//!< freier Betriebswert, Start oder Sundenzähler
	E_db_VA10,					//!< freier Betriebswert, Start oder Sundenzähler
	E_db_VA11,					//!< freier Betriebswert, Start oder Sundenzähler
	E_db_VA12,					//!< freier Betriebswert, Start oder Sundenzähler
	E_db_VA13,					//!< freier Betriebswert, Start oder Sundenzähler
	E_db_VA14,					//!< freier Betriebswert, Start oder Sundenzähler
	E_db_VA15,					//!< freier Betriebswert, Start oder Sundenzähler
	E_db_VA16,					//!< freier Betriebswert, Start oder Sundenzähler
	// nicht im Pflichtenheft
	E_db_VAidSystemTimeNanoSec,	//!< High Resolution Zeitwert in Sekunden
	E_db_VAidCount,	
} te_db_VAid;


typedef struct {
	int		id;
	char	strDescription [C_db_dataStringSize];
} ts_db_va;


/******************************************************************************
 * Exported Variables                                                         *
 *                                                                            *
 ******************************************************************************/
extern const char *C_db_tableTypeStr [E_db_tableTypeCount];


/******************************************************************************
 * Exported Functions                                                         *
 *                                                                            *
 ******************************************************************************/
int db_connectInit (ts_db_connect	*pConnect,
					char			*pName,
					char			*pHost,
					char			*pUser,
					char			*pPassword);

void db_connectDebug (ts_db_connect *pConnect);

int	db_create (ts_db_connect *pConnect);
int	db_delete (ts_db_connect *pConnect);

int	db_tableCreate (ts_db_connect	*pConnect,
					te_db_tableType	tableType);

int	db_tableDelete (ts_db_connect	*pConnect,
					te_db_tableType	tableType);

te_db_tablePermission db_tablePermission (ts_db_connect	  *pConnect,
										  te_db_tableType tableType,
										  int			  ethNumber);

int db_tableExport (ts_db_connect	*pConnect,
					te_db_tableType	tableType,
					te_db_listType	listType,
					int				channel,
					char			*pFilename);

int	db_tableCreateAllDatalists (ts_db_connect *pConnect);

int	db_datalistCutToLimitation (ts_db_connect	*pConnect,
								te_db_tableType	tableType,
								te_db_listType	listType,
								int				channel,
								int				limit);

int	db_rowAdd (ts_db_connect	*pConnect,
			  te_db_tableType	tableType,
			  te_db_listType	listType,
			  int				channel,
			  void				*pTable);

int db_rowUpdate (ts_db_connect		*pConnect,
                  te_db_tableType	tableType,
                  void				*pTable);
                  
int	db_rowDelete (ts_db_connect		*pConnect,
				  te_db_tableType	tableType,
				  te_db_listType	listType,
				  int				channel,
				  int				startID,
				  int				endID); 

int db_rowCount (ts_db_connect	*pConnect,
			  te_db_tableType	tableType,
			  te_db_listType	listType,
			  int				channel);                  

int db_rowGet (ts_db_connect	*pConnect,
               te_db_tableType	tableType,
			   te_db_listType	listType,
			   int				channel,
               void				*pTable);

int db_rowGetSorted (ts_db_connect	 *pConnect,
					 te_db_tableType tableType,
					 te_db_listType	 listType,
					 int			 channel,
					 void			 *pTable);
					 
void db_rowDebug (te_db_tableType	tableType,
				  void				*pTable);

int db_datalistsCut (ts_db_connect	*pConnect);

int db_csvReadFile (ts_db_connect	*pConnect,
					te_db_tableType	tableType,
					char			*pCsvFileName);

int db_csvWriteFile (ts_db_connect	 *pConnect,
					 te_db_tableType tableType,
					 char			 *pCsvFileName);

int db_csvWriteDataLists (ts_db_connect	*pConnect,
						  char			*pCsvFileBody);

void db_flagClear (ts_db_connect *pConnect,
				   te_db_flag	 flag);

void db_flagSet (ts_db_connect	*pConnect,
				te_db_flag		flag);

int db_flagGet (ts_db_connect	*pConnect,
				te_db_flag		flag);

/*
te_db_flag db_vaNumberToFlagIndex (ts_db_connect *pConnect,
								   int			 vaNumber);
*/

int db_vaUpdateTime (ts_db_connect	*pConnect); 

int db_vaUpdateHiResTime (ts_db_connect	*pConnect);

te_db_PArangeCheck db_PArowRangeCheck ( ts_db_pa	*pDbPA);


#endif //__db_H


/* End db.h */
