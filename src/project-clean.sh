#! /bin/bash

# file          project-clean.sh
# brief         script for project control4log
# details       deletes all builded file output from make of project
# date          25.04.2023
# author        Michael Mislin
# copyright     techdock GmbH, 4102 Binningen, Switzerland
# license       The MIT License (MIT)
#
# version       25.04.2023      mimi    - creating
# version       20.10.2023      mm		- modified
#

# declare script variables and dierctories
project="control4log"
name="c4l"
filename=$(basename $0)

printf "\n### Running ${filename} to install project ${project}-files and symlinks\n"

# create instdir = /opt/<name>/
instdir="/opt/${name}"
printf "Install directory is:   ${instdir}\n"

# create srcdir = /home/<user>/projects/<project>/src/
srcdir="$(cd -P -- "$(dirname -- "$0")" && pwd -P)"
printf "Source directory is:    ${srcdir}\n"

# create bindir = /home/<user>/projects/<project>/opt/<name>/
cd srcdir > /dev/null 2>&1 # redirect output for make cmd silent
cd ../ > /dev/null 2>&1 # redirect output for make cmd silent

# bindir = /home/<user>/projects/<project>/opt/<name>/
bindir="$(cd -P -- "$(dirname -- "$0")" && pwd -P)${instdir}"
printf "Binary directory is:    ${bindir}\n"


# start cleaning build libs

## delete all build libs of make configeths
path=$srcdir/tools/configeths
#ls -la
sudo rm $path/configeths.o
sudo rm $path/csv.o
sudo rm $path/db.o
#ls -la
printf "All build libs from make configeths by ${filename} are delete\n"

## delete all build libs of make configntp
path=$srcdir/tools/configntp
#ls -la
sudo rm $path/configntp.o
sudo rm $path/csv.o
sudo rm $path/db.o
#ls -la
printf "All build libs from make configntp by ${filename} are delete\n"

## delete all build libs of make conv2utf8
path=$srcdir/tools/conv2utf8
#ls -la
sudo rm $path/conv2utf8.o
#ls -la
printf "All build libs from make conv2utf8 by ${filename} are delete\n"

## delete all build libs of make dbcreate
path=$srcdir/tools/dbcreate
#ls -la
sudo rm $path/csv.o
sudo rm $path/db.o
sudo rm $path/dbcreate.o
#ls -la
printf "All build libs from make dbcreate by ${filename} are delete\n"

## delete all build libs of make dbdelete
path=$srcdir/tools/dbdelete
#ls -la
sudo rm $path/csv.o
sudo rm $path/db.o
sudo rm $path/dbdelete.o
#ls -la
printf "All build libs from make dbdelete by ${filename} are delete\n"

## delete all build libs of make iolog
path=$srcdir/iolog
#ls -la
sudo rm $path/ads1115.o
sudo rm $path/ads7924.o
sudo rm $path/ai.o
sudo rm $path/ao.o
sudo rm $path/csv.o
sudo rm $path/db.o
sudo rm $path/di.o
sudo rm $path/do.o
sudo rm $path/ioLog.o
sudo rm $path/ltc2617.o
sudo rm $path/mail.o
sudo rm $path/va.o
#ls -la
printf "All build libs from make iolog by ${filename} are delete\n"

## delete all build libs of make tcpss
path=$srcdir/tcpss
#ls -la
sudo rm $path/csv.o
sudo rm $path/db.o
sudo rm $path/mail.o
sudo rm $path/ptkC4l.o
sudo rm $path/tcpss.o
sudo rm $path/va.o
#ls -la
printf "All build libs from make tcpss by ${filename} are delete\n"

## delete all build libs of make usbevent
path=$srcdir/tools/usbevent
#ls -la
sudo rm $path/c4lusb.o
sudo rm $path/csv.o
sudo rm $path/db.o
sudo rm $path/mail.o
sudo rm $path/usbevent.o
#ls -la
printf "All build libs from make usbevent by ${filename} are delete\n"

## delete all build libs of make usbhello
path=$srcdir/tools/usbhello
#ls -la
sudo rm $path/csv.o
sudo rm $path/db.o
sudo rm $path/usbhello.o
#ls -la
printf "All build libs from make usbhello by ${filename} are delete\n"


# start cleaning build symbolic links

## delete all symbolic links for make configets
path=$srcdir/tools/configeths
#ls -la
sudo rm $path/buildDate.h
sudo rm $path/csv.c
sudo rm $path/csv.h
sudo rm $path/db.c
sudo rm $path/db.h
#ls -la
printf "All symlinks for make configeths are by ${filename} delete\n"

## delete all symbolic links for make configntp
path=$srcdir/tools/configntp
#ls -la
sudo rm $path/buildDate.h
sudo rm $path/csv.c
sudo rm $path/csv.h
sudo rm $path/csw.h
sudo rm $path/db.c
sudo rm $path/db.h
#ls -la
printf "All symlinks for make configntp are by ${filename} delete\n"

## delete all symbolic links for make conv2utf8
path=$srcdir/tools/conv2utf8
sudo rm $path/buildDate.h
printf "All symlinks for make conv2utf8 are by ${filename} delete\n"

## delete all symbolic links for make dbcreate
path=$srcdir/tools/dbcreate
#ls -la
sudo rm $path/buildDate.h
sudo rm $path/csv.c
sudo rm $path/csv.h
sudo rm $path/csw.h
sudo rm $path/db.c
sudo rm $path/db.h
#ls -la
printf "All symlinks for make decreate are by ${filename} delete\n"

## delete all symbolic links for make dbdelete
path=$srcdir/tools/dbdelete
#ls -la
sudo rm $path/buildDate.h
sudo rm $path/csv.c
sudo rm $path/csv.h
sudo rm $path/csw.h
sudo rm $path/db.c
sudo rm $path/db.h
#ls -la
printf "All symlinks for make dbdelete are by ${filename} delete\n"

## delete all symbolic links for make iolog
path=$srcdir/iolog
#ls -la
sudo rm $path/ads1115.c
sudo rm $path/ads1115.h
sudo rm $path/ads7924.c
sudo rm $path/ads7924.h
sudo rm $path/ai.c
sudo rm $path/ai.h
sudo rm $path/ao.c
sudo rm $path/ao.h
sudo rm $path/buildDate.h
sudo rm $path/csv.c
sudo rm $path/csv.h
sudo rm $path/csw.h
sudo rm $path/db.c
sudo rm $path/db.h
sudo rm $path/di.c
sudo rm $path/di.h
sudo rm $path/do.c
sudo rm $path/do.h
sudo rm $path/ioa.h
sudo rm $path/ltc2617.c
sudo rm $path/ltc2617.h
sudo rm $path/mail.c
sudo rm $path/mail.h
sudo rm $path/va.c
sudo rm $path/va.h
#ls -la
printf "All symlinks for make iolog are by ${filename} delete\n"

## delete all symbolic links for make tcpss
path=$srcdir/tcpss
#ls -la
sudo rm $path/buildDate.h
sudo rm $path/ai.c
sudo rm $path/ai.h
sudo rm $path/ao.c
sudo rm $path/ao.h
sudo rm $path/csv.c
sudo rm $path/csv.h
sudo rm $path/csw.h
sudo rm $path/db.c
sudo rm $path/db.h
sudo rm $path/di.c
sudo rm $path/di.h
sudo rm $path/do.c
sudo rm $path/do.h
sudo rm $path/ioa.h
sudo rm $path/ptkC4l.c
sudo rm $path/ptkC4l.h
sudo rm $path/mail.c
sudo rm $path/mail.h
sudo rm $path/va.c
sudo rm $path/va.h
#ls -la
printf "All symlinks for make tcpss are by ${filename} delete\n"

## delete all symbolic links for make usbevent
path=$srcdir/tools/usbevent
#ls -la
sudo rm $path/buildDate.h
sudo rm $path/c4lusb.c
sudo rm $path/c4lusb.h
sudo rm $path/csv.c
sudo rm $path/csv.h
sudo rm $path/csw.h
sudo rm $path/db.c
sudo rm $path/db.h
sudo rm $path/mail.c
sudo rm $path/mail.h
sudo rm $path/ptkC4l.c
sudo rm $path/ptkC4l.h
#ls -la
printf "All symlinks for make usbevent are by ${filename} delete\n"

## delete all symbolic links for make usbhello
path=$srcdir/tools/usbhello
#ls -la
sudo rm $path/buildDate.h
sudo rm $path/csv.c
sudo rm $path/csv.h
sudo rm $path/csw.h
sudo rm $path/db.c
sudo rm $path/db.h
#ls -la
printf "All symlinks for make usbhello are by ${filename} delete\n"


# end skript
printf "### end ${filename}\n\n"
