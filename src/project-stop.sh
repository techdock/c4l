#! /bin/bash

# file          project-stop.sh
# brief         script for project control4log
# details       stopping all running services from project
# date          25.04.2023
# author        Michael Mislin
# copyright     techdock GmbH, 4102 Binningen, Switzerland
# license	The MIT License (MIT)
#
# version       25.04.2023	mimi	- creating
# 


# declare script variables and dierctories
project="control4log"
name="c4l"
filename=$(basename $0)

printf "\n### Running ${filename} to stop project ${project} services\n"

# create instdir = /opt/<name>/
instdir="/opt/${name}"
printf "Install directory is:   ${instdir}\n"

# create srcdir = /home/<user>/projects/<project>/src/
srcdir="$(cd -P -- "$(dirname -- "$0")" && pwd -P)"
printf "Source directory is:    ${srcdir}\n"

# create bindir = /home/<user>/projects/<project>/opt/<name>/
cd srcdir > /dev/null 2>&1 # redirect output for make cmd silent
cd ../ > /dev/null 2>&1 # redirect output for make cmd silent

# bindir = /home/<user>/projects/<project>/opt/<name>/
bindir="$(cd -P -- "$(dirname -- "$0")" && pwd -P)${instdir}"
printf "Binary directory is:    ${bindir}\n"


# starting process
 
## stop v4l services
sudo systemctl stop configeths
sudo systemctl stop configntp
sudo systemctl stop ioLog
sudo systemctl stop tcpss
sudo systemctl stop usbevent
printf "Stopped all ${filename} running services\n"

## kill v4l services
sudo pkill configeths
sudo pkill configntp
sudo pkill ioLog
sudo pkill tcpss
sudo pkill usbevent
printf "Killed all ${filename} running services\n"

## remove v4l services from systemctl list
sudo systemctl disable configeths
sudo systemctl disable configntp
sudo systemctl disable ioLog
sudo systemctl disable tcpss
sudo systemctl disable usbevent
printf "Removeed all ${filename} services from systemctl\n"

## load  systemctl list new for make sure services are not started when rebooting 
sudo systemctl daemon-reload
printf "Reloaded systemctl\n"


# end skript
printf "### end ${filename}\n\n"

