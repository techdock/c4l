# file			$bindir/configeths/makefile
# brief			makefile for project control4log
# details		build dependency lib's and binary ioLog
# date			19.07.2021
# author		Marcel Ming
# copyright		TRiMADA AG, CH-5610 Wohlen, Switzerland
# license		The MIT License (MIT)
#
# version		19.07.2021 mm	- creating
#				25.04.2023 mimi - make dynamic
#				29.09.2023 mm	- add va.o, va.h

# declare script variables and dierctories
name = "c4l"


#This sample makefile has been setup for a project which contains the following files: main.h, ap-main.c, ap-main.h, ap-gen.c, ap-gen.h   Edit as necessary for your project

#Change output_file_name.a below to your desired executible filename

#first Cleanup
$(shell rm -f *.o *~ core *~ \)

#Set all your object files (the object files of all the .c files in your project, e.g. main.o my_sub_functions.o )
OBJ = \
	ioLog.o \
	ads1115.o \
	ads7924.o \
	ai.o \
	ao.o \
	csv.o \
	db.o \
	di.o \
	do.o \
	va.o \
	ltc2617.o \
	mail.o

#Set any dependant header files so that if they are edited they cause a complete re-compile (e.g. main.h some_subfunctions.h some_definitions_file.h ), or leave blank
DEPS = \
	ads1115.h \
	ads7924.h \
	ai.h \
	ao.h \
	buildDate.h \
	csv.h \
	csw.h \
	db.h \
	di.h \
	do.h \
	va.h \
	ioa.h \
	ltc2617.h \
	mail.h

#Any special libraries you are using in your project (e.g. -lbcm2835 -lrt `pkg-config --libs gtk+-3.0` ), or leave blank
LIBS = \
	-lgpiod \
	-li2c \
	-lmysqlclient \
	-lrt

#Set any compiler flags you want to use (e.g. -I/usr/include/somefolder `pkg-config --cflags gtk+-3.0` ), or leave blank
CFLAGS = -lrt

#Set the compiler you are using ( gcc for C or g++ for C++ )
CC = gcc

#Set the filename extensiton of your C files (e.g. .c or .cpp )
EXTENSION = .c

#define a rule that applies to all files ending in the .o suffix, which says that the .o file depends upon the .c version of the file and all the .h files included in the DEPS macro.  Compile each object file
%.o: %$(EXTENSION) $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

#Combine them into the output file
#Set your desired exe output file name here
../../opt/$(name)/ioLog: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)

#Cleanup
.PHONY: clean

clean:
	rm -f *.o *~ core *~ 
