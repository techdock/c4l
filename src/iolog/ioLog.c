/**************************************************************************//**
 *
 *	\file		ioLog.c
 *
 *	\brief		Hauptprogramm für den I/O-Logger des techdock Projekts control4log.
 *
 *	\details	Bedient die analogen und digitalen I/O's gemäss Datenbank Konfiguration.
 *				Schreibt die verschiedenen Logeinträge gemäss Datenbank Konfiguration.
 *				Sendet Mails gemäss Datenbank Konfiguration.
 *
 *	\date		13.04.2021
 *	\author		Marcel Ming
 *	\copyright	TRiMADA AG   CH-5610 Wohlen
 *	\remark		Compiler- / IDE-Version
 *
 *	\version	13.04.21 mm
 *					- Erstellt.
 *	\version	06.08.21 mm
 *					- GPIO17 als C_do_lockFeedback implementiert.
 *	\version	12.08.21 mm
 *					- Termination Handler implementiert.
 *	\version	01.09.21 mm
 *					- Funktionen ioLog_processQuickAi, ..Ao, ..Di und ..Do
 *					  implementiert.
 *	\version	03.09.21 mm
 *					- Reaktion auf Flag E_db_flagReboot implementiert.
 *	\version	06.09.21 mm
 *					- Definition der Compiler Switches in csw.h ausgelagert
 *	\version	15.09.21 mm
 *					- in ioLog_sampleDebug werden Werte von ausgeschalteten Ein-
 *					  oder Ausgängen rot dargestellt.
 *	\version	25.10.21 mm
 *					- in main bei einem "reboot System angefordert" auch das
 *					  Flag "Service Mode" in der Datenbank löschen.
 *	\version	26.10.21 mm
 *					- main mit Parameterübergabe erweitert. Ohne Parameter wird
 *					  strerr auf /dev/null umgeleitet um das Loggen in daemon.log
 *					  zu unterbinden, da sonst mehrere GB Daten pro Tag generiert
 *					  werden.
 *					  Mit der Option -v werden die mit den Compilerschaltern
 *					  aktivierten Meldungen ausgegeben.
 *	\version	26.10.21 mm
 *					- in Funktion ioLog_show bei ai und ao [mV] oder [uA] gemäss
 *					  Konfiguration anzeigen.
 *	\version	12.11.21 mm
 *					- Funktionen ioLog_clrFlag und ioLog_getFlag durch
 *					  db_flagClear und db_flagGet ersetzt.
 *	\version	17.11.21 mm
 *					- C_iolog_appVersion über builDate.h implementiert.
 *	\version	29.11.21 mm
 *					- Auswertung der Parameter mit Funktion getopt realisiert.
 *	\version	03.12.21 mm
 *					- Die Bearbeitung von FC30 und FC32 erfolgt auch wenn der
 *					  Service Mode bereits wieder verlassen wurde.
 *	\version	28.03.22 mm
 *					- ioLog_setBaseClockTimer ohne Parameter. Baseclock richtet
 *					  sich nach den DI Funktionen:
 *						-  10 ms: Zähler (Counter)
 * 						- 100 ms: Aus, Zustandswechsel (Off, Changes)
 *					- Aufruf von ioLog_setBaseClockTimer erst nach einem
 *					  eventuellen ioLog_configOverride.
 *					- Zählerfunktionalität ohne Rücksicht auf konfigurierte
 *					  Funktion in ioLog_sampleInterrupt integriert damit
 *					  geforderte Zählfrequenz erfüllt werden kann.
 *	\version	25.04.22 mm
 *					- Zählerfunktionalität nur bei konfigurierter Zählerfunktion
 *					  ausführen. Da stateLogged = stateActual ausgeführt wird,
 *					  kann sonst ein Zustandswechsel in di_process nicht mehr
 *					  erkannt werden.
 *	\version	02.05.22 mm
 *					- C_ioLog_dataBaseName, C_ioLog_dataBaseHost, C_ioLog_dataBaseUser
 *					  und C_ioLog_dataBasePassword gelöscht.
 *	\version	22.09.23 mm
 * 					- in Funktion ioLog_configOverride Parameter gelöscht, da diese
 * 					  nur für mail_configOverride benötigt werden.
 * 					  mail_configOverride wird separat aufgerufen.
 *	\version	29.09.23 mm
 *					- Funktion ioLog_readConfigVa implementiert.
 *					- Funktion ioLog_setPermissionFlagVa implementiert.
 *					- Funktion ioLog_sampleInterrupt for Loops eingefügt.
 *	\version	22.11.23 mm
 *					- Funktion ioLog_readConfigVa auch in ioLog_init aufgerufen.
 *	\version	11.12.23 mm
 *					- im ServiceMode werden die Zählerstände der VA's auch bearbeitet.
 *
 *
 ******************************************************************************/


/******************************************************************************
 * Compilerswitches                                                           *
 *                                                                            *
 ******************************************************************************/
//	#define __ioLog_showSampleDebug		// Ausgabe erfolgt alle 100 (10)ms anstelle 1s


/******************************************************************************
 * Modules                                                                    *
 *                                                                            *
 ******************************************************************************/
#include <errno.h>
#include <fcntl.h>
#include <gpiod.h>
#include <i2c/smbus.h>
#include <linux/i2c-dev.h>
#include <limits.h>
#include <mysql/mysql.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>

#include "ads1115.h"
#include "ads7924.h"
#include "ai.h"
#include "ao.h"
#include "buildDate.h"
#include "csw.h"
#include "db.h"
#include "di.h"
#include "do.h"
#include "ioa.h"
#include "ltc2617.h"
#include "mail.h"
#include "va.h"



/******************************************************************************
 * Definitions and Macros                                                     *
 *                                                                            *
 ******************************************************************************/
#define	C_ioLog_appName		"ioLog"


const uint8_t C_ioLog_appVersion [] = {
	C_buildDate_year_CH0,
	C_buildDate_year_CH1,
	C_buildDate_month_CH0,
	C_buildDate_month_CH1,
	C_buildDate_day_CH0,
	C_buildDate_day_CH1,
	'-',
	C_buildDate_hour_CH0,
	C_buildDate_hour_CH1,
	C_buildDate_min_CH0,
	C_buildDate_min_CH1
};


/******************************************************************************
 * Typedefs and Structures                                                    *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * External Variables                                                         *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Global Variables                                                           *
 *                                                                            *
 ******************************************************************************/
te_ioa_adcSampleState	adcSampleState;		//!< global wegen ioLog_sampleInterrupt
ts_ads7924_dataBuffer	ads7924data;		//!< global wegen ioLog_sampleInterrupt
tu_ads1115_config		ads1115config;		//!< global wegen ioLog_sampleInterrupt
ts_ioa					ioa;				//!< global wegen ioLog_sampleInterrupt
ts_csw_flag				csw_flag;

const int C_ioLog_gpioList [] = {
	C_do_gpio_0,
	C_do_gpio_1,
	C_do_gpio_2,
	C_do_gpio_3,
};


/******************************************************************************
 * Local Functions                                                            *
 *                                                                            *
 ******************************************************************************/

/**************************************************************************//**
 *
 *	\brief		ioLog_debugHiResTime
 *
 *	\details	schreibt die aktuelle high resolution time auf stderr.
 *
 *	\return		-
 *
 ******************************************************************************/
static void ioLog_debugHiResTime (void)
{
	if (csw_flag.hardwareTest)
	{
		clock_gettime (CLOCK_REALTIME, &ioa.hiResTime);
		fprintf (stderr, "ioLog_debugHiResTime: time %09ld.%09d[s]\n",
					 ioa.hiResTime.tv_sec, ioa.hiResTime.tv_nsec);
	} // if (csw_flag.hardwareTest)
} /* ioLog_debugHiResTime */


/**************************************************************************//**
 *
 *	\brief		ioLog_debugCharset
 *
 *	\details	schreibt die alle Zeichen von 0..255 auf stderr.
 *
 *	\param		-
 *	\return		-
 *
 ******************************************************************************/
static void ioLog_debugCharset (void)
{
	int	i;
	int	j;


	fprintf (stderr, "ioLog_debugCharset:\n");
	fprintf (stderr, "  0 1 2 3 4 5 6 7 8 9 A B C D E F\n");

	for (i = 2; i < 16; i++)
	{
		fprintf (stderr, "%1x ", i);

		for (j = 0; j < 16; j++)
		{
			fprintf (stderr, "%c ", (i * 16) + j);
		}

		fprintf (stderr,  "\n");
	} // for (i = 0; i < 16; i++)
} /* ioLog_debugCharset */


/**************************************************************************//**
 *
 *	\brief		ioLog_readConfigAi
 *
 *	\details	liest die Konfiguration der analogen Eingänge.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *
 *	\return		-
 *
 ******************************************************************************/
static void ioLog_readConfigAi (ts_db_connect *pConnect)
{
	int	i;


	for (i = 0; i < C_ai_channelCount; i++)
	{
		fprintf (stderr, "read config Ai%d - ", i);

		if (ai_configRead (pConnect, &ioa.AI [i], i))
		{
			fprintf (stderr, "failed\n");
		}
		else
		{
			fprintf (stderr, "ok\n");
		}

		ai_configDebug (&ioa.AI [i], i);
		ai_initMean (&ioa.AI [i].mean, ioa.AI [i].meanSampleCount);
	} // for (i = 0; i < C_ai_channelCount; i++)
} /* ioLog_readConfigAi */


/**************************************************************************//**
 *
 *	\brief		ioLog_readConfigAo
 *
 *	\details	liest die Konfiguration der analogen Ausgänge.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *
 *	\return		-
 *
 ******************************************************************************/
static void ioLog_readConfigAo (ts_db_connect *pConnect)
{
	int	i;


	for (i = 0; i < C_ao_channelCount; i++)
	{
		fprintf (stderr, "read config Ao%d - ", i);

		if (ao_configRead (pConnect, &ioa.AO [i], i))
		{
			fprintf (stderr, "failed\n");
		}
		else
		{
			fprintf (stderr, "ok\n");
		}

		ao_configDebug (&ioa.AO [i], i);
	} // for (i = 0; i < C_ao_channelCount; i++)
} /* ioLog_readConfigAo */


/**************************************************************************//**
 *
 *	\brief		ioLog_readConfigDi
 *
 *	\details	liest die Konfiguration der digitalen Eingänge.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *
 *	\return		-
 *
 ******************************************************************************/
static void ioLog_readConfigDi (ts_db_connect *pConnect)
{
	int	i;


	for (i = 0; i < C_di_channelCount; i++)
	{
		fprintf (stderr, "read config Di%d - ", i);

		if (di_configRead (pConnect, &ioa.DI [i], i))
		{
			fprintf (stderr, "failed\n");
		}
		else
		{
			fprintf (stderr, "ok\n");
		}

		di_configDebug (&ioa.DI [i], i);
	} // for (i = 0; i < C_di_channelCount; i++)
} /* ioLog_readConfigDi */


/**************************************************************************//**
 *
 *	\brief		ioLog_readConfigDo
 *
 *	\details	liest die Konfiguration der digitalen Ausgänge.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *
 *	\return		-
 *
 ******************************************************************************/
static void ioLog_readConfigDo (ts_db_connect *pConnect)
{
	int	i;


	for (i = 0; i < C_do_channelCount; i++)
	{
		fprintf (stderr, "read config Do%d - ", i);

		if (do_configRead (pConnect, &ioa.DO [i], i))
		{
			fprintf (stderr, "failed\n");
		}
		else
		{
			fprintf (stderr, "ok\n");
		}

		do_configDebug (&ioa.DO [i], i);
	} // for (i = 0; i < C_do_channelCount; i++)
} /* ioLog_readConfigDo */


/**************************************************************************//**
 *
 *	\brief		ioLog_readConfigVa
 *
 *	\details	liest die Konfiguration der Betriebsvariablen.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *
 *	\return		-
 *
 ******************************************************************************/
static void ioLog_readConfigVa (ts_db_connect *pConnect)
{
	int	i;


	for (i = C_va_counterMinNumber; i <= C_va_counterMaxNumber; i++)
	{
		fprintf (stderr, "read config Va%d - ", i);

		if (va_configRead (pConnect, &ioa.VA [i - C_va_counterMinNumber], i))
		{
			fprintf (stderr, "failed\n");
		}
		else
		{
			fprintf (stderr, "ok\n");
		}

		va_configDebug (&ioa.VA [i - C_va_counterMinNumber], i);
		va_configSetPermissionFlag (pConnect, &ioa.VA [i - C_va_counterMinNumber], i);
	} // for (i = C_va_counterMinNumber; i <= C_va_counterMaxNumber; i++)
} /* ioLog_readConfigVa */


/**************************************************************************//**
 *
 *	\brief		ioLog_setPermissionFlagVa
 *
 *	\details	schreibt in der Datenbanktabelle FLAG die Schreibrechte für alle
 *				Counter Variablen.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *
 *	\return		-
 *
 ******************************************************************************/
static void ioLog_setPermissionFlagVa (ts_db_connect *pConnect)
{
	int	i;


	fprintf (stderr, "ioLog_setPermissionFlagVa:\n");

	for (i = C_va_counterMinNumber; i <= C_va_counterMaxNumber; i++)
	{
		va_configSetPermissionFlag (pConnect, &ioa.VA [i - C_va_counterMinNumber], i);
	}
} /* ioLog_setPermissionFlagVa */


/**************************************************************************//**
 *
 *	\brief		ioLog_processQuickAi
 *
 *	\details	wertet die Signale der analogen Eingänge ohne Datenbankeinträge
 *				aus um eine schnelle Hardwarereaktion zu ermöglichen.
 *
 *	\param		-
 *	\return		-
 *
 ******************************************************************************/
static void ioLog_processQuickAi (void)
{
	int	i;


	for (i = 0; i < C_ai_channelCount; i++)
	{
		ai_processQuick (&ioa.AI [i], i);
	} // for (i = 0; i < C_ai_channelCount; i++)
} /* ioLog_processQuickAi */


/**************************************************************************//**
 *
 *	\brief		ioLog_processQuickAo
 *
 *	\details	wertet die Signale der analogen Ausgänge ohne Datenbankeinträge
 *				aus um eine schnelle Hardwarereaktion zu ermöglichen.
 *
 *	\param		-
 *	\return		-
 *
 ******************************************************************************/
static void ioLog_processQuickAo (void)
{
	int	i;


	for (i = 0; i < C_ao_channelCount; i++)
	{
		ao_processQuick (&ioa.AO [i], i);
	} // for (i = 0; i < C_ao_channelCount; i++)
} /* ioLog_processQuickAo */


/**************************************************************************//**
 *
 *	\brief		ioLog_processQuickDi
 *
 *	\details	wertet die Signale der digitalen Eingänge ohne Datenbankeinträge
 *				aus um eine schnelle Hardwarereaktion zu ermöglichen.
 *
 *	\param		-
 *	\return		-
 *
 ******************************************************************************/
static void ioLog_processQuickDi (void)
{
	int	i;


	for (i = 0; i < C_di_channelCount; i++)
	{
		di_processQuick (&ioa.DI [i], i);
	} // for (i = 0; i < C_di_channelCount; i++)
} /* ioLog_processQuickDi */


/**************************************************************************//**
 *
 *	\brief		ioLog_processQuickDo
 *
 *	\details	wertet die Signale der digitalen Ausgänge ohne Datenbankeinträge
 *				aus um eine schnelle Hardwarereaktion zu ermöglichen.
 *
 *	\param		-
 *	\return		-
 *
 ******************************************************************************/
static void ioLog_processQuickDo (void)
{
	int	i;


	for (i = 0; i < C_do_channelCount; i++)
	{
		// Returnwert wird nicht bearbeitet
		do_processQuick (&ioa, i);
	} // for (i = 0; i < C_do_channelCount; i++)
} /* ioLog_processQuickDo */


/**************************************************************************//**
 *
 *	\brief		ioLog_processAi
 *
 *	\details	wertet die Signale der analogen Eingänge aus.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		*pMail		Pointer auf Struktur ts_mail
 *
 *	\return		-
 *
 ******************************************************************************/
static void ioLog_processAi (ts_db_connect	*pConnect,
							 ts_mail		*pMail)
{
	int	i;


	for (i = 0; i < C_ai_channelCount; i++)
	{
		// Returnwert wird nicht bearbeitet
		ai_process (pConnect, &ioa.AI [i], i, pMail);
	} // for (i = 0; i < C_ai_channelCount; i++)
} /* ioLog_processAi */


/**************************************************************************//**
 *
 *	\brief		ioLog_processAo
 *
 *	\details	wertet die Signale der analogen Ausgänge aus.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		*pMail		Pointer auf Struktur ts_mail
 *
 *	\return		-
 *
 ******************************************************************************/
static void ioLog_processAo (ts_db_connect	*pConnect,
							 ts_mail		*pMail)
{
	int	i;


	for (i = 0; i < C_ao_channelCount; i++)
	{
		// Returnwert wird nicht bearbeitet
		ao_process (pConnect, &ioa, i, pMail);
	} // for (i = 0; i < C_ao_channelCount; i++)
} /* ioLog_processAo */


/**************************************************************************//**
 *
 *	\brief		ioLog_processDi
 *
 *	\details	wertet die Signale der digitalen Eingänge aus.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		*pMail		Pointer auf Struktur ts_mail
 *
 *	\return		-
 *
 ******************************************************************************/
static void ioLog_processDi (ts_db_connect	*pConnect,
							 ts_mail		*pMail)
{
	int	i;


	for (i = 0; i < C_di_channelCount; i++)
	{
		// Returnwert wird nicht bearbeitet
		di_process (pConnect, &ioa.DI [i], i, pMail);
	} // for (i = 0; i < C_di_channelCount; i++)
} /* ioLog_processDi */


/**************************************************************************//**
 *
 *	\brief		ioLog_processDo
 *
 *	\details	wertet die Signale der digitalen Ausgänge aus.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		*pMail		Pointer auf Struktur ts_mail
 *
 *	\return		-
 *
 ******************************************************************************/
static void ioLog_processDo (ts_db_connect	*pConnect,
							 ts_mail		*pMail)
{
	int	i;


	for (i = 0; i < C_do_channelCount; i++)
	{
		// Returnwert wird nicht bearbeitet
		do_process (pConnect, &ioa, i, pMail);
	} // for (i = 0; i < C_do_channelCount; i++)
} /* ioLog_processDo */


/**************************************************************************//**
 *
 *	\brief		ioLog_processDiCounter
 *
 *	\details	wertet die Signale der digitalen Eingänge im Countermode aus.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		*pMail		Pointer auf Struktur ts_mail
 *
 *	\return		-
 *
 ******************************************************************************/
static void ioLog_processDiCounter (ts_db_connect	*pConnect,
									ts_mail			*pMail)
{
	int	i;


	for (i = 0; i < C_di_channelCount; i++)
	{
		// Returnwert wird nicht bearbeitet
		di_processCounter (pConnect, &ioa.DI [i], i);
	} // for (i = 0; i < C_di_channelCount; i++)
} /* ioLog_processDiCounter */


/**************************************************************************//**
 *
 *	\brief		ioLog_processVa
 *
 *	\details	wertet die Signale für die freien Betriebswerte aus.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		*pMail		Pointer auf Struktur ts_mail
 *
 *	\return		-
 *
 ******************************************************************************/
static void ioLog_processVa (ts_db_connect	*pConnect,
							 ts_mail		*pMail)
{
	int	i;


	for (i = C_va_counterMinNumber; i <= C_va_counterMaxNumber; i++)
	{
		// Returnwert wird nicht bearbeitet
		va_process (pConnect, &ioa, i, pMail);
	} // for (i = C_va_counterMinNumber; i <= C_va_counterMaxNumber; i++)
} /* ioLog_processVa */


/**************************************************************************//**
 *
 *	\brief		ioLog_scaledAi
 *
 *	\details	schreibt die skalierten Werte gemäss Taktzeit zum Speichern in
 *				die Datenliste der analogen Eingänge.
 *
 *	\attention	damit die Taktzeit stimmt, muss diese Funktion pro Sekunde einmal
 *				aufgerufen werden.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		*pMail		Pointer auf Struktur ts_mail
 *
 *	\return		-
 *
 ******************************************************************************/
static void ioLog_scaledAi (ts_db_connect	*pConnect)
{
	int	i;


	for (i = 0; i < C_ai_channelCount; i++)
	{
		if (ioa.AI [i].storageIntervallCounter <= 1)
		{
			ioa.AI [i].storageIntervallCounter = ioa.AI [i].storageIntervall;

			// Returnwert wird nicht bearbeitet
			ai_logScaled (pConnect, &ioa.AI [i], i);
		}
		else
		{
			ioa.AI [i].storageIntervallCounter--;
		} // else if (ioa.AI [i].storageIntervallCounter == 0)
	} // for (i = 0; i < C_ai_channelCount; i++)
} /* ioLog_scaledAi */


/**************************************************************************//**
 *
 *	\brief		ioLog_scaledAo
 *
 *	\details	schreibt die skalierten Werte gemäss Taktzeit zum Speichern in
 *				die Datenliste der analogen Ausgänge.
 *
 *	\attention	damit die Taktzeit stimmt, muss diese Funktion pro Sekunde einmal
 *				aufgerufen werden.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		*pMail		Pointer auf Struktur ts_mail
 *
 *	\return		-
 *
 ******************************************************************************/
static void ioLog_scaledAo (ts_db_connect	*pConnect,
							ts_mail			*pMail)
{
	int	i;


	for (i = 0; i < C_ao_channelCount; i++)
	{
		if (ioa.AO [i].storageIntervallCounter <= 1)
		{
			ioa.AO [i].storageIntervallCounter = ioa.AO [i].storageIntervall;

			// Returnwert wird nicht bearbeitet
			ao_logScaled (pConnect, &ioa.AO [i], i);
		}
		else
		{
			ioa.AO [i].storageIntervallCounter--;
		} // else if (ioa.AO [i].storageIntervallCounter == 0)
	} // for (i = 0; i < C_ao_channe * 	\version	16.08.21 mm
} /* ioLog_scaledAo */


/**************************************************************************//**
 *
 *	\brief		ioLog_sampleDebug
 *
 *	\details	schreibt die Werte aller analogen und digitalen Ein- und Ausgänge
 *				auf stderr.
 *
 *	\param		-
 *	\return		-
 *
 ******************************************************************************/
static void ioLog_sampleDebug (void)
{
	int	i;


	fprintf (stderr, "\n\033[1mioLog_sampleDebug");

	if (ioa.serviceMode)
	{
		fprintf (stderr, " service mode");
	}

	fprintf (stderr, "\033[0m\n");


	// Analogeingänge debuggen
	for (i = 0; i < C_ai_channelCount; i++)
	{
		if (ioa.AI [i].enableFunction)
		{
			fprintf (stderr, "ai%d:%5d[]\t", i, ioa.AI [i].binary);
		}
		else
		{
			fprintf (stderr, "ai%d:\033[31m%5d[]\033[0m\t", i, ioa.AI [i].binary);
		}
	} // for (i = 0; i < C_ai_channelCount; i++)
	fprintf (stderr,  " binary\n");

	for (i = 0; i < C_ai_channelCount; i++)
	{
		if (ioa.AI [i].enableFunction)
		{
			if (ioa.AI [i].unitVoltage)
			{
				fprintf (stderr, "ai%d:%5d[mv]\t", i, ioa.AI [i].voltage);
			}
			else
			{
				fprintf (stderr, "ai%d:%5d[uA]\t", i, ioa.AI [i].current);
			} // else if (ioa.AI [i].unitVoltage)
		}
		else
		{
			fprintf (stderr, "ai%d: off\t", i);
		} // if (ioa.AI [i].enableFunction)
	} // for (i = 0; i < C_ai_channelCount; i++)
	fprintf (stderr,  " input level\n");

	for (i = 0; i < C_ai_channelCount; i++)
	{
		if (ioa.AI [i].enableFunction)
		{
			fprintf (stderr, "ai%d:%5d[%s]\t", i, ioa.AI [i].scaledValue, ioa.AI [i].scaleUnit);
		}
		else
		{
			fprintf (stderr, "ai%d: --- [%s]\t", i, ioa.AI [i].scaleUnit);
		} // if (ioa.AI [i].enableFunction)
	}
	fprintf (stderr,  " scaled value\n");

	for (i = 0; i < C_ai_channelCount; i++)
	{
		if (ioa.AI [i].enableFunction)
		{
			fprintf (stderr, "ai%dv%5d[%s]\t", i, ioa.AI [i].valueTooLo, ioa.AI [i].scaleUnit);
		}
		else
		{
			fprintf (stderr, "ai%dv --- [%s]\t", i, ioa.AI [i].scaleUnit);
		} // if (ioa.AI [i].enableFunction)
	}
	fprintf (stderr,  " scaled limit too low\n\n");


	// Analogausgänge debuggen
	for (i = 0; i < C_ao_channelCount; i++)
	{
		if (ioa.AO [i].function != E_ao_fktOff)
		{
			fprintf (stderr, "ao%d:%5d[]\t", i, ioa.AO [i].binary);
		}
		else
		{
			fprintf (stderr, "ao%d:\033[31m%5d[]\033[0m\t", i, ioa.AO [i].binary);
		}
	} // for (i = 0; i < C_ao_channelCount; i++)
	fprintf (stderr,  " binary\n");

	for (i = 0; i < C_ao_channelCount; i++)
	{
		if (ioa.AO [i].function != E_ao_fktOff)
		{
			fprintf (stderr, "ao%d:%5d[mean]\t", i, ioa.AO [i].meanValue);
		}
		else
		{
			fprintf (stderr, "ao%d: --- [mean]\t", i);
		} // else if (ioa.AO [i].function != E_ao_fktOff)
	} // for (i = 0; i < C_ao_channelCount; i++)
	fprintf (stderr,  " mean value\n");

	for (i = 0; i < C_ao_channelCount; i++)
	{
		fprintf (stderr, "ao%d:%5d[Ye]\t", i, ioa.AO [i].scaleElements);
	} // for (i = 0; i < C_ao_channelCount; i++)
	fprintf (stderr,  " scale elements\n");

	for (i = 0; i < C_ao_channelCount; i++)
	{
		fprintf (stderr, "ao%d:%5d[Yo]\t", i, ioa.AO [i].scaleOffset);
	} // for (i = 0; i < C_ao_channelCount; i++)
	fprintf (stderr,  " scale offset\n");

	for (i = 0; i < C_ao_channelCount; i++)
	{
		if (ioa.AO [i].function != E_ao_fktOff)
		{
			fprintf (stderr, "ao%d:%5d[%s]\t", i, ioa.AO [i].scaledValue, ioa.AO [i].scaleUnit);
		}
		else
		{
			fprintf (stderr, "ao%d: --- [%s]\t", i, ioa.AO [i].scaleUnit);
		} // else if (ioa.AO [i].function != E_ao_fktOff)
	} // for (i = 0; i < C_ao_channelCount; i++)
	fprintf (stderr,  " scaled value\n\n");


	// Digitaleingänge debuggen
	for (i = 0; i < C_di_channelCount; i++)
	{
		switch (ioa.DI [i].function)
		{
			case E_di_fktOff:
				fprintf (stderr, "di%d:\033[31m%5d[]\033[0m\t", i, ioa.DI [i].binary);
				break;

			case E_di_fktChanges:
				fprintf (stderr, "di%d:%5d[]\t", i, ioa.DI [i].binary);
				break;

			case E_di_fktCounter:
				fprintf (stderr, "di%d:\033[32m%5d[^v]\033[0m\t", i, ioa.DI [i].counter);
				break;

			default:
				break;

		} // switch (ioa.DI [i].function)
	} // for (i = 0; i < C_di_channelCount; i++)

	fprintf (stderr,  " binary / count events\n");

	for (i = 0; i < C_di_channelCount; i++)
	{
		fprintf (stderr, "di%d:%5d[mV]\t", i, ioa.DI [i].voltage);
	} // for (i = 0; i < C_di_channelCount; i++)
	fprintf (stderr,  " input level\n");

	for (i = 0; i < C_di_channelCount; i++)
	{
		fprintf (stderr, "di%d:%5d[>mV]\t", i, ioa.DI [i].limitLoHi);
	} // for (i = 0; i < C_di_channelCount; i++)
	fprintf (stderr,  " level on\n");

	for (i = 0; i < C_di_channelCount; i++)
	{
		fprintf (stderr, "di%d:%5d[<mV]\t", i, ioa.DI [i].limitHiLo);
	} // for (i = 0; i < C_di_channelCount; i++)
	fprintf (stderr,  " level off\n\n");

	// Digitalausgänge debuggen
	for (i = 0; i < C_do_channelCount; i++)
	{
		if (ioa.DO [i].function != E_do_fktOff)
		{
			fprintf (stderr, "do%d:%5d\t", i, ioa.DO [i].coil);
		}
		else
		{
			fprintf (stderr, "do%d:\033[31m%5d\033[0m\t", i, ioa.DO [i].coil);
		}
	} // for (i = 0; i < C_do_channelCount; i++)
	fprintf (stderr,  "\n");
} /* ioLog_sampleDebug */


/**************************************************************************//**
 *
 *	\brief		ioLog_sampleInterrupt
 *
 *	\details	der Sample Interrupt liest und schreibt alle analogen und
 *				digitalen Ein- und Ausgänge.
 *
 *	\param		-
 *	\return		-
 *
 ******************************************************************************/
static void ioLog_sampleInterrupt (void)
{
	int	i;


	if (ioa.interval == C_ioa_interval_10ms)
	{
		ioa.counter10ms++;
		ioa.tick10ms = 1;

		if (ioa.counter10ms >= 10)
		{
			ioa.counter10ms	= 0;

			ioa.counter100ms++;
			ioa.tick100ms = 1;
		} // if (ioa.counter10ms == 10)
	}
	else
	{
		ioa.counter100ms++;
		ioa.tick100ms = 1;
	} // else if (ioa.interval == C_ioa_intervall_10ms)

	if (ioa.counter100ms >= 10)
	{
		ioa.counter100ms = 0;
		ioa.counter1s++;
		ioa.tick1s = 1;

		if (ioa.counter1s >= 60)
		{
			ioa.counter1s = 0;
			ioa.counter1m++;
			ioa.tick1m = 1;

			if (ioa.counter1m >= 60)
			{
				ioa.counter1m = 0;
				ioa.counter1h++;
				ioa.tick1h = 1;
			} // if (ioa.counter1m >= 60)
		} // if (ioa.counter1s >= 60)
	} // if (ioa.counter100ms >= 10)

	// Timestamp setzen
	clock_gettime (CLOCK_REALTIME, &ioa.hiResTime);

	ioa.hiResTimeStamp	= C_ioa_MultiplierSecToNsec * (int64_t)(ioa.hiResTime.tv_sec) + (int64_t)(ioa.hiResTime.tv_nsec);
	ioa.hiResDeltaStamp	= ioa.hiResTimeStamp - ioa.hiResLastStamp;
	ioa.hiResLastStamp	= ioa.hiResTimeStamp;

	if (ioa.hiResDeltaStamp > C_ioa_hiResDeltaMax)
	{
		ioa.hiResDeltaErrCount++;
	}

	// Digitalausgänge setzen
	for (i = 0; i < C_do_channelCount; i++)
	{
		gpiod_line_set_value (ioa.DO [i].pHandle, ioa.DO [i].coil);
	} // for (i = 0; i < C_do_channelCount; i++)

	// Analogausgänge setzen
	for (i = 0; i < C_ao_channelCount; i++)
	{
		ltc2617_setDac (ioa.i2cDeviceDescriptor,
						C_ltc2617_addressFloatFloatFloat,
						i,
						ioa.AO [i].binary);
	} // for (i = 0; i < C_ao_channelCount; i++)

	// Digitaleingänge lesen
	if (ads7924_getAllDataChannels (ioa.i2cDeviceDescriptor, C_ads7924_addressA0lo, &ads7924data) < 0)
	{
		fprintf (stderr, "ads7924_getAlldataChannels failed\n");

		for (i = 0; i < C_di_channelCount; i++)
		{
			ioa.DI [i].readError = true;
		}
	}
	else
	{
		ioa.DI [0].binary = ads7924data.adcValue [E_ads7924_channel0];
		ioa.DI [1].binary = ads7924data.adcValue [E_ads7924_channel1];
		ioa.DI [2].binary = ads7924data.adcValue [E_ads7924_channel2];
		ioa.DI [3].binary = ads7924data.adcValue [E_ads7924_channel3];

		ioa.DI [0].voltage = ads7924data.voltage [E_ads7924_channel0];
		ioa.DI [1].voltage = ads7924data.voltage [E_ads7924_channel1];
		ioa.DI [2].voltage = ads7924data.voltage [E_ads7924_channel2];
		ioa.DI [3].voltage = ads7924data.voltage [E_ads7924_channel3];

		ioa.DI [0].stateActual = ads7924data.logicLevel [E_ads7924_channel0];
		ioa.DI [1].stateActual = ads7924data.logicLevel [E_ads7924_channel1];
		ioa.DI [2].stateActual = ads7924data.logicLevel [E_ads7924_channel2];
		ioa.DI [3].stateActual = ads7924data.logicLevel [E_ads7924_channel3];

		// Zählfunktionen ohne Verwaltung hier erledigen
		for (i = 0; i < C_di_channelCount; i++)
		{
			if ((ioa.DI [i].function == E_di_fktCounter) && (ioa.DI [i].stateLogged != ioa.DI [i].stateActual))
			{
				ioa.DI [i].stateLogged = ioa.DI [i].stateActual;
				ioa.DI [i].counter++;
				ioa.DI [i].counted = 1;
			}

			ioa.DI [i].readError = false;
		} // for (i = 0; i < C_di_channelCount; i++)
	} // else if (ads7924_getAllDataChannels ...

	// Analogeingänge lesen
	switch (adcSampleState)
	{
		#if	C_ai_channelCount == 2
			case E_ioa_readCh2_configCh0:
				ioa.AI [1].readError = ads1115_getConversion (ioa.i2cDeviceDescriptor, C_ads1115_addressA0hi, &ioa.AI [1].binary);
		#endif

		#if	C_ai_channelCount == 4
			case E_ioa_readCh3_configCh0:
				ioa.AI [3].readError = ads1115_getConversion (ioa_i2cDeviceDescriptor, C_ads1115_addressA0hi, &ioa.AI [3].binary]);
		#endif
			ads1115config.cnf_inputMux = E_ads1115_imx_Ain0_GND;
			ads1115_setConfig (ioa.i2cDeviceDescriptor, C_ads1115_addressA0hi, &ads1115config);

			adcSampleState++;
			break;

		case E_ioa_configCh0pause0:
			adcSampleState++;
			break;

		case E_ioa_configCh0pause1:
			adcSampleState++;
			break;

		#if	C_ai_channelCount == 2
			case E_ioa_readCh0_configCh2:
				ioa.AI [0].readError = ads1115_getConversion (ioa.i2cDeviceDescriptor, C_ads1115_addressA0hi, &ioa.AI [0].binary);
				ads1115config.cnf_inputMux = E_ads1115_imx_Ain2_GND;
		#endif

		#if	C_ai_channelCount == 4
			case E_ioa_readCh0_configCh1:
				ioa.AI [0].readError = ads1115_getConversion (ioa.i2cDeviceDescriptor, C_ads1115_addressA0hi, &ioa.AI [0].binary);
				ads1115config.cnf_inputMux = E_ads1115_imx_Ain1_GND;
		#endif
			ads1115_setConfig (ioa.i2cDeviceDescriptor, C_ads1115_addressA0hi, &ads1115config);

			adcSampleState++;
			break;

		case E_ioa_configCh1pause0:
			adcSampleState++;
			break;

		case E_ioa_configCh1pause1:
			#if	C_ai_channelCount == 2
				adcSampleState = E_ioa_readCh2_configCh0;
			#endif

			#if	C_ai_channelCount == 4
				adcSampleState++;
			#endif
			break;

		#if	C_ai_channelCount == 4
			case E_ioa_readCh1_configCh2:
				ioa.AI [1].readError = ads1115_getConversion (ioa.i2cDeviceDescriptor, C_ads1115_addressA0hi, &ioa.AI [1].binary);

				ads1115config.cnf_inputMux = E_ads1115_imx_Ain2_GND;
				ads1115_setConfig (ioa.i2cDeviceDescriptor, C_ads1115_addressA0hi, &ads1115config);

				adcSampleState++;
				break;

			case E_ioa_configCh2pause0:
				adcSampleState++;
				break;

			case E_ioa_configCh2pause1:
				adcSampleState++;
				break;

			case E_ioa_readCh2_configCh3:
				ioa.AI [2].readError = ads1115_getConversion (ioa.i2cDeviceDescriptor, C_ads1115_addressA0hi, &ioa.AI [2].binary);

				ads1115config.cnf_inputMux = E_ads1115_imx_Ain3_GND;
				ads1115_setConfig (ioa.i2cDeviceDescriptor, C_ads1115_addressA0hi, &ads1115config);

				adcSampleState++;
				break;

			case E_ioa_configCh3pause0:
				adcSampleState++;
				break;

			case E_ioa_configCh3pause1:
				adcSampleState = E_ioa_readCh3_configCh0;
				break;
		#endif

		default:
			#if	C_ai_channelCount == 2
				adcSampleState = E_ioa_readCh2_configCh0;
			#endif

			#if	C_ai_channelCount == 4
				adcSampleState = E_ioa_readCh3_configCh0;
			#endif
			break;

	} // switch (adcSampleState)

	#ifdef __ioLog_showSampleDebug
		if (csw_flag.hardwareTest == 0)
		{
			ioLog_sampleDebug ();
		} // if (csw_flag.hardwareTest == 0)
	#endif
} /* ioLog_sampleInterrupt */


/**************************************************************************//**
 *
 *	\brief		ioLog_setBaseClockTimer
 *
 *	\details	initialisiert den Basis Timer Interrupt für die Messungen.
 *
 *	\attention	PA2 enthält die Zykluszeit in 10 ms Schritten (1..6000). Dies
 *				ergibt Zykluszeiten von 10 Millisekunden bis zu 1 Minute.
 *				Diese freie Zykluswahl ist etwas kompliziert in der Handhabung.
 *
 *	\todo		Vorschlag Abstufung: 10, 20, 50, 100, 200, 500, 1000 und 6000 ms.
 *				Damit lassen sich auch bruchlose Zeiten bis zu 1 s ableiten.
 * 				-> Entscheid: Es wird nicht mit einer konfigurierbaren Zykluszeit
 *							  gearbeitet. Wird ein Counterkanal definiert wird
 *							  eine Zykluszeit von 10 ms, sonst 100 ms verwendet.
 *
 *	\param		-
 *	\return		-
 *
 ******************************************************************************/
static void ioLog_setBaseClockTimer (void)
{
	int					i;
	struct itimerval	newTimerVal;


	ioa.interval = C_ioa_interval_100ms;

	// Zählerfunktion für einen digitalen Eingang konfiguriert?
	for (i = 0; i < C_di_channelCount; i++)
	{
		if (ioa.DI [i].function == E_di_fktCounter)
		{
//			ioa.diFktCounter = 1;
			ioa.interval	 = C_ioa_interval_10ms;
		}
	} // for (i = 0; i < C_di_channelCount; i++)

	#ifdef	__csw_slowdown
		ioa.interval = C_ioa_intervall_999ms;
	#endif

	// erster Ablauf des Timers nach ...
	newTimerVal.it_value.tv_sec = 0;
	newTimerVal.it_value.tv_usec = ioa.interval;

	// ... und wiederholt nach ...
	newTimerVal.it_interval.tv_sec = 0;
	newTimerVal.it_interval.tv_usec = ioa.interval;

	if (signal (SIGALRM, (void (*)(int)) ioLog_sampleInterrupt) == SIG_ERR)
	{
		fprintf (stderr, "ioLog_setBaseClockTimer: unable to catch SIGALRM\n");
	}
	else
	{
		if (setitimer (ITIMER_REAL, &newTimerVal, NULL) == 0)
		{
			fprintf (stderr, "ioLog_setBaseClockTimer: itimer ok\n");
		}
		else
		{
			fprintf (stderr, "ioLog_setBaseClockTimer: itimer error %d\n", errno);
		}
	} // else if signal (SIGALRM, (void (*)(int) main_doIntervall) == SIG_ERR)
} /* ioLog_setBaseClockTimer */


/**************************************************************************//**
 *
 *	\brief		ioLog_gpioInit
 *
 *	\details	initialisiert die GPIO Schnittstellen.
 *
 *	\param		-
 *
 *	\return		Anzahl Fehler
 *
 ******************************************************************************/
static int ioLog_gpioInit (void)
{
	int	retValue = 0;
	int	i;


	fprintf(stderr, "ioLog_gpioInit: gpiod_chip_open - ");

	ioa.pChip = gpiod_chip_open ("/dev/gpiochip0");

	if (!ioa.pChip)
	{
		fprintf(stderr, "failed\n");
		retValue++;
	}
	else
	{
		fprintf(stderr, "ok\n");

		// Alle Relaisspulen Ausgänge initialisieren
		for (i = 0; i < C_do_channelCount; i++)
		{
			ioa.DO [i].pHandle = gpiod_chip_get_line (ioa.pChip, C_ioLog_gpioList [i]);

			fprintf(stderr, "ioLog_gpioInit: ioa.DO [%d].pHandle: %p\n", i, ioa.DO [i].pHandle);

			if (ioa.DO [i].pHandle == NULL)
			{
				fprintf(stderr, "ioLog_gpioInit: gpiod_chip_get_line %d failed\n", C_ioLog_gpioList [i]);
				retValue++;
			}
			else
			{
				if (gpiod_line_request_output (ioa.DO [i].pHandle, C_ioLog_appName, 0) != 0)
				{
					fprintf(stderr, "ioLog_gpioInit: gpiod_line_request_output %d failed\n", C_ioLog_gpioList [i]);
					retValue++;
				} // if (gpiod_line_request_output (ioa.DO [i].pHandle, "relais test", 0) != 0)
			} // else if (ioa.DO [i].pHandle == NULL)
		} // for (i = 0; i < C_do_channelCount; i++)

		#ifdef __do_hwWithLockFeedback
			// Feedback Relaisspulenspeisung initialisieren
			ioa.pHandleDoLockFB = gpiod_chip_get_line (ioa.pChip, C_do_lockFeedback);

			fprintf(stderr, "ioLog_gpioInit: pHandleDoLockFB: %p\n", ioa.pHandleDoLockFB);

			if (ioa.pHandleDoLockFB == NULL)
			{
				fprintf(stderr, "ioLog_gpioInit: gpiod_chip_get_line %d failed\n", C_do_lockFeedback);
				retValue++;
			}
			else
			{
				if (gpiod_line_request_input (ioa.pHandleDoLockFB, C_ioLog_appName) != 0)
				{
					fprintf(stderr, "ioLog_gpioInit: gpiod_line_request_input %d failed\n", C_do_lockFeedback);
					retValue++;
				}
				else
				{
					// do_lock (pHandleDoLockFB);
				} // else if (gpiod_line_request_output (pHandleDoLock, C_ioLog_appName, 0) != 0)
			} // else if (pHandleDoLockFB == NULL)
		#endif // __do_hwWithLockFeedback

		// Freigabe Relaisspulenspeisung initialisieren
		ioa.pHandleDoLock = gpiod_chip_get_line (ioa.pChip, C_do_lock);

		fprintf(stderr, "ioLog_gpioInit: pHandleDoLock: %p\n", ioa.pHandleDoLock);

		if (ioa.pHandleDoLock == NULL)
		{
			fprintf(stderr, "ioLog_gpioInit: gpiod_chip_get_line %d failed\n", C_do_lock);
			retValue++;
		}
		else
		{
			if (gpiod_line_request_output (ioa.pHandleDoLock, C_ioLog_appName, 0) != 0)
			{
				fprintf(stderr, "ioLog_gpioInit: gpiod_line_request_output %d failed\n", C_do_lock);
				retValue++;
			}
			else
			{
				do_unlock (ioa.pHandleDoLock, ioa.pHandleDoLockFB);
			} // else if (gpiod_line_request_output (pHandleDoLock, C_ioLog_appName, 0) != 0)
		} // else if (pHandleDoLock == NULL)
	} // else if (!ioa.pChip)

	return (retValue);
} /* ioLog_gpioInit */


/**************************************************************************//**
 *
 *	\brief		ioLog_i2cInit
 *
 *	\details	initialisiert die i2c Schnittstelle.
 *
 *	\param		-
 *
 *	\return		errno
 *
 ******************************************************************************/
static int ioLog_i2cInit (void)
{
	unsigned long				functions;
	int							retValue = 0;
	tu_ads7924_modeControl		modeControl;
	tu_ads7924_acquireConfig	acquireConfig;


	ioa.i2cDeviceDescriptor = open ("/dev/i2c-1", O_RDWR);

	if (ioa.i2cDeviceDescriptor < 0)
	{
		retValue = errno;
	}
	else
	{
		// sind die I2C Funktionen vorhanden?
		if (ioctl (ioa.i2cDeviceDescriptor, I2C_FUNCS, &functions) == 0)
		{
			fprintf(stderr, "ioLog_i2cInit: available i2c functions:\n");

			if (functions & I2C_FUNC_I2C)
			{
				fprintf(stderr, "\t- I2C\n");
			}

			if (functions & (I2C_FUNC_SMBUS_BYTE))
			{
				fprintf(stderr, "\t- I2C_FUNC_SMBUS_BYTE\n");
			}

			// ADC ADS7924 konfigurieren
			modeControl.mc_channel	= E_ads7924_channel0;
			modeControl.mc_mode		= E_ads7924_mode_autoScan;

			if (ads7924_setModeCntrl (ioa.i2cDeviceDescriptor, C_ads7924_addressA0lo, &modeControl) < 0)
			{
				fprintf(stderr, "ioLog_i2cInit: ADS7924 modeconfig failed\n");
			}

			acquireConfig.acnf_acquireTime = 31;		// 31 x 2 + 6 = 68 us

			if (ads7924_setAcquireConfig (ioa.i2cDeviceDescriptor, C_ads7924_addressA0lo, &acquireConfig) < 0)
			{
				fprintf(stderr, "ioLog_i2cInit: ADS7924 acquireconfig failed\n");
			}

			// Grundkonfiguration für ADS1115 setzen
			ads1115config.cnf_comparatorQueue		= E_ads1115_cq_disableComparator;
			ads1115config.cnf_comparatorLatching	= 0;
			ads1115config.cnf_comparatorPolarity	= 0;
			ads1115config.cnf_comparatorMode		= 0;
			ads1115config.cnf_dataRate				= E_ads1115_dr_128sps;
			ads1115config.cnf_operatingMode			= 1;
			ads1115config.cnf_progGainAmp			= E_ads1115_pga_2_048V;
			ads1115config.cnf_operationalStatus		= 1;
		}
		else
		{
			retValue = errno;
		} // else if (ioctl (ioa.i2cDeviceDescriptor, I2C_FUNCS, &functions) == 0)
	} // else if (ioa.i2cDeviceDescriptor < 0)

	return (retValue);
} /* ioLog_i2cInit */


/**************************************************************************//**
 *
 *	\brief		ioLog_close
 *
 *	\details	schliesst alle offenen Verbindungen und Peripherie Handler.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *
 *	\return		-
 *
 ******************************************************************************/
static void ioLog_close (ts_db_connect *pConnect)
{
	close (ioa.i2cDeviceDescriptor);
	gpiod_chip_close (ioa.pChip);
} /* ioLog_close */


/**************************************************************************//**
 *
 *	\brief		ioLog_sigtermHandler
 *
 *	\details	-
 *
 *	\param		signum		Signal Nummer
 *	\param		*pInfo		Pointer auf siginfo_t Struktur
 *	\param		*ptr		Ponter
 *
 *	\attention	Programm läuft bei SIGINT anschliessend weiter
 *
 *	\return		-
 *
 ******************************************************************************/
 void ioLog_sigtermHandler (int			signum,
							siginfo_t	*pInfo,
							void		*ptr)
 {
	fprintf (stderr, "ioLog_sigtermHandler: called, signum: %d\n", signum);

	do_lock (ioa.pHandleDoLock, ioa.pHandleDoLockFB);

//	close (ioa.i2cDeviceDescriptor);
//	gpiod_chip_close (ioa.pChip);

	ioa.ctrlC = 1;
 } /* ioLog_sigtermHandler */


/**************************************************************************//**
 *
 *	\brief		ioLog_sigtermCatcher
 *
 *	\details	-
 *
 *	\param		-
 *	\return		-
 *
 ******************************************************************************/
 void ioLog_sigtermCatcher (void)
 {
	static struct sigaction _sigaction;

	memset (&_sigaction, 0, sizeof (_sigaction));

	_sigaction.sa_sigaction	= ioLog_sigtermHandler;
	_sigaction.sa_flags		= SA_SIGINFO;

	sigaction (SIGINT, &_sigaction, NULL);
 } /* ioLog_sigtermCatcher */


/**************************************************************************//**
 *
 *	\brief		ioLog_configOverride
 *
 *	\details	überschreibt die Konfiguration aus der Datenbank mit den Werten
 *				im Code.
 *
 *	\param		-
 *	\return		-
 *
 ******************************************************************************/
static void ioLog_configOverride (void)
{
	int	i;

	// Datenbankeinstellung für Tests überschreiben
	// ioa.interval = C_ioa_intervall_100ms;
	ai_configOverride (&ioa.AI [0], 0);
	ai_configDebug (&ioa.AI [0], 0);
	ai_configOverride (&ioa.AI [1], 1);
	ai_configDebug (&ioa.AI [1], 1);

	ao_configOverride (&ioa.AO [0], 0);
	ao_configDebug (&ioa.AO [0], 0);
	ao_configOverride (&ioa.AO [1], 1);
	ao_configDebug (&ioa.AO [1], 1);

	for (i = 0; i < C_di_channelCount; i++)
	{
		di_configOverride (&ioa.DI [i], i);
		di_configDebug (&ioa.DI [i], i);
	} // for (i = 0; i < C_di_channelCount; i++)

	for (i = 0; i < C_do_channelCount; i++)
	{
		do_configOverride (&ioa.DO [i], i);
		do_configDebug (&ioa.DO [i], i);
	} // for (i = 0; i < C_do_channelCount; i++)

	for (i = C_va_counterMinNumber; i <= C_va_counterMaxNumber; i++)
	{
		va_configOverride (&ioa.VA [i - C_va_counterMinNumber], i);
		va_configDebug (&ioa.VA [i - C_va_counterMinNumber], i);
	} // for (i = 0; i <= C_va_counterCount; i++)
} /* ioLog_configOverride */


/**************************************************************************//**
 *
 *	\brief		ioLog_show
 *
 *	\details	schreibt die aktuellen I/O's auf stderr.
 *
 *	\param		-
 *	\return		-
 *
 ******************************************************************************/
static void ioLog_show (void)
{
	static int writeHeader = 0;
	int	i;


	if (writeHeader == 0)
	{
		// Header schreiben
		for (i = 0; i < C_ai_channelCount; i++)
		{
			fprintf (stderr, "ai%d[ ]\t", i);

			if (ioa.AI [i].unitVoltage)
			{
				fprintf (stderr, "ai%d[mV]\t", i);
			}
			else
			{
				fprintf (stderr, "ai%d[uA]\t", i);
			}
		} // for (i = 0; i < C_ai_channelCount; i++)

		for (i = 0; i < C_ao_channelCount; i++)
		{
			if (ioa.AO [i].unitVoltage)
			{
				fprintf (stderr, "ao%d[mV]\t", i);
			}
			else
			{
				fprintf (stderr, "ao%d[uA]\t", i);
			}

			fprintf (stderr, "ao%d[ ]\t", i);
		} // for (i = 0; i < C_ao_channelCount; i++)

		for (i = 0; i < C_di_channelCount; i++)
		{
			fprintf (stderr, "di%d[mV]\t", i);
		} // for (i = 0; i < C_di_channelCount; i++)

		for (i = 0; i < C_do_channelCount; i++)
		{
			fprintf (stderr, "do%d[ ]\t", i);
		} // for (i = 0; i < C_do_channelCount; i++)

		fprintf (stderr, "loop[ms] ");
		fprintf (stderr, "min[ms] ");
		fprintf (stderr, "max[ms]\t");

		fprintf (stderr,  "\n");
	} // if (writeHeader == 0)

	writeHeader++;

	if (writeHeader >= 10)
	{
		writeHeader = 0;
	} // if (writeHeader >= 10)

	// Analogeingänge
	for (i = 0; i < C_ai_channelCount; i++)
	{
		fprintf (stderr, "%5d\t", ioa.AI [i].binaryMean);

		if (ioa.AI [i].unitVoltage)
		{
			fprintf (stderr, "%5d\t", ioa.AI [i].voltage);
		}
		else
		{
			fprintf (stderr, "%5d\t", ioa.AI [i].current);
		}
	} // for (i = 0; i < C_ai_channelCount; i++)

	// Analogausgänge
	for (i = 0; i < C_ao_channelCount; i++)
	{
		fprintf (stderr, "%5d\t", ioa.AO [i].meanValue);
		fprintf (stderr, "%5d\t", ioa.AO [i].binary);
	} // for (i = 0; i < C_ao_channelCount; i++)

	// Digitaleingänge
	for (i = 0; i < C_di_channelCount; i++)
	{
		fprintf (stderr, "%5d\t", ioa.DI [i].voltage);
	} // for (i = 0; i < C_di_channelCount; i++)

	// Digitalausgänge
	for (i = 0; i < C_do_channelCount; i++)
	{
		fprintf (stderr, "%5d\t", ioa.DO [i].coil);
	} // for (i = 0; i < C_do_channelCount; i++)

	// Main Loop Zeiten
	fprintf (stderr, "%5lld\t", ioa.loopTime / 1000000);
	fprintf (stderr, "%5lld\t", ioa.loopTimeMin / 1000000);
	fprintf (stderr, "%5lld\t", ioa.loopTimeMax / 1000000);
	
	fprintf (stderr,  "\n");
} /* ioLog_show */


/**************************************************************************//**
 *
 *	\brief		ioLog_tick1s
 *
 *	\details	erledigt die Aufgaben welche jede Sekunde anfallen.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *
 *	\return		mysql_errno
 *
 ******************************************************************************/
static void ioLog_tick1s (ts_db_connect *pConnect,
						  ts_mail		*pMail)
{
	int	retValue;


	if (csw_flag.hardwareTest)
	{
		ioLog_show ();
	}
	else
	{
		ioLog_sampleDebug ();
	} // else if (csw_flag.hardwareTest)

		// init Eventlist angefordert?
		if (db_flagGet (pConnect, E_db_flagInitEventlist))
		{
			fprintf (stderr, "ioLog_main: init eventlist pending\n");

			retValue = db_tableDelete (pConnect, E_db_tableTypeEV);
			retValue = db_tableCreate (pConnect, E_db_tableTypeEV);

			if (retValue == 0)
			{
				db_flagClear (pConnect, E_db_flagInitEventlist);
				fprintf (stderr, "ioLog_main: init eventlist done\n");
			}
			else
			{
				fprintf (stderr, "ioLog_main: create eventlist, err:%d\n", retValue);
			} // else if (retValue == 0)
		}
		else
		{
			// init Parameter angefordert?
			if (db_flagGet (pConnect, E_db_flagInitParameter))
			{
				fprintf (stderr, "ioLog_main: init parameter pending\n");

				retValue = db_tableDelete (pConnect, E_db_tableTypePA);
				retValue = db_tableDelete (pConnect, E_db_tableTypeTP);
				retValue = db_tableCreate (pConnect, E_db_tableTypePA);

				if (retValue == 0)
				{
					retValue = db_csvReadFile (pConnect, E_db_tableTypePA, C_db_csvPaDefaultFile);

					if (retValue == 0)
					{
						retValue = db_tableCreate (pConnect, E_db_tableTypeTP);

						if (retValue == 0)
						{
							retValue = db_csvReadFile (pConnect, E_db_tableTypeTP, C_db_csvTpDefaultFile);

							if (retValue == 0)
							{
								db_flagClear (pConnect, E_db_flagInitParameter);
								fprintf (stderr, "ioLog_main: init parameter done\n");
							}
							else
							{
								fprintf (stderr, "ioLog_main: db_csvReadFile TP, err:%s\n", strerror(retValue));
							} // else if (db_csvReadFile (pConnect, E_db_tableTypeTP, C_db_csvTpDefaultFile))
						} // if (db_tableDelete (&connect, E_db_tableTypeTP) == 0)
						else
						{
							fprintf (stderr, "ioLog_main: ceate parameter TP, err:%d\n", retValue);
						} // else if (retValue == 0)

						if (csw_flag.configOverride)
						{
							fprintf (stderr, "ioLog_main: flag config override is set\n");
						}
						else
						{
							// neue Konfiguration übernehmen
							fprintf (stderr, "ioLog_main: read new configuration\n");

							ioLog_readConfigAi (pConnect);
							ioLog_readConfigAo (pConnect);
							ioLog_readConfigDi (pConnect);
							ioLog_readConfigDo (pConnect);
							ioLog_readConfigVa (pConnect);
						} // else if (csw_flag.configOverride)
					}
					else
					{
						fprintf (stderr, "ioLog_main: db_csvReadFile PA, err:%s\n", strerror(retValue));
					} // else if (db_csvReadFile (pConnect, E_db_tableTypePA, C_db_csvPaDefaultFile))
				} // if (db_tableDelete (&connect, E_db_tableTypePA) == 0)
				else
				{
					fprintf (stderr, "ioLog_main: ceate parameter PA, err:%d\n", retValue);
				} // else if (retValue == 0)
			}
			else
			{
				// reboot System angefordert?
				if (db_flagGet (pConnect, E_db_flagReboot))
				{
					db_flagClear (pConnect, E_db_flagServiceMode);
					db_flagClear (pConnect, E_db_flagReboot);
					do_lock (ioa.pHandleDoLock, ioa.pHandleDoLockFB);

					system ("sudo shutdown -r now");
				} // if (db_flagGet (&connect, E_db_flagReboot)
			} // if (db_flagGet (&connect, E_db_flagInitParameter)
		} // else if (db_flagGet (&connect, E_db_flagInitEventlist)

		// aktueller Servicemode aus Datenbank lesen
		ioa.serviceMode = db_flagGet (pConnect, E_db_flagServiceMode);

		if (ioa.serviceMode)
		{
			if (ioa.lastMode == 0)
			{
				fprintf (stderr, "ioLog_main: enter service mode\n");
				ioa.lastMode = 1;

				ioLog_setPermissionFlagVa (pConnect);
			} // if (ioa.lastMode != ioa.lastMode)
		}
		else
		{
			if (ioa.lastMode)
			{
				fprintf (stderr, "ioLog_main: leave service mode\n");
				ioa.lastMode = 0;

				ioLog_setPermissionFlagVa (pConnect);
			} // if (ioa.lastMode)
		} // else if (ioa.serviceMode)

	// ioLog_scaledAi (&connect, &mail);
	ioLog_scaledAi (pConnect);
	ioLog_scaledAo (pConnect, pMail);

	db_vaUpdateTime (pConnect);
} /* ioLog_tick1s */


/**************************************************************************//**
 *
 *	\brief		ioLog_init
 *
 *	\details	initialisiert die I/O's gemäss Datenbank.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *
 *	\return		mysql_errno
 *
 ******************************************************************************/
static int ioLog_init (ts_db_connect *pConnect)
{
	int	retValue = db_connectInit (pConnect,
								   C_db_dataBaseName,
								   C_db_dataBaseHost,
								   C_db_dataBaseUser,
								   C_db_dataBasePassword);

	if (retValue == 0)
	{
		ioLog_readConfigAi (pConnect);
		ioLog_readConfigAo (pConnect);
		ioLog_readConfigDi (pConnect);
		ioLog_readConfigDo (pConnect);
		ioLog_readConfigVa (pConnect);

		retValue = ioLog_i2cInit ();

		if (retValue == 0)
		{
			retValue = ioLog_gpioInit ();

			if (retValue == 0)
			{
				ioa.serviceMode = 0;
				ioa.ctrlC		= 0;

				ioLog_sigtermCatcher ();
			} // if (retValue == 0)
		}
		else
		{
			retValue = errno;
		} // else if (retValue == 0)
	} // if (retValue == 0)

	return (retValue);
} /* ioLog_init */


/******************************************************************************
 * Exported Functions                                                         *
 *                                                                            *
 ******************************************************************************/

/**************************************************************************//**
 *
 *	\brief		main
 *
 *	\details	Hauptprogramm für den I/O-Logger des techdock Projekts control4log.
 *				Folgende Aufgaben werden ausgeführt:
 *				- öffnen der Datenbank.
 *				- lesen der I/O Konfiguration aus der Datenbank.
 *				- Konfiguration der I/O's
 *				- Bedienung der I/O's in einer Endlosschlaufe.
 *
 *	\param		-
 *	\return		-
 *
 ******************************************************************************/
int main (int	argc,
		  char*	argv [])
{
	ts_db_connect	connect;
	ts_mail			mail;
	char			*pValue = '\0';
	int				option;
	int				i;
	int64_t			startTime;					//!< Nanosekunden Timestamp
	int64_t			endTime;					//!< Nanosekunden letzter Timestamp


	// immer auf Konsole ausgeben
	fprintf (stderr, "start %s version %s, as user %s\n", C_ioLog_appName, C_ioLog_appVersion, getlogin ());

	csw_flag.configOverride	= 0;
	csw_flag.hardwareTest	= 0;
	csw_flag.unitVoltage	= 1;
	csw_flag.verbose		= 0;
	csw_flag.simpleTest		= 0;

	// Paramater auswerten
	while ((option = getopt (argc, argv, "ot::vs")) != -1)
	{
		switch (option)
		{
			case 'o':
				csw_flag.configOverride = 1;
				break;

			case 't':
				pValue = optarg;
				csw_flag.configOverride = 1;
				csw_flag.hardwareTest = 1;
				csw_flag.verbose = 1;
				break;

			case 'v':
				csw_flag.verbose = 1;
				break;

			case 's':
				csw_flag.configOverride = 1;
				csw_flag.hardwareTest = 1;
				csw_flag.verbose = 1;
				csw_flag.simpleTest = 1;
				break;

			default:
				break;
		} // switch (option)
	} // while ((option = getopt (argc, argv, "ov")) != -1)

	if (pValue != NULL)
	{
		csw_flag.unitVoltage = strcmp (pValue, "c");
	}

	fprintf (stderr, "-o flag config override   : %d\n", csw_flag.configOverride);
	fprintf (stderr, "-s flag simple test       : %d\n", csw_flag.simpleTest);

	if (csw_flag.unitVoltage)
	{
		fprintf (stderr, "-t flag hardware test [mV]: %d, %s\n", csw_flag.hardwareTest, pValue);
	}
	else
	{
		fprintf (stderr, "-t flag hardware test [uA]: %d, %s\n", csw_flag.hardwareTest, pValue);
	} // else if (csw_flag.unitVoltage)

	fprintf (stderr, "-v flag verbose           : %d\n", csw_flag.verbose);

	if (csw_flag.verbose == 0)
	{
		freopen ("dev/null", "w", stderr);
	} // if (csw_flag.verbose == 0)

	if (csw_flag.simpleTest == 0)
	{
		if (ioLog_init (&connect) == 0)
		{
			mail_configRead (&connect, &mail);

			if (csw_flag.configOverride)
			{
				ioLog_configOverride ();
				mail_configOverride (&connect, &mail);
			} // if (csw_flag.configOverride)

			ioLog_setPermissionFlagVa (&connect);

			mail_configDebug (&mail);
			mail_configWriteToMsmtprc (&mail);
			mail_initSubject (&connect, &mail);

			// Limiten für digitale Eingangspegel setzen
			for (i = 0; i < C_di_channelCount; i++)
			{
				ads7924data.voltageOn [i]	= ioa.DI [i].limitLoHi;
				ads7924data.voltageOff [i]	= ioa.DI [i].limitHiLo;
			} // for (i = 0; i < C_di_channelCount; i++)

			ioLog_setBaseClockTimer ();

			ioa.loopTimeMin = LLONG_MAX;
			ioa.loopTimeMax = 0;

			while (!ioa.ctrlC)
			{
				sleep (1);	// wird von ioLog_sampleInterrupt geweckt

				clock_gettime (CLOCK_REALTIME, &ioa.hiResTime);
				startTime = C_ioa_MultiplierSecToNsec * (int64_t)(ioa.hiResTime.tv_sec) + (int64_t)(ioa.hiResTime.tv_nsec);

				if (ioa.tick100ms)
				{
					ioa.tick100ms = 0;

					// VIP Funktionen, sollten vor dem nächsten Interrupt bearbeitet sein
					// ioLog_processQuickAi (); Funktion ohne Inhalt
					// ioLog_processQuickDi (); Funktion ohne Inhalt
					ioLog_processQuickDo ();
					// ioLog_processQuickAo (); Funktion ohne Inhalt

					// aufwändige Funktionen, könnten vom nächsten Interrupt unterbrochen werden
					ioLog_processAi (&connect, &mail);
					ioLog_processDi (&connect, &mail);

					// im ServiceMode werden die Zustände der AO's und DO's aus der Datanbank gelesen
					ioLog_processDo (&connect, &mail);
					ioLog_processAo (&connect, &mail);

					// im ServiceMode werden die Zählerstände der VA's nicht bearbeitet -> doch
					ioLog_processVa (&connect, &mail);
				} // if (ioa.tick100ms)


				if (ioa.tick1s)
				{
					ioa.tick1s = 0;

					ioLog_tick1s (&connect, &mail);
				} // if (ioa.tick1s)


				if (ioa.tick1m)
				{
					ioa.tick1m = 0;

					ioLog_processDiCounter (&connect, &mail);

					#ifdef __db_accelerateTableCut
						db_datalistsCut (&connect);
					#endif
				} // if (ioa.tick1m)


				if (ioa.tick1h)
				{
					ioa.tick1h = 0;

					db_datalistsCut (&connect);
				} // if (ioa.tick1h)

				// Messung der Mainloop Auslastung
				clock_gettime (CLOCK_REALTIME, &ioa.hiResTime);
				endTime = C_ioa_MultiplierSecToNsec * (int64_t)(ioa.hiResTime.tv_sec) + (int64_t)(ioa.hiResTime.tv_nsec);

				ioa.loopTime = endTime - startTime;

				if (ioa.loopTime > ioa.loopTimeMax)
				{
					ioa.loopTimeMax = ioa.loopTime;
				}

				if (ioa.loopTime < ioa.loopTimeMin)
				{
					ioa.loopTimeMin = ioa.loopTime;
				}
			} // while (1)

			ioLog_close (&connect);
			mysql_library_end ();
		} // if (ioLog_init (&connect) == 0)
	} // if (csw_flag.simpleTest == 0)
	else
	{
		if (ioLog_i2cInit () == 0)
		{
			if (ioLog_gpioInit () == 0)
			{
				ioLog_configOverride ();
				ioLog_sigtermCatcher ();
				ioa.ctrlC = 0;

				// Limiten für digitale Eingangspegel setzen
				for (i = 0; i < C_di_channelCount; i++)
				{
					ads7924data.voltageOn [i]	= ioa.DI [i].limitLoHi;
					ads7924data.voltageOff [i]	= ioa.DI [i].limitHiLo;
				} // for (i = 0; i < C_di_channelCount; i++)

				ioLog_setBaseClockTimer ();

				while (!ioa.ctrlC)
				{
					sleep (1);	// wird von ioLog_sampleInterrupt geweckt

					if (ioa.tick1s)
					{
						ioa.tick1s = 0;

						for (i = 0; i < C_ai_channelCount; i++)
						{
							ioa.AI[i].binaryMean = ioa.AI[i].binary;

							if (ioa.AI[i].unitVoltage)
							{
								// Mittelwert in Spannung [mV] umrechnen
								ioa.AI[i].voltage = ai_getVoltage (ioa.AI[i].binaryMean);
								ioa.AI[i].current = 0;
								ioa.AI[i].meanValue = (int32_t)ioa.AI[i].voltage;
							}
							else
							{
								// Mittelwert in Strom [uA] umrechnen
								ioa.AI[i].current = ai_getCurrent (ioa.AI[i].binaryMean);
								ioa.AI[i].voltage = 0;
								ioa.AI[i].meanValue = (int32_t)ioa.AI[i].current;
							} // else if (pAi->unitVoltage)

							// Analogeingang auf Analogausgang kopieren
							ioa.AO[i].meanValue = ioa.AI[i].meanValue;
							ioa.AO[i].binary = ao_getBinary (ioa.AO[i].meanValue, ioa.AO[i].unitVoltage);
						} // for (i = 0; i < C_ai_channelCount; i++)

						ioLog_show ();
					} // if (ioa.tick1s)
				} // while (!ioa.ctrlC)
			} // if (ioLog_i2cInit () == 0)
		} // if (ioLog_i2cInit () == 0)
	} // else if (csw_flag.simpleTest == 0)


	fprintf (stderr, "end %s\n", C_ioLog_appName);

	return (0);
} /* main */

/* ioLog.c */

