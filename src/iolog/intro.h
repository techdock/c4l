/**************************************************************************//**
 *
 *  \mainpage   Projektbeschreibung
 *
 *  \section    Einleitung
 *  \subsection Allgemein
 *				Das Gerät control4log ist ein Datenlogger und bietet die Möglichkeiten,
 *				das Gerät mit TCP/IP oder USB-Stick zu konfigurieren, mit TCP/IP oder
 *				USB-Stick gespeicherte Werte auszulesen, mit TCP/IP aktuelle Werte
 *				abzufragen, digitale und analoge Grenzwerte per E-Mail zu alarmieren
 *				und USB-Anschlüsse über das Internet zu vermitteln.
 *
 *              <img src="../../03-Dokumentation/pictures/Control4logKomplett.png" alt="Gerät komplett">
 *
 *  \section    Software
 *  \subsection Systemübersicht
 *				Das Gerät verfügt über folgende Schnittstellen:
 *				- Analoge Ein- und Ausgänge
 *				- 2 AI mit einem Arbeitsbereich von 0 – 20 mA oder 0-10 V, frei skalierbar über Parameter.
 *				- 2 AO mit einem Arbeitsbereich von 0 – 20 mA oder 0-10 V, frei skalierbar über Parameter.
 *				- 4 DI mit einem Arbeitsbereich von 0 – 24 VDC, frei skalierbar über Parameter.
 *				- 4 DO mit einem Arbeitsbereich von 0 – 24 VDC, frei skalierbar über Parameter.
 *				- 2 USB 2.0
 *				- 2 USB 3.0
 *				- 3 ETH
 *				- 2 micro-HDMI 2.0
 *				<br>
 *				<br>
 *
 *              <img src="../../03-Dokumentation/pictures/Systemübersicht.png" alt="Systemübersicht">
 *
 *				<br>
 *				<br>
 *				<br>
 *
 *  \subsection Kontextdiagramm
 *				Die Gerätefunktion ist in einzelne Applikationen aufgeteilt, welche vom Betriebssystem
 *				gestartet oder ausgeführt werden wenn sie benötigt werden.
 *				Es sind folgende Applikationen vorhanden:
 *				- mySQL Datenbank (Datenbank Standardsoftware)
 *				- mailx (Mail Standardsoftware)
 *				- ioLog (I/O Handler und Logger)
 *				- tcpss (TCP Socket Server)
 *				- dbcreate (erstellt und initialisiert alle Datenbanktabellen)
 *				- configeths (konfiguriert die Ethernetschnittstellen gemäss Parameterliste)
 *				- configntp (konfiguriert das Network Time Protokol gemäss Parameterliste)
 *				- usbhello (führt Aktionen bei einem USB Plug Ereignis aus)
 *				- remail (wiederholt missglückte Mailsendungen)
 *				<br>
 *				<br>
 *
 *              <img src="../../03-Dokumentation/pictures/KontextDiagramm.png" alt="Kontext Diagramm">
 *
 *				<br>
 *				<br>
 *				<br>
 *
 *  \subsection Analog-Eingang-Manager
 *				Der Analog-Eingang-Manager behandelt die Zugriffe auf die AI Tabelle.
 *				Diese Tabelle enthält die Rohwerte der analogen Eingänge.
 *				<br>
 *				<br>
 *
 *              <img src="../../03-Dokumentation/pictures/AI-Modul.png" alt="AI-Manager">
 *
 *				<br>
 *				<br>
 *				<br>
 *
 *  \subsection Analog-Ausgang-Manager
 *				Der Analog-Ausgang-Manager behandelt die Zugriffe auf die AO Tabelle.
 *				Diese Tabelle enthält die Ausgangswerte der analogen Ausgänge.
 *				<br>
 *				<br>
 *
 *              <img src="../../03-Dokumentation/pictures/AO-Modul.png" alt="AO-Manager">
 *
 *				<br>
 *				<br>
 *				<br>
 *
 *  \subsection Digital-Eingang-Manager
 *				Der Digital-Eingang-Manager behandelt die Zugriffe auf die DI Tabelle.
 *				Diese Tabelle enthält die gemäss Parameterliste bearbeiteten Werte der
 *				digitalen Eingänge.
 *				<br>
 *				<br>
 *
 *              <img src="../../03-Dokumentation/pictures/DI-Modul.png" alt="DI-Manager">
 *
 *				<br>
 *				<br>
 *				<br>
 *
 *  \subsection Digital-Ausgang-Manager
 *				Der Digital-Ausgang-Manager behandelt die Zugriffe auf die DO Tabelle.
 *				Diese Tabelle enthält die Werte der digitalen Ausgänge.
 *				<br>
 *				<br>
 *
 *              <img src="../../03-Dokumentation/pictures/DO-Modul.png" alt="DO-Manager">
 *
 *				<br>
 *				<br>
 *				<br>
 *
 *  \subsection Datenlisten-Manager
 *				Der Datenlisten-Manager behandelt die Zugriffe auf die verschiedenen DL Tabellen.
 *				Diese Tabellen enthalten alle Werte der analogen und digitalen Ein- und Ausgänge,
 *				sowie alle Variablen Tabellen. Jeder Eintrag wird dabei mit Zeit- und Datumsangabe
 *				versehen.
 *				<br>
 *				<br>
 *
 *              <img src="../../03-Dokumentation/pictures/DL-Modul.png" alt="DL-Manager">
 *
 *				<br>
 *				<br>
 *				<br>
 *
 *  \subsection Event-Manager
 *				Der Event-Manager behandelt die Zugriffe auf die EV Tabelle.
 *				<br>
 *				<br>
 *
 *              <img src="../../03-Dokumentation/pictures/EV-Modul.png" alt="EV-Manager">
 *
 *				<br>
 *				<br>
 *				<br>
 *
 *  \subsection Function-Code-Manager
 *				Der Function-Code-Manager behandelt die Function Codes welche über die TCP
 *				Schnittstelle eintreffen.
 *				<br>
 *				<br>
 *
 *              <img src="../../03-Dokumentation/pictures/FC-Modul.png" alt="FC-Manager">
 *
 *				<br>
 *				<br>
 *				<br>
 *
 *  \subsection Parameter-Manager
 *				Der Parametermanager behandelt die Zugriffe auf die PA Tabelle.
 *				<br>
 *				<br>
 *
 *              <img src="../../03-Dokumentation/pictures/PA-Modul.png" alt="PA-Manager">
 *
 *				<br>
 *				<br>
 *				<br>
 *
 *  \subsection System-Clock-Manager
 *				Der System-Clock-Manager behandelt die Zugriffe auf die System Clock.
 *				<br>
 *				<br>
 *
 *              <img src="../../03-Dokumentation/pictures/SC-Modul.png" alt="SC-Manager">
 *
 *				<br>
 *				<br>
 *				<br>
 *
 *  \subsection Text-Parameter-Manager
 *				Der Text-Parameter-Manager behandelt die Zugriffe auf die TP Tabelle.
 *				<br>
 *				<br>
 *
 *              <img src="../../03-Dokumentation/pictures/TP-Modul.png" alt="TP-Manager">
 *
 *				<br>
 *				<br>
 *				<br>
 *
 *  \subsection Variablen-Manager
 *				Der Variablen-Manager behandelt die Zugriffe auf die VA Tabelle.
 *				<br>
 *				<br>
 *
 *              <img src="../../03-Dokumentation/pictures/VA-Modul.png" alt="VA-Manager">
 *
 *				<br>
 *				<br>
 *				<br>
 *
 *  \tableofcontents
 *
 */
