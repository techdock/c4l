/**************************************************************************//**
 *
 *	\file		ao.h
 *
 *	\brief		Headerfile der Funktionen für die analogen Eingänge.
 *
 *	\details	-
 *
 *	\date		09.03.2021
 *	\author		Marcel Ming
 *	\copyright	TRiMADA AG   CH-5610 Wohlen
 *	\remark		Compiler- / IDE-Version
 *
 *	\version	09.03.21 mm
 *					- Erstellt.
 *	\version	08.04.21 mm
 *					- Anpassungen an Pflichtenheft Revision 4.
 *	\version	29.04.21 mm
 *					- Tabellen Basis- und Offsetdefinitionen in die entsprechenden
 *					  dbXY verschoben.
 *	\version	05.08.21 mm
 *					- ao_configOverride definiert.
 *	\version	09.08.21 mm
 *					- ao_getBinary, ao_getCurrent und ao_getVoltage definiert.
 *	\version	01.09.21 mm
 *					- ao_processQuick definiert.
 *	\version	06.09.21 mm
 *					- Compilerschalter __ao_showDebugStrings definiert.
 *	\version	30.11.21 mm
 *					- in enum te_ao_depency E_ao_depFollowOff wieder definiert.
 *	\version	23.12.21 mm
 *					- in der Struktur ts_ao den Datentyp der Variablen valueTooLo,
 *					  valueLo, valueHi und valueTooHi von uint16_t auf int32_t
 *					  gesetzt. Die Spezifikation wurde in der Version 3 des
 *					  Pflichtenheftes geändert, bis dahin war der Wertebereich
 *					  mit 0 .. 1000 spezifiziert.
 *					- Variablen für PI Regler als signed definiert.
 *	\version	22.09.23 mm
 *					- in Funktion ao_configOverride den nicht benötigten Parameter
 * 					  *pConnect gelöscht.
 *
 *
 ******************************************************************************/
#ifndef __ao_H
    #define __ao_H


/******************************************************************************
 * Compilerswitches                                                           *
 *                                                                            *
 ******************************************************************************/
//	#define __ao_showDebugStrings


/******************************************************************************
 * Modules                                                                    *
 *                                                                            *
 ******************************************************************************/
#include <stdint.h>

#include "db.h"
#include "mail.h"


/******************************************************************************
 * Definitions and Macros                                                     *
 *                                                                            *
 ******************************************************************************/
#define	C_ao_channelCount			2
#define	C_ao_maxAverageSampleCount	600


/******************************************************************************
 * Typedefs and Structures                                                    *
 *                                                                            *
 ******************************************************************************/
typedef enum {
	E_ao_fktOff,			//!< ausgeschaltet 
	E_ao_fktOn,				//!< Ausgang aktiviert als AO
	E_ao_fktPi,				//!< Ausgang aktiviert als PI-Regler
	E_ao_fktCount			//!< Anzahl Funktionen
} te_ao_function;


typedef enum {
	E_ao_depFollowOff,		//!< folgen ausgeschaltet
	E_ao_depFollowDI1,		//!< DI 1 folgen
	E_ao_depFollowDI2,		//!< DI 2 folgen
	E_ao_depFollowDI3,		//!< DI 3 folgen
	E_ao_depFollowDI4,		//!< DI 4 folgen
	E_ao_depFollowAI1,		//!< AI 1 folgen
	E_ao_depFollowAI2,		//!< AI 2 folgen
} te_ao_depency;


typedef enum {
	E_ao_piWundef,			//!< Führungsgrösse undefiniert
	E_ao_piWAI1,			//!< Führungsgrösse Analogeingang 1
	E_ao_piWAI2,			//!< Führungsgrösse Analogeingang 2
} te_ao_piWsource;


typedef enum {
	E_ao_opUndef,
	E_ao_opEqual,			//!< == 
	E_ao_opNotEqual,		//!< !=
	E_ao_opGreater,			//!< >
	E_ao_opLess,			//!< <
	E_ao_opGreaterOrEqual,	//!< >=
	E_ao_opLessOrEqual,		//!< <=
} te_ao_operator;


typedef struct {
	int16_t		data [C_ao_maxAverageSampleCount];	//!< Datenarray für Mittelwertbildung
	int64_t		sum;								//!< Summe aller Datenpunkte für Mittelwertbildung
	int			posNewest;							//!< Position des neusten Datenpunktes
	int			posOldest;							//!< Position des ältesten Datenpunktes
	int			posCount;							//!< Anzahl gültiger Datenpunkte
	int			posMax;								//!< Anzahl Datenpunkte
} ts_ao_mean;


typedef struct {
	// Datenbank Werte
	te_ao_function	function;				//!< Ausgangsfunktion
	uint8_t			enableScaledLog:1;		//!< 1: Aufzeichnung skaliert aktiviert
	int16_t			limitLo;				//!< Pegel tief [mV/uA], z.B. 4 mA
	int16_t			limitHi;				//!< Pegel hoch [mV/uA], z.B. 20 mA
	uint16_t		averageTime;			//!< Zahl entspricht der Zeit in welcher die Samples gesammelt werden [0.1s]
	int32_t			meanValue;				//!< Mittelwert (Spannung oder Strom zum weiterrechnen)
	int32_t			scaleElements;			//!< (limitHi - limitLo) wird auf n Elemente abgebildet, z.B. [°C]
	int32_t			scaleOffset;			//!< scaleElements - scaleOffset, z.B. [°C]
	int32_t			storageIntervall;		//!< Taktzeit zum Speichern des aktuellen Wertes [s]
	te_ao_depency	depency;				//!< Abhängigkeit von ...
	uint16_t		msgTooLo;				//!< 0: off, >0 Textnummer aus TP
	te_ao_operator	msgOperatorTooLo;		//!< Operator für Text zu tief in SMTP
	int32_t			valueTooLo;				//!< Wert für zu tief
	uint16_t		msgLo;					//!< 0: off, >0 Textnummer aus TP
	te_ao_operator	msgOperatorLo;			//!< Operator für Text tief in SMTP
	int32_t			valueLo;				//!< Wert für tief
	uint16_t		msgHi;					//!< 0: off, >0 Textnummer aus TP
	te_ao_operator	msgOperatorHi;			//!< Operator für Text hoch in SMTP
	int32_t			valueHi;				//!< Wert für hoch
	uint16_t		msgTooHi;				//!< 0: off, >0 Textnummer aus TP
	te_ao_operator	msgOperatorTooHi;		//!< Operator für Text zu hoch in SMTP
	int32_t			valueTooHi;				//!< Wert für zu hoch
	uint32_t		piTr;					//!< Regelzeitintervall Tr [0.01s]
	te_ao_piWsource	piWsource;				//!< Quelle für Führungsgrösse W
	int32_t			piFp;					//!< Proportionalitätsfaktor Fp
	int32_t			piFi;					//!< Integralfaktor Fi
	uint8_t			piInvert;				//!< Wirkungsrichtung 0: normal, 1: invertiert
	uint16_t		piOutputMin;			//!< Minimalwert Ausgangsgrösse 0.00 .. 100.00 [%]
	uint16_t		piOutputMax;			//!< Maximalwert Ausgangsgrösse 0.00 .. 100.00 [%]
	uint32_t		piTimeout;				//!< Timeout für Nichterreichen Regelband 0 .. 360000 [0.01s]
	uint16_t		piOutputDefault;		//!< Ausgabewert im Stillstand 0.00 .. 100.00 [%]
	uint16_t		piOutputRange;			//!< Zulässige Regelbandbreite 0.00 .. 100.00 [%]
	uint16_t		piMsgMin;				//!< 0: off, >0 Textnummer aus TP
	te_ao_operator	piMsgOperatorMin;		//!< Operator für Text Minimum in SMTP
	int32_t			piValueMin;				//!< Wert für Minimalwert
	uint16_t		piMsgMax;				//!< 0: off, >0 Textnummer aus TP
	te_ao_operator	piMsgOperatorMax;		//!< Operator für Text Maximum in SMTP
	int32_t			piValueMax;				//!< Wert für Maximalwert
	char			scaleUnit [C_db_dataStringSize];	//!< Einheit zum DP Register skaliert

	// interne Werte
	te_ao_function	lastFunction;				//!< letzte Ausgangsfunktion
	int16_t			binary;						//!< Ausgang als Binärwert (DAC Wert)
	uint16_t		counter100ms;				//!< 100 ms Zähler für Sample Zeit
	int32_t			scaledValue;				//!< skalierter Wert
	int32_t			storageIntervallCounter;	//!< Zähler Taktzeit zum Speichern des aktuellen Wertes
	int32_t			piTimeoutCounter;			//!< Zähler Timeout für Nichterreichen Regelband
	int32_t			piFi_x_piTr;				//!< Integralfaktor Fi x Regelzeitintervall Tr
	int32_t			piOutputRangeLo;			//!< unterer Wert Regelbandbreite nachgeführt auf Ai
	int32_t			piOutputRangeHi;			//!< oberer Wert Regelbandbreite nachgeführt auf Ai
	ts_ao_mean		mean;						//!< Struktur für Mittelwertbildung
	uint			unitVoltage			: 1;	//!< 0: Strommessung, 1: Spannungsmessung
	uint			valueTooLoActive	: 1;	//!< Wert als zu tief erkannt
	uint			valueLoActive		: 1;	//!< Wert als tief erkannt
	uint			valueHiActive		: 1;	//!< Wert als hoch erkannt
	uint			valueTooHiActive	: 1;	//!< Wert als zu hoch erkannt
	uint			piOutputMinActive	: 1;	//!< Minimalwert Ausgangsgrösse erkannt
	uint			piOutputMaxActive	: 1;	//!< Maximalwert Ausgangsgrösse erkannt
	uint			piOutRangeMinActive	: 1;	//!< Minimalwert Regelbereich erkannt
	uint			piOutRangeMaxActive	: 1;	//!< Maximalwert Regelbereich erkannt
} ts_ao;


/******************************************************************************
 * Exported Variables                                                         *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Exported Functions                                                         *
 *                                                                            *
 ******************************************************************************/
void ao_configDebug (ts_ao	*pAo,
					 int	channel);

int ao_configRead (ts_db_connect	*pConnect,
                   ts_ao			*pAo,
                   int				channel);

void ao_configOverride (ts_ao	*pAo,
						int		channel);

int16_t ao_getBinary (int16_t value,
					  uint	  unit);
int16_t ao_getCurrent (int16_t binary);
int16_t ao_getVoltage (int16_t binary);

void ao_processQuick (ts_ao	*pAo,
					  int	channel);

int ao_process (ts_db_connect	*pConnect,
				void			*pVoid,
				int				channel,
				ts_mail			*pMail);

void ao_logScaled (ts_db_connect *pConnect,
				   ts_ao		 *pAo,
				   int			 channel);


#endif //__ao_H


/* End ao.h */
