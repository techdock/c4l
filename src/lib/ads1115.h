/**************************************************************************//**
 *
 *	\file		ads1115.h
 *
 *	\brief		Headerfiles für den ads1115 Zugriff.
 *
 *	\details	Der ads1115 ist ein ADC mit 16 Bit Auflösung, 4 Kanälen und einer
 *				i2c Schnittstelle.
 *
 *	\date		22.01.2021
 *	\author		Marcel Ming
 *	\copyright	TRiMADA AG   CH-5610 Wohlen
 *	\remark		Compiler- / IDE-Version
 *
 *	\version	22.01.21 mm
 *					- Erstellt.
 *
 ******************************************************************************/
#ifndef __ads1115_H
    #define __ads1115_H


/******************************************************************************
 * Modules                                                                    *
 *                                                                            *
 ******************************************************************************/
#include <stdbool.h>
#include <stdint.h>


/******************************************************************************
 * Definitions and Macros                                                     *
 *                                                                            *
 ******************************************************************************/
#define	C_ads1115_addressA0lo	0x48	//!< Adresse des ADC ads1115 wenn A0 = lo
#define	C_ads1115_addressA0hi	0x49	//!< Adresse des ADC ads1115 wenn A0 = hi
#define	C_ads1115_addressA0sda	0x4A	//!< Adresse des ADC ads1115 wenn A0 mit SDA verbunden
#define	C_ads1115_addressA0sdl	0x4B	//!< Adresse des ADC ads1115 wenn A0 mit SDL verbunden

#define	C_ads1115_resolution	32767	//!< Auflösung des ADC (16 Bit +- 32767)
#define	C_ads1115_maxVoltage	10000	//!< maximale Eingangsspannung [mV]
#define	C_ads1115_maxCurrent	20000	//!< maximaler Eingangsstrom [uA]

#define C_ads1115_configDelay	7500	//!< Verzögerung in us nach Konfiguration <7500 führt zu Fehler
										//!< so wird eine Zykluszeit von knapp über 20 ms erreicht.


/******************************************************************************
 * Typedefs and Structures                                                    *
 *                                                                            *
 ******************************************************************************/
typedef enum {
    E_ads1115_ra_conversion,
	E_ads1115_ra_config,
	E_ads1115_ra_loThreshold,
	E_ads1115_ra_hiThreshold,
	E_ads1115_ra_registerCount
} te_ads1115_registerAddress;

typedef enum {
    E_ads1115_cq_assertAftrer1conversion,
    E_ads1115_cq_assertAftrer2conversion,
    E_ads1115_cq_assertAftrer4conversion,
    E_ads1115_cq_disableComparator				//!< default
} te_ads1115_comparatorQueue;

typedef enum {
    E_ads1115_dr_8sps,
    E_ads1115_dr_16sps,
    E_ads1115_dr_32sps,
    E_ads1115_dr_64sps,
    E_ads1115_dr_128sps,	//!< default
    E_ads1115_dr_250sps,
    E_ads1115_dr_475sps,
    E_ads1115_dr_860sps,
} te_ads1115_dataRate;

typedef enum {
    E_ads1115_pga_6_144V,	//!< full scale +- 6.144V
    E_ads1115_pga_4_096V,	//!< full scale +- 4.096V
    E_ads1115_pga_2_048V,	//!< full scale +- 2.048V (default)
    E_ads1115_pga_1_024V,	//!< full scale +- 1.024V
    E_ads1115_pga_0_512V,	//!< full scale +- 0.512V
    E_ads1115_pga_0_256V,	//!< full scale +- 0.256V
} te_ads1115_progGainAmp;

typedef enum {
    E_ads1115_imx_Ain0_Ain1,	//!< (default)
    E_ads1115_imx_Ain0_Ain3,	//!<
    E_ads1115_imx_Ain1_Ain3,	//!<
    E_ads1115_imx_Ain2_Ain3,	//!<
    E_ads1115_imx_Ain0_GND,		//!<
    E_ads1115_imx_Ain1_GND,		//!<
    E_ads1115_imx_Ain2_GND,		//!<
    E_ads1115_imx_Ain3_GND,		//!<
} te_ads1115_inputMux;

typedef union {
	uint16_t	value;
	
	struct {
		te_ads1115_comparatorQueue	cnf_comparatorQueue		: 2;	//!< comparator queue and disable
		uint16_t					cnf_comparatorLatching	: 1;	//!< 0: non latching (default)
		uint16_t					cnf_comparatorPolarity	: 1;	//!< 0: active lo (default), 1: active hi 
		uint16_t					cnf_comparatorMode		: 1;	//!< 0: traditional (default), 1: windowed
		te_ads1115_dataRate			cnf_dataRate			: 3;	//!< data rate
		uint16_t					cnf_operatingMode		: 1;	//!< 0: continuos, 1: power down single shot (default)
		te_ads1115_progGainAmp		cnf_progGainAmp			: 3;	//!< programmable gain amplifier
		te_ads1115_inputMux			cnf_inputMux			: 3;	//!< input multiplexer
		uint16_t					cnf_operationalStatus	: 1;	//!< read: 0: busy, 1: ready
	};																//!< write: 0: no effect, 1: start conversion
} tu_ads1115_config;


/******************************************************************************
 * Exported Variables                                                         *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Exported Functions                                                         *
 *                                                                            *
 ******************************************************************************/
int	ads1115_getConversion (int deviceDescriptor, uint8_t chipAddr, int16_t *pAdcValue);
int	ads1115_setConfig (int deviceDescriptor, uint8_t chipAddr, tu_ads1115_config *pConfig);
int	ads1115_getConfig (int deviceDescriptor, uint8_t chipAddr, tu_ads1115_config *pConfig);
int	ads1115_setUpperThreshold (int deviceDescriptor, uint8_t chipAddr, uint16_t threshold);
int	ads1115_getUpperThreshold (int deviceDescriptor, uint8_t chipAddr, uint16_t *pThreshold);
int	ads1115_setLowerThreshold (int deviceDescriptor, uint8_t chipAddr, uint16_t threshold);
int	ads1115_getLowerThreshold (int deviceDescriptor, uint8_t chipAddr, uint16_t *pThreshold);

#endif //__ads1115_H

/* End ads1115.h */
