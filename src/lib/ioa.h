/**************************************************************************//**
 *
 *	\file		ioa.h
 *
 *	\brief		Headerfile der Funktionen für die I/O Arrays.
 *
 *	\details	in den I/O Arrays sind entsprechend der Hardware alle verfügbaren
 *				analogen und digitalen Ein- und Ausgänge zu Arrays zusammengefasst.
 *
 *	\date		14.04.2021
 *	\author		Marcel Ming
 *	\copyright	TRiMADA AG   CH-5610 Wohlen
 *	\remark		Compiler- / IDE-Version
 *
 *	\version	14.04.21 mm
 *					- Erstellt.
 *	\version	17.11.21 mm
 *					- Typ der Bitfelder in ts_ioa von int auf uint gesetzt.
 *	\version	27.09.23 mm
 *					- ts_ioa um ts_va erweitert.
 *	\version	17.10.23 mm
 *					- ts_ioa um loopTime, loopTimeMin und loopTimeMax erweitert.
 *
 *
 ******************************************************************************/
#ifndef __ioa_H
    #define __ioa_H


/******************************************************************************
 * Modules                                                                    *
 *                                                                            *
 ******************************************************************************/
#include <stdint.h>

#include "ai.h"
#include "ao.h"
#include "di.h"
#include "do.h"
#include "va.h"


/******************************************************************************
 * Definitions and Macros                                                     *
 *                                                                            *
 ******************************************************************************/
#define	C_ioa_interval_10ms			10000		// Mikrosekunden
#define	C_ioa_interval_100ms		100000		// Mikrosekunden
#define	C_ioa_interval_500ms		500000		// Mikrosekunden
#define	C_ioa_interval_999ms		999999		// Mikrosekunden
#define	C_ioa_interval_1s			1000000		// Mikrosekunden
#define	C_ioa_hiResDeltaMax			110000000	// Nanosekunden -> 110 ms -> 10% Toleranz
#define	C_ioa_MultiplierSecToNsec	1000000000	// Skunden -> Nanosekunden


/******************************************************************************
 * Typedefs and Structures                                                    *
 *                                                                            *
 ******************************************************************************/
#if	C_ai_channelCount == 2
	typedef enum {
		E_ioa_readCh2_configCh0,
		E_ioa_configCh0pause0,
		E_ioa_configCh0pause1,
		E_ioa_readCh0_configCh2,
		E_ioa_configCh1pause0,
		E_ioa_configCh1pause1,
	} te_ioa_adcSampleState;
#endif

#if	C_ai_channelCount == 4
	typedef enum {
		E_ioa_readCh3_configCh0,
		E_ioa_configCh0pause0,
		E_ioa_configCh0pause1,
		E_ioa_readCh0_configCh1,
		E_ioa_configCh1pause0,
		E_ioa_configCh1pause1,
		E_ioa_readCh1_configCh2,
		E_ioa_configCh2pause0,
		E_ioa_configCh2pause1,
		E_ioa_readCh2_configCh3,
		E_ioa_configCh3pause0,
		E_ioa_configCh3pause1,
	} te_ioa_adcSampleState;
#endif


typedef struct {
	ts_ai				AI [C_ai_channelCount];		//!< Parameter Array der analogen Eingänge
	ts_ao				AO [C_ao_channelCount];		//!< Parameter Array der analogen Ausgänge
	ts_di				DI [C_di_channelCount];		//!< Parameter Array der digitalen Eingänge
	ts_do				DO [C_do_channelCount];		//!< Parameter Array der digitalen Ausgänge
	ts_va				VA [C_va_counterCount];		//!< Parameter Array der Zähler
	struct timespec		hiResTime;					//!< Unix Timestamp
	struct gpiod_chip	*pChip;						//!< GPIO chip handle oder NULL bei Fehler
	struct gpiod_line	*pHandleDoLock;				//!< Freigabe Relaisspulenspeisung
	struct gpiod_line	*pHandleDoLockFB;			//!< Feedback Freigabe Relaisspulenspeisung
	int					i2cDeviceDescriptor;		//!< i2c Device Descriptor
	int64_t 			loopTime;					//!< Nanosekunden Differenz Timestamp
	int64_t 			loopTimeMin;				//!< Nanosekunden Differenz Timestamp Minimum
	int64_t 			loopTimeMax;				//!< Nanosekunden Differenz Timestamp Maximum
	int64_t				hiResTimeStamp;				//!< Nanosekunden Timestamp
	int64_t				hiResLastStamp;				//!< Nanosekunden letzter Timestamp
	int64_t				hiResDeltaStamp;			//!< Nanosekunden Differenz Timestamp
	int64_t				hiResDeltaErrCount;			//!< Errorcounter für Interrupt Zeitüberschreitungen
	int					interval;					//!< Basis Abtastrate in Mikrosekunden
	int					counter10ms;				//!< 10 ms Zähler
	int					counter100ms;				//!< 100 ms Zähler
	int					counter1s;					//!< 1 Sekunden Zähler
	int					counter1m;					//!< 1 Minuten Zähler
	int					counter1h;					//!< 1 Stunden Zähler
	uint				tick10ms	: 1;			//!< 10 ms Ticker
	uint				tick100ms	: 1;			//!< 100 ms Ticker
	uint				tick1s		: 1;			//!< 1 Sekunden Ticker
	uint				tick1m		: 1;			//!< 1 Minuten Ticker
	uint				tick1h		: 1;			//!< 1 Stunden Ticker
	uint				serviceMode	: 1;			//!< Flag Service Mode aktiv
	uint				lastMode	: 1;			//!< Flag letzter Service Mode
	uint				ctrlC		: 1;			//!< Ctrl C ist erkannt worden
} ts_ioa;


/******************************************************************************
 * Exported Variables                                                         *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Exported Functions                                                         *
 *                                                                            *
 ******************************************************************************/


#endif //__ioa_H

/* End ioa.h */
