/**************************************************************************//**
 *
 *	\file		ptkC4l.h
 *
 *	\brief		Headerfile der Funktionen für die free2sample Protokoll Funktionen.
 *
 *	\details	-
 *
 *	\date		18.03.2021
 *	\author		Marcel Ming
 *	\copyright	TRiMADA AG   CH-5610 Wohlen
 *	\remark		Compiler- / IDE-Version
 *
 *	\version	18.03.21 mm
 *					- Erstellt.
 *	\version	25.11.21 mm
 *					- Funktion ptkC4l_evaluateCmd um Parameter ethNumber erweitert.
 *					- C_ptkC4l_errMsgValueAccessDenied definiert.
 *	\version	29.11.21 mm
 *					- verlorene Definitionen von C_ptkC4l_errMsgxyz eingefügt.
 *	\version	06.12.21 mm
 *					- Funktion ptkC4l_replaceBSrToBS0 durch flexiblere Funktion
 *					  ptkC4l_replaceChar1withChar2 ersetzt.
 *
 *
 ******************************************************************************/
#ifndef __ptkC4l_H
    #define __ptkC4l_H


/******************************************************************************
 * Modules                                                                    *
 *                                                                            *
 ******************************************************************************/
#include <stdint.h>

#include "db.h"


/******************************************************************************
 * Definitions and Macros                                                     *
 *                                                                            *
 ******************************************************************************/
#define	C_ptkC4l_digitCount					4
#define C_ptkC4l_commandStrLength			64

#define	C_ptkC4l_PAmaxHeaderLenRead			7
#define	C_ptkC4l_PAmaxHeaderLenWrite		8

#define	C_ptkC4l_errMsgCmdInvalid			".error.cmd_invalid"
#define	C_ptkC4l_errMsgValueInvalid			".error.value_invalid"
#define	C_ptkC4l_errMsgValueProtected		".error.value_protected"
#define	C_ptkC4l_errMsgAccessDenied			".error.access_denied"
#define	C_ptkC4l_errMsgDbRead			 	".error.db_read"
#define	C_ptkC4l_errMsgDbWrite			 	".error.db_write"
#define	C_ptkC4l_errMsgUnknown			 	".error.unknown"


/******************************************************************************
 * Typedefs and Structures                                                    *
 *                                                                            *
 ******************************************************************************/
/*
typedef struct {
	char	**pCmdCode;		//!< Befehlscode
	int		n;				//!< numerischer Wert als ergänzung zum Befehlscode
	char	**pData;		//!< Daten (String)
	char	serviceMode;	//!< 0: ServiceMode aus
} ts_ptkC4l;
*/


/******************************************************************************
 * Exported Variables                                                         *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Exported Functions                                                         *
 *                                                                            *
 ******************************************************************************/
void ptkC4l_evaluateCmd (ts_db_connect	*pConnect,
                         char			*pCmdStr,
                         char			*pReplyStr,
                         int			ethNumber);

int ptkC4l_replaceChar1withChar2 (char *pCmdStr,
								  char char1,
								  char char2);


#endif //__ptkC4l_H

/* End ptkC4l.h */
