/**************************************************************************//**
 *
 *	\file		hwm.c
 *
 *	\brief		stellt Funktionen für den sysfs hwmon Zugriff zur Verfügung.
 *
 *	\details	-
 *
 *	\date		03.02.2021
 *	\author		Marcel Ming
 *	\copyright	TRiMADA AG   CH-5610 Wohlen
 *	\remark		Compiler- / IDE-Version
 *
 *	\version	03.02.21 mm
 *					- Erstellt.
 *
 ******************************************************************************/


/******************************************************************************
 * Compilerswitches                                                           *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Modules                                                                    *
 *                                                                            *
 ******************************************************************************/
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "hwm.h"


/******************************************************************************
 * Definitions and Macros                                                     *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Typedefs and Structures                                                    *
 *                                                                            *
 ******************************************************************************/
 
/******************************************************************************
 * Exported Variables                                                         *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Global Variables                                                           *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Local Functions                                                            *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Exported Functions                                                         *
 *                                                                            *
 ******************************************************************************/

/**************************************************************************//**
 *
 *  \brief		hwm_getTemperatureName
 *
 *  \details	gibt den Namen des Sensors zurück. 
 *
 *	\param		pFileName	Pointer auf sysfs Filename mit Pfad zum Temperatursensor
 * 	\param		pSensorName	Pointer auf Temperatursensorname
 *
 *	\return		0	Funktion erfolgreich ausgeführt
 * 				< 0 Funktion fehlgeschlagen
 *
 ******************************************************************************/
int	hwm_getTemperatureName (char *pFileName, char *pSensorName)
{
	int fd;
	int len;
	
	fd = open (pFileName, O_RDONLY);

	if (fd < 0)
	{
		printf ("error open %s\n", pFileName);
	}
	else
	{
		len = read (fd, pSensorName, 16);

		if (len < 0)
		{
			fd = len;
		}

		close (fd);
	} // if (fd < 0)

	return (fd);
} /* hwm_getTemperatureName */


/**************************************************************************//**
 *
 *  \brief		hwm_getTemperatureValue
 *
 *  \details	gibt den Messwert des Sensors zurück. 
 *
 *	\param		pFileName		Pointer auf sysfs Filename mit Pfad zum Temperatursensor
 * 	\param		pSensorValue	Pointer auf Temperatursensorwert
 *
 *	\return		0	Funktion erfolgreich ausgeführt
 * 				< 0 Funktion fehlgeschlagen
 *
 ******************************************************************************/
int	hwm_getTemperatureValue (char *pFileName, int *pSensorValue)
{
	int fd;
	int len;
	
	fd = open (pFileName, O_RDONLY);

	if (fd < 0)
	{
		printf ("error open %s\n", pFileName);
	}
	else
	{
		len = read (fd, pSensorValue, 16);

		if (len > 0)
		{
			*pSensorValue = atoi (pSensorValue);
		}

		close (fd);
	} // if (fd < 0)

	return (fd);
} /* hwm_getTemperatureValue */


/* End hwm.c */
