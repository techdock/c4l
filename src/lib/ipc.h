/**************************************************************************//**
 *
 *	\file		ipc.h
 *
 *	\brief		Interface für die ipc-Ansteuerung
 *
 *	\details
 *
 *	\date		17.06.2015
 *	\author		Marcel Ming
 *	\copyright	TRiMADA AG   CH-5610 Wohlen
 *	\remark		GCC Embedded
 *
 *	\version	17.06.15 mm Erstellt
 *
 ******************************************************************************/
#ifndef __ipc_H
    #define __ipc_H

#ifdef __cplusplus
    extern "C" {
#endif // __cplusplus


/******************************************************************************
 * Compiler Switches                                                          *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Modules                                                                    *
 *                                                                            *
 ******************************************************************************/
#include <fcntl.h>			// for open(), close(), lseek()
#include <string.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>		// for open()
#include <unistd.h>


/******************************************************************************
 * Definitions and Macros                                                     *
 *                                                                            *
 ******************************************************************************/
#define C_ipc_bufferSize		255
#define C_ipc_fifoname			"/tmp/11007FiFo"


/******************************************************************************
 * Typedefs and Structures                                                    *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Exported Variables                                                         *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Exported Functions                                                         *
 *                                                                            *
 ******************************************************************************/
int	ipc_createFiFo (const char *pFiFoName);
int	ipc_deleteFiFo (const char *pFiFoName);
int ipc_writeFiFo (const char *FiFoName, const char *DataString);
int ipc_readFiFo (const char *FiFoName, char *DataString);


#ifdef __cplusplus
    } // extern "C"
#endif // __cplusplus


#endif // __ipc_H


/* End ipc.h */
