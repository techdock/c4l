/**************************************************************************//**
 *
 *	\file		c4lusb.h
 *
 *	\brief		Headerfile der Funktionen für die Verwaltung eines USB Memory
 *				Sticks.
 *
 *	\details	-
 *
 *	\date		08.06.2021
 *	\author		Marcel Ming
 *	\copyright	TRiMADA AG   CH-5610 Wohlen
 *	\remark		Compiler- / IDE-Version
 *
 *	\version	08.06.21 mm
 *					- Erstellt.
 *	\version	18.06.22 mm
 *					- #include "c4lusb.h" entfernt.
 *
 *
 ******************************************************************************/
#ifndef __c4lusb_H
    #define __c4lusb_H


/******************************************************************************
 * Modules                                                                    *
 *                                                                            *
 ******************************************************************************/
#include <stdint.h>

#include "db.h"


/******************************************************************************
 * Definitions and Macros                                                     *
 *                                                                            *
 ******************************************************************************/
#define	C_c4lusb_mountName					"disk"

#define	C_c4lusb_filenamePassword			"passwd.csv"
#define	C_c4lusb_filenameParamUpload		"confup.csv"
#define	C_c4lusb_filenameParamDownload		"confdown.csv"
#define	C_c4lusb_filenameTextUpload			"textup.csv"
#define	C_c4lusb_filenameTextDownload		"textdown.csv"
#define	C_c4lusb_filenameDataDownload		"data"
#define	C_c4lusb_fileextentionDataDownload	".csv"

#define	C_c4lusb_fileToEnableParamUpload	"paraup"
#define	C_c4lusb_fileToEnableParamDownload	"paradown"
#define	C_c4lusb_fileToEnableTextUpload		"textup"
#define	C_c4lusb_fileToEnableTextDownload	"textdown"
#define	C_c4lusb_fileToEnableDataDownload	"data"

#define	C_c4lusb_sleep1Sec					1				// Sekunden
#define	C_c4lusb_sleep2Sec					2				// Sekunden


/******************************************************************************
 * Typedefs and Structures                                                    *
 *                                                                            *
 ******************************************************************************/
typedef struct {
	char	strDirectory [C_db_dataStringSize];
	char	strPassword [C_db_dataStringSize];
	int		msgSMTPuploadPA;
	int		msgSMTPdownloadPA;
	int		msgSMTPuploadTP;
	int		msgSMTPdownloadTP;
} ts_c4lusb;


/******************************************************************************
 * Exported Variables                                                         *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Exported Functions                                                         *
 *                                                                            *
 ******************************************************************************/
int c4lusb_configRead (ts_db_connect *pConnect,
					   ts_c4lusb	 *pC4lusb);

void c4lusb_configOverride (ts_db_connect	*pConnect,
							ts_c4lusb		*pC4lusb);

#endif //__c4lusb_H


/* End c4lusb.h */
