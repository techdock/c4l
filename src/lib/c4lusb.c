/**************************************************************************//**
 *
 *	\file		c4lusb.c
 *
 *	\brief		Stellt die Funktionen für die Verwaltung eines USB Memory
 *				Sticks zur Verfügung.
 *
 *	\details	-
 *
 *	\date		08.06.2021
 *	\author		Marcel Ming
 *	\copyright	TRiMADA AG   CH-5610 Wohlen
 *	\remark		Compiler- / IDE-Version
 *
 *	\version	08.06.21 mm
 *					- Erstellt.
 *
 *
 ******************************************************************************/


/******************************************************************************
 * Compilerswitches                                                           *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Modules                                                                    *
 *                                                                            *
 ******************************************************************************/
#include <stdlib.h>
#include <stdio.h>

#include "c4lusb.h"
#include "db.h"


/******************************************************************************
 * Definitions and Macros                                                     *
 *                                                                            *
 ******************************************************************************/
#define	C_c4lusb_pathLen		255


/******************************************************************************
 * Typedefs and Structures                                                    *
 *                                                                            *
 ******************************************************************************/
 
/******************************************************************************
 * External Variables                                                         *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Global Variables                                                           *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Local Functions                                                            *
 *                                                                            *
 ******************************************************************************/

/**************************************************************************//**
 *
 *	\brief		c4lusb_getDbValue
 *
 *	\details	liest den Konfigurationswert dbIdOffset für die c4lusbfunktion
 *				aus der Datenbanktabelle PA.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		dbIdOffset	Datenbank id Offset zur Basis id des Eintrags.
 *	\param		*pFail		Resultat Flag, wird bei einem Fehler auf 1 gesetzt
 *
 *	\return		Integerwert aus Datenbank
 *
 ******************************************************************************/
static int c4lusb_getDbValue (ts_db_connect			*pConnect,
							  te_db_PAidOffsetSMTP	dbIdOffset,
							  int					*pFail)
{
	static ts_db_pa dbPA;


	dbPA.id = C_db_paBaseUSB + dbIdOffset;

	if (db_rowGet (pConnect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA) == 0)
	{
		return (atoi (dbPA.strValue));
	}
	else
	{
		*pFail = 1;

		return (0);
	} // else if (db_rowGet (pConnect, E_db_tableTypePA, &dbPA) == 0)
} /* c4lusb_getDbValue */


/**************************************************************************//**
 *
 *	\brief		c4lusb_getDbText
 *
 *	\details	liest den Konfigurationstext dbIdOffset für die c4lusbfunktion
 *				aus der Datenbanktabelle PA.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		dbIdOffset	Datenbank id Offset zur Basis id des Eintrags.
 *	\param		*pText		Pointer auf den gelesenen Text.
 *	\param		*pFail		Resultat Flag, wird bei einem Fehler auf 1 gesetzt
 *
 *	\return		0: Lesen erfolgreich
 *
 ******************************************************************************/
static void c4lusb_getDbText (ts_db_connect			*pConnect,
							  te_db_PAidOffsetSMTP	dbIdOffset,
							  char					*pText,
							  int					*pFail)
{
	ts_db_pa	dbPA;


	dbPA.id = C_db_paBaseUSB + dbIdOffset;

	if (db_rowGet (pConnect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA) == 0)
	{
		snprintf (pText, C_db_dataStringSize, "%s", dbPA.strValue);

		fprintf (stderr, "c4lusb_getDbText: %s\n", dbPA.strValue);
	}
	else
	{
		*pFail = 1;
	} // else if (db_rowGet (pConnect, E_db_tableTypePA, &dbPA) == 0)
} /* c4lusb_getDbText */


/******************************************************************************
 * Exported Functions                                                         *
 *                                                                            *
 ******************************************************************************/

/**************************************************************************//**
 *
 *  \brief		c4lusb_configRead
 *
 *  \details	liest die Konfiguration für die c4lusbs aus der Datenbank-
 *				tabelle PA.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		*pC4lusb	Pointer auf Struktur ts_c4lusb
 *
 *	\return		0: Lesen erfolgreich
 *
 ******************************************************************************/
int c4lusb_configRead (ts_db_connect	*pConnect,
					   ts_c4lusb		*pC4lusb)
{
	int	fail = 0;

	c4lusb_getDbText (pConnect, E_db_PAoUSBDirectory, pC4lusb->strDirectory, &fail);
	c4lusb_getDbText (pConnect, E_db_PAoUSBPassword, pC4lusb->strPassword, &fail);

	pC4lusb->msgSMTPuploadPA	= c4lusb_getDbValue (pConnect, E_db_PAoUSBSMTPuploadPA, &fail);
	pC4lusb->msgSMTPdownloadPA	= c4lusb_getDbValue (pConnect, E_db_PAoUSBSMTPdownloadPA, &fail);
	pC4lusb->msgSMTPuploadTP	= c4lusb_getDbValue (pConnect, E_db_PAoUSBSMTPuploadTP, &fail);
	pC4lusb->msgSMTPdownloadTP	= c4lusb_getDbValue (pConnect, E_db_PAoUSBSMTPdownloadTP, &fail);

	return (fail);
} /* c4lusb_configRead */


/**************************************************************************//**
 *
 *  \brief		c4lusb_configOverride
 *
 *  \details	überschreibt die Konfiguration mit Testwerten.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		*pC4lusb		Pointer auf Struktur ts_c4lusb
 *
 *	\return		-
 *
 ******************************************************************************/
void c4lusb_configOverride (ts_db_connect	*pConnect,
							ts_c4lusb		*pC4lusb)
{ 
} /* c4lusb_configOverride */


/* c4lusb.c */

