/**************************************************************************//**
 *
 *	\file		mail.h
 *
 *	\brief		Headerfile der Funktionen für die Versendung von Mails.
 *
 *	\details	-
 *
 *	\date		11.05.2021
 *	\author		Marcel Ming
 *	\copyright	TRiMADA AG   CH-5610 Wohlen
 *	\remark		Compiler- / IDE-Version
 *
 *	\version	11.05.21 mm
 *					- Erstellt.
 *	\version	03.09.21 mm
 *					- Anpassungen für mailx.
 *	\version	22.10.21 mm
 *					- Funktion mail_configDebug definiert.
 *	\version	26.10.21 mm
 *					- C_mail_dataStringSize definiert. Die Verwendung von
 *					  C_db_dataStringSize führte zu ungewollten Einschränkungen.
 *	\version	02.05.22 mm
 *					- C_mail_passwordEvalTxt und C_mail_passwordEvalLen definiert.
 *
 *
 ******************************************************************************/
#ifndef __mail_H
    #define __mail_H


/******************************************************************************
 * Compilerswitches                                                           *
 *                                                                            *
 ******************************************************************************/
	#define __mail_showDebugStrings


/******************************************************************************
 * Modules                                                                    *
 *                                                                            *
 ******************************************************************************/
#include <stdint.h>

#include "db.h"


/******************************************************************************
 * Definitions and Macros                                                     *
 *                                                                            *
 ******************************************************************************/
#define C_mail_dataStringSize	(4 * C_db_dataStringSize)
#define	C_mail_passwordEvalTxt	"gpg --decrypt"
#define	C_mail_passwordEvalLen	13


/******************************************************************************
 * Typedefs and Structures                                                    *
 *                                                                            *
 ******************************************************************************/
typedef enum {
	E_mail_modeOff,			//!< aus
	E_mail_modeOnce,		//!< ein Sendeversuch 
	E_mail_modeRepeat,		//!< mehere Sendeversuche
	E_mail_modeCount		//!< Anzahl Modi
} te_mail_mode;


typedef enum {
	E_mail_encryptOff,		//!< encryption aus
	E_mail_encryptTLS,		//!< encryption TLS
	E_mail_encryptSSL,		//!< encryption SSL
	E_mail_encryptCount		//!< Anzahl Encryptionvarianten
} te_mail_encryption;


typedef struct {
	// Datenbank Werte
	uint16_t			port;								//!< SMTP Port, >0: Funktion aktiviert
	te_mail_mode		mode;								//!< SMTP Mode
	char				account [C_db_dataStringSize];		//!< Projectnummer
	char				from [C_db_dataStringSize];			//!< SMTP mail from address
	char				to [C_db_dataStringSize];			//!< SMTP mail to address
	char				server [C_db_dataStringSize];		//!< SMTP Server oder URL Adresse
	int					iteration;							//!< Anzahl Sende-Wiederholungen bei Fehler
	char				user [C_db_dataStringSize];			//!< SMTP Username
	char				password [C_db_dataStringSize];		//!< SMTP Passwort
	te_mail_encryption	encryption;							//!< Encryption Methode
	int					subjectId;							//!< ID aus Tabelle TP
	int					sendErrMsgId;						//!< ID aus Tabelle TP

	// interne Werte
	int					languageOffset;						//!< Sprache
	char				subject [C_db_dataStringSize];		//!< Betreff
	char				message [C_db_dataStringSize];		//!< Mail Text
} ts_mail;


/******************************************************************************
 * Exported Variables                                                         *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Exported Functions                                                         *
 *                                                                            *
 ******************************************************************************/
int mail_configWriteToMsmtprc (ts_mail	*pMail);

void mail_configDebug (ts_mail *pMail);

int mail_configRead (ts_db_connect	*pConnect,
					 ts_mail		*pMail);

void mail_configOverride (ts_db_connect	*pConnect,
						  ts_mail		*pMail);

void mail_initSubject (ts_db_connect *pConnect,
					   ts_mail		 *pMail);

int mail_send  (const char *from,
				const char *to,
				const char *subject,
				const char *message);

#endif //__mail_H

/* End mail.h */
