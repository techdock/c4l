/**************************************************************************//**
 *
 *	\file		va.h
 *
 *	\brief		Headerfile der Funktionen für VA Register.
 *
 *	\details	-
 *
 *	\date		26.09.2023
 *	\author		Marcel Ming
 *	\copyright	TRiMADA AG   CH-5610 Wohlen
 *	\remark		Compiler- / IDE-Version
 *
 *	\version	26.09.23 mm
 *					- Erstellt.
 *	\version	16.10.23 mm
 *					- te_va_source angepasst.
 *	\version	19.10.23 mm
 *					- Funktion db_vaNumberToFlagIndex als va_numberToFlagIndex
 * 					  in diesem Modul implementiert um Abhängkeiten bei Linken
 * 					  zu reduzieren.
 *	\version	29.11.23 mm
 *					- Struktur ts_va um resultCondition und resultConditionLast
 *					  erweitert.
 *
 *
 ******************************************************************************/
#ifndef __va_H
    #define __va_H


/******************************************************************************
 * Compilerswitches                                                           *
 *                                                                            *
 ******************************************************************************/
	#define __va_showDebugStrings
//	#define __va_showCheckConditionStrings	
	#define __va_showCheckLimitsStrings	
	#define	__va_accelerateHourCount


/******************************************************************************
 * Modules                                                                    *
 *                       va_initCounter                                                     *
 ******************************************************************************/
#include <stdbool.h>
#include <stdint.h>

#include "mail.h"


/******************************************************************************
 * Definitions and Macros                                                     *
 *                                                                            *
 ******************************************************************************/
#define	C_va_counterMinNumber	9
#define	C_va_counterMaxNumber	16
#define	C_va_counterCount		(C_va_counterMaxNumber - C_va_counterMinNumber + 1)


/******************************************************************************
 * Typedefs and Structures                                                    *
 *                                                                            *
 ******************************************************************************/
typedef enum {
	E_va_fktOff,			//!< ausgeschaltet 
	E_va_fktOnLocked,		//!< Eingeschaltet, nur im Servicemode bearbeitbar
	E_va_fktOnUnlocked,		//!< Eingeschaltet, bearbeitbar
	E_va_fktCount			//!< Anzahl Funktionen
} te_va_function;


typedef enum {
	E_va_typeNone,			//!< kein Zähler, VAx könnte durch eine externe App beschrieben werden.
	E_va_typeHour,			//!< Stundenzähler
	E_va_typeEvents,		//!< Startzähler
	E_va_typeCount			//!< Anzahl Typen
} te_va_type;


typedef enum {
	E_va_source_none,
	E_va_source_DI1,
	E_va_source_DI2,
	E_va_source_DI3,
	E_va_source_DI4,
	E_va_source_DO1,
	E_va_source_DO2,
	E_va_source_DO3,
	E_va_source_DO4,
	E_va_source_VA1,		//!< AI1 skaliert
	E_va_source_VA2,		//!< AI2 skaliert
	E_va_source_VA3,		//!< AO1 skaliert
	E_va_source_VA4,		//!< AO2 skaliert
	E_va_source_count,
} te_va_source;


typedef enum {
	E_va_scNone,			//!< keine gültige Startbedingung
	E_va_scGreaterOrEqual,	//!< >=
	E_va_scLessOrEqual,		//!< <=
	E_va_scCount			//!< Anzahl Typen
} te_va_startCondition;


typedef enum {
	E_va_opUndef,
	E_va_opEqual,			//!< == 
	E_va_opNotEqual,		//!< !=
	E_va_opGreater,			//!< >
	E_va_opLess,			//!< <
	E_va_opGreaterOstrUnitrEqual,	//!< >=
	E_va_opLessOrEqual,		//!< <=
} te_va_operator;


typedef struct {
	// Datenbank Werte
	te_va_function			function;			//!< Funktion
	te_va_type				type;				//!< Zählerart
	te_va_source			source;				//!< Ereignisquelle
	te_va_startCondition	startCondition;		//!< 1: >=, 2: <=
	int32_t					valueCondition;		//!< Wert, Zähler läuft, wenn er die beiden Condition erfüllt
	int32_t					hysteresis;			//!< ?
	uint16_t				msgTooLo;			//!< 0: off, >0 Textnummer aus TP
	te_va_operator			msgOperatorTooLo;	//!< Operator für Text zu tief in SMTP
	int32_t					valueTooLo;			//!< Wert für zu tief
	uint16_t				msgLo;				//!< 0: off, >0 Textnummer aus TP
	te_va_operator			msgOperatorLo;		//!< Operator für Text tief in SMTP
	int32_t					valueLo;			//!< Wert für tief
	uint16_t				msgHi;				//!< 0: off, >0 Textnummer aus TP
	te_va_operator			msgOperatorHi;		//!< Operator für Text hoch in SMTP
	int32_t					valueHi;			//!< Wert für hoch
	uint16_t				msgTooHi;			//!< 0: off, >0 Textnummer aus TP
	te_va_operator			msgOperatorTooHi;	//!< Operator für Text zu hoch in SMTP
	int32_t					valueTooHi;			//!< Wert für zu hoch

	// interne Werte
	int32_t	counter;					//!< Zähler für Counterfunktion
	uint8_t	lastHourValue;				//!< letzter Stundenwert zum Vergleich
	uint	counterEnabled		: 1;	//!< Flag Bedingung zum zählen erfüllt, im Zusammenhang mit Hysterese
	uint	resultCondition		: 1;	//!< Resultat der Zählbedingung	
	uint	resultConditionLast	: 1;	//!< vorheriges Resultat der Zählbedingung
	uint	valueTooLoActive	: 1;	//!< Flag Wert als zu tief erkannt
	uint	valueLoActive		: 1;	//!< Flag Wert als tief erkannt
	uint	valueHiActive		: 1;	//!< Flag Wert als hoch erkannt
	uint	valueTooHiActive	: 1;	//!< Flag Wert als zu hoch erkannt
} ts_va;


/******************************************************************************
 * Exported Variables                                                         *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Exported Functions                                                         *
 *                                                                            *
 ******************************************************************************/
te_db_flag va_numberToFlagIndex (ts_db_connect *pConnect,
								   int			 vaNumber);

void va_configDebug (ts_va	*pVa,
					 int	vaNumber);

int va_configRead (ts_db_connect *pConnect,
                   ts_va		 *pVa,
                   int			 vaNumber);

void va_configSetPermissionFlag (ts_db_connect	*pConnect,
								 ts_va			*pVa,
								 int			vaNumber);

void va_configOverride (ts_va	*pVa,
						int		vaNumber);

int va_process (ts_db_connect	*pConnect,
				void			*pVoid,
				int				vaNumber,
				ts_mail			*pMail);


#endif //__va_H

/* End va.h */
