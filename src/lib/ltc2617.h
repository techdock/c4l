/**************************************************************************//**
 *
 *	\file		ltc2617.h
 *
 *	\brief		Headerfiles für den ltc2617 Zugriff.
 *
 *	\details	Der ltc2617 ist ein DAC mit 14 Bit Auflösung, 2 Kanälen und einer
 *				i2c Schnittstelle.
 *
 *	\date		25.01.2021
 *	\author		Marcel Ming
 *	\copyright	TRiMADA AG   CH-5610 Wohlen
 *	\remark		Compiler- / IDE-Version
 *
 *	\version	25.01.21 mm
 *					- Erstellt.
 *	\version	27.01.21 mm
 *					- ins /11007/library/ verschoben -> in einem Projekt als
 *					  Link einfügen.
 *
 ******************************************************************************/
#ifndef __ltc2617_H
    #define __ltc2617_H


/******************************************************************************
 * Modules                                                                    *
 *                                                                            *
 ******************************************************************************/
#include <stdbool.h>
#include <stdint.h>


/******************************************************************************
 * Definitions and Macros                                                     *
 *                                                                            *
 ******************************************************************************/
// Die Adressbit Kombinationen sind in der Reihenfolge A2 A1 A0 augelistet
#define	C_ltc2617_addressGndGndGnd			0x10	//!< Adresse des DAC ltc2617 wenn A2 = GND, A1 = GND, A0 = GND
#define	C_ltc2617_addressGndGndFloat		0x11	//!< Adresse des DAC ltc2617 wenn A2 = GND, A1 = GND, A0 = Float
#define	C_ltc2617_addressGndGndVcc			0x12	//!< Adresse des DAC ltc2617 wenn A2 = GND, A1 = GND, A0 = Vcc
#define	C_ltc2617_addressGndFloatGnd		0x13	//!< Adresse des DAC ltc2617 wenn A2 = GND, A1 = Float, A0 = GND
#define	C_ltc2617_addressGndFloatFloat		0x20	//!< Adresse des DAC ltc2617 wenn A2 = GND, A1 = Float, A0 = Float
#define	C_ltc2617_addressGndFloatVcc		0x21	//!< Adresse des DAC ltc2617 wenn A2 = GND, A1 = Float, A0 = Vcc
#define	C_ltc2617_addressGndVccGnd			0x22	//!< Adresse des DAC ltc2617 wenn A2 = GND, A1 = Vcc, A0 = GND
#define	C_ltc2617_addressGndVccFloat		0x23	//!< Adresse des DAC ltc2617 wenn A2 = GND, A1 = Vcc, A0 = Float
#define	C_ltc2617_addressGndVccVcc			0x30	//!< Adresse des DAC ltc2617 wenn A2 = GND, A1 = Vcc, A0 = Vcc
#define	C_ltc2617_addressFloatGndGnd		0x31	//!< Adresse des DAC ltc2617 wenn A2 = Float, A1 = GND, A0 = GND
#define	C_ltc2617_addressFloatGndFloat		0x32	//!< Adresse des DAC ltc2617 wenn A2 = Float, A1 = GND, A0 = Float
#define	C_ltc2617_addressFloatGndVcc		0x33	//!< Adresse des DAC ltc2617 wenn A2 = Float, A1 = GND, A0 = Vcc
#define	C_ltc2617_addressFloatFloatGnd		0x40	//!< Adresse des DAC ltc2617 wenn A2 = Float, A1 = Float, A0 = GND
#define	C_ltc2617_addressFloatFloatFloat	0x41	//!< Adresse des DAC ltc2617 wenn A2 = Float, A1 = Float, A0 = Float
#define	C_ltc2617_addressFloatFloatVcc		0x42	//!< Adresse des DAC ltc2617 wenn A2 = Float, A1 = Float, A0 = Vcc
#define	C_ltc2617_addressFloatVccGnd		0x43	//!< Adresse des DAC ltc2617 wenn A2 = Float, A1 = Vcc, A0 = GND
#define	C_ltc2617_addressFloatVccFloat		0x50	//!< Adresse des DAC ltc2617 wenn A2 = Float, A1 = Vcc, A0 = Float
#define	C_ltc2617_addressFloatVccVcc		0x51	//!< Adresse des DAC ltc2617 wenn A2 = Float, A1 = Vcc, A0 = Vcc
#define	C_ltc2617_addressVccGndGnd			0x52	//!< Adresse des DAC ltc2617 wenn A2 = Vcc, A1 = GND, A0 = GND
#define	C_ltc2617_addressVccGndFloat		0x53	//!< Adresse des DAC ltc2617 wenn A2 = Vcc, A1 = GND, A0 = Float
#define	C_ltc2617_addressVccGndVcc			0x60	//!< Adresse des DAC ltc2617 wenn A2 = Vcc, A1 = GND, A0 = Vcc
#define	C_ltc2617_addressVccFloatGnd		0x61	//!< Adresse des DAC ltc2617 wenn A2 = Vcc, A1 = Float, A0 = GND
#define	C_ltc2617_addressVccFloatFloat		0x62	//!< Adresse des DAC ltc2617 wenn A2 = Vcc, A1 = Float, A0 = Float
#define	C_ltc2617_addressVccFloatVcc		0x63	//!< Adresse des DAC ltc2617 wenn A2 = Vcc, A1 = Float, A0 = Vcc
#define	C_ltc2617_addressVccVccGnd			0x70	//!< Adresse des DAC ltc2617 wenn A2 = Vcc, A1 = Vcc, A0 = GND
#define	C_ltc2617_addressVccVccFloat		0x71	//!< Adresse des DAC ltc2617 wenn A2 = Vcc, A1 = Vcc, A0 = Float
#define	C_ltc2617_addressVccVccVcc			0x72	//!< Adresse des DAC ltc2617 wenn A2 = Vcc, A1 = Vcc, A0 = Vcc
#define	C_ltc2617_addressGlobal				0x73	//!< Globale Adresse des DAC ltc2617, nicht konfigurierbar

#define	C_ltc2617_resolution	16383	//!< Auflösung des DAC
#define	C_ltc2617_maxVoltage	10000	//!< maximale Ausgangsspannung [mV]
#define	C_ltc2617_maxCurrent	20000	//!< maximaler Ausgangsstrom [uA]


/******************************************************************************
 * Typedefs and Structures                                                    *
 *                                                                            *
 ******************************************************************************/
typedef enum {
    E_ltc2617_adr_dacA,					//!< Command an DAC A
    E_ltc2617_adr_dacB,					//!< Command an DAC B
    E_ltc2617_adr_allDacs = 0x0F		//!< Command an DAC A bis DAC x
} te_ltc2617_address;

typedef enum {
    E_ltc2617_cmd_writeToInput,			//!< Input Register
    E_ltc2617_cmd_updateDAC,			//!< DAC Register
    E_ltc2617_cmd_writeToAndUpdate,		//!< Input und DAC Register
    E_ltc2617_cmd_powerDown,			//!< Power Down
    E_ltc2617_cmd_noOperation = 0x0F	//!< 
} te_ltc2617_command;

typedef union {
	uint8_t	both;
	
	struct {
		te_ltc2617_address	adr	: 4;	//!<
		te_ltc2617_command	cmd	: 4;	//!<
	};																//!< write: 0: no effect, 1: start conversion
} tu_ltc2617_adressAndCommand;


/******************************************************************************
 * Exported Variables                                                         *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Exported Functions                                                         *
 *                                                                            *
 ******************************************************************************/
int	ltc2617_setDac (int deviceDescriptor, uint8_t chipAddr, te_ltc2617_address channel, uint16_t dacValue);
int	ltc2617_setVoltage (int deviceDescriptor, uint8_t chipAddr, te_ltc2617_address channel, uint16_t voltage);
int	ltc2617_setCurrent (int deviceDescriptor, uint8_t chipAddr, te_ltc2617_address channel, uint16_t current);

#endif //__ltc2617_H

/* End ltc2617.h */
