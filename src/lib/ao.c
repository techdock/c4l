/**************************************************************************//**
 *
 *	\file		ao.c
 *
 *	\brief		Stellt die Funktionen für die Verwaltung der analogen Eingänge
 *				zur Verfügung.
 *
 *	\details	-
 *
 *	\date		09.03.2021
 *	\author		Marcel Ming
 *	\copyright	TRiMADA AG   CH-5610 Wohlen
 *	\remark		Compiler- / IDE-Version
 *
 *	\version	09.03.21 mm
 *					- Erstellt.
 *	\version	08.04.21 mm
 *					- Anpassungen an Pflichtenheft Revision 4.
 *	\version	03.08.21 mm
 *					- Debugmeldungen angepasst.
 *	\version	05.08.21 mm
 *					- ao_configOverride implementiert.
 *	\version	30.08.21 mm
 *					- ao_onFollowAi korrigiert, Spannung wurde nicht in Binärwert
 *					  umgerechnet.
 *					- skalierter Werte in VA Tabelle schreiben von ao_logScaled
 *					  in ao_process verschoben damit nach neuer Anforderung
 *					  unabhängig vom Logintervall.
 *	\version	01.09.21 mm
 *					- ao_processQuick implementiert.
 *	\version	06.09.21 mm
 *					- Compilerschalter __ao_showDebugStrings definiert.
 *	\version	09.09.21 mm
 *					- PA605 (Einheit zum Pegel) und 655 von uADC/mVDC auf 0/1
 *					  geändert um Konfigurationsfehler zu vermeiden.
 *					  Grund: Beim Testen wurde PA605 als mV definiert, was von
 *					  ao_configRead nicht erkannt und somit falsch interpretiert
 *					  wurde.
 *	\version	29.10.21 mm
 *					- Compilerschalter __csw_configAoCurrent uimplementiert.
 *	\version	02.11.21 mm
 *					- Funktion ao_getBinary um Parameter unit erweitert, damit
 *					  die Umrechnung von Spannung oder Strom korrekt durchgeführt
 *					  werden kann.
 *	\version	22.11.21 mm
 *					- Funktion ao_process schreibt bei ausgeschaltetem AO den
 *					  Wert "off" in die AO und VA Tabellen.
 *	\version	29.11.21 mm
 *					- Compilerschalter __csw_configAoCurrent durch csw_flag
 *					  ersetzt.
 *	\version	30.11.21 mm
 *					- Ausgang bei E_ao_depFollowOff in seinem aktuellen Zustand
 *					  belassen.
 *	\version	08.11.21 mm
 *					- in Funktion ao_process den Wert von lastFunction nachgeführt
 *					  um auch bei dynamischen Konfigurationen richtig reagieren zu
 *					  können.
 *	\version	13.12.21 mm
 *					- Berechnungen Signalchain gemäss Issue #51.
 *	\version	15.12.21 mm
 *					- Funktionen ao_getDbText und ao_limitBinary implementiert.
 *	\version	18.02.22 mm
 *					- Kommentar ergänzt / korrigiert.
 *	\version	22.09.23 mm
 *					- in Funktion ao_configOverride den nicht benötigten Parameter
 * 					  *pConnect gelöscht.
 *
 *
 ******************************************************************************/


/******************************************************************************
 * Compilerswitches                                                           *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Modules                                                                    *
 *                                                                            *
 ******************************************************************************/
#include <mysql/mysql.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ao.h"
#include "csw.h"
#include "db.h"
#include "ioa.h"
#include "ltc2617.h"


/******************************************************************************
 * Definitions and Macros                                                     *
 *                                                                            *
 ******************************************************************************/
#define __GNU_SOURCE


/******************************************************************************
 * Typedefs and Structures                                                    *
 *                                                                            *
 ******************************************************************************/
 
/******************************************************************************
 * External Variables                                                         *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Global Variables                                                           *
 *                                                                            *
 ******************************************************************************/
const char* const C_ao_operatorText [] = {
	"   \0",
	"==\0",
	"<>\0",
	">\0",
	"<\0",
	">=\0",
	"<=\0"
};


/******************************************************************************
 * Local Functions                                                            *
 *                                                                            *
 ******************************************************************************/

/**************************************************************************//**
 *
 *	\brief		ao_getDbText
 *
 *	\details	liest den Konfigurationstext dbIdOffset für den Kanal channel
 *				aus der Datenbanktabelle PA.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		dbIdOffset	Datenbank id Offset zur Basis id des Eintrags.
 *	\param		*pText		Pointer auf den gelesenen Text.
 *
 *	\return		 0: Lesen erfolgreich
 * 				-1: Fehler beim Lesen
 *
 ******************************************************************************/
static int ao_getDbText (ts_db_connect		*pConnect,
						 int				channel,
						 te_db_PAidOffsetAi	dbIdOffset,
						 char				*pText)
{
	ts_db_pa	dbPA;
	int			retValue = -1;


	dbPA.id = C_db_paChannelBaseAo + (channel * C_db_paChannelOffsetAo) + dbIdOffset;

	if (db_rowGet (pConnect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA) == 0)
	{
		snprintf (pText, C_db_dataStringSize, "%s", dbPA.strValue);
		retValue = 0;
	} // if (db_rowGet (pConnect, E_db_tableTypePA, &dbPA) == 0)

	return (retValue);
} /* ao_getDbText */


/**************************************************************************//**
 *
 *	\brief		ao_getDbValue
 *
 *  \details	liest den Konfigurationswert dbIdOffset für den Kanal channel
 *				aus der Datenbanktabelle PA.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		channel		Kannalnummer des analog Ausgang [0..1]
 *	\param		dbIdOffset	Datenbank id Offset zur Basis id des Eintrags.
 *	\param		*pFail		Resultat Flag, wird bei einem Fehler auf 1 gesetzt
 *
 *	\return		0: Lesen erfolgreich
 *
 ******************************************************************************/
static int ao_getDbValue (ts_db_connect		 *pConnect,
						  int				 channel,
						  te_db_PAidOffsetAo dbIdOffset,
						  int				 *pFail)
{
	static ts_db_pa dbPA;


	dbPA.id = C_db_paChannelBaseAo + (channel * C_db_paChannelOffsetAo) + dbIdOffset;

	if (db_rowGet (pConnect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA) == 0)
	{
		return (atoi (dbPA.strValue));
	}
	else
	{
		*pFail = 1;
		return (0);
	} // else if (db_rowGet (pConnect, E_db_tableTypePA, &dbPA) == 0)
} /* ao_getDbValue */
 
 
/**************************************************************************//**
 *
 *	\brief		ao_logEvent
 *
 *  \details	
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		channel		Kannalnummer des analog Ausgang [0..1]
 *	\param		idOffset	Offset für Event Wert
 *
 *	\return		-
 *
 ******************************************************************************/
static void ao_logEvent (ts_db_connect		*pConnect,
						 int				channel,
						 te_db_PAidOffsetAi	idOffset)
{
	static ts_db_ev	dbEV;


	// PA Index ermitteln und in Eventliste eintragen
	snprintf (dbEV.strValue, C_db_dataStringSize, "%d", C_db_paChannelBaseAo + channel * C_db_paChannelOffsetAo + idOffset);
	snprintf (dbEV.strDateTime, C_db_dataStringSize, "%s",  "");
	db_rowAdd (pConnect, E_db_tableTypeEV, E_db_listTypeNone, 0, &dbEV);

	#ifdef __ao_showDebugStrings
		fprintf (stderr, "ao_logEvent: ao%d EV:%s\n", channel, dbEV.strValue);
	#endif
} /* ao_logEvent */


/**************************************************************************//**
 *
 *	\brief		ao_onCheckLimits
 *
 *	\details	überprüft den Wert auf die verschiedenen ON Funktion Limiten und
 *				reagiert entsprechend.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		*pAo		Pointer auf Struktur ts_ao
 *	\param		channel		Kannalnummer des analog Ausgang [0..1]
 *	\param		*pMail		Pointer auf Struktur ts_mail
 *
 *	\return		-
 *
 ******************************************************************************/
static void ao_onCheckLimits (ts_db_connect	*pConnect,
							  ts_ao			*pAo,
							  int			channel,
							  ts_mail		*pMail)
{
	static ts_db_pa	dbPA;
	static ts_db_tp	dbTP;
	static char		strUnit [C_db_dataStringSize];

	// Unit lesen
	dbPA.id = C_db_paChannelBaseAo + channel * C_db_paChannelOffsetAo + E_db_PAoAoScaleUnit;
	db_rowGet (pConnect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA);
	snprintf (strUnit, C_db_dataStringSize, "%s", dbPA.strValue);

	// Prüfung auf "too lo"
	if ((pAo->scaledValue < pAo->valueTooLo) && (pAo->msgTooLo > 0))
	{
		if (pAo->valueTooLoActive == 0)
		{
			pAo->valueTooLoActive = 1;

			ao_logEvent (pConnect, channel, E_db_PAoAoValueTooLo);

			if ((pMail->mode != E_mail_modeOff) && (pAo->msgTooLo > 0))
			{
				// Mailtext zusammensetzen und senden
				dbPA.id = C_db_paChannelBaseAo + channel * C_db_paChannelOffsetAo + E_db_PAoAoValueTooLo;
				db_rowGet (pConnect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA);

				dbTP.id = pMail->languageOffset + C_db_tpChannelBaseAo + channel * C_db_tpChannelOffsetAo + E_db_TPoAoMsgTooLo;
				db_rowGet (pConnect, E_db_tableTypeTP, E_db_listTypeNone, 0, &dbTP);

				snprintf (pMail->message, C_db_dataStringSize, "%s %s %s %s",
						  dbTP.strText, C_ao_operatorText [pAo->msgOperatorTooLo], dbPA.strValue, strUnit);
				mail_send (pMail->from, pMail->to, pMail->subject, pMail->message);

				#ifdef __ao_showDebugStrings
					fprintf (stderr, "ao_onCheckLimits: mail from:%s, to:%s, sbj:%s, msg:%s\n",
									pMail->from, pMail->to, pMail->subject, pMail->message);
				#endif
			} // if ((pMail->mode != E_mail_modeOff) && (pAo->msgTooLo > 0))
		} // if (pAo->valueTooLoActive == 0)
	}
	else
	{
		if (pAo->valueTooLoActive)
		{
			pAo->valueTooLoActive = 0;
		} // if (pAo->valueTooLoActive)

		// Prüfung auf "lo"
		if ((pAo->scaledValue < pAo->valueLo) && (pAo->msgLo > 0))
		{
			if (pAo->valueLoActive == 0)
			{
				pAo->valueLoActive = 1;

				ao_logEvent (pConnect, channel, E_db_PAoAoValueLo);

				if ((pMail->mode != E_mail_modeOff) && (pAo->msgLo > 0))
				{
					// Mailtext zusammensetzen und senden
					dbPA.id = C_db_paChannelBaseAo + channel * C_db_paChannelOffsetAo + E_db_PAoAoValueLo;
					db_rowGet (pConnect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA);

					dbTP.id	= pMail->languageOffset + C_db_tpChannelBaseAo + channel * C_db_tpChannelOffsetAo + E_db_TPoAoMsgLo;
					db_rowGet (pConnect, E_db_tableTypeTP, E_db_listTypeNone, 0, &dbTP);

					snprintf (pMail->message, C_db_dataStringSize, "%s %s %s %s",
							  dbTP.strText, C_ao_operatorText [pAo->msgOperatorLo], dbPA.strValue, strUnit);
					mail_send (pMail->from, pMail->to, pMail->subject, pMail->message);

					#ifdef __ao_showDebugStrings
						fprintf (stderr, "ao_onCheckLimits: mail from:%s, to:%s, sbj:%s, msg:%s\n",
										pMail->from, pMail->to, pMail->subject, pMail->message);
					#endif
				} // if ((pMail->mode != E_mail_modeOff) && (pAo->msgLo > 0))
			} // if (pAo->valueLoActive == 0)
		}
		else
		{
			if (pAo->valueLoActive)
			{
				pAo->valueLoActive = 0;
			} // if (pAo->valueLoActive)
		} // else if (pAo->scaledValue < pAo->valueLo) ...
	} // else if ((pAo->scaledValue < pAo->valueTooLo) ...


	// Prüfung auf "too hi"
	if ((pAo->scaledValue > pAo->valueTooHi) && (pAo->msgTooHi > 0))
	{
		if (pAo->valueTooHiActive == 0)
		{
			pAo->valueTooHiActive = 1;

			ao_logEvent (pConnect, channel, E_db_PAoAoValueTooHi);

			if ((pMail->mode != E_mail_modeOff) && (pAo->msgTooHi > 0))
			{
				// Mailtext zusammensetzen und senden
				dbPA.id = C_db_paChannelBaseAo + channel * C_db_paChannelOffsetAo + E_db_PAoAoValueTooHi;
				db_rowGet (pConnect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA);

				dbTP.id	= pMail->languageOffset + C_db_tpChannelBaseAo + channel * C_db_tpChannelOffsetAo + E_db_TPoAoMsgTooHi;
				db_rowGet (pConnect, E_db_tableTypeTP, E_db_listTypeNone, 0, &dbTP);

				snprintf (pMail->message, C_db_dataStringSize, "%s %s %s %s",
						  dbTP.strText, C_ao_operatorText [pAo->msgOperatorTooHi], dbPA.strValue, strUnit);
				mail_send (pMail->from, pMail->to, pMail->subject, pMail->message);

				#ifdef __ao_showDebugStrings
					fprintf (stderr, "ao_onCheckLimits: mail from:%s, to:%s, sbj:%s, msg:%s\n",
									pMail->from, pMail->to, pMail->subject, pMail->message);
				#endif
			} // if ((pMail->mode != E_mail_modeOff) && (pAo->msgLo > 0))
		} // if (pAo->valueLoActive == 0)
	}
	else
	{
		if (pAo->valueTooHiActive)
		{
			pAo->valueTooHiActive = 0;
		} // if (pAo->valueTooHiActive)

		// Prüfung auf "hi"
		if ((pAo->scaledValue > pAo->valueHi) && (pAo->msgHi > 0))
		{
			if (pAo->valueHiActive == 0)
			{
				pAo->valueHiActive = 1;

				ao_logEvent (pConnect, channel, E_db_PAoAoValueHi);

				if ((pMail->mode != E_mail_modeOff) && (pAo->msgHi > 0))
				{
					// Mailtext zusammensetzen und senden
					dbPA.id = C_db_paChannelBaseAo + channel * C_db_paChannelOffsetAo + E_db_PAoAoValueHi;
					db_rowGet (pConnect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA);

					dbTP.id	= pMail->languageOffset + C_db_tpChannelBaseAo + channel * C_db_tpChannelOffsetAo + E_db_TPoAoMsgHi;
					db_rowGet (pConnect, E_db_tableTypeTP, E_db_listTypeNone, 0, &dbTP);

					snprintf (pMail->message, C_db_dataStringSize, "%s %s %s %s",
							  dbTP.strText, C_ao_operatorText [pAo->msgOperatorTooHi], dbPA.strValue, strUnit);
					mail_send (pMail->from, pMail->to, pMail->subject, pMail->message);

					#ifdef __ao_showDebugStrings
						fprintf (stderr, "ao_onCheckLimits: mail from:%s, to:%s, sbj:%s, msg:%s\n",
										pMail->from, pMail->to, pMail->subject, pMail->message);
					#endif
				} // if ((pMail->mode != E_mail_modeOff) && (pAo->msgLo > 0))
			} // if (pAo->valueLoActive == 0)
		}
		else
		{
			if (pAo->valueHiActive)
			{
				pAo->valueHiActive = 0;
			} // if (pAo->valueHiActive)
		} // else if (pAo->scaledValue < pAo->valueHi) ...
	} // else if (pAo->scaledValue < pAo->valueTooHi) ...
} /* ao_onCheckLimits */


/**************************************************************************//**
 *
 *	\brief		ao_piCheckLimits
 *
 *	\details	überprüft den Wert auf die verschiedenen PI Regler Limiten und
 *				reagiert entsprechend.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		*pAo		Pointer auf Struktur ts_ao
 *	\param		channel		Kannalnummer des analog Ausgang 
 *	\param		*pMail		Pointer auf Struktur ts_mail
 *
 *	\return		-
 *
 ******************************************************************************/
static void ao_piCheckLimits (ts_db_connect	*pConnect,
							  ts_ao			*pAo,
							  int			channel,
							  ts_mail		*pMail)
{
	static ts_db_pa	dbPA;
	static ts_db_tp	dbTP;
	static char		strUnit [C_db_dataStringSize];

	// Unit lesen
	dbPA.id = C_db_paChannelBaseAo + channel * C_db_paChannelOffsetAo + E_db_PAoAoScaleUnit;
	db_rowGet (pConnect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA);
	snprintf (strUnit, C_db_dataStringSize, "%s", dbPA.strValue);

	
	// Prüfung auf Output Minimum
	if (pAo->meanValue < pAo->piOutputMin)
	{
		if (pAo->piOutputMinActive == 0)
		{
			pAo->piOutputMinActive = 1;
			ao_logEvent (pConnect, channel, E_db_PAoAoPiOutputMin);

			if ((pMail->mode != E_mail_modeOff) && (pAo->piMsgMin > 0))
			{
				// Mailtext zusammensetzen und senden
				dbPA.id = C_db_paChannelBaseAo + channel * C_db_paChannelOffsetAo + E_db_PAoAoPiOutputMin;
				db_rowGet (pConnect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA);

				dbTP.id	= pMail->languageOffset + C_db_tpChannelBaseAo + channel * C_db_tpChannelOffsetAo + E_db_TPoAoMsgPiLo;
				db_rowGet (pConnect, E_db_tableTypeTP, E_db_listTypeNone, 0, &dbTP);

				snprintf (pMail->message, C_db_dataStringSize, "%s %s %s %s",
						  dbTP.strText, C_ao_operatorText [pAo->piMsgMin], dbPA.strValue, strUnit);
				mail_send (pMail->from, pMail->to, pMail->subject, pMail->message);

				#ifdef __ao_showDebugStrings
					fprintf (stderr, "ao_piCheckLimits: mail from:%s, to:%s, sbj:%s, msg:%s\n",
									pMail->from, pMail->to, pMail->subject, pMail->message);
				#endif
			} // if ((pMail->mode != E_mail_modeOff) && (pAo->msgLo > 0))
		} // if (pAo->piOutputMinActive == 0)
	}
	else
	{
		pAo->piOutputMinActive = 0;
	} // else if (pAo->meanValue < pAo->piOutputMin)


	// Prüfung auf Output Maximum
	if (pAo->meanValue > pAo->piOutputMax)
	{
		if (pAo->piOutputMaxActive == 0)
		{
			pAo->piOutputMaxActive = 1;
			ao_logEvent (pConnect, channel, E_db_PAoAoPiOutputMax);

			if ((pMail->mode != E_mail_modeOff) && (pAo->piMsgMax > 0))
			{
				// Mailtext zusammensetzen und senden
				dbPA.id = C_db_paChannelBaseAo + channel * C_db_paChannelOffsetAo + E_db_PAoAoPiOutputMax;
				db_rowGet (pConnect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA);

				dbTP.id	= pMail->languageOffset + C_db_tpChannelBaseAo + channel * C_db_tpChannelOffsetAo + E_db_TPoAoMsgPiHi;
				db_rowGet (pConnect, E_db_tableTypeTP, E_db_listTypeNone, 0, &dbTP);

				snprintf (pMail->message, C_db_dataStringSize, "%s %s %s %s",
						  dbTP.strText, C_ao_operatorText [pAo->piMsgMax], dbPA.strValue, strUnit);
				mail_send (pMail->from, pMail->to, pMail->subject, pMail->message);

				#ifdef __ao_showDebugStrings
					fprintf (stderr, "ao_piCheckLimits:  mail from:%s, to:%s, sbj:%s, msg:%s\n",
									pMail->from, pMail->to, pMail->subject, pMail->message);
				#endif
			} // if ((pMail->mode != E_mail_modeOff) && (pAo->msgMax > 0))
		} // if (pAo->piOutputMaxActive == 0)
	}
	else
	{
		pAo->piOutputMaxActive = 0;
	} // else if (pAo->meanValue < pAo->piOutputMax)


	// Prüfung auf Regelbandbreite Minimum
	if (pAo->meanValue < pAo->piOutputRangeLo)
	{
		// todo es müsste eine Reaktion / Event für diesen Fall spezifiziert werden
	}
	else
	{
	} // else if (pAo->meanValue < pAo->piOutputRangeLo)


	// Prüfung auf Regelbandbreite Maximum
	if (pAo->meanValue > pAo->piOutputRangeHi)
	{
		// todo es müsste eine Reaktion / Event für diesen Fall spezifiziert werden
	}
	else
	{
	} // else if (pAo->meanValue > pAo->piOutputRangeHi)
} /* ao_piCheckLimits */


/**************************************************************************//**
 *
 *  \brief		ao_limitBinary
 *
 *  \details	überprüft und begrenzt den Wertebereich des Binärwertes auf die
 *				Limiten des ltc2617.
 *
 *	\param		pBinary		Pointer auf Binärwert
 *
 *	\return		-
 *
 ******************************************************************************/
static void ao_limitBinary (int16_t *pBinary)
{
	if (*pBinary > (C_ltc2617_resolution - 1))
	{
		*pBinary = C_ltc2617_resolution - 1;
	}

	if (*pBinary < 0)
	{
		*pBinary = 0;
	}
} /* ao_limitBinary */


/**************************************************************************//**
 *
 *	\brief		ao_onFollowAi
 *
 *	\details	der analoge Ausgang oChannel folgt dem skalierten Mittelwert
 *				des analogen Eingang iChannel.
 *
 *	\param		*pIoa		Pointer auf Struktur ts_ioa
 *	\param		oChannel	Kannalnummer des analog Ausgang [0..1]
 *	\param		iChannel	Kannalnummer des digital Eingang [0..3]
 *
 *	\return		-
 *
 ******************************************************************************/
static void ao_onFollowAi (ts_ioa	*pIoa,
						   int		oChannel,
						   int		iChannel)
{
	ts_ao *pAo;


	pAo = &pIoa->AO [oChannel];

	pAo->scaledValue = pIoa->AI [iChannel].scaledValue; // ACHTUNG der kommt in mV

	// bei Berechnung Integerverhalten beachten
	pAo->meanValue	= (((pAo->scaledValue - pAo->scaleOffset) * (pAo->limitHi - pAo->limitLo)) / pAo->scaleElements) + pAo->limitLo;
	pAo->binary		= ao_getBinary (pAo->meanValue, pAo->unitVoltage);

	// 14 Bit DAC kann keine zu grossen oder negative Werte ausgeben
	ao_limitBinary (&pAo->binary);
} /* ao_onFollowAi */


/**************************************************************************//**
 *
 *	\brief		ao_onFollowDi
 *
 *  \details	folgt dem digital Eingang iChannel. Entsprechend 0/1, lo/hi wird
 *				limitLo/limitHi auf den analog Ausgang gelegt.
 *
 *	\param		*pIoa		Pointer auf Struktur ts_ioa
 *	\param		oChannel	Kannalnummer des analog Ausgang [0..1]
 *	\param		iChannel	Kannalnummer des digital Eingang [0..3]
 *
 *	\return		-
 *
 ******************************************************************************/
static void ao_onFollowDi (ts_ioa	*pIoa,
						   int		oChannel,
						   int		iChannel)
{
	ts_ao	*pAo;


	// Zugriff verkürzen
	pAo = &pIoa->AO [oChannel];

	if (pIoa->DI [iChannel].stateActual)
	{
		pAo->scaledValue = pAo->limitHi;
		pAo->binary		 = ao_getBinary (pAo->limitHi, pAo->unitVoltage);
	}
	else
	{
		pAo->scaledValue = pAo->limitLo;
		pAo->binary		 = ao_getBinary (pAo->limitLo, pAo->unitVoltage);
	} // else if (pIoa->DI [1].stateActual)

	// 14 Bit DAC kann keine zu grossen oder negative Werte ausgeben
	ao_limitBinary (&pAo->binary);
} /* ao_onFollowDi */


/**************************************************************************//**
 *
 *	\brief		ao_piFollowAi
 *
 *  \details	
 *
 *	\param		*pIoa		Pointer auf Struktur ts_ioa
 *	\param		oChannel	Kannalnummer des analog Ausgang
 *	\param		iChannel	Kannalnummer des digital Eingang
 *
 *	\attention	damit der Regler richtig funktionert, muss diese Funktion alle
 *				100 ms aufegrufen werden.
 *
 *	\return		-
 *
 ******************************************************************************/
static void ao_piFollowAi (ts_db_connect	*pConnect,
							ts_ioa			*pIoa,
							int				oChannel,
							int				iChannel,
							ts_mail			*pMail)
{
	static int		deviation;
	static int		deviationSum	= 0;
	static ts_ao	*pAo;


	pAo = &pIoa->AO [oChannel];
	
	deviation		= pIoa->AI [iChannel].meanValue - pAo->meanValue;
	deviationSum	= deviationSum + deviation;
	pAo->meanValue	= pAo->piFp * deviation + pAo->piFi_x_piTr * deviationSum;
	pAo->binary		= ao_getBinary (pAo->meanValue, pAo->unitVoltage);

	// 14 Bit DAC kann keine zu grossen oder negative Werte ausgeben
	ao_limitBinary (&pAo->binary);
} /* ao_piFollowAi */


/**************************************************************************//**
 *
 *	\brief		ao_piFollowDi
 *
 *  \details	
 *
 *	\param		*pIoa		Pointer auf Struktur ts_ioa
 *	\param		oChannel	Kannalnummer des analog Ausgang
 *	\param		iChannel	Kannalnummer des digital Eingang
 *
 *	\return		-
 *
 ******************************************************************************/
static void ao_piFollowDi (ts_ioa	*pIoa,
						   int		oChannel,
						   int		iChannel)
{
	ts_ao	*pAo;


	// Zugriff verkürzen
	pAo = &pIoa->AO [oChannel];

	if (pIoa->DI [iChannel].stateActual)
	{
		pAo->binary = ao_getBinary (pAo->limitHi, pAo->unitVoltage);
	}
	else
	{
		pAo->binary = ao_getBinary (pAo->limitLo, pAo->unitVoltage);
	} // else if (pIoa->DI [1].stateActual)

	// 14 Bit DAC kann keine zu grossen oder negative Werte ausgeben
	ao_limitBinary (&pAo->binary);
} /* ao_piFollowDi */


/******************************************************************************
 * Exported Functions                                                         *
 *                                                                            *
 ******************************************************************************/

/**************************************************************************//**
 *
 *	\brief		ao_configDebug
 *
 *	\details	schreibt die Werte der übergebenen ts_ao Struktur auf stderr
 *				aus.
 *
 *	\param		*pAo	Pointer auf Struktur ts_ao
 *	\param		channel		Kannalnummer des analog Ausgang
 *
 *	\return		-
 *
 ******************************************************************************/
void ao_configDebug (ts_ao	*pAo,
					 int	channel)
{
	fprintf (stderr, "\033[1mao_configDebug: ao%d\033[0m\n", channel);
	fprintf (stderr, "function        : %d\n", pAo->function);
	fprintf (stderr, "enableScaledLog : %d\n", pAo->enableScaledLog);
	fprintf (stderr, "limitLo         : %d\n", pAo->limitLo);
	fprintf (stderr, "limitHi         : %d\n", pAo->limitHi);
	fprintf (stderr, "unit            : %d (", pAo->unitVoltage);

	if (pAo->unitVoltage)
	{
		fprintf (stderr, "millivolt [mV])\n");
	}
	else
	{
		fprintf (stderr, "microampere [uA])\n");
	} // else if (pAo->unitVoltage)

	fprintf (stderr, "averageTime     : %d\n", pAo->averageTime);
	fprintf (stderr, "scaleUnit       : %s\n", pAo->scaleUnit);
	fprintf (stderr, "scaleElements   : %d\n", pAo->scaleElements);
	fprintf (stderr, "scaleOffset     : %d\n", pAo->scaleOffset);
	fprintf (stderr, "storageIntervall: %d\n", pAo->storageIntervall);
	fprintf (stderr, "depency         : %d\n", pAo->depency);
	fprintf (stderr, "msgTooLo        : %d\n", pAo->msgTooLo);
	fprintf (stderr, "msgOperatorTooLo: %d\n", pAo->msgOperatorTooLo);
	fprintf (stderr, "valueTooLo      : %u\n", pAo->valueTooLo);
	fprintf (stderr, "msgLo           : %d\n", pAo->msgLo);
	fprintf (stderr, "msgOperatorLo   : %d\n", pAo->msgOperatorLo);
	fprintf (stderr, "valueLo         : %u\n", pAo->valueLo);
	fprintf (stderr, "msgHi           : %d\n", pAo->msgHi);
	fprintf (stderr, "msgOperatorHi   : %d\n", pAo->msgOperatorHi);
	fprintf (stderr, "valueHi         : %u\n", pAo->valueHi);
	fprintf (stderr, "msgTooHi        : %d\n", pAo->msgTooHi);
	fprintf (stderr, "msgOperatorTooHi: %d\n", pAo->msgOperatorTooHi);
	fprintf (stderr, "valueTooHi      : %u\n", pAo->valueTooHi);
	fprintf (stderr, "piTr            : %u\n", pAo->piTr);
	fprintf (stderr, "piWsource       : %d\n", pAo->piWsource);
	fprintf (stderr, "piOutputMin     : %u\n", pAo->piOutputMin);
	fprintf (stderr, "piOutputMax     : %u\n", pAo->piOutputMax);
	fprintf (stderr, "piTimeout       : %u\n", pAo->piTimeout);
	fprintf (stderr, "piOutputDefault : %u\n", pAo->piOutputDefault);
	fprintf (stderr, "piOutputRange   : %u\n", pAo->piOutputRange);
	fprintf (stderr, "piMsgMin        : %d\n", pAo->piMsgMin);
	fprintf (stderr, "piMsgOperatorMin: %d\n", pAo->piMsgOperatorMin);
	fprintf (stderr, "piValueMin      : %u\n", pAo->piValueMin);
	fprintf (stderr, "piMsgMax        : %d\n", pAo->piMsgMax);
	fprintf (stderr, "piMsgOperatorMax: %d\n", pAo->piMsgOperatorMax);
	fprintf (stderr, "piValueMax      : %u\n", pAo->piValueMax);
} /* ao_configDebug */


/**************************************************************************//**
 *
 *  \brief		ao_configRead
 *
 *  \details	liest die Konfiguration für den Kanal channel aus der Datenbank-
 *				tabelle PA.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		*pAo		Pointer auf Struktur ts_ao
 *	\param		channel		Kannalnummer des analog Ausgang
 *
 *	\return		0: Lesen erfolgreich
 *
 ******************************************************************************/
int ao_configRead (ts_db_connect *pConnect,
                   ts_ao		 *pAo,
                   int			 channel)
{
	int	fail = 0;


	if (channel < C_ao_channelCount)
	{
		pAo->function			= ao_getDbValue (pConnect, channel, E_db_PAoAoFunction, &fail);
		pAo->enableScaledLog	= ao_getDbValue (pConnect, channel, E_db_PAoAoEnableScaledLog, &fail);
		pAo->limitLo			= ao_getDbValue (pConnect, channel, E_db_PAoAoLimitLo, &fail);
		pAo->limitHi			= ao_getDbValue (pConnect, channel, E_db_PAoAoLimitHi, &fail);
		pAo->unitVoltage		= ao_getDbValue (pConnect, channel, E_db_PAoAoUnit, &fail);
		pAo->averageTime		= ao_getDbValue (pConnect, channel, E_db_PAoAoMeanSampleCount, &fail);
		pAo->scaleElements		= ao_getDbValue (pConnect, channel, E_db_PAoAoScaleElements, &fail);
		pAo->scaleOffset		= ao_getDbValue (pConnect, channel, E_db_PAoAoScaleOffset, &fail);
		pAo->storageIntervall	= ao_getDbValue (pConnect, channel, E_db_PAoAoStorageIntervall, &fail);
		pAo->depency			= ao_getDbValue (pConnect, channel, E_db_PAoAoDepency, &fail);
		pAo->msgTooLo			= ao_getDbValue (pConnect, channel, E_db_PAoAoMsgTooLo, &fail);
		pAo->msgOperatorTooLo	= ao_getDbValue (pConnect, channel, E_db_PAoAoMsgOperatorTooLo, &fail);
		pAo->valueTooLo			= ao_getDbValue (pConnect, channel, E_db_PAoAoValueTooLo, &fail);
		pAo->msgLo				= ao_getDbValue (pConnect, channel, E_db_PAoAoMsgLo, &fail);
		pAo->msgOperatorLo		= ao_getDbValue (pConnect, channel, E_db_PAoAoMsgOperatorLo, &fail);
		pAo->valueLo			= ao_getDbValue (pConnect, channel, E_db_PAoAoValueLo, &fail);
		pAo->msgHi				= ao_getDbValue (pConnect, channel, E_db_PAoAoMsgHi, &fail);
		pAo->msgOperatorHi		= ao_getDbValue (pConnect, channel, E_db_PAoAoMsgOperatorHi, &fail);
		pAo->valueHi			= ao_getDbValue (pConnect, channel, E_db_PAoAoValueHi, &fail);
		pAo->msgTooHi			= ao_getDbValue (pConnect, channel, E_db_PAoAoMsgTooHi, &fail);
		pAo->msgOperatorTooHi	= ao_getDbValue (pConnect, channel, E_db_PAoAoMsgOperatorTooHi, &fail);
		pAo->valueTooHi			= ao_getDbValue (pConnect, channel, E_db_PAoAoValueTooHi, &fail);
		pAo->piTr				= ao_getDbValue (pConnect, channel, E_db_PAoAoPiTr, &fail);
		pAo->piTr				= 10; // feste Abtastrate von 100 ms
		pAo->piWsource			= ao_getDbValue (pConnect, channel, E_db_PAoAoPiWsource, &fail);
		pAo->piOutputMin		= ao_getDbValue (pConnect, channel, E_db_PAoAoPiOutputMin, &fail);
		pAo->piOutputMax		= ao_getDbValue (pConnect, channel, E_db_PAoAoPiOutputMax, &fail);
		pAo->piTimeout			= ao_getDbValue (pConnect, channel, E_db_PAoAoPiTimeout, &fail);
		pAo->piOutputDefault	= ao_getDbValue (pConnect, channel, E_db_PAoAoPiOutputDefault, &fail);
		pAo->piOutputRange		= ao_getDbValue (pConnect, channel, E_db_PAoAoPiOutputRange, &fail);
		pAo->piMsgMin			= ao_getDbValue (pConnect, channel, E_db_PAoAoPiMsgMin, &fail);
		pAo->piMsgOperatorMin	= ao_getDbValue (pConnect, channel, E_db_PAoAoPiMsgOperatorMin, &fail);
		pAo->piValueMin			= ao_getDbValue (pConnect, channel, E_db_PAoAoPiValueMin, &fail);
		pAo->piMsgMax			= ao_getDbValue (pConnect, channel, E_db_PAoAoPiMsgMax, &fail);
		pAo->piMsgOperatorMax	= ao_getDbValue (pConnect, channel, E_db_PAoAoPiMsgOperatorMax, &fail);
		pAo->piValueMax			= ao_getDbValue (pConnect, channel, E_db_PAoAoPiValueMax, &fail);

		pAo->piFi_x_piTr		= pAo->piFi * pAo->piTr;
		pAo->lastFunction		= E_ao_fktCount;

		// Stringwert
		if (ao_getDbText (pConnect, channel, E_db_PAoAoScaleUnit, pAo->scaleUnit) != 0)
		{
			pAo->scaleUnit [0] = '\0';
			fail = 1;
		} // if (ao_getDbValue (pConnect, channel, E_db_PAoAoScaleUnit, pAo->scaleUnit) != 0)
	}
	else
	{
		fail = 1;
	} // else if (channel < C_ao_channelCount)
	
	return (fail);
} /* ao_configRead */


/**************************************************************************//**
 *
 *  \brief		ao_configOverride
 *
 *  \details	überschreibt die Konfiguration mit Testwerten.
 *
 *	\param		*pAo		Pointer auf Struktur ts_ao
 *	\param		channel		Kannalnummer des analog Ausgang
 *
 *	\return		-
 *
 ******************************************************************************/
void ao_configOverride (ts_ao	*pAo,
						int		channel)
{
	if (channel < C_ao_channelCount)
	{
		#ifdef __ao_showDebugStrings
			fprintf (stderr, "ao_configOverride: ao%d\n", channel);
		#endif

		pAo->function			= E_ao_fktOn;
		pAo->enableScaledLog	= 1;

		if (csw_flag.unitVoltage)
		{
			pAo->limitLo			= 0;			// [mV]
			pAo->limitHi			= 10000;		// [mV]
			pAo->unitVoltage		= 1;
			pAo->scaleElements		= 10000;		// entspricht z.B [mV]
			pAo->scaleOffset		= 0;			// entspricht z.B [°C]
		}
		else
		{
			pAo->limitLo			= 0;			// [uA]
			pAo->limitHi			= 20000;		// [uA]
			pAo->unitVoltage		= 0;
			pAo->scaleElements		= 1000;			// Techdock Testwert // entspricht [10 uA]
			pAo->scaleOffset		= 0;
		} // else if (csw_flag.unitVoltage)

		pAo->averageTime		= 50;				// [0.1s] -> 5s
		pAo->storageIntervall	= 10;				// [s]
		pAo->depency			= E_ao_depFollowAI1 + channel;
//		pAo->depency			= E_ao_depFollowOff;
		pAo->msgTooLo			= 1;
		pAo->msgOperatorTooLo	= 4;
		pAo->valueTooLo			= 5;				// [°C]
		pAo->msgLo				= 1;
		pAo->msgOperatorLo		= 4;
		pAo->valueLo			= 20;				// [°C]
		pAo->msgHi				= 1;
		pAo->msgOperatorHi		= 5;
		pAo->valueHi			= 600;				// [°C]
		pAo->msgTooHi			= 1;
		pAo->msgOperatorTooHi	= 5;
		pAo->valueTooHi			= 900;				// [°C]
		pAo->piTr				= 10;				// [0.01s] feste Abtastrate von 100 ms
		pAo->piWsource			= 0;
		pAo->piOutputMin		= 0;
		pAo->piOutputMax		= 0;
		pAo->piTimeout			= 0;
		pAo->piOutputDefault	= 0;
		pAo->piOutputRange		= 0;
		pAo->piMsgMin			= 0;
		pAo->piMsgOperatorMin	= 0;
		pAo->piValueMin			= 0;
		pAo->piMsgMax			= 0;
		pAo->piMsgOperatorMax	= 0;
		pAo->piValueMax			= 0;

		pAo->piFi_x_piTr		= pAo->piFi * pAo->piTr;
		pAo->lastFunction		= E_ao_fktCount;

		snprintf (pAo->scaleUnit, C_db_dataStringSize, "°C");
	}
} /* ao_configOverride */


/**************************************************************************//**
 *
 *  \brief		ao_getBinary
 *
 *  \details	rechnet den übergebenen Wert mit Berücksichtigung der
 *				verwendeten Hardware in den Binärwert um.
 *
 *	\param		value		zu konvertierender Strom- oder Spannungswert
 *	\param		unit		0:Strom, 1:Spannung
 *
 *	\return		Binärwert
 *
 ******************************************************************************/
int16_t ao_getBinary (int16_t value,
					  uint	  unit)
{
	if (unit)
	{
		return (int16_t)(( (int32_t)value * C_ltc2617_resolution) / C_ltc2617_maxVoltage);
	}
	else
	{
		return (int16_t)(( (int32_t)value * C_ltc2617_resolution) / C_ltc2617_maxCurrent);
	}
} /* ao_getBinary */


/**************************************************************************//**
 *
 *  \brief		ao_getCurrent
 *
 *  \details	rechnet den übergebenen Binärwert mit Berücksichtigung der
 *				verwendeten Hardware in Strom [uA] um.
 *
 *	\param		binary		zu konvertierende Binärwert
 *
 *	\return		Stromwert [uA]
 *
 ******************************************************************************/
int16_t ao_getCurrent (int16_t binary)
{
	return (int16_t)(( (int32_t)binary * C_ltc2617_maxCurrent) / C_ltc2617_resolution);
} /* ao_getCurrent */


/**************************************************************************//**
 *
 *  \brief		ao_getVoltage
 *
 *  \details	rechnet den übergebenen Binärwert mit Berücksichtigung der
 *				verwendeten Hardware in Spannung [mV] um.
 *
 *	\param		binary		zu konvertierende Binärwert
 *
 *	\return		Spannungswert [mv]
 *
 ******************************************************************************/
int16_t ao_getVoltage (int16_t binary)
{
	return (int16_t)(((int32_t)binary * C_ltc2617_maxVoltage) / C_ltc2617_resolution);
} /* ao_getVoltage */


/**************************************************************************//**
 *
 *  \brief		ao_processQuick
 *
 *  \details	wertet das Signal des analogen Ausgangs ohne Datenbankeinträge
 *				aus um eine schnelle Hardwarereaktion zu ermöglichen.
 *
 *	\param		*pAo		Pointer auf Struktur ts_ao
 *	\param		channel		Kannalnummer des analog Ausgang
 *
 *	\return		-
 *
 ******************************************************************************/
void ao_processQuick (ts_ao	*pAo,
					  int	channel)
{
} /* ao_processQuick */


/**************************************************************************//**
 *
 *  \brief		ao_process
 *
 *  \details	wertet das Signal des analogen Ausgangs aus.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		*pVoid		Pointer
 *	\param		channel		Kannalnummer des analog Ausgang
 *	\param		*pMail		Pointer auf Struktur ts_mail
 *
 *	\attention	der Voidpointer muss anstelle des ts_ioa Pointers übergeben werden
 *				da sonst eine rekursive Abhängigkeit in ao.h erzeugt wird.
 *	\return		-
 *
 ******************************************************************************/
int ao_process (ts_db_connect	*pConnect,
				void			*pVoid,
				int				channel,
				ts_mail			*pMail)
{
	static ts_db_ao	dbAO;
	static ts_db_va	dbVA;
	ts_ioa			*pIoa;
	ts_ao			*pAo;
	int				i;


	pIoa	= pVoid;
	dbAO.id	= channel + 1;

	// Zugriff verkürzen
	pAo = &pIoa->AO [channel];

	if (pIoa->serviceMode)
	{
		// AO aus der Datenbank lesen
		db_rowGet (pConnect, E_db_tableTypeAO, E_db_listTypeNone, dbAO.id, &dbAO);
		pAo->binary = atoi (dbAO.strValue);

		// DAC kann keine zu grossen oder negative Werte ausgeben
		ao_limitBinary (&pAo->binary);

		#ifdef __ao_showDebugStrings
			fprintf (stderr, "ao_process: ao%d: servicemode, %d\n", channel, pAo->binary);
		#endif
	}
	else
	{
		switch (pAo->function)
		{
			case E_ao_fktOff:
				if (pAo->lastFunction != E_ao_fktOff)
				{
					pAo->lastFunction = E_ao_fktOff;
					pAo->binary = 0;
					snprintf (dbAO.strValue, C_db_dataStringSize, "off"); 

					// ohne Auswertung des Rückgabewertes
					if (db_rowUpdate (pConnect, E_db_tableTypeAO, &dbAO))
					{
						db_rowAdd (pConnect, E_db_tableTypeAO, E_db_listTypeNone, dbAO.id, &dbAO);
					}

					// skalierter Wert in VA Tabelle schreiben
					dbVA.id = E_db_VAidAo1Scaled + channel;
					snprintf (dbVA.strDescription, C_db_dataStringSize, "off");

					if (db_rowUpdate (pConnect, E_db_tableTypeVA, &dbVA))
					{
						db_rowAdd (pConnect, E_db_tableTypeVA, E_db_listTypeNone, dbVA.id, &dbVA);
					}
				} // if (pAo->lastFunction != E_ao_fktOff)

				#ifdef __ao_showDebugStrings
					fprintf (stderr, "ao_process: ao%d: off\n", channel);
				#endif
				break;

			case E_ao_fktOn:
				pAo->lastFunction = E_ao_fktOn;

				#ifdef __ao_showDebugStrings
					fprintf (stderr, "ao_process: ao%d: on(follow ", channel);
				#endif
								 
				switch (pAo->depency)
				{
					case E_ao_depFollowOff:
						// Ausgang muss im aktuellen Zustand belassen werden
						break;

					case E_ao_depFollowAI1:
						ao_onFollowAi (pIoa, channel, 0);

						#ifdef __ao_showDebugStrings
							fprintf (stderr, "ai0) (%d -> %d)\n", pIoa->AI [0].scaledValue, pAo->scaledValue);
						#endif
						break;

					case E_ao_depFollowAI2:
						ao_onFollowAi (pIoa, channel, 1);

						#ifdef __ao_showDebugStrings
							fprintf (stderr, "ai1) (%d -> %d)\n", pIoa->AI [1].scaledValue, pAo->scaledValue);
						#endif
						break;

					case E_ao_depFollowDI1:
						ao_onFollowDi (pIoa, channel, 0);

						#ifdef __ao_showDebugStrings
							fprintf (stderr, "di0) (%d -> %d)\n", pIoa->DI [0].stateActual, pAo->scaledValue);
						#endif
						break;

					case E_ao_depFollowDI2:
						ao_onFollowDi (pIoa, channel, 1);

						#ifdef __ao_showDebugStrings
							fprintf (stderr, "di1) (%d -> %d)\n", pIoa->DI [1].stateActual, pAo->scaledValue);
						#endif
						break;

					case E_ao_depFollowDI3:
						ao_onFollowDi (pIoa, channel, 2);

						#ifdef __ao_showDebugStrings
							fprintf (stderr, "di2) (%d -> %d)\n", pIoa->DI [2].stateActual, pAo->scaledValue);
						#endif
						break;

					case E_ao_depFollowDI4:
						ao_onFollowDi (pIoa, channel, 3);

						#ifdef __ao_showDebugStrings
							fprintf (stderr, "di3) (%d -> %d)\n", pIoa->DI [3].stateActual, pAo->scaledValue);
						#endif
						break;

					default:
						#ifdef __ao_showDebugStrings
							fprintf (stderr, "default -> error)\n");
						#endif
						break;

				} // switch (pAo->depency)

				ao_onCheckLimits (pConnect, pAo, channel, pMail);

				snprintf (dbAO.strValue, C_db_dataStringSize, "%d", pAo->binary); 

				if (db_rowUpdate (pConnect, E_db_tableTypeAO, &dbAO))
				{
					db_rowAdd (pConnect, E_db_tableTypeAO, E_db_listTypeNone, dbAO.id, &dbAO);
				}

				// skalierter Wert in VA Tabelle schreiben
				dbVA.id = E_db_VAidAo1Scaled + channel;
				snprintf (dbVA.strDescription, C_db_dataStringSize, "%d", pAo->scaledValue);

				if (db_rowUpdate (pConnect, E_db_tableTypeVA, &dbVA))
				{
					db_rowAdd (pConnect, E_db_tableTypeVA, E_db_listTypeNone, dbVA.id, &dbVA);
				}
				break;

			case E_ao_fktPi:
				pAo->lastFunction = E_ao_fktPi;

				#ifdef __ao_showDebugStrings
					fprintf (stderr, "ao_process: ao%d: pi(", channel);
				#endif
								 
				switch (pAo->depency)
				{
					case E_ao_depFollowOff:
						// Ausgang muss im aktuellen Zustand belassen werden
						break;

					case E_ao_depFollowAI1:
						ao_piFollowAi (pConnect, pIoa, channel, 0, pMail);

						#ifdef __ao_showDebugStrings
							fprintf (stderr, "ai0) (%d -> %d)\n", pIoa->AI [0].scaledValue, pAo->scaledValue);
						#endif
						break;

					case E_ao_depFollowAI2:
						ao_piFollowAi (pConnect, pIoa, channel, 1, pMail);

						#ifdef __ao_showDebugStrings
							fprintf (stderr, "ai1) (%d -> %d)\n", pIoa->AI [1].scaledValue, pAo->scaledValue);
						#endif
						break;

					case E_ao_depFollowDI1:
						ao_piFollowDi (pIoa, channel, 0);

						#ifdef __ao_showDebugStrings
							fprintf (stderr, "di0) ( tbd )\n");
						#endif
						break;

					case E_ao_depFollowDI2:
						ao_piFollowDi (pIoa, channel, 1);

						#ifdef __ao_showDebugStrings
							fprintf (stderr, "di1) ( tbd )\n");
						#endif
						break;

					case E_ao_depFollowDI3:
						ao_piFollowDi (pIoa, channel, 2);

						#ifdef __ao_showDebugStrings
							fprintf (stderr, "di2) ( tbd )\n");
						#endif
						break;

					case E_ao_depFollowDI4:
						ao_piFollowDi (pIoa, channel, 3);

						#ifdef __ao_showDebugStrings
							fprintf (stderr, "di3) ( tbd )\n");
						#endif
						break;

					default:
						#ifdef __ao_showDebugStrings
							fprintf (stderr, "default -> error)\n");
						#endif
						break;

				} // switch (pAo->depency)

				ao_onCheckLimits (pConnect, pAo, channel, pMail);

				snprintf (dbAO.strValue, C_db_dataStringSize, "%d", pAo->binary); 

				if (db_rowUpdate (pConnect, E_db_tableTypeAO, &dbAO))
				{
					db_rowAdd (pConnect, E_db_tableTypeAO, E_db_listTypeNone, dbAO.id, &dbAO);
				}

				// skalierter Wert in VA Tabelle schreiben
				dbVA.id = E_db_VAidAo1Scaled + channel;
				snprintf (dbVA.strDescription, C_db_dataStringSize, "%d", pAo->scaledValue);

				if (db_rowUpdate (pConnect, E_db_tableTypeVA, &dbVA))
				{
					db_rowAdd (pConnect, E_db_tableTypeVA, E_db_listTypeNone, dbVA.id, &dbVA);
				}
				break;

			default:
				pAo->lastFunction = E_ao_fktCount;

				#ifdef __ao_showDebugStrings
					fprintf (stderr, "ao_process: ao%d: default -> error\n", channel);
				#endif
				break;

		} // switch (pAo->function)
	} // else if (pIoa->serviceMode)
} /* ao_process */


/**************************************************************************//**
 *
 *	\brief		ao_logScaled
 *
 *  \details	schreibt, falls freigegeben, den Mittelwert und den skalierten
 *				Wert des analogen Ausgangs channel in die entsprechenden Daten-
 *				listen der Datenbank.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		*pAo		Pointer auf Struktur ts_ao
 *	\param		channel		Kannalnummer des analog Ausgang
 *
 *	\return		-
 *
 ******************************************************************************/
void ao_logScaled (ts_db_connect *pConnect,
				   ts_ao		 *pAo,
				   int			 channel)
{
	static ts_db_dl	dbDL;


	if (pAo->enableScaledLog)
	{
		// Mittelwert in DLAOx Tabelle schreiben (hat autoincrement)
		snprintf (dbDL.strValue, C_db_dataStringSize, "%d", pAo->meanValue);
		db_rowAdd (pConnect, E_db_tableTypeDL, E_db_listTypeAO, channel + 1, &dbDL);

		// skalierter Wert in DLVAx Tabelle schreiben (hat autoincrement)
		snprintf (dbDL.strValue, C_db_dataStringSize, "%d", pAo->scaledValue);
		db_rowAdd (pConnect, E_db_tableTypeDL, E_db_listTypeVA, E_db_VAidAo1Scaled + channel, &dbDL);

		#ifdef __ao_showDebugStrings
			fprintf (stderr, "ao_logScaled: ao%d scaled value:%d\n", channel, pAo->scaledValue);
		#endif
	} // if (pAo->enableScaledLog)
} /* ao_logScaled */


/* ao.c */

