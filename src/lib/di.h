/**************************************************************************//**
 *
 *	\file		di.h
 *
 *	\brief		Headerfile der Funktionen für die digitalen Eingänge.
 *
 *	\details	-
 *
 *	\date		10.03.2021
 *	\author		Marcel Ming
 *	\copyright	TRiMADA AG   CH-5610 Wohlen
 *	\remark		Compiler- / IDE-Version
 *
 *	\version	10.03.21 mm
 *					- Erstellt.
 *	\version	08.04.21 mm
 *					- Anpassungen an Pflichtenheft Revision 4.
 *	\version	29.04.21 mm
 *					- Tabellen Basis- und Offsetdefinitionen in die entsprechenden
 *					  dbXY verschoben.
 *	\version	01.09.21 mm
 *					- di_processQuick definiert.
 *	\version	06.09.21 mm
 *					- Compilerschalter __di_showDebugStrings definiert.
 *	\version	28.03.22 mm
 *					- Struktur ts_di mit Flag counted erweitert.
 *	\version	22.09.23 mm
 *					- in Funktion di_configOverride den nicht benötigten Parameter
 * 					  *pConnect gelöscht.
 *
 *
 ******************************************************************************/
#ifndef __di_H
    #define __di_H


/******************************************************************************
 * Compilerswitches                                                           *
 *                                                                            *
 ******************************************************************************/
//	#define __di_showDebugStrings


/******************************************************************************
 * Modules                                                                    *
 *                                                                            *
 ******************************************************************************/
#include <stdbool.h>
#include <stdint.h>

#include "db.h"
#include "mail.h"


/******************************************************************************
 * Definitions and Macros                                                     *
 *                                                                            *
 ******************************************************************************/
#define	C_di_channelCount		4


/******************************************************************************
 * Typedefs and Structures                                                    *
 *                                                                            *
 ******************************************************************************/
typedef enum {
	E_di_fktOff,					//!< ausgeschaltet 
	E_di_fktChanges,				//!< Eingang des Zustandswechsels
	E_di_fktCounter,				//!< Zähler
	E_di_fktCount					//!< Anzahl Funktionen
} te_di_function;


typedef struct {
	// Datenbank Werte
	te_di_function	function;		//!< Eingangsfunktion
	uint16_t		limitLoHi;		//!< Limit für Pegel true (Schmitt Trigger Einschaltschwelle)
	uint16_t		limitHiLo;		//!< Limit für Pegel false (Schmitt Trigger Ausschaltschwelle)
	uint8_t			enableLog :1;	//!< 1: Funktion aktiviert
	uint16_t		countTime;		//!< Zahl entspricht der Zeit in welcher die Counts aufsummiert werden [min]
	uint16_t		msgLoHi;		//!< 0: off, >0 Textnummer aus TP
	uint16_t		msgHiLo;		//!< 0: off, >0 Textnummer aus TP

	// interne Werte
	te_di_function	lastFunction;	//!< letzte Eingangsfunktion
	int16_t			binary;			//!< Eingang, Binärwert (ADC Wert)
	int16_t			voltage;		//!< Eingang, Spannungswert in mV
	uint16_t		remainingTime;	//!< verbleibende Zeit in welcher die Counts aufsummiert werden [min]
	int32_t			counter;		//!< Zähler für Counterfunktion (zählt alle Wechsel, d.h. lo->hi und hi->lo)
	uint			counted		:1;	//!< Flag Zählerereignis vorhanden
	uint			stateActual	:1;	//!< Eingang, aktueller Logikwert (0..1)
	uint			stateLogged	:1;	//!< Eingang, geloggter Logicwert (0..1)
	uint			trigger		:1;	//!< Trigger für Taster Funktion digitaler Ausgang
	uint			readError	:1;	//!< Fehler Eingang kann nicht gelesen werden, Werte ungültig
} ts_di;


/******************************************************************************
 * Exported Variables                                                         *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Exported Functions                                                         *
 *                                                                            *
 ******************************************************************************/
void di_configDebug (ts_di	*pDi,
					 int	channel);

int di_configRead (ts_db_connect	*pConnect,
                   ts_di			*pDi,
                   int				channel);

void di_configOverride (ts_di	*pDi,
						int		channel);

void di_processQuick (ts_di	*pDi,
					  int	channel);

int di_process (ts_db_connect	*pConnect,
				ts_di			*pDi,
				int				channel,
				ts_mail			*pMail);

int di_processCounter (ts_db_connect *pConnect,
					   ts_di		 *pDi,
					   int			 channel);

#endif //__di_H

/* End di.h */
