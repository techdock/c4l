/**************************************************************************//**
 *
 *	\file		ai.h
 *
 *	\brief		Headerfile der Funktionen für die analogen Eingänge.
 *
 *	\details	-
 *
 *	\date		09.03.2021
 *	\author		Marcel Ming
 *	\copyright	TRiMADA AG   CH-5610 Wohlen
 *	\remark		Compiler- / IDE-Version
 *
 *	\version	09.03.21 mm
 *					- Erstellt.
 *	\version	07.04.21 mm
 *					- Anpassungen an Pflichtenheft Revision 4.
 *	\version	29.04.21 mm
 *					- Tabellen Basis- und Offsetdefinitionen in die entsprechenden
 *					  dbXY verschoben.
 *	\version	09.08.21 mm
 *					- ai_getBinary und ai_getVoltage definiert.
 *	\version	01.09.21 mm
 *					- ai_processQuick definiert.
 *	\version	06.09.21 mm
 *					- Compilerschalter __ai_showDebugStrings definiert.
 *	\version	22.12.21 mm
 *					- in der Struktur ts_ai den Datentyp der Variablen valueTooLo,
 *					  valueLo, valueHi und valueTooHi von uint16_t auf int32_t
 *					  gesetzt. Die Spezifikation wurde in der Version 3 des
 *					  Pflichtenheftes geändert, bis dahin war der Wertebereich
 *					  mit 0 .. 1000 spezifiziert.
 *	\version	22.09.23 mm
 *					- in Funktion ai_configOverride den nicht benötigten Parameter
 * 					  *pConnect gelöscht.
 *
 *
 ******************************************************************************/
#ifndef __ai_H
    #define __ai_H


/******************************************************************************
 * Compilerswitches                                                           *
 *                                                                            *
 ******************************************************************************/
//	#define __ai_showDebugStrings
//	#define __ai_showCheckLimitsStrings


/******************************************************************************
 * Modules                                                                    *
 *                                                                            *
 ******************************************************************************/
#include <stdbool.h>
#include <stdint.h>

#include "db.h"
#include "mail.h"


/******************************************************************************
 * Definitions and Macros                                                     *
 *                                                                            *
 ******************************************************************************/
#define	C_ai_channelCount			2
#define	C_ai_maxAverageSampleCount	600


/******************************************************************************
 * Typedefs and Structures                                                    *
 *                                                                            *
 ******************************************************************************/
typedef enum {
	E_ai_opUndef,
	E_ai_opEqual,				//!< == 
	E_ai_opNotEqual,			//!< !=
	E_ai_opGreater,				//!< >
	E_ai_opLess,				//!< <
	E_ai_opGreaterOrEqual,		//!< >=
	E_ai_opLessOrEqual,			//!< <=
} te_ai_operator;


typedef struct {
	int16_t		data [C_ai_maxAverageSampleCount];	//!< Datenarray für Mittelwertbildung
	int64_t		sum;								//!< Summe aller Datenpunkte für Mittelwertbildung
	int			posNewest;							//!< Position des neusten Datenpunktes
	int			posOldest;							//!< Position des ältesten Datenpunktes
	int			posCount;							//!< Anzahl gültiger Datenpunkte
	int			posMax;								//!< Anzahl Datenpunkte
} ts_ai_mean;


typedef struct {
	// Datenbank Werte
	uint8_t			enableFunction:1;	//!< 1: Funktion aktiviert
	uint8_t			enableScaledLog:1;	//!< 1: Aufzeichnung skaliert aktiviert
	int32_t			limitLo;			//!< Pegel tief z.B. 4000 uA
	int32_t			limitHi;			//!< Pegel hoch z.B. 20000 uA
	int16_t			meanSampleCount;	//!< Zahl entspricht der Zeit in welcher die Samples gesammelt werden [0.1s]
	int32_t			scaleElements;		//!< (limitHi - limitLo) wird auf n Elemente abgebildet
	int32_t			scaleOffset;		//!< scaleElements - scaleOffset
	int32_t			storageIntervall;	//!< Taktzeit zum Speichern des aktuellen Wertes [s]
	int16_t			msgTooLo;			//!< 0: off, >0 Textnummer aus TP
	te_ai_operator	msgOperatorTooLo;	//!< Operator für Text zu tief in EV und SMTP
	int32_t			valueTooLo;			//!< Wert "zu tief"
	uint16_t		msgLo;				//!< 0: off, >0 Textnummer aus TP
	te_ai_operator	msgOperatorLo;		//!< Operator für Text tief in EV und SMTP
	int32_t			valueLo;			//!< Wert "tief"
	uint16_t		msgHi;				//!< 0: off, >0 Textnummer aus TP
	te_ai_operator	msgOperatorHi;		//!< Operator für Text hoch in EV und SMTP
	int32_t			valueHi;			//!< Wert "hoch"
	uint16_t		msgTooHi;			//!< 0: off, >0 Textnummer aus TP
	te_ai_operator	msgOperatorTooHi;	//!< Operator für Text zu hoch in EV und SMTP
	int32_t			valueTooHi;			//!< Wert "zu hoch"
	char			scaleUnit [C_db_dataStringSize];	//!< Einheit zum DP Register skaliert

	// interne Werte
	int16_t			binary;						//!< Eingang als Binärwert (ADC Wert)
	int16_t			binaryMean;					//!< Eingang gemittelt als Binärwert
	int16_t			voltage;					//!< Mittelwert als Spannungswert [mV]
	int16_t			current;					//!< Mittelwert als Stromwert [uA]
	uint16_t		counter100ms;				//!< 100 ms Zähler für Sample Zeit
	int32_t			meanValue;					//!< Mittelwert (Spannung oder Strom zum weiterrechnen)
	int32_t			scaledValue;				//!< skalierter Wert
	int32_t			storageIntervallCounter;	//!< Taktzeit zum Speichern des aktuellen Wertes [s]
	ts_ai_mean		mean;						//!< Struktur für Mittelwertbildung
	uint			lastEnableFunction	: 1;	//!< 1: Funktion aktiviert
	uint			readError			: 1;	//!< Fehler Eingang kann nicht gelesen werden, Werte ungültig
	uint			unitVoltage			: 1;	//!< 0: Strommessung, 1: Spannungsmessung
	uint			valueTooLoActive	: 1;	//!< Wert als zu tief erkannt
	uint			valueLoActive		: 1;	//!< Wert als tief erkannt
	uint			valueHiActive		: 1;	//!< Wert als hoch erkannt
	uint			valueTooHiActive	: 1;	//!< Wert als zu hoch erkannt
} ts_ai; 


/******************************************************************************
 * Exported Variables                                                         *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Exported Functions                                                         *
 *                                                                            *
 ******************************************************************************/
void ai_configDebug (ts_ai	*pAi,
					 int	channel);

int ai_configRead (ts_db_connect	*pConnect,
				   ts_ai			*pAi,
				   int				channel);

void ai_configOverride (ts_ai	*pAi,
						int		channel);

void ai_initMean (ts_ai_mean	*pMean,
				  uint16_t		meanCount);

int16_t ai_getBinary (int16_t voltage);
int16_t ai_getCurrent (int16_t binary);
int16_t ai_getVoltage (int16_t binary);

void ai_processQuick (ts_ai	*pAi,
					  int	channel);

int ai_process (ts_db_connect	*pConnect,
				ts_ai			*pAi,
				int				channel,
				ts_mail			*pMail);

void ai_logScaled (ts_db_connect *pConnect,
				   ts_ai		 *pAi,
				   int			 channel);


#endif //__ai_H

/* End ai.h */
