/**************************************************************************//**
 *
 *	\file		va.c
 *
 *	\brief		Stellt die Funktionen für die Verwaltung der freien Betriebswert,
 *				Start- oder Stundenzähler zur Verfügung.
 *
 *	\details	-
 *
 *	\date		26.09.2023
 *	\author		Marcel Ming
 *	\copyright	TRiMADA AG   CH-5610 Wohlen
 *	\remark		Compiler- / IDE-Version
 *
 *	\version	26.09.23 mm
 *					- Erstellt.
 *	\version	16.10.23 mm
 *					- te_va_source angepasst.
 *	\version	19.10.23 mm
 *					- Funktion db_vaNumberToFlagIndex als va_numberToFlagIndex
 * 					  in diesem Modul implementiert um Abhängkeiten bei Linken
 * 					  zu reduzieren.
 *	\version	26.10.23 mm
 *					- Funktion va_logEvent implementiert.
 *					- Aufrufe von va_logEvent aus va_checkLimits implementiert.
 *	\version	22.11.23 mm
 *					- Funktion va_configRead korrigiert, es wurde mit falschen
 *					  Indizes gelesen.
 *					- Funktion va_configDebug erweitert.
 *	\version	27.11.23 mm
 *					- Funktion va_configRead modifiziert. Die Variable counter
 *					  wird mit dem Wert aus der Datenbank initialisiert.
 *					- Funktion va_getDbCounter implementiert.
 *	\version	08.12.23 mm
 *					- Textdefinitionen zu te_va_source definiert.
 *					- DI's und DO's zählen nur die positive Flanke.
 *					- Funktion va_checkCondition implementiert, da diese
 *					  Bedingungen von mehreren Zählertypen geprüft werden müssen.
 *	\version	23.01.24 mm
 *					- in Funktion va_checkLimits die Tests auf toolo, lo, hi und
 *					  toohi unabhämgig voneinander gemacht, da diese als Wert 1-4
 *					  in der Datenbank eingetragen werden und daher völlig
 *					  zusammenhangslos betrachtet werden müssen.
 *					- zuvor auskommentierter Code gelöscht.
 *	\version	15.02.24 mm
 *					- in Funktion va_checkLimits werden neu alle Limiten (Wert1 -
 *					  Wert4) auf grösser gleich (>=) getestet.
 *
 *
 ******************************************************************************/


/******************************************************************************
 * Compilerswitches                                                           *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Modules                                                                    *
 *                                                                            *
 ******************************************************************************/
#include <mysql/mysql.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "ai.h"
#include "ao.h"
#include "csw.h"
#include "db.h"
#include "di.h"
#include "do.h"
#include "ioa.h"
#include "mail.h"
#include "va.h"


/******************************************************************************
 * Definitions and Macros                                                     *
 *                                                                            *
 ******************************************************************************/
#define __GNU_SOURCE


/******************************************************************************
 * Typedefs and Structures                                                    *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Exported Variables                                                         *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Global Variables                                                           *
 *                                                                            *
 ******************************************************************************/
const char* const C_va_operatorText [] = {
	"  \0",
	"==\0",
	"<>\0",
	">\0",
	"<\0",
	">=\0",
	"<=\0"
};

// Textdefinitionen zu te_va_source
const char *C_va_source [E_va_source_count] = {
	"",
	"DI1",
	"DI2",
	"DI3",
	"DI4",
	"DO1",
	"DO2",
	"DO3",
	"DO4",
	"VA1",
	"VA2",
	"VA3",
	"VA4",
};


/******************************************************************************
 * Local Functions                                                            *
 *                                                                            *
 ******************************************************************************/


/**************************************************************************//**
 *
 *	\brief		va_checkConditionDi
 *
 *  \details	prüft ob die Bedingungen für ein Weiterzählen durch den
 *				übergebenen Digitaleingang erfüllt sind.
 *
 *	\param		*pVA		Pointer auf Struktur ts_va (Variable)
 *	\param		*pDi		Pointer auf Struktur ts_di (Digitaleingang)
 *
 *	\return		0: Bedingung nicht erfüllt
 *				1: Bedingung erfüllt
 *
 ******************************************************************************/
static uint8_t va_checkConditionDi (ts_va	*pVa,
									ts_di	*pDi)
{
	uint8_t	retValue = 0;


	#ifdef __va_showCheckConditionStrings
		fprintf (stderr, "va_checkConditionDi: conditiontype: %d, actual: %d, value: %d, result: ",
		         pVa->startCondition, pDi->stateActual, pVa->valueCondition);
	#endif

	switch (pVa->startCondition)
	{
		case E_va_scGreaterOrEqual:
			if (pDi->stateActual >= pVa->valueCondition)
			{
				retValue = 1;
			}
			break;

		case E_va_scLessOrEqual:
			if (pDi->stateActual <= pVa->valueCondition)
			{
				retValue = 1;
			}
			break;

		default:
			break;

	} // switch (pVa->startCondition)

	#ifdef __va_showCheckConditionStrings
		fprintf (stderr, "%d\n", retValue);
	#endif

	return retValue;
} /* va_checkConditionDi */


/**************************************************************************//**
 *
 *	\brief		va_checkConditionDo
 *
 *  \details	prüft ob die Bedingungen für ein Weiterzählen durch den
 *				übergebenen Digitalausgang erfüllt sind.
 *
 *	\param		*pVA		Pointer auf Struktur ts_va (Variable)
 *	\param		*pDo		Pointer auf Struktur ts_do (Digitalausgang)
 *
 *	\return		0: Bedingung nicht erfüllt
 *				1: Bedingung erfüllt
 *
 ******************************************************************************/
static uint8_t va_checkConditionDo (ts_va	*pVa,
									ts_do	*pDo)
{
	uint8_t	retValue = 0;


	#ifdef __va_showCheckConditionStrings
		fprintf (stderr, "va_checkConditionDo: ");
	#endif

	switch (pVa->startCondition)
	{
		case 1:
			if (pDo->coil >= pVa->valueCondition)
			{
				retValue = 1;
			}
			break;

		case 2:
			if (pDo->coil <= pVa->valueCondition)
			{
				retValue = 1;
			}
			break;

		default:
			break;

	} // switch (pVa->startCondition)

	#ifdef __va_showCheckConditionStrings
		fprintf (stderr, "%d\n", retValue);
	#endif

	return retValue;
} /* va_checkConditionDo */


/**************************************************************************//**
 *
 *	\brief		va_checkConditionAi
 *
 *  \details	prüft ob die Bedingungen für ein Weiterzählen durch den
 *				übergebenen Analogeingang erfüllt sind.
 *				Bedingung genauer erklärt im Mail und Anhang vom 13.12.23.
 *
 *	\param		*pVA		Pointer auf Struktur ts_va (Variable)
 *	\param		*pAi		Pointer auf Struktur ts_ai (Analogeingang)
 *
 *	\return		0: Bedingung nicht erfüllt
 *				1: Bedingung erfüllt
 *
 ******************************************************************************/
static uint8_t va_checkConditionAi (ts_va	*pVa,
									ts_ai	*pAi)
{
	switch (pVa->startCondition)
	{
		case E_va_scGreaterOrEqual: //!< Bedingung >=
			// Zähler ein, wenn Value >= Condition
			// Zähler aus, wenn Value <= Condition + Hysterese
			#ifdef __va_showCheckConditionStrings
				fprintf (stderr, "va_checkConditionAi: >=, scaledValue: %d, valueCondition: %d, hysteresis: %d ",
								pAi->scaledValue, pVa->valueCondition, pVa->hysteresis);
			#endif

			if (pVa->counterEnabled)
			{
				// Test für Ausschalten
				if (pAi->scaledValue <= (pVa->valueCondition + pVa->hysteresis))
				{
					pVa->counterEnabled = 0;
				}
			}
			else
			{
				// Test für Einschalten
				if (pAi->scaledValue >= pVa->valueCondition)
				{
					pVa->counterEnabled = 1;
				}
			} // else if (pVa->counterEnabled)

			break;

		case E_va_scLessOrEqual: //!< Bedingung <=
			// Zähler ein, wenn Value <= Condition
			// Zähler aus, wenn Value >= Condition + Hysterese
			#ifdef __va_showCheckConditionStrings
				fprintf (stderr, "va_checkConditionAi: <=, scaledValue: %d, valueCondition: %d, hysteresis: %d ",
								pAi->scaledValue, pVa->valueCondition, pVa->hysteresis);
			#endif

			if (pVa->counterEnabled)
			{
				// Test für Ausschalten
				if (pAi->scaledValue >= (pVa->valueCondition + pVa->hysteresis))
				{
					pVa->counterEnabled = 0;
				}
			}
			else
			{
				// Test für Einschalten
				if (pAi->scaledValue <= pVa->valueCondition)
				{
					pVa->counterEnabled = 1;
				}
			} // else if (pVa->counterEnabled)

			break;

		default:
			break;

	} // switch (pVa->startCondition)

	#ifdef __va_showCheckConditionStrings
		fprintf (stderr, "result: %d\n", pVa->counterEnabled);
	#endif

	return pVa->counterEnabled;
} /* va_checkConditionAi */


/**************************************************************************//**
 *
 *	\brief		va_checkConditionAo
 *
 *  \details	prüft ob die Bedingungen für ein Weiterzählen durch den
 *				übergebenen Analogausgang erfüllt sind.
 *
 *	\param		*pVA		Pointer auf Struktur ts_va (Variable)
 *	\param		*pAo		Pointer auf Struktur ts_ao (Analogausgang)
 *
 *	\return		0: Bedingung nicht erfüllt
 *				1: Bedingung erfüllt
 *
 ******************************************************************************/
static uint8_t va_checkConditionAo (ts_va	*pVa,
									ts_ao	*pAo)
{
	#ifdef __va_showCheckConditionStrings
		fprintf (stderr, "va_checkConditionAo: %d, scaledValue: %d, valueCondition: %d, hysteresis: %d ",
						pVa->startCondition, pAo->scaledValue, pVa->valueCondition, pVa->hysteresis);
	#endif

	switch (pVa->startCondition)
	{
		case E_va_scGreaterOrEqual: //!< Bedingung >=
			// Zähler ein, wenn Value >= Condition
			// Zähler aus, wenn Value <= Condition + Hysterese
			if (pVa->counterEnabled)
			{
				// Test für Ausschalten
				if (pAo->scaledValue <= (pVa->valueCondition + pVa->hysteresis))
				{
					pVa->counterEnabled = 0;
				}
			}
			else
			{
				// Test für Einschalten
				if (pAo->scaledValue >= pVa->valueCondition)
				{
					pVa->counterEnabled = 1;
				}
			} // else if (pVa->counterEnabled)

			break;

		case E_va_scLessOrEqual: //!< Bedingung <=
			// Zähler ein, wenn Value <= Condition
			// Zähler aus, wenn Value >= Condition + Hysterese
			if (pVa->counterEnabled)
			{
				// Test für Ausschalten
				if (pAo->scaledValue >= (pVa->valueCondition + pVa->hysteresis))
				{
					pVa->counterEnabled = 0;
				}
			}
			else
			{
				// Test für Einschalten
				if (pAo->scaledValue <= pVa->valueCondition)
				{
					pVa->counterEnabled = 1;
				}
			} // else if (pVa->counterEnabled)

			break;

		default:
			break;

	} // switch (pVa->startCondition)

	#ifdef __va_showCheckConditionStrings
		fprintf (stderr, "result: %d\n", pVa->counterEnabled);
	#endif

	return pVa->counterEnabled;
} /* va_checkConditionAo */


/**************************************************************************//**
 *
 *	\brief		va_checkCondition
 *
 *  \details	prüft ob die Bedingungen für ein Ereignis erfüllt sind.
 *
 *	\param		*pVA		Pointer auf Struktur ts_va (Variable)
 *	\param		*pIao		Pointer auf Struktur ts_iao (alle Ereignisquellen)
 *
 *	\return		0: Bedingung nicht erfüllt
 *				1: Bedingung erfüllt
 *
 ******************************************************************************/
static uint8_t va_checkCondition (ts_va	 *pVa,
								  ts_ioa *pIoa)
{
	switch (pVa->source)
	{
		case E_va_source_DI1:
			pVa->resultCondition = va_checkConditionDi (pVa, &pIoa->DI [0]);
			break;

		case E_va_source_DI2:
			pVa->resultCondition = va_checkConditionDi (pVa, &pIoa->DI [1]);
			break;

		case E_va_source_DI3:
			pVa->resultCondition = va_checkConditionDi (pVa, &pIoa->DI [2]);
			break;

		case E_va_source_DI4:
			pVa->resultCondition = va_checkConditionDi (pVa, &pIoa->DI [3]);
			break;

		case E_va_source_DO1:
			pVa->resultCondition = va_checkConditionDo (pVa, &pIoa->DO [0]);
			break;

		case E_va_source_DO2:
			pVa->resultCondition = va_checkConditionDo (pVa, &pIoa->DO [1]);
			break;

		case E_va_source_DO3:
			pVa->resultCondition = va_checkConditionDo (pVa, &pIoa->DO [2]);
			break;

		case E_va_source_DO4:
			pVa->resultCondition = va_checkConditionDo (pVa, &pIoa->DO [3]);
			break;

		case E_va_source_VA1:
			pVa->resultCondition = va_checkConditionAi (pVa, &pIoa->AI [0]);
			break;

		case E_va_source_VA2:
			pVa->resultCondition = va_checkConditionAi (pVa, &pIoa->AI [1]);
			break;

		case E_va_source_VA3:
			pVa->resultCondition = va_checkConditionAo (pVa, &pIoa->AO [0]);
			break;

		case E_va_source_VA4:
			pVa->resultCondition = va_checkConditionAo (pVa, &pIoa->AO [1]);
			break;

		default:
			break;

	} // switch (pVa->source)

	return pVa->resultCondition;
} /* va_checkCondition */


/**************************************************************************//**
 *
 *	\brief		va_getDbValue
 *
 *  \details	liest den Konfigurationswert dbIdOffset für die Variable vaNumber
 *				aus der Datenbanktabelle PA.
 *	\attention	Die vaNumber darf sich nur im Bereich zwischen C_va_counterMinNumber
 *				und C_va_counterMaxNumber bewegen.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		vaNumber	Variablennummer
 *	\param		dbIdOffset	Datenbank id Offset zur Basis id des Eintrags.
 *	\param		*pFail		Resultat Flag, wird bei einem Fehler auf 1 gesetzt
 *
 *	\return		0: Lesen erfolgreich
 *
 ******************************************************************************/
static int va_getDbValue (ts_db_connect		 *pConnect,
						  int				 vaNumber,
						  te_db_PAidOffsetVa dbIdOffset,
						  int				 *pFail)
{
	static ts_db_pa dbPA;


	dbPA.id = C_db_paCounterBase + ((vaNumber - C_va_counterMinNumber) * C_db_paCounterOffset) + dbIdOffset;

	if (db_rowGet (pConnect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA) == 0)
	{
		return (atoi (dbPA.strValue));
	}
	else
	{
		*pFail = 1;

		return (0);
	} // else if (db_rowGet (pConnect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA) == 0)
} /* va_getDbValue */


/**************************************************************************//**
 *
 *	\brief		va_logEvent
 *
 *	\details	schreibt die Eventnummer, welche dem PA Index entspricht in die
 *				EV Tabelle.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		vaNumber	Variablennummer
 *	\param		idOffset	Offset für Event Wert
 *
 *	\return		-
 *
 ******************************************************************************/
static void va_logEvent (ts_db_connect		*pConnect,
						 int				vaNumber,
						 te_db_PAidOffsetVa	idOffset)
{
	static ts_db_ev	dbEV;


	// PA Index ermitteln und in Eventliste eintragen
	snprintf (dbEV.strValue, C_db_dataStringSize, "%d", C_db_paCounterBase + vaNumber * C_db_paCounterOffset + idOffset);
	snprintf (dbEV.strDateTime, C_db_dataStringSize, "%s",  "");
	db_rowAdd (pConnect, E_db_tableTypeEV, E_db_listTypeNone, 0, &dbEV);

	#ifdef __va_showDebugStrings
		fprintf (stderr, "va_logEvent: va%d EV:%s\n", vaNumber, dbEV.strValue);
	#endif
} /* va_logEvent */


/**************************************************************************//**
 *
 *	\brief		va_mailEvent
 *
 *	\details	setzt den Mailtext zusammen und sendet ein Mail an den Empfänger
 *				gemäss ts_mail.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		vaNumber	Variablennummer
 *	\param		PAidOffset	Parameter Offset für Event Wert
 *	\param		TPidOffset	Textparameter Offset für Event Wert
 *	\param		operator	Operatornummer für Texttabelle
 *	\param		*pMail		Pointer auf Mail
 *
 *	\return		-
 *
 ******************************************************************************/
static void va_mailEvent (ts_db_connect		 *pConnect,
						  ts_va				 *pVa,
						  int				 vaNumber,
						  te_db_PAidOffsetVa PAidOffset,
						  te_db_TPidOffsetVa TPidOffset,
						  te_va_operator	 operator,
						  ts_mail			 *pMail)
{
	static ts_db_pa	dbPA;
	static ts_db_tp	dbTP;

	// Mailtext zusammensetzen und senden
	//			701						9				20					8
	dbPA.id = C_db_paCounterBase + ((vaNumber - C_va_counterMinNumber) * C_db_paCounterOffset) + PAidOffset;
	db_rowGet (pConnect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA);

	dbTP.id = pMail->languageOffset + C_db_tpCounterBase + ((vaNumber - C_va_counterMinNumber) * C_db_tpCounterOffset) + TPidOffset;
	db_rowGet (pConnect, E_db_tableTypeTP, E_db_listTypeNone, 0, &dbTP);

	snprintf (pMail->message, C_db_dataStringSize, "%s %s %s %s",
			  dbTP.strText, C_va_operatorText [operator], dbPA.strValue, C_va_source [pVa->source]);

	mail_send (pMail->from, pMail->to, pMail->subject, pMail->message);
} /* va_mailEvent */


/**************************************************************************//**
 *
 *	\brief		va_checkLimits
 *
 *	\details	überprüft den Wert auf die verschiedenen Limiten und reagiert
 *				entsprechend.
 *
 *	\attention	Alle Limiten (Wert1 - Wert4) werden auf grösser gleich (>=)
 *				getestet.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		*pVa		Pointer auf Struktur ts_va
 *	\param		vaNumber	Variablennummer
 *	\param		*pMail		Pointer auf Struktur ts_mail
 *
 *	\return		-
 *
 ******************************************************************************/
static void va_checkLimits (ts_db_connect	*pConnect,
							ts_va			*pVa,
							int				vaNumber,
							ts_mail			*pMail)
{
	static ts_db_pa	dbPA;
	static ts_db_tp	dbTP;
	static char		strUnit [C_db_dataStringSize];


	// Unit lesen
	dbPA.id = C_db_paCounterBase + vaNumber * C_db_paCounterOffset + E_db_PAoVaUnit;
	db_rowGet (pConnect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA);
	snprintf (strUnit, C_db_dataStringSize, "%s", dbPA.strValue);

	#ifdef __va_showCheckLimitsStrings
		fprintf (stderr, "va_checkLimits: VA%d: counter:%d, <<:%d, <:%d, >:%d, >>:%d\n",
							vaNumber, pVa->counter, pVa->valueTooLo, pVa->valueLo, pVa->valueHi, pVa->valueTooHi);
		fprintf (stderr, "                VA%d msg <<:%d, <:%d, >:%d, >>:%d\n",
							vaNumber, pVa->msgTooLo, pVa->msgLo, pVa->msgHi, pVa->msgTooHi);
	#endif


	// Prüfung auf Wert 1 (vorher "too lo")
	if ((pVa->counter >= pVa->valueTooLo) && (pVa->msgTooLo > 0))
	{
		if (pVa->valueTooLoActive == 0)
		{
			pVa->valueTooLoActive = 1;

			#ifdef __va_showCheckLimitsStrings
				fprintf (stderr, "va_checkLimits: VA%d 'too lo'\n", vaNumber);
			#endif

			va_logEvent (pConnect, vaNumber, E_db_PAoVaValueTooLo - 180);

			if ((pMail->mode != E_mail_modeOff) && (pVa->msgTooLo > 0))
			{
				va_mailEvent (pConnect, pVa, vaNumber,
							  E_db_PAoVaValueTooLo,
							  E_db_TPoVaMsgTooLo,
							  pVa->msgOperatorTooLo, pMail);
			} // if ((pMail->mode != E_mail_modeOff) && (pAi->msgTooLo > 0))
		} // if (pVa->valueTooLoActive == 0)
	}
	else
	{
		if (pVa->valueTooLoActive)
		{
			pVa->valueTooLoActive = 0;
		} // if (pVa->valueTooLoActive)
	} // else if ((pVa->counter < pVa->valueTooLo) ...


	// Prüfung auf Wert 2 (vorher "lo")
	if ((pVa->counter >= pVa->valueLo) && (pVa->msgLo > 0))
	{
		if (pVa->valueLoActive == 0)
		{
			pVa->valueLoActive = 1;

			#ifdef __va_showCheckLimitsStrings
				fprintf (stderr, "va_checkLimits: VA%d 'lo'\n", vaNumber);
			#endif

			va_logEvent (pConnect, vaNumber, E_db_PAoVaValueLo -180);

			if ((pMail->mode != E_mail_modeOff) && (pVa->msgLo > 0))
			{
				va_mailEvent (pConnect, pVa, vaNumber,
							  E_db_PAoVaValueLo,
							  E_db_TPoVaMsgLo,
							  pVa->msgOperatorLo, pMail);
			} // if ((pMail->mode != E_mail_modeOff) && (pVa->msgLo > 0))
		} // if (pVa->valueLoActive == 0)
	}
	else
	{
		if (pVa->valueLoActive)
		{
			pVa->valueLoActive = 0;
		} // if (pVa->valueLoActive)
	} // else if (pVa->counter < pVa->valueLo) ...


	// Prüfung auf Wert 3 (vorher "hi")
	if ((pVa->counter >= pVa->valueHi) && (pVa->msgHi > 0))
	{
		if (pVa->valueHiActive == 0)
		{
			pVa->valueHiActive = 1;

			#ifdef __va_showCheckLimitsStrings
				fprintf (stderr, "va_checkLimits: VA%d 'hi'\n", vaNumber);
			#endif

			va_logEvent (pConnect, vaNumber, E_db_PAoVaValueHi - 180);

			if ((pMail->mode != E_mail_modeOff) && (pVa->msgHi > 0))
			{
				va_mailEvent (pConnect, pVa, vaNumber,
							  E_db_PAoVaValueHi,
							  E_db_TPoVaMsgHi,
							  pVa->msgOperatorHi, pMail);
			} // if ((pMail->mode != E_mail_modeOff) && (pVa->msgLo > 0))
		} // if (pVa->valueLoActiv	ts_db_va	dbVA;
	}
	else
	{
		if (pVa->valueHiActive)
		{
			pVa->valueHiActive = 0;
		} // if (pVa->valueHiActive)
	} // else if (pVa->counter < pVa->valueHi) ...


	// Prüfung auf Wert 4 (vorher "too hi")
	if ((pVa->counter >= pVa->valueTooHi) && (pVa->msgTooHi > 0))
	{
		if (pVa->valueTooHiActive == 0)
		{
			pVa->valueTooHiActive = 1;

			#ifdef __va_showCheckLimitsStrings
				fprintf (stderr, "va_checkLimits: VA%d 'too hi'\n", vaNumber);
			#endif

			va_logEvent (pConnect, vaNumber, E_db_PAoVaValueTooHi - 180);

			if ((pMail->mode != E_mail_modeOff) && (pVa->msgTooHi > 0))
			{
				va_mailEvent (pConnect, pVa, vaNumber,
							  E_db_PAoVaValueTooHi,
							  E_db_TPoVaMsgTooHi,
							  pVa->msgOperatorTooHi, pMail);
			} // if ((pMail->mode != E_mail_modeOff) && (pAi->msgLo > 0))
		} // if (pVa->valueLoActive == 0)
	}
	else
	{
		if (pVa->valueTooHiActive)
		{
			pVa->valueTooHiActive = 0;
		} // if (pVa->valueTooHiActive)
	} // else if (pVa->counter < pVa->valueTooHi) ...
} /* va_checkLimits */


/**************************************************************************//**
 *
 *  \brief		va_getDbCounter
 *
 *  \details	liest den aktuellen Zählerstand von pVA.id aus der
 *				Datenbank pConnect und schreibt ihn in pVa.counter.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		*pVa		Pointer auf Struktur ts_va
 *	\param		vaNumber	Variablennummer
 *
 *	\return		-
 *
 ******************************************************************************/
void va_getDbCounter (ts_db_connect *pConnect,
					  ts_va			*pVa,
					  int			vaNumber)
{
	ts_db_va	dbVA;


	dbVA.id = vaNumber;

	// VA lesen
	if (db_rowGet (pConnect, E_db_tableTypeVA, E_db_listTypeNone, 0, &dbVA) == 0)
	{
		pVa->counter = atoi (dbVA.strDescription);
	}
	else
	{
		pVa->counter = 0;
	} // else if (db_rowGet (pConnect, E_db_tableTypeVA, &dbVA) == 0)
} /* va_getDbCounter */


/******************************************************************************
 * Exported Functions                                                         *
 *                                                                            *
 ******************************************************************************/

/**************************************************************************//**
 *
 *  \brief		va_numberToFlagIndex
 *
 *  \details	gibt zur vaNummer den Index für das writePermission Flag zurück.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		vaNumber	Variablennummer
 *
 *	\return		mysql_errno
 *
 ******************************************************************************/
te_db_flag va_numberToFlagIndex (ts_db_connect *pConnect,
								   int			 vaNumber)
{
	if ((vaNumber >= C_va_counterMinNumber) &&
		(vaNumber <= C_va_counterMaxNumber))
	{
		switch (vaNumber)
		{
			case C_va_counterMinNumber	  : return E_db_flagVa9writePermission;  break;
			case C_va_counterMinNumber + 1: return E_db_flagVa10writePermission; break;
			case C_va_counterMinNumber + 2: return E_db_flagVa11writePermission; break;
			case C_va_counterMinNumber + 3: return E_db_flagVa12writePermission; break;
			case C_va_counterMinNumber + 4: return E_db_flagVa13writePermission; break;
			case C_va_counterMinNumber + 5: return E_db_flagVa14writePermission; break;
			case C_va_counterMinNumber + 6: return E_db_flagVa15writePermission; break;
			case C_va_counterMinNumber + 7: return E_db_flagVa16writePermission; break;
		} // switch (vaNumber)
	} // if ((vaNumber >= C_va_counterMinNumber) && ...
} /* va_numberToFlagIndex */


/**************************************************************************//**
 *
 *	\brief		va_configDebug
 *
 *	\details	schreibt die Werte der übergebenen ts_va Struktur auf stderr
 *				aus.
 *
 *	\param		*pVa		Pointer auf Struktur ts_va
 *	\param		vaNumber	Variablennummer
 *
 *	\return		-
 *
 ******************************************************************************/
void va_configDebug (ts_va	*pVa,
					 int	vaNumber)
{
	fprintf (stderr, "\033[1mva_configDebug: va%d\033[0m\n", vaNumber);
	fprintf (stderr, "function        : %d\n", pVa->function);
	fprintf (stderr, "type            : %d\n", pVa->type);
	fprintf (stderr, "source          : %d\n", pVa->source);
	fprintf (stderr, "startCondition  : %d\n", pVa->startCondition);
	fprintf (stderr, "valueCondition  : %d\n", pVa->valueCondition);
	fprintf (stderr, "hysteresis      : %d\n", pVa->hysteresis);
	fprintf (stderr, "msgSmtpTooLo    : %d\n", pVa->msgTooLo);
	fprintf (stderr, "msgOperatorTooLo: %d\n", pVa->msgOperatorTooLo);
	fprintf (stderr, "valueTooLo      : %d\n", pVa->valueTooLo);
	fprintf (stderr, "msgSmtpLo       : %d\n", pVa->msgLo);
	fprintf (stderr, "msgOperatorLo   : %d\n", pVa->msgOperatorLo);
	fprintf (stderr, "valueLo         : %d\n", pVa->valueLo);
	fprintf (stderr, "msgSmtpHi       : %d\n", pVa->msgHi);
	fprintf (stderr, "msgOperatorHi   : %d\n", pVa->msgOperatorHi);
	fprintf (stderr, "valueHi         : %d\n", pVa->valueHi);
	fprintf (stderr, "msgSmtpTooHi    : %d\n", pVa->msgTooHi);
	fprintf (stderr, "msgOperatorTooHi: %d\n", pVa->msgOperatorTooHi);
	fprintf (stderr, "valueTooHi      : %d\n", pVa->valueTooHi);
} /* va_configDebug */


/**************************************************************************//**
 *
 *  \brief		va_configRead
 *
 *  \details	liest die Konfiguration für die Variable vaNumber aus der
 *				Datenbanktabelle PA.
 *				Die Variable counter wird mit dem Wert aus der Datenbank
 *				initialisiert.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		*pVa		Pointer auf Struktur ts_va
 *	\param		vaNumber	Variablennummer
 *
 *	\return		0: Lesen erfolgreich
 *
 ******************************************************************************/
int va_configRead (ts_db_connect *pConnect,
                   ts_va		 *pVa,
                   int			 vaNumber)
{
	int	fail = 0;


	if ((vaNumber >= C_va_counterMinNumber) &&
	    (vaNumber <= C_va_counterMaxNumber))
	{
		pVa->function			= va_getDbValue (pConnect, vaNumber, E_db_PAoVaFunction, &fail);
		pVa->type				= va_getDbValue (pConnect, vaNumber, E_db_PAoVaType, &fail);
		pVa->source				= va_getDbValue (pConnect, vaNumber, E_db_PAoVaSource, &fail);
		pVa->startCondition		= va_getDbValue (pConnect, vaNumber, E_db_PAoVaStartCondition, &fail);
		pVa->valueCondition		= va_getDbValue (pConnect, vaNumber, E_db_PAoVaValueCondition, &fail);
		pVa->hysteresis			= va_getDbValue (pConnect, vaNumber, E_db_PAoVaHysteresis, &fail);
		pVa->msgTooLo			= va_getDbValue (pConnect, vaNumber, E_db_PAoVaMsgTooLo, &fail);
		pVa->msgOperatorTooLo	= va_getDbValue (pConnect, vaNumber, E_db_PAoVaMsgOperatorTooLo, &fail);
		pVa->valueTooLo			= va_getDbValue (pConnect, vaNumber, E_db_PAoVaValueTooLo, &fail);
		pVa->msgLo				= va_getDbValue (pConnect, vaNumber, E_db_PAoVaMsgLo, &fail);
		pVa->msgOperatorLo		= va_getDbValue (pConnect, vaNumber, E_db_PAoVaMsgOperatorLo, &fail);
		pVa->valueLo			= va_getDbValue (pConnect, vaNumber, E_db_PAoVaValueLo, &fail);
		pVa->msgHi				= va_getDbValue (pConnect, vaNumber, E_db_PAoVaMsgHi, &fail);
		pVa->msgOperatorHi		= va_getDbValue (pConnect, vaNumber, E_db_PAoVaMsgOperatorHi, &fail);
		pVa->valueHi			= va_getDbValue (pConnect, vaNumber, E_db_PAoVaValueHi, &fail);
		pVa->msgTooHi			= va_getDbValue (pConnect, vaNumber, E_db_PAoVaMsgTooHi, &fail);
		pVa->msgOperatorTooHi	= va_getDbValue (pConnect, vaNumber, E_db_PAoVaMsgOperatorTooHi, &fail);
		pVa->valueTooHi			= va_getDbValue (pConnect, vaNumber, E_db_PAoVaValueTooHi, &fail);

		// counter mit Wert aus Datenbank initialisieren
		va_getDbCounter (pConnect, pVa, vaNumber);
	}
	else
	{
		fail = 1;
	} // else if (vaNumber >= C_va_counterMinNumber) && ...

	return (fail);
} /* va_configRead */


/**************************************************************************//**
 *
 *  \brief		va_configSetPermissionFlag
 *
 *  \details	schreibt in der Datenbanktabelle FLAG die Schreibrechte für die
 *				Counter Variable vaNumber.
 *
 *	\attention	gemäss Definition im Pflichtenheft kann ein Zählerstand im
 *				ausgeschalteten Zustand in der Datenbank nicht manipuliert werden.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		*pVa		Pointer auf Struktur ts_va
 *	\param		vaNumber	Variablennummer
 *
 *	\return		0: Lesen erfolgreich
 *
 ******************************************************************************/
void va_configSetPermissionFlag (ts_db_connect	*pConnect,
								 ts_va			*pVa,
								 int			vaNumber)
{
	if ((vaNumber >= C_va_counterMinNumber) &&
		(vaNumber <= C_va_counterMaxNumber))
	{
		if (db_flagGet (pConnect, E_db_flagServiceMode))
		{
			// Service Mode aktiv
			if (pVa->function != E_va_fktOff)
			{
				db_flagSet (pConnect, va_numberToFlagIndex (pConnect, vaNumber));
			}
			else
			{
				db_flagClear (pConnect, va_numberToFlagIndex (pConnect, vaNumber));
			} // else if (pVa->function != E_va_fktOff)
		}
		else
		{
			// Normal Mode aktiv
			if (pVa->function == E_va_fktOnUnlocked)
			{
				db_flagSet (pConnect, va_numberToFlagIndex (pConnect, vaNumber));
			}
			else
			{
				db_flagClear (pConnect, va_numberToFlagIndex (pConnect, vaNumber));
			} // else if (pVa->function != E_va_fktOff)
		} // else if (flagGet (pConnect, E_db_flagServiceMode))
	} // if ((vaNumber >= C_va_counterMinNumber) && ...
} /* va_configSetPermissionFlag */


/**************************************************************************//**
 *
 *  \brief		va_configOverride
 *
 *  \details	überschreibt die Konfiguration mit Testwerten.
 *
 *	\param		*pVa		Pointer auf Struktur ts_va
 *	\param		vaNumber	Variablennummer
 *
 *	\return		-
 *
 ******************************************************************************/
void va_configOverride (ts_va	*pVa,
						int		vaNumber)
{
	if ((vaNumber >= C_va_counterMinNumber) &&
	    (vaNumber <= C_va_counterMaxNumber))
	{
		#ifdef __va_showDebugStrings
			fprintf (stderr, "va_configOverride: va%d\n", vaNumber);
		#endif

		pVa->function			= E_va_fktOnUnlocked;
		pVa->type				= E_va_typeEvents;
		pVa->source				= E_va_source_VA1;			//!< Auf Testboard defekt
		pVa->source				= E_va_source_VA4;
		pVa->startCondition		= E_va_scLessOrEqual;

		// VA9 6000, V10 7000, V11 8000, VA12 9000, VA13 10000, VA14 > 10000 und somit nicht erreichbar
		pVa->valueCondition		= (vaNumber - C_va_counterMinNumber) * 1000 + 6000;					//!< Wert, Zähler läuft, wenn er die beiden Condition erfüllt
		pVa->hysteresis			= -1000;					//!< keine
		pVa->hysteresis			= 0;						//!< keine
		pVa->msgTooLo			= 709;						//!< 0: off, >0 Textnummer aus TP
		pVa->msgOperatorTooLo	= E_va_scGreaterOrEqual;	//!< Operator für Text zu tief in SMTP
		pVa->valueTooLo			= vaNumber;					//!< Wert für zu tief
		pVa->valueTooHi			= 1; 
		
		// config DI
		pVa->function			= E_va_fktOnUnlocked;
		pVa->type				= E_va_typeEvents;
		pVa->source				= E_va_source_DI1;
		pVa->startCondition		= E_va_scGreaterOrEqual;
		pVa->valueCondition		= 1;
		pVa->hysteresis			= 0;						//!< keine
		pVa->msgTooLo			= 709;						//!< 0: off, >0 Textnummer aus TP
		pVa->msgOperatorTooLo	= E_va_scGreaterOrEqual;	//!< Operator für Text zu tief in SMTP
		pVa->valueTooLo			= 0;						//!< Wert für zu tief

		pVa->type				= E_va_typeNone;			//!< kein Zählertyp

		// 01 Test Funktion aus, kein Zähler
		pVa->function			= E_va_fktOff;
		pVa->type				= E_va_typeNone;
		pVa->source				= E_va_source_DI1;
		pVa->startCondition		= E_va_scGreaterOrEqual;
		pVa->valueCondition		= 1;
		pVa->hysteresis			= 0;						//!< keine
		pVa->msgTooLo			= 709;						//!< 0: off, >0 Textnummer aus TP
		pVa->msgOperatorTooLo	= E_va_opEqual;				//!< Operator für Text zu tief in SMTP
		pVa->valueTooLo			= 4;						//!< Wert für zu tief
		pVa->msgLo				= 712;						//!< 0: off, >0 Textnummer aus TP
		pVa->msgOperatorLo		= E_va_opNotEqual;			//!< Operator für Text tief in SMTP
		pVa->valueLo			= 6;						//!< Wert für tief
		pVa->msgHi				= 715;						//!< 0: off, >0 Textnummer aus TP
		pVa->msgOperatorHi		= E_va_opGreater;			//!< Operator für Text hoch in SMTP
		pVa->valueHi			= 8;						//!< Wert für hoch
		pVa->msgTooHi			= 718;						//!< 0: off, >0 Textnummer aus TP
		pVa->msgOperatorTooHi	= E_va_opLess;				//!< Operator für Text zu hoch in SMTP
		pVa->valueTooHi			= 10;						//!< Wert für zu hoch

		// 02  Test Funktion ein, bearbeitbar im Service Mode, kein Zähler (nur Änderungen zu 01)
		pVa->function			= E_va_fktOnLocked;

		// 03  Test Funktion ein, immer bearbeitbar, kein Zähler (nur Änderungen zu 02)
		pVa->function			= E_va_fktOnUnlocked;

		// 04  Test Funktion ein, bearbeitbar im Service Mode, Zähler ein (nur Änderungen zu 03)
		pVa->function			= E_va_fktOnLocked;
		pVa->type				= E_va_typeEvents;

		// 05  Test Funktion ein, bearbeitbar im Service Mode, Zähler ein (nur Änderungen zu 04)
		pVa->startCondition		= E_va_scLessOrEqual;
		pVa->valueCondition		= 0;

		// 06  Test Funktion ein, bearbeitbar im Service Mode, Zähler ein (nur Änderungen zu 05)
		pVa->source				= E_va_source_VA2;			//!< VA1 Auf Testboard defekt
		pVa->startCondition		= E_va_scGreaterOrEqual;
		pVa->valueCondition		= (vaNumber - C_va_counterMinNumber) * 1000 + 2000;	//!< Wert, Zähler läuft, wenn er die beiden Condition erfüllt
//		pVa->valueCondition		= (vaNumber - C_va_counterMinNumber) * 500 + 3000;	//!< Wert, Zähler läuft, wenn er die beiden Condition erfüllt
		pVa->hysteresis			= -1000;					//!< keine
		
		// 07  Test Funktion ein, bearbeitbar im Service Mode, Zähler ein (nur Änderungen zu 06)
		pVa->startCondition		= E_va_scLessOrEqual;
		pVa->hysteresis			= 1000;						//!< keine

		// 08  Test Funktion ein, bearbeitbar im Service Mode, Zähler ein (nur Änderungen zu 07)
		pVa->type				= E_va_typeHour;
		pVa->source				= E_va_source_DI1;
		pVa->startCondition		= E_va_scGreaterOrEqual;
		pVa->valueCondition		= 1;
		pVa->hysteresis			= 0;

		// 09  Test Funktion ein, bearbeitbar im Service Mode, Zähler ein (nur Änderungen zu 08)
		pVa->startCondition		= E_va_scLessOrEqual;
		pVa->valueCondition		= 0;

		// last configuratio wins

	} // if (vaNumber >= C_va_counterMinNumber) && ...
} /* va_configOverride */


/**************************************************************************//**
 *
 *  \brief		va_process
 *
 *  \details	wertet die Bedingungen der Variablen vaNumber aus.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		*pVa		Pointer auf Struktur ts_va
 *	\param		vaNumber	Variablennummer
 *	\param		*pMail		Pointer auf Struktur ts_mail
 *
 *	\return		-
 *
 ******************************************************************************/
 int va_process (ts_db_connect	*pConnect,
				void			*pVoid,
				int				vaNumber,
				ts_mail			*pMail)
{
	static ts_db_va		dbVA;
	static ts_ioa		*pIoa;
	static ts_va		*pVa;

	static int32_t		counterLast;
	static char			strUnit [C_db_dataStringSize];
		   time_t		timer = time (NULL);
	static struct tm	*pDateTime;	//  = localtime (&timer);


	#ifdef __va_showDebugStrings
//		fprintf (stderr, "va_process: VA%d ", vaNumber);
	#endif

	pIoa = pVoid;

	if ((vaNumber >= C_va_counterMinNumber) &&
	    (vaNumber <= C_va_counterMaxNumber))
	{
		pVa = &pIoa->VA [vaNumber - C_va_counterMinNumber];
		pDateTime = localtime (&timer);

		// aktuellen Zählerstand aus der Datenbank lesen
		va_getDbCounter (pConnect, pVa, vaNumber);
		counterLast	= pVa->counter;

		switch (pVa->function)
		{
			case E_va_fktOff:
				break;

			case E_va_fktOnLocked:
			case E_va_fktOnUnlocked:
				switch (pVa->type)
				{
					case E_va_typeNone:
						// VAx könnte extern geschrieben worden sein
						pVa->resultCondition = va_checkCondition (pVa, pIoa);

						if (pVa->resultCondition != pVa->resultConditionLast)
						{
							pVa->resultConditionLast = pVa->resultCondition;
							va_checkLimits (pConnect, pVa, vaNumber, pMail);
						}

						break;

					case E_va_typeHour:
						#ifdef __va_accelerateHourCount
							if (pVa->lastHourValue != pDateTime->tm_min)
							{
								pVa->lastHourValue = pDateTime->tm_min;

								#ifdef __va_showDebugStrings
//									fprintf (stderr, "lastHourValue: %d\n", pVa->lastHourValue);
								#endif
						#else
							if (pVa->lastHourValue != pDateTime->tm_hour)
							{
								pVa->lastHourValue = pDateTime->tm_hour;

								#ifdef __va_showDebugStrings
//									fprintf (stderr, "lastHourValue: %d\n", pVa->lastHourValue);
								#endif
						#endif

							if (va_checkCondition (pVa, pIoa))
							{
								pVa->counter++;
								va_checkLimits (pConnect, pVa, vaNumber, pMail);
							}

						} // if (pVa->lastHourValue != pDateTime->tm_hour)

						break;

					case E_va_typeEvents:
						pVa->resultCondition = va_checkCondition (pVa, pIoa);

						if (pVa->resultCondition != pVa->resultConditionLast)
						{
							pVa->resultConditionLast = pVa->resultCondition;

							// nur die positiven Flanken zählen
							if (pVa->resultCondition == 1)
							{
								pVa->counter++;
								va_checkLimits (pConnect, pVa, vaNumber, pMail);

								#ifdef __va_showDebugStrings
									fprintf (stderr, "count event detected for VA%d, ", vaNumber);
									fprintf (stderr, " condiNow %d, condiLast %d ", pVa->resultCondition, pVa->resultConditionLast);
								#endif
							} // if (pVa->resultCondition == 1)
						}
						else
						{
							#ifdef __va_showDebugStrings
//								fprintf (stderr, "no count event");
							#endif
						} // else if (pVa->resultCondition != pVa->resultConditionLast)

						break;

				} // switch (pVa->type)
				break;

			default:
				break;

		} // switch (pVa->function)

		// falls Zählerstand geändert, neuer Wert in Datenbank schreiben
		if (counterLast != pVa->counter)
		{
			dbVA.id = vaNumber;
			snprintf (dbVA.strDescription, C_db_dataStringSize, "%d", pVa->counter);

			db_rowUpdate (pConnect, E_db_tableTypeVA, &dbVA);
		} // if (counterLast != pVa->counter)
	}
	else
	{
		#ifdef __va_showDebugStrings
//			fprintf (stderr, "vaNumber out of range");
		#endif
	} // if (vaNumber >= C_va_counterMinNumber) && ...

	#ifdef __va_showDebugStrings
//		fprintf (stderr, "\n");
	#endif
} /* va_process */


/* va.c */

