/**************************************************************************//**
 *
 *	\file		hwm.h
 *
 *	\brief		Headerfiles für den sysfs hwmon Zugriff.
 *
 *	\details	-
 *
 *	\date		03.02.2021
 *	\author		Marcel Ming
 *	\copyright	TRiMADA AG   CH-5610 Wohlen
 *	\remark		Compiler- / IDE-Version
 *
 *	\version	03.02.21 mm
 *					- Erstellt.
 *
 ******************************************************************************/
#ifndef __hwm_H
    #define __hwm_H


/******************************************************************************
 * Modules                                                                    *
 *                                                                            *
 ******************************************************************************/
#include <stdbool.h>
#include <stdint.h>


/******************************************************************************
 * Definitions and Macros                                                     *
 *                                                                            *
 ******************************************************************************/
#define	C_hwm_path		"/sys/class/hwmon/hwmon0/"				//!< Pfad zum Temperatursensor
#define	C_hwm_fileName	"/sys/class/hwmon/hwmon0/name"			//!< File mit dem Namen des Temperatursensors
#define	C_hwm_fileValue	"/sys/class/hwmon/hwmon0/temp1_input"	//!< File mit dem Wert des Temperatursensor

#define	C_hwm_fileValue	"/sys/class/hwmon/hwmon0/temp1_input"	//!< File mit dem Wert des Temperatursensor

/******************************************************************************
 * Typedefs and Structures                                                    *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Exported Variables                                                         *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Exported Functions                                                         *
 *                                                                            *
 ******************************************************************************/
int	hwm_getTemperatureName (char *pFileName, char *pSensorName);
int	hwm_getTemperatureValue (char *pFileName, int *pSensorValue);

#endif //__hwm_H

/* End hwm.h */
