/**************************************************************************//**
 *
 *	\file		do.h
 *
 *	\brief		Headerfile der Funktionen für die digitalen Ausgänge.
 *
 *	\details	-
 *
 *	\date		09.03.2021
 *	\author		Marcel Ming
 *	\copyright	TRiMADA AG   CH-5610 Wohlen
 *	\remark		Compiler- / IDE-Version
 *
 *	\version	09.03.21 mm
 *					- Erstellt.
 *	\version	29.04.21 mm
 *					- Tabellen Basis- und Offsetdefinitionen in die entsprechenden
 *					  dbXY verschoben.
 *	\version	05.08.21 mm
 *					- do_configOverride, do_lock und do_unlock definiert.
 *					- GPIO17 als C_do_lockFeedback definiert.
 *	\version	01.09.21 mm
 *					- do_processQuick definiert.
 *					- Struktur ts_do mit str erweitert um die in do_processQuick
 *					  ermittelten Meldung zu puffern.
 *	\version	06.09.21 mm
 *					- Compilerschalter __do_showDebugStrings definiert.
 *	\version	22.09.23 mm
 *					- in Funktion do_configOverride den nicht benötigten Parameter
 * 					  *pConnect gelöscht.
 *
 *
 ******************************************************************************/
#ifndef __do_H
    #define __do_H


/******************************************************************************
 * Compilerswitches                                                           *
 *                                                                            *
 ******************************************************************************/
	#define __do_hwWithLockFeedback
//	#define __do_showDebugStrings


/******************************************************************************
 * Modules                                                                    *
 *                                                                            *
 ******************************************************************************/
#include <gpiod.h>
#include <stdint.h>

#include "db.h"
#include "mail.h"


/******************************************************************************
 * Definitions and Macros                                                     *
 *                                                                            *
 ******************************************************************************/
#define	C_do_channelCount	4		//!< Anzahl der digitalen Ausgänge

#define	C_do_gpio_0			4		//!< GPIO Nummer für Do1 
#define	C_do_gpio_1			5		//!< GPIO Nummer für Do2
#define	C_do_gpio_2			6		//!< GPIO Nummer für Do3
#define	C_do_gpio_3			24		//!< GPIO Nummer für Do4

#define	C_do_lock			23		//!< GPIO Nummer für Freigabe Relaisspulenspeisung
#define	C_do_lockFeedback	17		//!< GPIO Nummer für Feedback Freigabe Relaisspulenspeisung


/******************************************************************************
 * Typedefs and Structures                                                    *
 *                                                                            *
 ******************************************************************************/
 typedef enum {
	E_do_fktOff,					//!< ausgeschaltet 
	E_do_fktSwitchNormallyOpen,		//!< Schalter Schliesser
	E_do_fktSwitchNormallyClose,	//!< Schalter Öffner
	E_do_fktButtonNormallyOpen,		//!< Taster Schliesser
	E_do_fktButtonNormallyClose,	//!< Taster Öffner
	E_do_fktImpulse,				//!< Impulsgeber
	E_do_fktCount					//!< Anzahl Funktionen
} te_do_function;


typedef enum {
	E_do_followOff,			//!< folgen ausgeschaltet
	E_do_followDi1,			//!< folge digital Eingang 1 
	E_do_followDi2,			//!< folge digital Eingang 2
	E_do_followDi3,			//!< folge digital Eingang 3
	E_do_followDi4,			//!< folge digital Eingang 4
	E_do_followCount		//!< Anzahl Funktionen
} te_do_follow;
  

typedef struct {
	// Datenbank Werte
	te_do_function	function;		//!< Ausgangsfunktion
	uint8_t			enableLog:1;	//!< 1: Logfunktion aktiviert
	uint8_t			activeTime;		//!< Tastzeit zu Taster oder Impulsgeber [s]
	te_do_follow	follow;			//!< digitalem Eingang folgen
	uint16_t		msgLoHi;		//!< 0: off, >0 Textnummer aus TP
	uint16_t		msgHiLo;		//!< 0: off, >0 Textnummer aus TP

	// interne Werte
	te_do_function		lastFunction;					//!< Ausgangsfunktion
	uint8_t				remainingTime;					//!< verbleibende Zeit in welcher die Taster oder der Impuls aktiv sind
	struct gpiod_line	*pHandle;						//!< GPIO line handle oder NULL bei Fehler
	char				message [C_db_dataStringSize];	//!< Zwischenspeicher für SMTP Meldung
	uint				impulseRunning	:1;				//!< Impulsegenerator läuft
	uint				coil			:1;				//!< Relais Spulenzustand
} ts_do;


/******************************************************************************
 * Exported Variables                                                         *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Exported Functions                                                         *
 *                                                                            *
 ******************************************************************************/
void do_configDebug (ts_do	*pDo,
					 int	channel);

int do_configRead (ts_db_connect	*pConnect,
                   ts_do			*pDo,
                   int				channel);

void do_configOverride (ts_do	*pDo,
						int		channel);

void do_lock	(struct gpiod_line *pHandleDoLock,
				 struct gpiod_line *pHandleDoLockFB);
void do_unlock	(struct gpiod_line *pHandleDoLock,
				 struct gpiod_line *pHandleDoLockFB);

void do_processQuick (void	*pVoid,
					  int	channel);

int do_process (ts_db_connect	*pConnect,
				void			*pVoid,
				int				channel,
				ts_mail			*pMail);


#endif //__do_H

/* End do.h */
