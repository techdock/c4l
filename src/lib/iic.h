/**************************************************************************//**
 *
 *	\file		iic.h
 *
 *	\brief		Headerfiles für den iic Zugriff.
 *
 *	\details	-
 *
 *	\date		06.08.2020
 *	\author		Marcel Ming
 *	\copyright	TRiMADA AG   CH-5610 Wohlen
 *	\remark		Compiler- / IDE-Version
 *
 *	\version	06.08.20 mm
 *					- Erstellt.
 *
 ******************************************************************************/
#ifndef __iic_H
    #define __iic_H


/******************************************************************************
 * Modules                                                                    *
 *                                                                            *
 ******************************************************************************/
#include <stdint.h>


/******************************************************************************
 * Definitions and Macros                                                     *
 *                                                                            *
 ******************************************************************************/
#define	C_iic_bus				"/dev/i2c-1"	//!< verwendeter iic Kontroller

#define	C_iic_AddressADS7924	0x48	//!< iic Adresse des ADC ADS7924
#define	C_iic_AddressLTC2617	0x41	//!< iic Adresse des DAC LTC2717


/******************************************************************************
 * Typedefs and Structures                                                    *
 *                                                                            *
 ******************************************************************************/
/*
typedef enum {
    E_iic_name_element_1,		//!< Beschreibung Element 1
    E_iic_name_element_2,		//!< Beschreibung Element 2
	...
    E_iic_name_element_n,		//!< Beschreibung Element n
    E_iic_name_count			//!< Anzahl der Elemente von te_iic_enumName
} te_iic_enumName;

typedef struct {
    uint8_t		structMember_1;		//!< Beschreibung Element 1
    uint32_t	structMember_2;		//!< Beschreibung Element 2
    float		structMember_3;		//!< Beschreibung Element 3
} ts_iic_structName;
*/

/**************************************************************************//**
 *
 *  \struct     ts_iic_TypeName (eigener Header bei grösseren Strukturen)
 *  \brief      Kurzebeschreibung der Struktur
 *
 *  \details    Detailbeschreibung der Struktur
 *
 ******************************************************************************/
/*
typedef struct {
	int	abc;						//!< Beschreibung der Strukturvariablen abc
	int def;						//!< Beschreibung der Strukturvariablen def
	...
} ts_iic_typeName;
*/


/******************************************************************************
 * Exported Variables                                                         *
 *                                                                            *
 ******************************************************************************/
/*
extern te_iic_enumName			iic_enumName;
extern ts_iic_structName		iic_structName;
extern ts_iic_typeName			iic_varName;
*/


/******************************************************************************
 * Exported Functions                                                         *
 *                                                                            *
 ******************************************************************************/
int iic_init (void);


#endif //__iic_H

/* End iic.h */
