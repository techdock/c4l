/**************************************************************************//**
 *
 *	\file		ads7924.c
 *
 *	\brief		stellt Funktionen für den ADS7924 Zugriff zur Verfügung.
 *
 *	\details	Der ADS7924 ist ein ADC mit 12 Bit Auflösung, 4 Kanälen und einer
 *				i2c Schnittstelle.
 *
 *	\date		13.01.2021
 *	\author		Marcel Ming
 *	\copyright	TRiMADA AG   CH-5610 Wohlen
 *	\remark		Compiler- / IDE-Version
 *
 *	\version	13.01.21 mm
 *					- Erstellt.
 *	\version	02.09.21 mm
 *					- Funtionen ads7924_getAcquireConfig und ads7924_setAcquireConfig
 *					  implementiert.
 *	\version	18.02.22 mm
 *					- Kommentar ergänzt / korrigiert.
 *
 * 
 ******************************************************************************/


/******************************************************************************
 * Compilerswitches                                                           *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Modules                                                                    *
 *                                                                            *
 ******************************************************************************/
#include <i2c/smbus.h>
#include <linux/i2c-dev.h>
#include <stdint.h>
#include <sys/ioctl.h>

#include "ads7924.h"


/******************************************************************************
 * Definitions and Macros                                                     *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Typedefs and Structures                                                    *
 *                                                                            *
 ******************************************************************************/
 
/******************************************************************************
 * Exported Variables                                                         *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Global Variables                                                           *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Local Functions                                                            *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Exported Functions                                                         *
 *                                                                            *
 ******************************************************************************/

/**************************************************************************//**
 *
 *	\brief		ads7924_setModeCntrl
 *
 *	\details	setzt die gewünschte Betriebsart im Mode Control Register.
 *
 *	\param		deviceDescriptor	Deskriptor zum geöffneten i2c Kontroller.
 *	\param		chipAddr			Adresse des ADS7924.
 *	\param		*pModeControl		Pointer auf ModeControl
 *
 *	\return		0	Funktion erfolgreich ausgeführt
 * 				< 0 Funktion fehlgeschlagen
 *
 ******************************************************************************/
int	ads7924_setModeCntrl (int 					 deviceDescriptor,
						  uint8_t				 chipAddr,
	                      tu_ads7924_modeControl *pModeControl)
{
	// i2c Controller auf ADS7924 Adresse setzen
	if (ioctl (deviceDescriptor, I2C_SLAVE, chipAddr) < 0)
	{
		return (-1);
	}
	else
	{
		if (i2c_smbus_write_byte_data (deviceDescriptor, E_ads7924_ra_modeCntrl, pModeControl->value) < 0)
		{
			return (-1);
		}

		return (0);
	} // else if (ioctl (deviceDescriptor, I2C_SLAVE, chipAddr) < 0)
} /* ads7924_setModeCntrl */


/**************************************************************************//**
 *
 *	\brief		ads7924_getModeCntrl
 *
 *	\details	gibt das Mode Control Register zurück.
 *
 *	\param		deviceDescriptor	Deskriptor zum geöffneten i2c Kontroller.
 *	\param		chipAddr			Adresse des ADS7924.
 *	\param		*pModeControl		Pointer auf ModeControl
 *
 *	\return		0	Funktion erfolgreich ausgeführt
 * 				< 0 Funktion fehlgeschlagen
 *
 ******************************************************************************/
int	ads7924_getModeCntrl (int					 deviceDescriptor,
						  uint8_t				 chipAddr,
	                      tu_ads7924_modeControl *pModeControl)
{
	// i2c Controller auf ADS7924 Adresse setzen
	if (ioctl (deviceDescriptor, I2C_SLAVE, chipAddr) < 0)
	{
		return (-1);
	}
	else
	{
		pModeControl->value = i2c_smbus_read_byte_data (deviceDescriptor, E_ads7924_ra_modeCntrl); 

		return (0);
	} // else if (ioctl (deviceDescriptor, I2C_SLAVE, chipAddr) < 0)
} /* ads7924_getModeCntrl */


/**************************************************************************//**
 *
 *	\brief		ads7924_setIntCntrl
 *
 *	\details	-
 *
 *	\param		deviceDescriptor	Deskriptor zum geöffneten i2c Kontroller.
 *	\param		chipAddr			Adresse des ADS7924.
 *	\param		*pInterruptControl	Pointer auf InterruptControl
 *
 *	\return		0	Funktion erfolgreich ausgeführt
 *				< 0 Funktion fehlgeschlagen
 *
 ******************************************************************************/
int	ads7924_setIntCntrl (int							deviceDescriptor,
						 uint8_t						chipAddr,
	                     ts_ads7924_interruptControl	*pInterruptControl)
{
	return (-1);
} /* ads7924_setIntCntrl */


/**************************************************************************//**
 *
 *	\brief		ads7924_getIntCntrl
 *
 *	\details	-
 *
 *	\param		deviceDescriptor	Deskriptor zum geöffneten i2c Kontroller.
 *	\param		chipAddr			Adresse des ADS7924.
 *	\param		*pInterruptControl	Pointer auf InterruptControl
 *
 *	\return		0	Funktion erfolgreich ausgeführt
 *				< 0 Funktion fehlgeschlagen
 *
 ******************************************************************************/
int	ads7924_getIntCntrl (int							deviceDescriptor,
						 uint8_t						chipAddr,
	                     ts_ads7924_interruptControl	*pInterruptControl)
{
	return (-1);
} /* ads7924_getIntCntrl */


/**************************************************************************//**
 *
 *	\brief		ads7924_getAllDataChannels
 *
 *	\details	gibt die Werte aller ADC Kanäle in der Struktur
 *				ts_ads7924_dataBuffer zurück. 
 *
 *	\param		deviceDescriptor	Deskriptor zum geöffneten i2c Kontroller.
 *	\param		chipAddr			Adresse des ADS7924.
 *	\param		*pDataBuffer		Pointer auf den Daten Buffer.
 *
 *	\return		0	Funktion erfolgreich ausgeführt
 * 				< 0 Funktion fehlgeschlagen
 *
 ******************************************************************************/
int	ads7924_getAllDataChannels (int						deviceDescriptor,
								uint8_t					chipAddr,
	                            ts_ads7924_dataBuffer	*pDataBuffer)
{
	te_ads7924_channel	i;
	uint16_t			adcValue;

	// i2c Controller auf ADS7924 Adresse setzen
	if (ioctl (deviceDescriptor, I2C_SLAVE, chipAddr) < 0)
	{
		return (-1);
	}
	else
	{
		for (i = 0; i < E_ads7924_channelCount; i++)
		{
			adcValue = i2c_smbus_read_word_data (deviceDescriptor, (E_ads7924_ra_data0hi + (i << 1)) | C_ads7924_registerAddressAutoInc); 

			// ADC Wert in der korrekten Bytereihenfolge auf uint16 ausrichten 
			pDataBuffer->adcValue [i] = ((adcValue << 8) | (adcValue >> 8)) >> 4; 

			// Rohwert in Spannung [mV] umrechnen
			pDataBuffer->voltage [i] = (uint16_t)(((uint32_t)pDataBuffer->adcValue [i] * C_ads7924_maxVoltage) / C_ads7924_resolution);

			// Logic Level bestimmen
			switch (pDataBuffer->logicLevel [i])
			{
				case true:
					if (pDataBuffer->voltage [i] < pDataBuffer->voltageOff [i])
					{
						pDataBuffer->logicLevel [i] = false;
					}
					break;

				case false:
					if (pDataBuffer->voltage [i] > pDataBuffer->voltageOn [i])
					{
						pDataBuffer->logicLevel [i] = true;
					}
					break;
			} // switch (pDataBuffer->logicLevel)
		} // for (i == 0; i < E_ads7924_channelCount; i++)

		return (0);
	} // else if (ioctl (deviceDescriptor, I2C_SLAVE, chipAddr) < 0)
} /* ads7924_getAllDataChannels */


/**************************************************************************//**
 *
 *	\brief		ads7924_setUpperLimitThreshold
 *
 *	\details	
 *
 *	\param		deviceDescriptor	Deskriptor zum geöffneten i2c Kontroller.
 *	\param		chipAddr			Adresse des ADS7924.
 *	\param		channel
 *	\param		threshold
 *
 *	\return		0	Funktion erfolgreich ausgeführt
 *				< 0 Funktion fehlgeschlagen
 *
 ******************************************************************************/
int	ads7924_setUpperLimitThreshold (int					deviceDescriptor,
									uint8_t				chipAddr,
	                                te_ads7924_channel	channel,
	                                uint16_t			threshold)
{
	return (-1);
} /* ads7924_setUpperLimitThreshold */


/**************************************************************************//**
 *
 *	\brief		ads7924_getUpperLimitThreshold
 *
 *	\details	
 *
 *	\param		deviceDescriptor	Deskriptor zum geöffneten i2c Kontroller.
 *	\param		chipAddr			Adresse des ADS7924.
 *	\param		channel
 *	\param		*pThreshold			Pointer auf Threshold
 *
 *	\return		0	Funktion erfolgreich ausgeführt
 * 				< 0 Funktion fehlgeschlagen
 *
 ******************************************************************************/
int	ads7924_getUpperLimitThreshold (int					deviceDescriptor,
									uint8_t				chipAddr,
	                                te_ads7924_channel	channel,
	                                uint16_t			*pThreshold)
{
	return (-1);
} /* ads7924_getUpperLimitThreshold */


/**************************************************************************//**
 *
 *	\brief		ads7924_setLowerLimitThreshold
 *
 *	\details	-
 *
 *	\param		deviceDescriptor	Deskriptor zum geöffneten i2c Kontroller.
 *	\param		chipAddr			Adresse des ADS7924.
 *	\param		channel
 *	\param		threshold
 *
 *	\return		0	Funktion erfolgreich ausgeführt
 *				< 0 Funktion fehlgeschlagen	// i2c Controller auf ADS7924 Adresse setzen
 *
 ******************************************************************************/
int	ads7924_setLowerLimitThreshold (int					deviceDescriptor,
									uint8_t				chipAddr,
	                                te_ads7924_channel	channel,
	                                uint16_t			threshold)
{
	return (-1);
} /* ads7924_setLowerLimitThreshold */


/**************************************************************************//**
 *
 *	\brief		ads7924_
 *
 *	\details	
 *
 *	\param		deviceDescriptor	Deskriptor zum geöffneten i2c Kontroller.
 *	\param		chipAddr			Adresse des ADS7924.
 *	\param		channel
 *	\param		*pThreshold			Pointer auf Threshold
 *
 *	\return		0	Funktion erfolgreich ausgeführt
 * 				< 0 Funktion fehlgeschlagen
 *
 ******************************************************************************/
int	ads7924_getLowerLimitThreshold (int					deviceDescriptor,
									uint8_t				chipAddr,
	                                te_ads7924_channel	channel,
	                                uint16_t			*pThreshold)
{
	return (-1);
} /* ads7924_getLowerLimitThreshold */


/**************************************************************************//**
 *
 *	\brief		ads7924_
 *
 *	\details	
 *
 *	\param		deviceDescriptor	Deskriptor zum geöffneten i2c Kontroller.
 *	\param		chipAddr			Adresse des ADS7924.
 *	\param		*pInterruptConfig	Pointer auf InterruptConfig
 *
 *	\return		0	Funktion erfolgreich ausgeführt
 * 				< 0 Funktion fehlgeschlagen
 *
 ******************************************************************************/
int	ads7924_setInterruptConfig (int							deviceDescriptor,
								uint8_t						chipAddr,
	                            ts_ads7924_interruptConfig	*pInterruptConfig)
{
	return (-1);
} /* ads7924_setInterruptConfig */


/**************************************************************************//**
 *
 *	\brief		ads7924_
 *
 *	\details	
 *
 *	\param		deviceDescriptor	Deskriptor zum geöffneten i2c Kontroller.
 *	\param		chipAddr			Adresse des ADS7924.
 *	\param		*pInterruptConfig	Pointer auf InterruptConfig
 *
 *	\return		0	Funktion erfolgreich ausgeführt
 * 				< 0 Funktion fehlgeschlagen
 *
 ******************************************************************************/
int	ads7924_getInterruptConfig (int							deviceDescriptor,
								uint8_t						chipAddr,
	                            ts_ads7924_interruptConfig	*pInterruptConfig)
{
	return (-1);
} /* ads7924_getInterruptConfig */


/**************************************************************************//**
 *
 *	\brief		ads7924_
 *
 *	\details	
 *
 *	\param		deviceDescriptor	Deskriptor zum geöffneten i2c Kontroller.
 *	\param		chipAddr			Adresse des ADS7924.
 *	\param		*pSleepConfig		Pointer auf SleepConfig
 *
 *	\return		0	Funktion erfolgreich ausgeführt
 * 				< 0 Funktion fehlgeschlagen
 *
 ******************************************************************************/
int	ads7924_setSleepConfig (int						deviceDescriptor,
							uint8_t					chipAddr,
	                        ts_ads7924_sleepConfig	*pSleepConfig)
{
	return (-1);
} /* ads7924_setSleepConfig */


/**************************************************************************//**
 *
 *	\brief		ads7924_
 *
 *	\details	
 *
 *	\param		deviceDescriptor	Deskriptor zum geöffneten i2c Kontroller.
 *	\param		chipAddr			Adresse des ADS7924.
 *	\param		*pSleepConfig		Pointer auf SleepConfig
 *
 *	\return		0	Funktion erfolgreich ausgeführt
 * 				< 0 Funktion fehlgeschlagen
 *
 ******************************************************************************/
int	ads7924_getSleepConfig (int						deviceDescriptor,
							uint8_t					chipAddr,
	                        ts_ads7924_sleepConfig	*pSleepConfig)
{
	return (-1);
} /* ads7924_getSleepConfig */


/**************************************************************************//**
 *
 *	\brief		ads7924_setAcquireConfig
 *
 *	\details	setzt den Wert des ACQCONFIG Registers. Der Registewert wird
 *				mit der Formel (Wert x 2 us) + 6 us umgesetzt.
 *
 *	\param		deviceDescriptor	Deskriptor zum geöffneten i2c Kontroller.
 *	\param		chipAddr			Adresse des ADS7924.
 *	\param		*pAcquireConfig		Pointer auf AcquireConfig
 *
 *	\return		0	Funktion erfolgreich ausgeführt
 * 				< 0 Funktion fehlgeschlagen
 *
 ******************************************************************************/
int	ads7924_setAcquireConfig (int						deviceDescriptor,
							  uint8_t					chipAddr,
	                          tu_ads7924_acquireConfig	*pAcquireConfig)
{
	// i2c Controller auf ADS7924 Adresse setzen
	if (ioctl (deviceDescriptor, I2C_SLAVE, chipAddr) < 0)
	{
		return (-1);
	}
	else
	{
		if (i2c_smbus_write_byte_data (deviceDescriptor, E_ads7924_ra_acquireConfig, pAcquireConfig->value) < 0)
		{
			return (-1);
		}

		return (0);
	} // else if (ioctl (deviceDescriptor, I2C_SLAVE, chipAddr) < 0)

} /* ads7924_setAcquireConfig */


/**************************************************************************//**
 *
 *	\brief		ads7924_getAcquireConfig
 *
 *	\details	gibt den Werte des ACQCONFIG Registers zurück. 
 *
 *
 *	\param		deviceDescriptor	Deskriptor zum geöffneten i2c Kontroller.
 *	\param		chipAddr			Adresse des ADS7924.
 *	\param		*pAcquireConfig		Pointer auf AcquireConfig
 *
 *	\return		0	Funktion erfolgreich ausgeführt
 * 				< 0 Funktion fehlgeschlagen
 *
 ******************************************************************************/
int	ads7924_getAcquireConfig (int						deviceDescriptor,
							  uint8_t					chipAddr,
	                          tu_ads7924_acquireConfig	*pAcquireConfig)
{
	// i2c Controller auf ADS7924 Adresse setzen
	if (ioctl (deviceDescriptor, I2C_SLAVE, chipAddr) < 0)
	{
		return (-1);
	}
	else
	{
		pAcquireConfig->value = i2c_smbus_read_byte_data (deviceDescriptor, E_ads7924_ra_acquireConfig); 

		return (0);
	} // else if (ioctl (deviceDescriptor, I2C_SLAVE, chipAddr) < 0)

} /* ads7924_getAcquireConfig */


/**************************************************************************//**
 *
 *	\brief		ads7924_
 *
 *	\details	
 *
 *	\param		deviceDescriptor	Deskriptor zum geöffneten i2c Kontroller.
 *	\param		chipAddr			Adresse des ADS7924.
 *	\param		*pPowerConfig		Pointer auf PowerConfig
 *
 *	\return		0	Funktion erfolgreich ausgeführt
 * 				< 0 Funktion fehlgeschlagen
 *
 ******************************************************************************/
int	ads7924_setPowerConfig (int						deviceDescriptor,
							uint8_t					chipAddr,
	                        ts_ads7924_powerConfig	*pPowerConfig)
{
	return (-1);
} /* ads7924_setPowerConfig */


/**************************************************************************//**
 *
 *	\brief		ads7924_getPowerConfig
 *
 *	\details	gibt die Power Konfiguration aus dem ADS7924 zurück.  
 *
 *	\param		deviceDescriptor	Deskriptor zum geöffneten i2c Kontroller.
 *	\param		chipAddr			Adresse des ADS7924.
 *	\param		*pPowerConfig		Pointer auf PowerConfig
 *
 *	\return		0	Funktion erfolgreich ausgeführt
 * 				< 0 Funktion fehlgeschlagen
 *
 ******************************************************************************/
int	ads7924_getPowerConfig (int						deviceDescriptor,
							uint8_t					chipAddr,
	                        ts_ads7924_powerConfig	*pPowerConfig)
{
	return (-1);
} /* ads7924_getPowerConfig */


/**************************************************************************//**
 *
 *	\brief		ads7924_reset
 *
 *	\details	führt einen Reset des ADS7924 aus.
 *
 *	\param		deviceDescriptor	Deskriptor zum geöffneten i2c Kontroller.
 *	\param		chipAddr			Adresse des ADS7924.
 *
 *	\return		0	Funktion erfolgreich ausgeführt
 * 				< 0 Funktion fehlgeschlagen
 *
 ******************************************************************************/
int	ads7924_reset (int		deviceDescriptor,
				   uint8_t	chipAddr)
{
	return (-1);
} /* ads7924_reset */


/**************************************************************************//**
 *
 *	\brief		ads7924_getDeviceID
 *
 *	\details	gibt die Device ID des ADS7924 zurück.
 *
 *	\param		deviceDescriptor	Deskriptor zum geöffneten i2c Kontroller.
 *	\param		chipAddr			Adresse des ADS7924.
 *	\param		*pDeviceID			Pointerauf Device ID
 *
 *	\return		0	Funktion erfolgreich ausgeführt
 * 				< 0 Funktion fehlgeschlagen
 *
 ******************************************************************************/
int	ads7924_getDeviceID (int	 deviceDescriptor,
						 uint8_t chipAddr,
	                     uint8_t *pDeviceID)
{
	if (ioctl (deviceDescriptor, I2C_SLAVE, chipAddr) < 0)
	{
		return (-1);
	}
	else
	{
		*pDeviceID = i2c_smbus_read_byte_data (deviceDescriptor, E_ads7924_ra_reset);
		return (0);
	} // else if (ioctl (deviceDescriptor, I2C_SLAVE, chipAddr) < 0)
} /* ads7924_getDeviceID */


/* End ads7924.c */
