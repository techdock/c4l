/**************************************************************************//**
 *
 *	\file		ltc2617.c
 *
 *	\brief		stellt Funktionen für den ltc2617 Zugriff zur Verfügung.
 *
 *	\details	Der ltc2617 ist ein DAC mit 14 Bit Auflösung, 2 Kanälen und einer
 *				i2c Schnittstelle.
 *
 *	\date		25.01.2021
 *	\author		Marcel Ming
 *	\copyright	TRiMADA AG   CH-5610 Wohlen
 *	\remark		Compiler- / IDE-Version
 *
 *	\version	25.01.21 mm
 *					- Erstellt.
 *	\version	27.01.21 mm
 *					- ins /11007/library/ verschoben -> in einem Projekt als
 *					  Link einfügen.
 *
 ******************************************************************************/


/******************************************************************************
 * Compilerswitches                                                           *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Modules                                                                    *
 *                                                                            *
 ******************************************************************************/
#include <i2c/smbus.h>
#include <linux/i2c-dev.h>
#include <stdint.h>
#include <sys/ioctl.h>

#include "ltc2617.h"


/******************************************************************************
 * Definitions and Macros                                                     *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Typedefs and Structures                                                    *
 *                                                                            *
 ******************************************************************************/
 
/******************************************************************************
 * Exported Variables                                                         *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Global Variables                                                           *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Local Functions                                                            *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Exported Functions                                                         *
 *                                                                            *
 ******************************************************************************/

/**************************************************************************//**
 *
 *  \brief		ltc2617_setDac
 *
 *  \details	setzt den Ausgang des DACs channel mit dem Wert dacValue. 
 *
 *	\param		deviceDescriptor	Deskriptor zum geöffneten i2c Kontroller.
 * 	\param		chipAddr			Adresse des ltc2617.
 *  \param		channel				DAC Kanal gemäss te_ltc2617_address.
 * 	\param		dacValue			DAC Wert.
 *
 *	\return		0	Funktion erfolgreich ausgeführt
 * 				< 0 Funktion fehlgeschlagen
 *
 *	\attention	beim Schreiben des 16 Bit Wertes muss das hi und lo Byte getauscht
 *				werden.
 *
 ******************************************************************************/
int	ltc2617_setDac (int					deviceDescriptor,
	                uint8_t				chipAddr,
	                te_ltc2617_address	channel,
	                uint16_t			dacValue)
{
	tu_ltc2617_adressAndCommand adressAndCommand;
    uint16_t					dacHiLoSwitched;

	adressAndCommand.adr = channel;
	adressAndCommand.cmd = E_ltc2617_cmd_writeToAndUpdate;

	// i2c Controller auf ltc2617 Adresse setzen 
	if (ioctl (deviceDescriptor, I2C_SLAVE, chipAddr) < 0)
	{
		return (-1);
	}
	else
	{
		dacHiLoSwitched = (dacValue << 8) | (dacValue >> 8);  
/*		
printf("chip Address         : 0x%x\n", chipAddr);
printf("deviceDescriptor     : %d\n", deviceDescriptor);
printf("adressAndCommand.both: 0x%x\n", adressAndCommand.both);
printf("dacValue             : %x\n", dacValue);
printf("dacHiLoSwitched      : %x\n", dacHiLoSwitched);
*/
		if (i2c_smbus_write_word_data (deviceDescriptor, adressAndCommand.both, (dacHiLoSwitched << 2)) < 0)
		{
			return (-1);
		}

		return (0);
	} // else if (ioctl (deviceDescriptor, I2C_SLAVE, chipAddr) < 0)
} /* ltc2617_setDac */


/**************************************************************************//**
 *
 *  \brief		ltc2617_setVoltage
 *
 *  \details	setzt den Ausgang des DACs channel auf den Spannungswert voltage.
 *				Die Einheit von voltage ist dabei [mV].
 *
 *	\param		deviceDescriptor	Deskriptor zum geöffneten i2c Kontroller.
 * 	\param		chipAddr			Adresse des ltc2617.
 *  \param		channel				DAC Kanal gemäss te_ltc2617_address.
 * 	\param		dacValue			Spannungswert [mv].
 *
 *	\return		0	Funktion erfolgreich ausgeführt
 * 				< 0 Funktion fehlgeschlagen
 *
 ******************************************************************************/
int	ltc2617_setVoltage (int					deviceDescriptor,
	                    uint8_t				chipAddr,
	                    te_ltc2617_address	channel,
	                    uint16_t			voltage)
{
	uint16_t dacValue = (uint16_t)(((uint32_t)voltage * C_ltc2617_resolution) / C_ltc2617_maxVoltage);

	if (ltc2617_setDac (deviceDescriptor, chipAddr, channel, dacValue) < 0)
	{
		return (-1);
	}
	else
	{
		return (0);
	}
} /* ltc2617_setVoltage */


/**************************************************************************//**
 *
 *  \brief		ltc2617_setCurrent
 *
 *  \details	setzt den Ausgang des DACs channel mit auf den Stromwert current.
 *				Die Einheit von curent ist dabei [uA].
 *
 *	\param		deviceDescriptor	Deskriptor zum geöffneten i2c Kontroller.
 * 	\param		chipAddr			Adresse des ltc2617.
 *  \param		channel				DAC Kanal gemäss te_ltc2617_address.
 * 	\param		current				Stromwert [uA].
 *
 *	\return		0	Funktion erfolgreich ausgeführt
 * 				< 0 Funktion fehlgeschlagen
 *
 ******************************************************************************/
int	ltc2617_setCurrent (int					deviceDescriptor,
	                    uint8_t				chipAddr,
	                    te_ltc2617_address	channel,
                    	uint16_t			current)
{
	uint16_t dacValue = (uint16_t)(((uint32_t)current * C_ltc2617_resolution) / C_ltc2617_maxCurrent);

	if (ltc2617_setDac (deviceDescriptor, chipAddr, channel, dacValue) < 0)
	{
		return (-1);
	}
	else
	{
		return (0);
	}
} /* ltc2617_setCurrent */


/* End ltc2617.c */
