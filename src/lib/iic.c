/**************************************************************************//**
 *
 *	\file		iic.c
 *
 *	\brief		stellt Funktionen für den i2c Zugriff zur Verfügung.
 *
 *	\details	-
 *
 *	\date		06.08.2020
 *	\author		Marcel Ming
 *	\copyright	TRiMADA AG   CH-5610 Wohlen
 *	\remark		Compiler- / IDE-Version
 *
 *	\version	06.08.20 mm
 *					- Erstellt.
 *
 ******************************************************************************/


/******************************************************************************
 * Compilerswitches                                                           *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Modules                                                                    *
 *                                                                            *
 ******************************************************************************/
#include <sys/fcntl.h>
#include <sys/ioctl.h>
#include <stdio.h>
#include <stdint.h>

#include <linux/i2c-dev.h>

#include "iic.h"


/******************************************************************************
 * Definitions and Macros                                                     *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Typedefs and Structures                                                    *
 *                                                                            *
 ******************************************************************************/
 
/******************************************************************************
 * Exported Variables                                                         *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Global Variables                                                           *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Local Functions                                                            *
 *                                                                            *
 ******************************************************************************/

/**************************************************************************//**
 *
 *  \brief		iic_local
 *
 *  \details	Detailbeschreibung der Funktion
 *
 *
 *	\param		param1		Beschreibung Parameter 1
 *	\param		param2		Beschreibung Parameter 2

 *	\return		iicIoResult		Beschreibung iicIoResultat
 *
 ******************************************************************************/
uint8_t iic_local (uint8_t param1,
						uint8_t param2)
{
	/*
	uint8_t	localVar1;
	uint8_t	localVar2 = initValue;
	*/
	return (0);
} /* iic_local */


/******************************************************************************
 * Exported Functions                                                         *
 *                                                                            *
 ******************************************************************************/

/**************************************************************************//**
 *
 *  \brief		iic_structNameInit
 *
 *  \details	initialisiert die Elemente der Struktur *pStructName.
 *
 *	\param		*pStructName	Pointer auf eine Instanz der Struktur ts_iic_structName
 *
 *	\return		-
 *
 ******************************************************************************/
/*
void iic_structNameInit (ts_iic_structName *pStructName)
{
	pStructName->structMember_1 = 0u;
	pStructName->structMember_2 = 12345u;
	pStructName->structMember_3 = 67.89;
}
*/
/* iic_structNameInit */


/**************************************************************************//**
 *
 *  \brief		iic_init
 *
 *  \details	initialisiert die iic-Schnittstelle und gibt das iicDevice
 *				zurück.
 *
 *	\param		-
 *
 *	\return		iicDevice 
 *
 ******************************************************************************/
int iic_init (void)
{
	int	iicIoResult;
	int	iicDevice = open (C_iic_bus, O_RDWR);
	
	if (iicDevice < 0)
	{
		printf("open(%s) failed ", C_iic_bus);
		perror("");
	}
	else
	{
		printf("success to open the iic bus\n");

 		iicIoResult = ioctl (iicDevice, I2C_SLAVE, 0x20);

		if (iicIoResult < 0)
		{
			printf("failed to acquire bus access and/or talk to slave\n");
		}
		else
		{
			printf("success to acquire bus access and/or talk to slave\n");
		} // else if (iicIoResult < 0)	
	} // else if (iicDevice < 0)

	return (iicDevice);
} /* iic_init */


/* End iic.c */
