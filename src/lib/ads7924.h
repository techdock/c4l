/**************************************************************************//**
 *
 *	\file		ads7924.h
 *
 *	\brief		Headerfiles für den ADS7924 Zugriff.
 *
 *	\details	Der ADS7924 ist ein ADC mit 12 Bit Auflösung, 4 Kanälen und einer
 *				i2c Schnittstelle.
 *
 *	\date		06.08.2020
 *	\author		Marcel Ming
 *	\copyright	TRiMADA AG   CH-5610 Wohlen
 *	\remark		Compiler- / IDE-Version
 *
 *	\version	06.08.20 mm
 *					- Erstellt.
 *	\version	06.08.21 mm
 *					- Umrecghnungsfunktionen ads7924_getBinary und ads7924_getVoltage
 *					  definiert.
 *	\version	01.09.21 mm
 *					- C_ads7924_maxVoltage dem neu dimensionierten Spannungsteiler
 *					  angepasst. (33k/2k2 ergeben 52V8/3V3). Die maximale Eingangs-
 *					  spannung bleibt bei 30V, das Übersprechen der DI's wird mit
 *					  dieser Massnahme jedoch verhindert.
 *
 *
 ******************************************************************************/
#ifndef __ads7924_H
    #define __ads7924_H


/******************************************************************************
 * Modules                                                                    *
 *                                                                            *
 ******************************************************************************/
#include <stdbool.h>
#include <stdint.h>


/******************************************************************************
 * Definitions and Macros                                                     *
 *                                                                            *
 ******************************************************************************/
#define	C_ads7924_addressA0lo				0x48	//!< Adresse des ADC ADS7924 wenn A0 = lo
#define	C_ads7924_addressA0hi				0x49	//!< Adresse des ADC ADS7924 wenn A0 = hi
#define	C_ads7924_registerAddressAutoInc	0x80	//!< Flag für automatisches hochzählen der Register Adressen

#define	C_ads7924_resolution				4095	//!< Auflösung des ADC
#define	C_ads7924_maxVoltage				52800	//!< maximale Eingangsspannung [mV]


/******************************************************************************
 * Typedefs and Structures                                                    *
 *                                                                            *
 ******************************************************************************/
typedef enum {
	E_ads7924_channel0,
	E_ads7924_channel1,
	E_ads7924_channel2,
	E_ads7924_channel3,
	E_ads7924_channelCount
} te_ads7924_channel;
	
typedef enum {
    E_ads7924_ra_modeCntrl,
	E_ads7924_ra_intCntrl,
	E_ads7924_ra_data0hi,
	E_ads7924_ra_data0lo,
	E_ads7924_ra_data1hi,
	E_ads7924_ra_data1lo,
	E_ads7924_ra_data2hi,
	E_ads7924_ra_data2lo,
	E_ads7924_ra_data3hi,
	E_ads7924_ra_data3lo,
	E_ads7924_ra_upperLimitThreshold0,
	E_ads7924_ra_lowerLimitThreshold0,
	E_ads7924_ra_upperLimitThreshold1,
	E_ads7924_ra_lowerLimitThreshold1,
	E_ads7924_ra_upperLimitThreshold2,
	E_ads7924_ra_lowerLimitThreshold2,
	E_ads7924_ra_upperLimitThreshold3,
	E_ads7924_ra_lowerLimitThreshold3,
	E_ads7924_ra_interruptConfig,
	E_ads7924_ra_sleepConfig,
	E_ads7924_ra_acquireConfig,
	E_ads7924_ra_powerConfig,
	E_ads7924_ra_reset,
	E_ads7924_ra_registerCount
} te_ads7924_registerAddress;

typedef enum {						//!< int pin configuration				|	conversion control event
    E_ads7924_icnf_alarm,			//!< alarm								|	alarm
	E_ads7924_icnf_busy,			//!< busy								|	alarm	
	E_ads7924_icnf_dataReadyOne,	//!< data ready, 1 conversion completed	|	data ready, 1 conversion complete	
	E_ads7924_icnf_busy_,			//!< busy								|	data ready, one conversion complete
	E_ads7924_icnf_doNotUse,		//!< do not use							|	-
	E_ads7924_icnf_doNotUse_,		//!< do not use							|	-
	E_ads7924_icnf_dataReadyAll,	//!< data ready, 4 conversion completed	|	data ready, 4 conversion complete
	E_ads7924_icnf_busy__,			//!< busy								|	data ready, 4 conversion complete
} te_ads7924_interuptPinConfig;

typedef enum {
	E_ads7924_mode_idle,
	E_ads7924_mode_awake				= 0b100000,
	E_ads7924_mode_manualSingle			= 0b110000,
	E_ads7924_mode_manualScan			= 0b110010,
	E_ads7924_mode_autoSingle			= 0b110001,
	E_ads7924_mode_autoScan				= 0b110011,
	E_ads7924_mode_autoSingleSleep		= 0b111001,
	E_ads7924_mode_autoScanSleep		= 0b111011,
	E_ads7924_mode_autoBurstScanSleep	= 0b111111,
} te_ads7924_mode;

typedef enum {
	E_ads7924_slt_2dot5ms,
	E_ads7924_slt_5ms,
	E_ads7924_slt_10ms,
	E_ads7924_slt_20ms,
	E_ads7924_slt_40ms,
	E_ads7924_slt_80ms,
	E_ads7924_slt_160ms,
	E_ads7924_slt_320ms
} te_ads7924_sleepTime;

typedef union {
	uint8_t	value;

	struct {
		te_ads7924_channel	mc_channel	: 2;	//!< Channel 0..3, Warnung wegen E_ads7924_channelCount
		te_ads7924_mode		mc_mode		: 6;	//!< Mode
	};
} tu_ads7924_modeControl;

typedef struct {
	uint8_t	ictl_alarmEnableCh0	: 1;	//!<
	uint8_t	ictl_alarmEnableCh1	: 1;	//!<
	uint8_t	ictl_alarmEnableCh2	: 1;	//!<
	uint8_t	ictl_alarmEnableCh3	: 1;	//!<
	uint8_t	ictl_alarmStatusCh0	: 1;	//!< read, status unmasked 
	uint8_t	ictl_alarmStatusCh1	: 1;	//!< read, status unmasked
	uint8_t	ictl_alarmStatusCh2	: 1;	//!< read, status unmasked
	uint8_t	ictl_alarmStatusCh3	: 1;	//!< read, status unmasked
} ts_ads7924_interruptControl;

typedef struct {
	uint8_t	icnf_intTrig		: 1;	//!<
	uint8_t	icnf_intPolarity	: 1;	//!<
	uint8_t	icnf_intConfig		: 3;	//!<
	uint8_t	icnf_alarmCount		: 3;	//!< = 0: every conversion generates an alarm
										//!< > 0: exceeding the threshold limit x times generates an alarm
} ts_ads7924_interruptConfig;

typedef struct {
	uint8_t	scnf_sleepTime			: 3;	//!< see te_ads7924_sleepTime
	uint8_t	scnf_alwaysWrite0		: 1;	//!<
	uint8_t	scnf_sleepTimeMul8		: 1;	//!< = 0: x1	= 1: x8
	uint8_t	scnf_sleepTimeDiv4		: 1;	//!< = 0: :1	= 1: :4
	uint8_t	scnf_conversionControl	: 1;	//!< = 0: conversions continue, independent of the control event status
											//!< = 1: conversions are stopped as soon as control event occurs
	uint8_t	scnf_alwaysWrite0_		: 1;	//!<
} ts_ads7924_sleepConfig;

typedef struct {
	uint8_t	pcnf_powerUpTimeSetting	: 5;	//!< Wert x 2 us
	uint8_t	pcnf_powerConEnable		: 1;	//!< Pin PowerCon
	uint8_t	pcnf_powerConPolarity	: 1;	//!< Pin PowerCon pin polarity
	uint8_t	pcnf_calibrationControl	: 1;	//!< 
} ts_ads7924_powerConfig;

typedef union {
	uint8_t	value;

	struct {
		uint8_t	acnf_acquireTime	: 5;	//!< (Wert x 2 us) + 6 us
		uint8_t	acnf_alwaysWrite0_	: 3;	//!<
	};
} tu_ads7924_acquireConfig;

/*
typedef struct {
	uint8_t	acnf_acquireTime		: 5;	//!< (Wert x 2 us) + 6 us
	uint8_t	acnf_alwaysWrite0_		: 3;	//!<
} ts_ads7924_acquireConfig;
*/

typedef struct {
	uint16_t	adcValue [E_ads7924_channelCount];
	uint16_t	voltage [E_ads7924_channelCount];
	uint16_t	voltageOn [E_ads7924_channelCount];
	uint16_t	voltageOff [E_ads7924_channelCount];
	bool		logicLevel [E_ads7924_channelCount];
} ts_ads7924_dataBuffer;


/******************************************************************************
 * Exported Variables                                                         *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Exported Functions                                                         *
 *                                                                            *
 ******************************************************************************/
int	ads7924_setModeCntrl (int deviceDescriptor, uint8_t chipAddr, tu_ads7924_modeControl *pModeControl);
int	ads7924_getModeCntrl (int deviceDescriptor, uint8_t chipAddr, tu_ads7924_modeControl *pModeControl);
int	ads7924_setIntCntrl (int deviceDescriptor, uint8_t chipAddr, ts_ads7924_interruptControl *pInterruptControl);
int	ads7924_getIntCntrl (int deviceDescriptor, uint8_t chipAddr, ts_ads7924_interruptControl *pInterruptControl);
int	ads7924_getAllDataChannels (int deviceDescriptor, uint8_t chipAddr, ts_ads7924_dataBuffer *pDataBuffer); 
int	ads7924_setUpperLimitThreshold (int deviceDescriptor, uint8_t chipAddr, te_ads7924_channel channel, uint16_t threshold);
int	ads7924_getUpperLimitThreshold (int deviceDescriptor, uint8_t chipAddr, te_ads7924_channel channel, uint16_t *pThreshold);
int	ads7924_setLowerLimitThreshold (int deviceDescriptor, uint8_t chipAddr, te_ads7924_channel channel, uint16_t threshold);
int	ads7924_getLowerLimitThreshold (int deviceDescriptor, uint8_t chipAddr, te_ads7924_channel channel, uint16_t *pThreshold);
int	ads7924_setInterruptConfig (int deviceDescriptor, uint8_t chipAddr, ts_ads7924_interruptConfig *pInterruptConfig);
int	ads7924_getInterruptConfig (int deviceDescriptor, uint8_t chipAddr, ts_ads7924_interruptConfig *pInterruptConfig);
int	ads7924_setSleepConfig (int deviceDescriptor, uint8_t chipAddr, ts_ads7924_sleepConfig *pSleepConfig);
int	ads7924_getSleepConfig (int deviceDescriptor, uint8_t chipAddr, ts_ads7924_sleepConfig *pSleepConfig);
int	ads7924_setAcquireConfig (int deviceDescriptor, uint8_t chipAddr, tu_ads7924_acquireConfig *pAcquireConfig);
int	ads7924_getAcquireConfig (int deviceDescriptor, uint8_t chipAddr, tu_ads7924_acquireConfig *pAcquireConfig);
int	ads7924_setPowerConfig (int deviceDescriptor, uint8_t chipAddr, ts_ads7924_powerConfig *pPowerConfig); 
int	ads7924_getPowerConfig (int deviceDescriptor, uint8_t chipAddr, ts_ads7924_powerConfig *pPowerConfig); 
int	ads7924_reset (int deviceDescriptor, uint8_t chipAddr);
int	ads7924_getDeviceID (int deviceDescriptor, uint8_t chipAddr, uint8_t *pDeviceID);

int	ads7924_getBinary (int voltage);
int	ads7924_getVoltage (int binary);

#endif //__ads7924_H

/* End ads7924.h */
