/**************************************************************************//**
 *
 *	\file		di.c
 *
 *	\brief		Stellt die Funktionen für die Verwaltung der digitalen Eingänge
 *				zur Verfügung.
 *
 *	\details	-
 *
 *	\date		08.04.2021
 *	\author		Marcel Ming
 *	\copyright	TRiMADA AG   CH-5610 Wohlen
 *	\remark		Compiler- / IDE-Version
 *
 *	\version	08.04.21 mm
 *					- Erstellt.
 *	\version	20.05.21 mm
 *					- Anpassungen an die Änderungen der Tabellenstrukturelemente
 *					  in ts_db_xy.
 *	\version	03.08.21 mm
 *					- Debugmeldungen angepasst.
 *	\version	24.08.21 mm
 *					- Mapping Hardware DI [0..3] zu Datenbank DI [1..4] korrigiert.
 *					- Beim Wechseln der Eingangsfunktion wird der aktuelle Zustand
 *					  in die Datenbank geschrieben.
 *	\version	01.09.21 mm
 *					- di_processQuick implementiert.
 *	\version	06.09.21 mm
 *					- Compilerschalter __di_showDebugStrings definiert.
 *	\version	26.11.21 mm
 *					- in Funktion di_process in case E_di_fktChanges trigger
 *					  nur bei positiven Flanken setzen.
 *	\version	08.11.21 mm
 *					- in Funktion di_process den Wert von lastFunction nachgeführt
 *					  um auch bei dynamischen Konfigurationen richtig reagieren zu
 *					  können.
 *	\version	16.12.21 mm
 *					- Wert pDi->counter (zählt Flankenwechsel) beim schreiben
 *					  in die Datenbanktabelle halbiert (Pulse).
 *	\version	18.02.22 mm
 *					- Kommentar ergänzt / korrigiert.
 *	\version	28.03.22 mm
 *					- in Funktion di_process case E_di_fktCounter angepasst, da
 *					  das Zählen der Ereignisse in ioLog_sampleInterrupt erfolgt.
 *	\version	22.09.23 mm
 *					- in Funktion di_configOverride den nicht benötigten Parameter
 * 					  *pConnect gelöscht.
 *	\version	17.10.23 mm
 *					- Funktion di_mailEvent aktiviert.
 * 					- Fehler für Mailkriterium behoben.

 *
 ******************************************************************************/


/******************************************************************************
 * Compilerswitches                                                           *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Modules                                                                    *
 *                                                                            *
 ******************************************************************************/
#include <mysql/mysql.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "di.h"
#include "csw.h"
#include "db.h"
#include "mail.h"


/******************************************************************************
 * Definitions and Macros                                                     *
 *                                                                            *
 ******************************************************************************/
#define __GNU_SOURCE


/******************************************************************************
 * Typedefs and Structures                                                    *
 *                                                                            *
 ******************************************************************************/
 
/******************************************************************************
 * Exported Variables                                                         *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Global Variables                                                           *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Local Functions                                                            *
 *                                                                            *
 ******************************************************************************/

/**************************************************************************//**
 *
 *	\brief		di_getDbValue
 *
 *  \details	liest den Konfigurationswert dbIdOffset für den Kanal channel
 *				aus der Datenbanktabelle PA.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		channel		Kannalnummer des analog Eingang
 *	\param		dbIdOffset	Datenbank id Offset zur Basis id des Eintrags.
 *	\param		*pFail		Resultat Flag, wird bei einem Fehler auf 1 gesetzt
 *
 *	\return		0: Lesen erfolgreich
 *
 ******************************************************************************/
static int di_getDbValue (ts_db_connect		 *pConnect,
						  int				 channel,
						  te_db_PAidOffsetDi dbIdOffset,
						  int				 *pFail)
{
	static ts_db_pa dbPA;


	dbPA.id = C_db_paChannelBaseDi + (channel * C_db_paChannelOffsetDi) + dbIdOffset;

	if (db_rowGet (pConnect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA) == 0)
	{
		return (atoi (dbPA.strValue));
	}
	else
	{
		*pFail = 1;

		return (0);
	} // else if (db_rowGet (pConnect, E_db_tableTypePA, &dbPA) == 0)
} /* di_getDbValue */
 
 
/**************************************************************************//**
 *
 *	\brief		di_logEvent
 *
 *	\details	schreibt die Eventnummer, welche dem PA Index entspricht in die
 *				EV Tabelle.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		channel		Kannalnummer des digital Eingang
 *	\param		idOffset	Offset für Event Wert
 *
 *	\return		-
 *
 ******************************************************************************/
static void di_logEvent (ts_db_connect		*pConnect,
						 int				channel,
						 te_db_PAidOffsetDi	idOffset)
{
	static ts_db_ev	dbEV;


	// PA Index ermitteln und in Eventliste eintragen
	snprintf (dbEV.strValue, C_db_dataStringSize, "%d", C_db_paChannelBaseDi + channel * C_db_paChannelOffsetDi + idOffset);
	snprintf (dbEV.strDateTime, C_db_dataStringSize, "%s",  "");
	db_rowAdd (pConnect, E_db_tableTypeEV, E_db_listTypeNone, 0, &dbEV);

	#ifdef __di_showDebugStrings
		fprintf (stderr, ", di_logEvent: di%d EV:%s\n", channel, dbEV.strValue);
	#endif
} /* di_logEvent */


/**************************************************************************//**
 *
 *	\brief		di_mailEvent
 *
 *	\details	setzt den Mailtext zusammen und sendet ein Mail an den Empfänger
 *				gemäss ts_mail.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		channel		Kannalnummer des digitalen Eingangs 
 *	\param		PAidOffset	Parameter Offset für Event Wert
 *	\param		TPidOffset	Textparameter Offset für Event Wert
 *	\param		*pMail		Pointer auf Mail
 *
 *	\return		-
 *
 ******************************************************************************/
static void di_mailEvent (ts_db_connect		 *pConnect,
						  int				 channel,
						  te_db_PAidOffsetDi PAidOffset,
						  te_db_TPidOffsetDi TPidOffset,
						  ts_mail			 *pMail)
{
	static ts_db_pa	dbPA;
	static ts_db_tp	dbTP;


	// Mailtext zusammensetzen und senden
	dbPA.id = C_db_paChannelBaseDi + channel * C_db_paChannelOffsetDi + PAidOffset;
	db_rowGet (pConnect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA);

	dbTP.id = pMail->languageOffset + C_db_tpChannelBaseDi + channel * C_db_tpChannelOffsetDi + TPidOffset;
	db_rowGet (pConnect, E_db_tableTypeTP, E_db_listTypeNone, 0, &dbTP);

	snprintf (pMail->message, C_db_dataStringSize, "%s", dbTP.strText);
	mail_send (pMail->from, pMail->to, pMail->subject, pMail->message);

	#ifdef __di_showDebugStrings
		fprintf (stderr, ", mail from:%s, to:%s, sbj:%s, msg:%s",
				 pMail->from, pMail->to, pMail->subject, pMail->message);
	#endif
} /* di_mailEvent */


/******************************************************************************
 * Exported Functions                                                         *
 *                                                                            *
 ******************************************************************************/

/**************************************************************************//**
 *
 *	\brief		di_configDebug
 *
 *	\details	schreibt die Werte der übergebenen ts_di Struktur auf stderr
 *				aus.
 *
 *	\param		*pDi		Pointer auf Struktur ts_di
 *	\param		channel		Kannalnummer des digital Eingang
 *
 *	\return		-
 *
 ******************************************************************************/
void di_configDebug (ts_di	*pDi,
					 int	channel)
{
	fprintf (stderr, "\033[1mdi_configDebug: di%d\033[0m\n", channel);
	fprintf (stderr, "function : %d\n", pDi->function);
	fprintf (stderr, "limitLoHi: %d\n", pDi->limitLoHi);
	fprintf (stderr, "limitHiLo: %d\n", pDi->limitHiLo);
	fprintf (stderr, "enableLog: %d\n", pDi->enableLog);
	fprintf (stderr, "countTime: %d\n", pDi->countTime);
	fprintf (stderr, "msgLoHi  : %d\n", pDi->msgLoHi);
	fprintf (stderr, "msgHiLo  : %d\n", pDi->msgHiLo);
} /* di_configDebug */


/**************************************************************************//**
 *
 *  \brief		di_configRead
 *
 *  \details	liest die Konfiguration für den Kanal channel aus der Datenbank-
 *				tabelle PA.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		*pDi		Pointer auf Struktur ts_di
 *	\param		channel		Kannalnummer des digital Eingang
 *
 *	\return		0: Lesen erfolgreich
 *
 ******************************************************************************/
int di_configRead (ts_db_connect *pConnect,
                   ts_di		 *pDi,
                   int			 channel)
{
	int	fail = 0;


	if (channel < C_di_channelCount)
	{
		pDi->function	= di_getDbValue (pConnect, channel, E_db_PAoDiFunction, &fail);
		pDi->limitLoHi	= di_getDbValue (pConnect, channel, E_db_PAoDiLimitLoHi, &fail);
		pDi->limitHiLo	= di_getDbValue (pConnect, channel, E_db_PAoDiLimitHiLo, &fail);
		pDi->enableLog	= di_getDbValue (pConnect, channel, E_db_PAoDiEnableLog, &fail);
		pDi->countTime	= di_getDbValue (pConnect, channel, E_db_PAoDiCountTime, &fail);
		pDi->msgLoHi	= di_getDbValue (pConnect, channel, E_db_PAoDiMsgLoHi, &fail);
		pDi->msgHiLo	= di_getDbValue (pConnect, channel, E_db_PAoDiMsgHiLo, &fail);

		pDi->lastFunction = E_di_fktCount;
	}
	else
	{
		fail = 1;
	} // else if (channel < C_di_channelCount)
	
	return (fail);
} /* di_configRead */


/**************************************************************************//**
 *
 *  \brief		di_configOverride
 *
 *  \details	überschreibt die Konfiguration mit Testwerten.
 *
 *	\param		*pDi		Pointer auf Struktur ts_di
 *	\param		channel		Kannalnummer des digital Eingang
 *
 *	\return		-
 *
 ******************************************************************************/
void di_configOverride (ts_di	*pDi,
						int		channel)
{
	if (channel < C_di_channelCount)
	{
		#ifdef __di_showDebugStrings
			fprintf (stderr, "di_configOverride: di%d\n", channel);
		#endif

		pDi->function	= E_di_fktChanges;
//		pDi->function	= E_di_fktCounter;
		pDi->limitLoHi	= 1700;
		pDi->limitHiLo	= 1300;
		pDi->enableLog	= 1;

		// eine Minute Zählen, bis zum Speichern
		pDi->countTime	= 1;

		pDi->msgLoHi	= 0; // 308 + (channel * C_db_paChannelOffsetDi);
		pDi->msgHiLo	= 0; // 309 + (channel * C_db_paChannelOffsetDi);
	} // if (channel < C_di_channelCount)
} /* di_configOverride */


/**************************************************************************//**
 *
 *  \brief		di_processQuick
 *
 *  \details	wertet das Signal des digitalen Eingangs ohne Datenbankeinträge
 *				aus um eine schnelle Hardwarereaktion zu ermöglichen.
 *
 *	\param		*pDi		Pointer auf Struktur ts_di
 *	\param		channel		Kannalnummer des digital Eingang 
 *
 *	\return		-
 *
 ******************************************************************************/
void di_processQuick (ts_di	*pDi,
					  int	channel)
{
} /* di_processQuick */


/**************************************************************************//**
 *
 *  \brief		di_process
 *
 *  \details	wertet das Signal des digitalen Eingangs aus.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		*pDi		Pointer auf Struktur ts_di
 *	\param		channel		Kannalnummer des digital Eingang
 *
 *	\return		-
 *
 ******************************************************************************/
int di_process (ts_db_connect	*pConnect,
				ts_di			*pDi,
				int				channel,
				ts_mail			*pMail)
{
	static ts_db_di	dbDI;
	static ts_db_di	dbDL;
	static ts_db_pa	dbPA;
//	static ts_db_tp	dbTP;  // brauchts nach Verwendung von di_mailEvent nicht mehr
	static char		strUnit [C_db_dataStringSize];


	if (!pDi->readError)
	{
		// Unit lesen
		dbPA.id = C_db_paChannelBaseDi + channel * C_db_paChannelOffsetDi + E_db_PAoDiScaleUnit;
		db_rowGet (pConnect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA);
		snprintf (strUnit, C_db_dataStringSize, "%s", dbPA.strValue);

		#ifdef __di_showDebugStrings
			fprintf (stderr, "di_process: di%d: %d, ", channel, pDi->stateActual);
		#endif

		switch (pDi->function)
		{
			case E_di_fktOff:
				#ifdef __di_showDebugStrings
					fprintf (stderr, "off\n");
				#endif

				if (pDi->lastFunction != E_di_fktOff)
				{
					pDi->lastFunction = E_di_fktOff;
					dbDI.id = channel + 1;
					snprintf (dbDI.strValue, C_db_dataStringSize, "off"); 

					if (db_rowUpdate (pConnect, E_db_tableTypeDI, &dbDI))
					{
						db_rowAdd (pConnect, E_db_tableTypeDI, E_db_listTypeNone, dbDI.id, &dbDI);
					}
				} // if (pDi->lastfunction != E_di_fktOff)
				break;

			case E_di_fktChanges:
				if ((pDi->stateLogged != pDi->stateActual) || (pDi->lastFunction != E_di_fktChanges))
				{
					#ifdef __di_showDebugStrings
						fprintf (stderr, "input changes");
					#endif

					pDi->stateLogged	= pDi->stateActual;
					pDi->lastFunction	= E_di_fktChanges;

					if (pDi->stateActual)
					{
						// nur auf positive Flanken triggern
						pDi->trigger = 1;
					}

					dbDI.id = channel + 1;
					snprintf (dbDI.strValue, C_db_dataStringSize, "%d", pDi->stateActual); 

					if (db_rowUpdate (pConnect, E_db_tableTypeDI, &dbDI))
					{
						db_rowAdd (pConnect, E_db_tableTypeDI, E_db_listTypeNone, dbDI.id, &dbDI);
					}

					if (pDi->enableLog)
					{
						#ifdef __di_showDebugStrings
							fprintf (stderr, ", log enabled");
						#endif

						// aktueller Wert in DLDIx Tabelle schreiben
						dbDL.id = channel + 1;
						snprintf (dbDL.strValue, C_db_dataStringSize, "%d", pDi->stateActual);
						db_rowAdd (pConnect, E_db_tableTypeDL, E_db_listTypeDI, dbDL.id, &dbDL);
					} // if (pDi->enableLog)

					// Ereignis aufbereiten und in Eventliste eintragen
					if (pDi->stateActual == 0)
					{
						#ifdef __di_showDebugStrings
							fprintf (stderr, ", stateActual = 0");
						#endif

						di_logEvent (pConnect, channel, E_db_PAoDiMsgHiLo);

						if ((pMail->mode != E_mail_modeOff) && (pDi->msgHiLo > 0))
						{
							#ifdef __di_showDebugStrings
								fprintf (stderr, ", pMail->mode active and pDi->msgHiLo > 0");
							#endif

							di_mailEvent (pConnect, channel, E_db_PAoDiMsgHiLo, E_db_TPoDiMsgChangeTrueFalse, pMail);

							/* Mailtext zusammensetzen und senden
							dbPA.id = C_db_paChannelBaseDi + channel * C_db_paChannelOffsetDi + E_db_TPoDiMsgChangeTrueFalse;
							db_rowGet (pConnect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA);

							dbTP.id = pMail->languageOffset + C_db_tpChannelBaseDi + channel * C_db_tpChannelOffsetDi + E_db_TPoDiMsgChangeTrueFalse;
							db_rowGet (pConnect, E_db_tableTypeTP, E_db_listTypeNone, 0, &dbTP);

							snprintf (pMail->message, C_db_dataStringSize, "%s", dbTP.strText);
							mail_send (pMail->from, pMail->to, pMail->subject, pMail->message);

							#ifdef __di_showDebugStrings
								fprintf (stderr, ", mail from:%s, to:%s, sbj:%s, msg:%s",
												pMail->from, pMail->to, pMail->subject, pMail->message);
							#endif
							*/  
						} // if ((pMail->mode != E_mail_modeOff) && (pDi.msgHiLo > 0))
					}
					else
					{
						di_logEvent (pConnect, channel, E_db_PAoDiMsgLoHi);

						if ((pMail->mode != E_mail_modeOff) && (pDi->msgLoHi > 0))
						{
							di_mailEvent (pConnect, channel, E_db_PAoDiMsgLoHi, E_db_TPoDiMsgChangeFalseTrue, pMail);

							/* Mailtext zusammensetzen und senden
							dbPA.id = C_db_paChannelBaseDi + channel * C_db_paChannelOffsetDi + E_db_TPoDiMsgChangeFalseTrue;
							db_rowGet (pConnect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA);

							dbTP.id	= pMail->languageOffset + C_db_tpChannelBaseDi + channel * C_db_tpChannelOffsetDi + E_db_TPoDiMsgChangeFalseTrue;
							db_rowGet (pConnect, E_db_tableTypeTP, E_db_listTypeNone, 0, &dbTP);

							snprintf (pMail->message, C_db_dataStringSize, "%s", dbTP.strText);
							mail_send (pMail->from, pMail->to, pMail->subject, pMail->message);

							#ifdef __di_showDebugStrings
								fprintf (stderr, ", mail from:%s, to:%s, sbj:%s, msg:%s",
												pMail->from, pMail->to, pMail->subject, pMail->message);
							#endif
							*/
						} // if ((pMail->mode != E_mail_modeOff) && (pDi.msgLoHi > 0))
					} // else if (pDi->stateActual == 0)

					#ifdef __di_showDebugStrings
						fprintf (stderr, "\n");
					#endif
				} // if (pDi->stateLogged != pDi->stateActual)
				else
				{
					#ifdef __di_showDebugStrings
						fprintf (stderr, "no changes\n");
					#endif
				}
				break;

			case E_di_fktCounter:
				pDi->lastFunction = E_di_fktCounter;

				#ifdef __di_showDebugStrings
					fprintf (stderr, "counter\n");
				#endif

				// Zählfunktion bereits in ioLog_sampleInterrupt ausgeführt
				if (pDi->counted)
				{
					pDi->counted = 0;

					// Ereignis aufbereiten und in Eventliste eintragen
					if (pDi->stateActual == 0)
					{
						#ifdef __di_showDebugStrings
							fprintf (stderr, ", stateActual = 0");
						#endif

						di_logEvent (pConnect, channel, E_db_PAoDiMsgHiLo);

						if ((pMail->mode != E_mail_modeOff) && (pDi->msgHiLo > 0))
						{
							#ifdef __di_showDebugStrings
								fprintf (stderr, ", pMail->mode active and pDi->msgHiLo > 0");
							#endif

							di_mailEvent (pConnect, channel, E_db_PAoDiMsgHiLo, E_db_TPoDiMsgChangeTrueFalse, pMail);

							/* Mailtext zusammensetzen und senden
							dbPA.id = C_db_paChannelBaseDi + channel * C_db_paChannelOffsetDi + E_db_TPoDiMsgChangeTrueFalse;
							db_rowGet (pConnect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA);

							dbTP.id = pMail->languageOffset + C_db_tpChannelBaseDi + channel * C_db_tpChannelOffsetDi + E_db_TPoDiMsgChangeTrueFalse;
							db_rowGet (pConnect, E_db_tableTypeTP, E_db_listTypeNone, 0, &dbTP);

							snprintf (pMail->message, C_db_dataStringSize, "%s", dbTP.strText);
							mail_send (pMail->from, pMail->to, pMail->subject, pMail->message);

							#ifdef __di_showDebugStrings
								fprintf (stderr, ", mail from:%s, to:%s, sbj:%s, msg:%s",
												pMail->from, pMail->to, pMail->subject, pMail->message);
							#endif
							*/
						} // if ((pMail->mode != E_mail_modeOff) && (pDi.msgHiLo > 0))
					}
					else
					{
						di_logEvent (pConnect, channel, E_db_PAoDiMsgLoHi);

						if ((pMail->mode != E_mail_modeOff) && (pDi->msgLoHi > 0))
						{
							di_mailEvent (pConnect, channel, E_db_PAoDiMsgLoHi, E_db_TPoDiMsgChangeFalseTrue, pMail);

							/* Mailtext zusammensetzen und senden
							dbPA.id = C_db_paChannelBaseDi + channel * C_db_paChannelOffsetDi + E_db_TPoDiMsgChangeFalseTrue;
							db_rowGet (pConnect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA);

							dbTP.id	= pMail->languageOffset + C_db_tpChannelBaseDi + channel * C_db_tpChannelOffsetDi + E_db_TPoDiMsgChangeFalseTrue;
							db_rowGet (pConnect, E_db_tableTypeTP, E_db_listTypeNone, 0, &dbTP);

							snprintf (pMail->message, C_db_dataStringSize, "%s", dbTP.strText);
							mail_send (pMail->from, pMail->to, pMail->subject, pMail->message);

							#ifdef __di_showDebugStrings
								fprintf (stderr, ", mail from:%s, to:%s, sbj:%s, msg:%s",
												pMail->from, pMail->to, pMail->subject, pMail->message);
							#endif
							*/
						} // if ((pMail->mode != E_mail_modeOff) && (pDi.msgLoHi > 0))
					} // else if (pDi->stateActual == 0)
				} // if (pDi->stateLogged != pDi->stateActual)

				if (pDi->lastFunction != E_di_fktCounter)
				{
					pDi->lastFunction = E_di_fktCounter;
					dbDI.id = channel + 1;
					snprintf (dbDI.strValue, C_db_dataStringSize, "counter"); 

					// ohne Auswertung des Rückgabewertes
					if (db_rowUpdate (pConnect, E_db_tableTypeDI, &dbDI))
					{
						db_rowAdd (pConnect, E_db_tableTypeDI, E_db_listTypeNone, dbDI.id, &dbDI);
					}
				} // if (pDi->lastfunction != E_di_fktOff)

				break;

			default:
				pDi->lastFunction = E_di_fktCount;

				#ifdef __di_showDebugStrings
					fprintf (stderr, "default -> error\n");
				#endif
				break;

		} // switch (pDi->function)
	} // if (!pDi->readError)
} /* di_process */


/**************************************************************************//**
 *
 *  \brief		di_processCounter
 *
 *  \details	wertet das Signal des digitalen Eingangs im Countermode aus.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		*pDi		Pointer auf Struktur ts_di
 *	\param		channel		Kannalnummer des digital Eingang
 *
 *	\return		-
 *
 ******************************************************************************/
int di_processCounter (ts_db_connect *pConnect,
					   ts_di		 *pDi,
					   int			 channel)
{
	static ts_db_di	dbDI;
	static ts_db_dl	dbDL;


	if (pDi->function == E_di_fktCounter)
	{
		if (pDi->remainingTime > 0)
		{
			pDi->remainingTime--;
		}
		else
		{
			pDi->remainingTime = pDi->countTime - 1;

			dbDI.id = channel + 1;
			snprintf (dbDI.strValue, C_db_dataStringSize, "%d", pDi->counter >> 1);	// quick div 2

			if (db_rowUpdate (pConnect, E_db_tableTypeDI, &dbDI))
			{
				db_rowAdd (pConnect, E_db_tableTypeDI, E_db_listTypeNone, dbDI.id, &dbDI);
			}

			if (pDi->enableLog)
			{
				// Zähler in DLDIx Tabelle schreiben
				dbDL.id = channel + 1;
				snprintf (dbDL.strValue, C_db_dataStringSize, "%d", pDi->counter >> 1);	// quick div 2
				db_rowAdd (pConnect, E_db_tableTypeDL, E_db_listTypeDI, dbDL.id, &dbDL);
			} // if (pDi->enableLog)

			pDi->counter = 0;
		} // else if (pDi->remainingTime > 0)
	} // if (pDi->function == E_di_fktCounter)
} /* di_processCounter */


/* di.c */

