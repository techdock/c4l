/**************************************************************************//**
 *
 *	\file		ads1115.c
 *
 *	\brief		stellt Funktionen für den ADS1115 Zugriff zur Verfügung.
 *
 *	\details	Der ADS1115 ist ein ADC mit 16 Bit Auflösung, 4 Kanälen und einer
 *				i2c Schnittstelle.
 *
 *	\date		22.01.2021
 *	\author		Marcel Ming
 *	\copyright	TRiMADA AG   CH-5610 Wohlen
 *	\remark		Compiler- / IDE-Version
 *
 *	\version	22.01.21 mm
 *					- Erstellt.
 *	\version	18.02.22 mm
 *					- Kommentar ergänzt / korrigiert.
 *
 *
 ******************************************************************************/


/******************************************************************************
 * Compilerswitches                                                           *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Modules                                                                    *
 *                                                                            *
 ******************************************************************************/
#include <i2c/smbus.h>
#include <linux/i2c-dev.h>
#include <stdint.h>
#include <sys/ioctl.h>

#include "ads1115.h"


#include <stdio.h>


/******************************************************************************
 * Definitions and Macros                                                     *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Typedefs and Structures                                                    *
 *                                                                            *
 ******************************************************************************/
 
/******************************************************************************
 * Exported Variables                                                         *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Global Variables                                                           *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Local Functions                                                            *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Exported Functions                                                         *
 *                                                                            *
 ******************************************************************************/

/**************************************************************************//**
 *
 *  \brief		ads1115_getConversion
 *
 *  \details	gibt den Wert des Conversion Registers zurück. 
 *
 *	\param		deviceDescriptor	Deskriptor zum geöffneten i2c Kontroller.
 * 	\param		chipAddr			Adresse des ADS1115.
 *  \param		*pAdcValue			Pointer auf den Daten Buffer.
 *
 *	\return		0	Funktion erfolgreich ausgeführt
 * 				< 0 Funktion fehlgeschlagen
 *
 ******************************************************************************/
int	ads1115_getConversion (int		deviceDescriptor,
                           uint8_t	chipAddr, 
	                       int16_t	*pAdcValue)
{
	int32_t conversion;
	
	// i2c Controller auf ADS1115 Adresse setzen 
	if (ioctl (deviceDescriptor, I2C_SLAVE, chipAddr) < 0)
	{
		return (-1);
	}
	else
	{
		conversion = i2c_smbus_read_word_data (deviceDescriptor, E_ads1115_ra_conversion);

		if (conversion < 0)
		{
			return (-2);
		}
		else
		{
			// 16 Bit maskieren
			conversion = conversion & 0xffff;

			// hi und lo Byte tauschen
			*pAdcValue = (int16_t)((conversion >> 8) | (conversion << 8));

			return (0);
		} // else if (conversion < 0)
	} // else if (ioctl (deviceDescriptor, I2C_SLAVE, chipAddr) < 0)
} /* ads1115_getConversion */


/**************************************************************************//**
 *
 *  \brief		ads1115_setConfig
 *
 *  \details	setzt die gewünschte Konfiguration im Config Register.
 *
 *	\param		deviceDescriptor	Deskriptor zum geöffneten i2c Kontroller.
 * 	\param		chipAddr			Adresse des ADS1115.
 *  \param		*pConfig			Pointer auf die Konfiguration
 *
 *	\return		0	Funktion erfolgreich ausgeführt
 * 				< 0 Funktion fehlgeschlagen
 *
 ******************************************************************************/
int	ads1115_setConfig (int				 deviceDescriptor,
	                   uint8_t			 chipAddr, 
	                   tu_ads1115_config *pConfig)
{
	uint16_t configHiLoSwitched;
	
	// i2c Controller auf ADS1115 Adresse setzen
	if (ioctl (deviceDescriptor, I2C_SLAVE, chipAddr) < 0)
	{
		return (-1);
	}
	else
	{
		// hi und lo Byte tauschen
		configHiLoSwitched = (pConfig->value << 8) | (pConfig->value >> 8); 

		if (i2c_smbus_write_word_data (deviceDescriptor, E_ads1115_ra_config, configHiLoSwitched) < 0)
		{
			return (-1);
		}

		return (0);
	} // else if (ioctl (deviceDescriptor, I2C_SLAVE, chipAddr) < 0)
} /* ads1115_setConfig */


/**************************************************************************//**
 *
 *  \brief		ads1115_getConfig
 *
 *  \details	gibt das Config Register zurück.
 *
 *	\param		deviceDescriptor	Deskriptor zum geöffneten i2c Kontroller.
 * 	\param		chipAddr			Adresse des ADS1115.
 *  \param		*pConfig			Pointer auf die Konfiguration
 *
 *	\return		0	Funktion erfolgreich ausgeführt
 * 				< 0 Funktion fehlgeschlagen
 *
 ******************************************************************************/
int	ads1115_getConfig (int				 deviceDescriptor,
	                   uint8_t			 chipAddr, 
	                   tu_ads1115_config *pConfig)
{
	uint16_t configHiLoSwitched;

	// i2c Controller auf ADS1115 Adresse setzen
	if (ioctl (deviceDescriptor, I2C_SLAVE, chipAddr) < 0)
	{
		return (-1);
	}
	else
	{
		configHiLoSwitched = i2c_smbus_read_byte_data (deviceDescriptor, E_ads1115_ra_config);

		// hi und lo Byte tauschen
		pConfig->value = (configHiLoSwitched >> 8) | (configHiLoSwitched << 8); 

		return (0);
	} // else if (ioctl (deviceDescriptor, I2C_SLAVE, chipAddr) < 0)
} /* ads1115_getConfig */


/**************************************************************************//**
 *
 *  \brief		ads1115_setUpperThreshold
 *
 *  \details	
 *
 *	\param		deviceDescriptor	Deskriptor zum geöffneten i2c Kontroller.
 * 	\param		chipAddr			Adresse des ADS1115.
 *  \param		threshold			oberer Threshold
 *
 *	\return		0	Funktion erfolgreich ausgeführt
 * 				< 0 Funktion fehlgeschlagen
 *
 ******************************************************************************/
int	ads1115_setUpperThreshold (int		deviceDescriptor,
			                   uint8_t	chipAddr, 
	                           uint16_t threshold)
{
	// i2c Controller auf ADS1115 Adresse setzen
	if (ioctl (deviceDescriptor, I2C_SLAVE, chipAddr) < 0)
	{
		return (-1);
	}
	else
	{
		if (i2c_smbus_write_word_data (deviceDescriptor, E_ads1115_ra_hiThreshold, threshold) < 0)
		{
			return (-1);
		}

		return (0);
	} // else if (ioctl (deviceDescriptor, I2C_SLAVE, chipAddr) < 0)
} /* ads1115_setUpperThreshold */


/**************************************************************************//**
 *
 *  \brief		ads1115_getUpperThreshold
 *
 *  \details	gibt das Upper Threshold Register zurück.
 *
 *	\param		deviceDescriptor	Deskriptor zum geöffneten i2c Kontroller.
 * 	\param		chipAddr			Adresse des ADS1115.
 *  \param		*pThreshold			Pointer auf oberen Threshold
 *
 *	\return		0	Funktion erfolgreich ausgeführt
 * 				< 0 Funktion fehlgeschlagen
 *
 ******************************************************************************/
int	ads1115_getUpperThreshold (int		deviceDescriptor,
			                   uint8_t	chipAddr, 
							   uint16_t *pThreshold)
{
	// i2c Controller auf ADS1115 Adresse setzen
	if (ioctl (deviceDescriptor, I2C_SLAVE, chipAddr) < 0)
	{
		return (-1);
	}
	else
	{
		*pThreshold = i2c_smbus_read_word_data (deviceDescriptor, E_ads1115_ra_hiThreshold);

		return (0);
	} // else if (ioctl (deviceDescriptor, I2C_SLAVE, chipAddr) < 0)
} /* ads1115_getUpperThreshold */


/**************************************************************************//**
 *
 *  \brief		ads1115_setLowerThreshold
 *
 *  \details	
 *
 *	\param		deviceDescriptor	Deskriptor zum geöffneten i2c Kontroller.
 * 	\param		chipAddr			Adresse des ADS1115.
 *  \param		threshold			unterer Threshold
 *
 *	\return		0	Funktion erfolgreich ausgeführt
 * 				< 0 Funktion fehlgeschlagen
 *
 ******************************************************************************/
int	ads1115_setLowerThreshold (int		deviceDescriptor,
			                   uint8_t	chipAddr, 
	                           uint16_t threshold)
{
	// i2c Controller auf ADS1115 Adresse setzen
	if (ioctl (deviceDescriptor, I2C_SLAVE, chipAddr) < 0)
	{
		return (-1);
	}
	else
	{
		if (i2c_smbus_write_word_data (deviceDescriptor, E_ads1115_ra_loThreshold, threshold) < 0)
		{
			return (-1);
		}

		return (0);
	} // else if (ioctl (deviceDescriptor, I2C_SLAVE, chipAddr) < 0)
} /* ads1115_setLowerThreshold */


/**************************************************************************//**
 *
 *  \brief		ads1115_getLowerThreshold
 *
 *  \details	
 *
 *	\param		deviceDescriptor	Deskriptor zum geöffneten i2c Kontroller.
 * 	\param		chipAddr			Adresse des ADS1115.
 *  \param		*pThreshold			Pointer auf unteren Threshold
 *
 *	\return		0	Funktion erfolgreich ausgeführt
 * 				< 0 Funktion fehlgeschlagen
 *
 ******************************************************************************/
int	ads1115_getLowerThreshold (int		deviceDescriptor,
			                   uint8_t	chipAddr, 
	                           uint16_t *pThreshold)
{
	// i2c Controller auf ADS1115 Adresse setzen
	if (ioctl (deviceDescriptor, I2C_SLAVE, chipAddr) < 0)
	{
		return (-1);
	}
	else
	{
		*pThreshold = i2c_smbus_read_word_data (deviceDescriptor, E_ads1115_ra_loThreshold);

		return (0);
	} // else if (ioctl (deviceDescriptor, I2C_SLAVE, chipAddr) < 0)
} /* ads1115_getLowerThreshold */


/* End ads1115.c */
