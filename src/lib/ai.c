/**************************************************************************//**
 *
 *	\file		ai.c
 *
 *	\brief		Stellt die Funktionen für die Verwaltung der analogen Eingänge
 *				zur Verfügung.
 *
 *	\details	-
 *
 *	\date		09.03.2021
 *	\author		Marcel Ming
 *	\copyright	TRiMADA AG   CH-5610 Wohlen
 *	\remark		Compiler- / IDE-Version
 *
 *	\version	09.03.21 mm
 *					- Erstellt.
 *	\version	07.04.21 mm
 *					- Anpassungen an Pflichtenheft Revision 4.
 *	\version	20.05.21 mm
 *					- Anpassungen an die Änderungen der Tabellenstrukturelemente
 *					  in ts_db_xy.
 *	\version	03.08.21 mm
 *					- Debugmeldungen angepasst.
 *	\version	19.08.21 mm
 *					- Fehler in Funktion ai_getBinary, ai_getCurrent und
 *					  ai_getVoltage korrigiert.
 *	\version	30.08.21 mm
 *					- skalierter Werte in VA Tabelle schreiben von ai_logScaled
 *					  in ai_process verschoben damit nach neuer Anforderung
 *					  unabhängig vom Logintervall.
 *	\version	01.09.21 mm
 *					- ai_processQuick implementiert.
 *	\version	06.09.21 mm
 *					- Compilerschalter __ai_showDebugStrings definiert.
 *	\version	09.09.21 mm
 *					- PA505 (Einheit zum Pegel) und 555 von uADC/mVDC auf 0/1
 *					  geändert um Konfigurationsfehler zu vermeiden.
 *					  Grund: Beim Testen wurde PA505 als mV definiert, was von
 *					  ai_configRead nicht erkannt und somit falsch interpretiert
 *					  wurde.
 *	\version	29.10.21 mm
 *					- Compilerschalter __csw_configAiCurrent implementiert.
 *	\version	03.11.21 mm
 *					- Eingangspegel beim Umrechnen auf Spannung oder Strom auf
 *					  positive Werte begrenzen um Überlauf beim Kopieren auf
 *					  die Ausgänge zu vermeiden.
 *	\version	05.11.21 mm
 *					- bei der Berechnung von scaledValue Operator vor scaleOffset
 *					  von - auf + geändert. Bei einem negativen Offset wird der
 *					  caledValue nun auf die negative Seite gezogen.
 *	\version	24.11.21 mm
 *					- Funktion ai_process schreibt bei ausgeschaltetem AI den
 *					  Wert "off" in die AI und VA Tabellen.
 *	\version	29.11.21 mm
 *					- Compilerschalter __csw_configAiCurrent durch csw_flag
 *					  ersetzt.
 *	\version	08.11.21 mm
 *					- lastEnableFunction mit !pAi->enableFunction initialisiert
 *					  um in jedem Fall einen Eintrag in AI und VA zu erhalten.
 *	\version	16.12.21 mm
 *					- Funktion ai_getDbText implementiert.
 *	\version	22.09.23 mm
 *					- in Funktion ai_configOverride den nicht benötigten Parameter
 * 					  *pConnect gelöscht.
 *
 *
 ******************************************************************************/


/******************************************************************************
 * Compilerswitches                                                           *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Modules                                                                    *
 *                                                                            *
 ******************************************************************************/
#include <mysql/mysql.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ads1115.h"
#include "ai.h"
#include "csw.h"
#include "db.h"
#include "mail.h"


/******************************************************************************
 * Definitions and Macros                                                     *
 *                                                                            *
 ******************************************************************************/
#define __GNU_SOURCE


/******************************************************************************
 * Typedefs and Structures                                                    *
 *                                                                            *
 ******************************************************************************/
 
/******************************************************************************
 * Exported Variables                                                         *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Global Variables                                                           *
 *                                                                            *
 ******************************************************************************/
const char* const C_ai_operatorText [] = {
	"  \0",
	"==\0",
	"<>\0",
	">\0",
	"<\0",
	">=\0",
	"<=\0"
};


/******************************************************************************
 * Local Functions                                                            *
 *                                                                            *
 ******************************************************************************/

/**************************************************************************//**
 *
 *	\brief		ai_getDbText
 *
 *	\details	liest den Konfigurationstext dbIdOffset für den Kanal channel
 *				aus der Datenbanktabelle PA.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		channel		Kannalnummer des analog Eingangs.
 *	\param		dbIdOffset	Datenbank id Offset zur Basis id des Eintrags.
 *	\param		*pText		Pointer auf den gelesenen Text.
 *
 *	\return		 0: Lesen erfolgreich
 * 				-1: Fehler beim Lesen
 *
 ******************************************************************************/
static int ai_getDbText (ts_db_connect		*pConnect,
						 int				channel,
						 te_db_PAidOffsetAi	dbIdOffset,
						 char				*pText)
{
	ts_db_pa	dbPA;
	int			retValue = -1;


	dbPA.id = C_db_paChannelBaseAi + (channel * C_db_paChannelOffsetAi) + dbIdOffset;

	if (db_rowGet (pConnect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA) == 0)
	{
		snprintf (pText, C_db_dataStringSize, "%s", dbPA.strValue);
		retValue = 0;
	} // if (db_rowGet (pConnect, E_db_tableTypePA, &dbPA) == 0)

	return (retValue);
} /* ai_getDbText */


/**************************************************************************//**
 *
 *	\brief		ai_getDbValue
 *
 *	\details	liest den Konfigurationswert dbIdOffset für den Kanal channel
 *				aus der Datenbanktabelle PA.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		channel		Kannalnummer des analog Eingangs.
 *	\param		dbIdOffset	Datenbank id Offset zur Basis id des Eintrags.
 *	\param		*pFail		Resultat Flag, wird bei einem Fehler auf 1 gesetzt
 *
 *	\return		Integerwert aus Datenbank
 *
 ******************************************************************************/
static int ai_getDbValue (ts_db_connect			*pConnect,
						  int					channel,
						  te_db_PAidOffsetAi	dbIdOffset,
						  int					*pFail)
{
	static ts_db_pa dbPA;


	dbPA.id = C_db_paChannelBaseAi + (channel * C_db_paChannelOffsetAi) + dbIdOffset;

	if (db_rowGet (pConnect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA) == 0)
	{
		return (atoi (dbPA.strValue));
	}
	else
	{
		*pFail = 1;

		return (0);
	} // else if (db_rowGet (pConnect, E_db_tableTypePA, &dbPA) == 0)
} /* ai_getDbValue */


/**************************************************************************//**
 *
 *	\brief		ai_getMean
 *
 *	\details	bildet mit den bisherigen Datenpunkten und dem neuen Wert den
 *				aktuellen Mittelwert.
 *
 *	\param		*pMean		Pointer auf Struktur ts_ai_mean
 *	\param		newValue	neuer Datenpunkt
 *
 *	\return		Mittelwert
 *
 ******************************************************************************/
static int16_t ai_getMean (ts_ai_mean	*pMean,
						   int16_t		newValue)
{
	pMean->data [pMean->posNewest] = newValue; 
	pMean->sum = pMean->sum - pMean->data [pMean->posOldest] + newValue;

	// Anzahl gültiger Datenpunkte nachführen
	if (pMean->posCount < pMean->posMax)
	{
		pMean->posCount++;
	}

	// Position auf neusten Eintrag nachführen
	if (pMean->posNewest < pMean->posMax)
	{
		pMean->posNewest++;
	}
	else
	{
		pMean->posNewest = 0;
	}

	// Position auf ältesten Eintrag nachführen
	if (pMean->posOldest < pMean->posMax)
	{
		pMean->posOldest++;
	}
	else
	{
		pMean->posOldest = 0;
	}

	#ifdef __ai_showDebugStrings
		/*
		fprintf (stderr, "ai_getMean: sum\t%d\n", pMean->sum);
		fprintf (stderr, "ai_getMean: posNewest\t%d\n", pMean->posNewest);
		fprintf (stderr, "ai_getMean: posOldest\t%d\n", pMean->posOldest);
		fprintf (stderr, "ai_getMean: posCount\t%d\n", pMean->posCount);
		fprintf (stderr, "ai_getMean: posMax\t%d\n", pMean->posMax);
		fprintf (stderr, "ai_getMean: avg\t%d\n", (pMean->sum / pMean->posCount));
		*/
	#endif

	// Mittelwert berechnen
	return ((int16_t)(pMean->sum / pMean->posCount));
} /* ai_getMean */


/**************************************************************************//**
 *
 *	\brief		ai_logEvent
 *
 *	\details	schreibt die Eventnummer, welche dem PA Index entspricht in die
 *				EV Tabelle.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		channel		Kannalnummer des digitalen Eingangs 
 *	\param		idOffset	Offset für Event Wert
 *
 *	\return		-
 *
 ******************************************************************************/
static void ai_logEvent (ts_db_connect		*pConnect,
						 int				channel,
						 te_db_PAidOffsetAi	idOffset)
{
	static ts_db_ev	dbEV;


	// PA Index ermitteln und in Eventliste eintragen
	snprintf (dbEV.strValue, C_db_dataStringSize, "%d", C_db_paChannelBaseAi + channel * C_db_paChannelOffsetAi + idOffset);
	snprintf (dbEV.strDateTime, C_db_dataStringSize, "%s",  "");
	db_rowAdd (pConnect, E_db_tableTypeEV, E_db_listTypeNone, 0, &dbEV);

	#ifdef __ai_showDebugStrings
		fprintf (stderr, "ai_logEvent: ai%d EV:%s\n", channel, dbEV.strValue);
	#endif
} /* ai_logEvent */


/**************************************************************************//**
 *
 *	\brief		ai_checkLimits
 *
 *	\details	überprüft den Wert auf die verschiedenen Limiten und reagiert
 *				entsprechend.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		*pAi		Pointer auf Struktur ts_ai
 *	\param		channel		Kannalnummer des digitalen Eingangs 
 *	\param		*pMail		Pointer auf Struktur ts_mail
 *
 *	\return		-
 *
 ******************************************************************************/
static void ai_checkLimits (ts_db_connect	*pConnect,
							ts_ai			*pAi,
							int				channel,
							ts_mail			*pMail)
{
	static ts_db_pa	dbPA;
	static ts_db_tp	dbTP;
	static char		strUnit [C_db_dataStringSize];


	// Unit lesen
	dbPA.id = C_db_paChannelBaseAi + channel * C_db_paChannelOffsetAi + E_db_PAoAiScaleUnit;
	db_rowGet (pConnect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA);
	snprintf (strUnit, C_db_dataStringSize, "%s", dbPA.strValue);

	#ifdef __ai_showCheckLimitsStrings
		fprintf (stderr, "ai_checkLimits: ai%d val:%d, <<:%d, <:%d, >:%d, >>:%d\n", channel, pAi->scaledValue, pAi->valueTooLo, pAi->valueLo, pAi->valueHi, pAi->valueTooHi);
		fprintf (stderr, "                ai%d msg <<:%d, <:%d, >:%d, >>:%d\n", channel, pAi->msgTooLo, pAi->msgLo, pAi->msgHi, pAi->msgTooHi);
	#endif

	// Prüfung auf "too lo"
	if ((pAi->scaledValue < pAi->valueTooLo) && (pAi->msgTooLo > 0))
	{
		#ifdef __ai_showCheckLimitsStrings
			fprintf (stderr, "if ((pAi->scaledValue < pAi->valueTooLo) && (pAi->msgTooLo > 0))\n");
		#endif

		if (pAi->valueTooLoActive == 0)
		{
			pAi->valueTooLoActive = 1;

			#ifdef __ai_showCheckLimitsStrings
				fprintf (stderr, "ai_checkLimits: ai%d 'too lo'\n", channel);
			#endif

			ai_logEvent (pConnect, channel, E_db_PAoAiValueTooLo);

			if ((pMail->mode != E_mail_modeOff) && (pAi->msgTooLo > 0))
			{
				// Mailtext zusammensetzen und senden
				dbPA.id = C_db_paChannelBaseAi + (channel * C_db_paChannelOffsetAi) + E_db_PAoAiValueTooLo;
				//			501									50						11
				db_rowGet (pConnect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA);

				dbTP.id = pMail->languageOffset + C_db_tpChannelBaseAi + (channel * C_db_tpChannelOffsetAi) + E_db_TPoAiMsgTooLo;
				//									512									50						0
				db_rowGet (pConnect, E_db_tableTypeTP, E_db_listTypeNone, 0, &dbTP);

				snprintf (pMail->message, C_db_dataStringSize, "%s %s %s %s",
						  dbTP.strText, C_ai_operatorText [pAi->msgOperatorTooLo], dbPA.strValue, strUnit);
				mail_send (pMail->from, pMail->to, pMail->subject, pMail->message);

				#ifdef __ai_showCheckLimitsStrings
					fprintf (stderr, "\033[31mai_checkLimits: ai%d dbTP.id=%d msg:%s\033[0m\n", channel, dbTP.id, pMail->message);
				#endif

				#ifdef __ai_showDebugStrings
					fprintf (stderr, "\033[35mai_checkLimits: mail from:%s, to:%s, sbj:%s, msg:%s\033[0m\n",
									pMail->from, pMail->to, pMail->subject, pMail->message);
				#endif
			} // if ((pMail->mode != E_mail_modeOff) && (pAi->msgTooLo > 0))
		} // if (pAi->valueTooLoActive == 0)
	}
	else
	{
		#ifdef __ai_showCheckLimitsStrings
			fprintf (stderr, "else if ((pAi->scaledValue < pAi->valueTooLo) && (pAi->msgTooLo > 0))\n");
		#endif

		if (pAi->valueTooLoActive)
		{
			pAi->valueTooLoActive = 0;
		} // if (pAi->valueTooLoActive)

		// Prüfung auf "lo"
		if ((pAi->scaledValue < pAi->valueLo) && (pAi->msgLo > 0))
		{
			#ifdef __ai_showCheckLimitsStrings
				fprintf (stderr, "else if ((pAi->scaledValue < pAi->valueLo) && (pAi->msgLo > 0))\n");
			#endif

			if (pAi->valueLoActive == 0)
			{
				pAi->valueLoActive = 1;

				#ifdef __ai_showCheckLimitsStrings
					fprintf (stderr, "ai_checkLimits: ai%d 'lo'\n", channel);
				#endif


				ai_logEvent (pConnect, channel, E_db_PAoAiValueLo);

				if ((pMail->mode != E_mail_modeOff) && (pAi->msgLo > 0))
				{
					// Mailtext zusammensetzen und senden
					dbPA.id = C_db_paChannelBaseAi + (channel * C_db_paChannelOffsetAi) + E_db_PAoAiValueLo;
					db_rowGet (pConnect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA);

					dbTP.id	= pMail->languageOffset + C_db_tpChannelBaseAi + (channel * C_db_tpChannelOffsetAi) + E_db_TPoAiMsgLo;
					db_rowGet (pConnect, E_db_tableTypeTP, E_db_listTypeNone, 0, &dbTP);

					snprintf (pMail->message, C_db_dataStringSize, "%s %s %s %s",
							  dbTP.strText, C_ai_operatorText [pAi->msgOperatorLo], dbPA.strValue, strUnit);
					mail_send (pMail->from, pMail->to, pMail->subject, pMail->message);

					#ifdef __ai_showCheckLimitsStrings
						fprintf (stderr, "\033[31mai_checkLimits: ai%d dbTP.id=%d msg:%s\033[0m\n", channel, dbTP.id, pMail->message);
					#endif

					#ifdef __ai_showDebugStrings
						fprintf (stderr, "\033[35mai_checkLimits: mail from:%s, to:%s, sbj:%s, msg:%s\033[0m\n",
										pMail->from, pMail->to, pMail->subject, pMail->message);
					#endif
				} // if ((pMail->mode != E_mail_modeOff) && (pAi->msgLo > 0))
			} // if (pAi->valueLoActive == 0)
		}
		else
		{
			#ifdef __ai_showCheckLimitsStrings
				fprintf (stderr, "else if ((pAi->scaledValue < pAi->valueLo) && (pAi->msgLo > 0))\n");
			#endif

			if (pAi->valueLoActive)
			{
				pAi->valueLoActive = 0;
			} // if (pAi->valueLoActive)
		} // else if (pAi->scaledValue < pAi->valueLo) ...
	} // else if ((pAi->scaledValue < pAi->valueTooLo) ...


	// Prüfung auf "too hi"
	if ((pAi->scaledValue > pAi->valueTooHi) && (pAi->msgTooHi > 0))
	{
		#ifdef __ai_showCheckLimitsStrings
			fprintf (stderr, "if ((pAi->scaledValue > pAi->valueTooHi) && (pAi->msgTooHi > 0))\n");
		#endif

		if (pAi->valueTooHiActive == 0)
		{
			pAi->valueTooHiActive = 1;

			#ifdef __ai_showCheckLimitsStrings
				fprintf (stderr, "ai_checkLimits: ai%d 'too hi'\n", channel);
			#endif	

			ai_logEvent (pConnect, channel, E_db_PAoAiValueTooHi);

			if ((pMail->mode != E_mail_modeOff) && (pAi->msgTooHi > 0))
			{
				// Mailtext zusammensetzen und senden
				dbPA.id = C_db_paChannelBaseAi + (channel * C_db_paChannelOffsetAi) + E_db_PAoAiValueTooHi;
				db_rowGet (pConnect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA);

				dbTP.id	= pMail->languageOffset + C_db_tpChannelBaseAi + (channel * C_db_tpChannelOffsetAi) + E_db_TPoAiMsgTooHi;
				db_rowGet (pConnect, E_db_tableTypeTP, E_db_listTypeNone, 0, &dbTP);

				snprintf (pMail->message, C_db_dataStringSize, "%s %s %s %s",
						  dbTP.strText, C_ai_operatorText [pAi->msgOperatorTooHi], dbPA.strValue, strUnit);
				mail_send (pMail->from, pMail->to, pMail->subject, pMail->message);

				#ifdef __ai_showCheckLimitsStrings
					fprintf (stderr, "\033[31mai_checkLimits: ai%d dbTP.id=%d msg:%s\033[0m\n", channel, dbTP.id, pMail->message);
				#endif

				#ifdef __ai_showDebugStrings
					fprintf (stderr, "\033[35mai_checkLimits: mail from:%s, to:%s, sbj:%s, msg:%s\033[0m\n",
									pMail->from, pMail->to, pMail->subject, pMail->message);
				#endif
			} // if ((pMail->mode != E_mail_modeOff) && (pAi->msgLo > 0))
		} // if (pAi->valueLoActive == 0)
	}
	else
	{
		#ifdef __ai_showCheckLimitsStrings
			fprintf (stderr, "else if ((pAi->scaledValue > pAi->valueTooHi) && (pAi->msgTooHi > 0))\n");
		#endif

		if (pAi->valueTooHiActive)
		{
			pAi->valueTooHiActive = 0;
		} // if (pAi->valueTooHiActive)

		// Prüfung auf "hi"
		if ((pAi->scaledValue > pAi->valueHi) && (pAi->msgHi > 0))
		{
			#ifdef __ai_showCheckLimitsStrings
				fprintf (stderr, "if ((pAi->scaledValue > pAi->valueHi) && (pAi->msgHi > 0))\n");
			#endif

			if (pAi->valueHiActive == 0)
			{
				pAi->valueHiActive = 1;

				#ifdef __ai_showCheckLimitsStrings
					fprintf (stderr, "ai_checkLimits: ai%d 'hi'\n", channel);
				#endif



				ai_logEvent (pConnect, channel, E_db_PAoAiValueHi);

				if ((pMail->mode != E_mail_modeOff) && (pAi->msgHi > 0))
				{
					// Mailtext zusammensetzen und senden
					dbPA.id = C_db_paChannelBaseAi + (channel * C_db_paChannelOffsetAi) + E_db_PAoAiValueHi;
					db_rowGet (pConnect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA);

					dbTP.id	= pMail->languageOffset + C_db_tpChannelBaseAi + (channel * C_db_tpChannelOffsetAi) + E_db_TPoAiMsgHi;
					//									512									50						2
					db_rowGet (pConnect, E_db_tableTypeTP, E_db_listTypeNone, 0, &dbTP);

					snprintf (pMail->message, C_db_dataStringSize, "%s %s %s %s",
							  dbTP.strText, C_ai_operatorText [pAi->msgOperatorTooHi], dbPA.strValue, strUnit);
					mail_send (pMail->from, pMail->to, pMail->subject, pMail->message);

					#ifdef __ai_showCheckLimitsStrings
						fprintf (stderr, "\033[31mai_checkLimits: ai%d dbTP.id=%d msg:%s\033[0m\n", channel, dbTP.id, pMail->message);
					#endif

					#ifdef __ai_showDebugStrings
						fprintf (stderr, "\033[35mai_checkLimits: mail from:%s, to:%s, sbj:%s, msg:%s\033[0m\n",
										pMail->from, pMail->to, pMail->subject, pMail->message);
					#endif
				} // if ((pMail->mode != E_mail_modeOff) && (pAi->msgLo > 0))
			} // if (pAi->valueLoActive == 0)
		}
		else
		{
			#ifdef __ai_showCheckLimitsStrings
				fprintf (stderr, "if ((pAi->scaledValue > pAi->valueHi) && (pAi->msgHi > 0))\n");
			#endif

			if (pAi->valueHiActive)
			{
				pAi->valueHiActive = 0;
			} // if (pAi->valueHiActive)
		} // else if (pAi->scaledValue < pAi->valueHi) ...
	} // else if (pAi->scaledValue < pAi->valueTooHi) ...
} /* ai_checkLimits */


/******************************************************************************
 * Exported Functions                                                         *
 *                                                                            *
 ******************************************************************************/

/**************************************************************************//**
 *
 *	\brief		ai_configDebug
 *
 *	\details	schreibt die Werte der übergebenen ts_ai Struktur auf stderr.
 *
 *	\param		*pAi	Pointer auf Struktur ts_ai
 *	\param		channel		Kannalnummer des analog Eingangs 
 *
 *	\return		-
 *
 ******************************************************************************/
void ai_configDebug (ts_ai	*pAi,
					 int	channel)
{
	fprintf (stderr, "\033[1mai_configDebug: ai%d\033[0m\n", channel);
	fprintf (stderr, "enableFunction  : %d\n", pAi->enableFunction);
	fprintf (stderr, "enableScaledLog : %d\n", pAi->enableScaledLog);
	fprintf (stderr, "limitLo         : %d\n", pAi->limitLo);
	fprintf (stderr, "limitHi         : %d\n", pAi->limitHi);
	fprintf (stderr, "unit            : %d (", pAi->unitVoltage);

	if (pAi->unitVoltage)
	{
		fprintf (stderr, "millivolt [mV])\n");
	}
	else
	{
		fprintf (stderr, "microampere [uA])\n");
	} // else if (pAi->unitVoltage)

	fprintf (stderr, "meanSampleCount : %d\n", pAi->meanSampleCount);
	fprintf (stderr, "scaleUnit       : %s\n", pAi->scaleUnit);
	fprintf (stderr, "scaleElements   : %d\n", pAi->scaleElements);
	fprintf (stderr, "scaleOffset     : %d\n", pAi->scaleOffset);
	fprintf (stderr, "storageIntervall: %d\n", pAi->storageIntervall);
	fprintf (stderr, "msgSmtpTooLo    : %d\n", pAi->msgTooLo);
	fprintf (stderr, "msgOperatorTooLo: %d\n", pAi->msgOperatorTooLo);
	fprintf (stderr, "valueTooLo      : %d\n", pAi->valueTooLo);
	fprintf (stderr, "msgSmtpLo       : %d\n", pAi->msgLo);
	fprintf (stderr, "msgOperatorLo   : %d\n", pAi->msgOperatorLo);
	fprintf (stderr, "valueLo         : %d\n", pAi->valueLo);
	fprintf (stderr, "msgSmtpHi       : %d\n", pAi->msgHi);
	fprintf (stderr, "msgOperatorHi   : %d\n", pAi->msgOperatorHi);
	fprintf (stderr, "valueHi         : %d\n", pAi->valueHi);
	fprintf (stderr, "msgSmtpTooHi    : %d\n", pAi->msgTooHi);
	fprintf (stderr, "msgOperatorTooHi: %d\n", pAi->msgOperatorTooHi);
	fprintf (stderr, "valueTooHi      : %d\n", pAi->valueTooHi);
} /* ai_configDebug */


/**************************************************************************//**
 *
 *  \brief		ai_configRead
 *
 *  \details	liest die Konfiguration für den Kanal channel aus der Datenbank-
 *				tabelle PA.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		*pAi		Pointer auf Struktur ts_ai
 *	\param		channel		Kannalnummer des analog Eingangs 
 *
 *	\return		0: Lesen erfolgreich
 *
 ******************************************************************************/
int ai_configRead (ts_db_connect *pConnect,
                   ts_ai		 *pAi,
                   int			 channel)
{
	int			fail = 0;


	if (channel < C_ai_channelCount)
	{
		pAi->enableFunction		= ai_getDbValue (pConnect, channel, E_db_PAoAiEnableFunction, &fail);
		pAi->enableScaledLog	= ai_getDbValue (pConnect, channel, E_db_PAoAiEnableScaledLog, &fail);
		pAi->limitLo			= ai_getDbValue (pConnect, channel, E_db_PAoAiLimitLo, &fail);
		pAi->limitHi			= ai_getDbValue (pConnect, channel, E_db_PAoAiLimitHi, &fail);
		pAi->unitVoltage		= ai_getDbValue (pConnect, channel, E_db_PAoAiUnit, &fail);
		pAi->meanSampleCount	= ai_getDbValue (pConnect, channel, E_db_PAoAiMeanSampleCount, &fail);
		pAi->scaleElements		= ai_getDbValue (pConnect, channel, E_db_PAoAiScaleElements, &fail);
		pAi->scaleOffset		= ai_getDbValue (pConnect, channel, E_db_PAoAiScaleOffset, &fail);
		pAi->storageIntervall	= ai_getDbValue (pConnect, channel, E_db_PAoAiStorageIntervall, &fail);
		pAi->msgTooLo			= ai_getDbValue (pConnect, channel, E_db_PAoAiMsgTooLo, &fail);
		pAi->msgOperatorTooLo	= ai_getDbValue (pConnect, channel, E_db_PAoAiMsgOperatorTooLo, &fail);
		pAi->valueTooLo			= ai_getDbValue (pConnect, channel, E_db_PAoAiValueTooLo, &fail);
		pAi->msgLo				= ai_getDbValue (pConnect, channel, E_db_PAoAiMsgLo, &fail);
		pAi->msgOperatorLo		= ai_getDbValue (pConnect, channel, E_db_PAoAiMsgOperatorLo, &fail);
		pAi->valueLo			= ai_getDbValue (pConnect, channel, E_db_PAoAiValueLo, &fail);
		pAi->msgHi				= ai_getDbValue (pConnect, channel, E_db_PAoAiMsgHi, &fail);
		pAi->msgOperatorHi		= ai_getDbValue (pConnect, channel, E_db_PAoAiMsgOperatorHi, &fail);
		pAi->valueHi			= ai_getDbValue (pConnect, channel, E_db_PAoAiValueHi, &fail);
		pAi->msgTooHi			= ai_getDbValue (pConnect, channel, E_db_PAoAiMsgTooHi, &fail);
		pAi->msgOperatorTooHi	= ai_getDbValue (pConnect, channel, E_db_PAoAiMsgOperatorTooHi, &fail);
		pAi->valueTooHi			= ai_getDbValue (pConnect, channel, E_db_PAoAiValueTooHi, &fail);

		// Stringwert
		if (ai_getDbText (pConnect, channel, E_db_PAoAiScaleUnit, pAi->scaleUnit) != 0)
		{
			pAi->scaleUnit [0] = '\0';
			fail = 1;
		} // if (ai_getDbValue (pConnect, channel, E_db_PAoAiScaleUnit, pAi->scaleUnit) != 0)

		// bei dynamischer Konfiguration mindestens ein Eintrag erzwingen
		pAi->lastEnableFunction	= !pAi->enableFunction;
	}
	else
	{
		fail = 1;
	} // else if (channel < C_ai_channelCount)
	
	return (fail);
} /* ai_configRead */


/**************************************************************************//**
 *
 *  \brief		ai_configOverride
 *
 *  \details	überschreibt die Konfiguration mit Testwerten.
 *
 *	\param		*pAi		Pointer auf Struktur ts_ai
 *	\param		channel		Kannalnummer des analog Eingangs 
 *
 *	\return		-
 *
 ******************************************************************************/
void ai_configOverride (ts_ai	*pAi,
						int		channel)
{
	if (channel < C_ai_channelCount)
	{
		#ifdef __ai_showDebugStrings
			fprintf (stderr, "ai_configOverride: ai%d\n", channel);
		#endif

		pAi->enableFunction		= 1;
		pAi->enableScaledLog	= 1;

		if (csw_flag.unitVoltage)
		{
			pAi->limitLo			= 0;		// [mV]
			pAi->limitHi			= 10000;	// [mV]
			pAi->unitVoltage		= 1;
			pAi->scaleElements		= 10000;	// entspricht z.B.[mV]
			pAi->scaleOffset		= 0;		// entspricht z.B.[°C]
		}
		else
		{
			pAi->limitLo			= 4;		// [uA]
			pAi->limitHi			= 20000;	// [uA]
			pAi->unitVoltage		= 0;
			pAi->scaleElements		= 1000;		// Techdock Testwert // entspricht [uA]
			pAi->scaleOffset		= 0;		// skalierter Wert sollte zwischen -10000 und 10000 uA liegen
		} // else if (csw_flag.unitVoltage)
		
		pAi->meanSampleCount	= 10;			// [0.1s]
		pAi->storageIntervall	= 10;			// alle 10s
		pAi->msgTooLo			= 5;	//12;
		pAi->msgOperatorTooLo	= 4;
		pAi->valueTooLo			= 5;			// [°C]
		pAi->msgLo				= 5;	//15;
		pAi->msgOperatorLo		= 4;
		pAi->valueLo			= 20;			// [°C]
		pAi->msgHi				= 5;	//18;
		pAi->msgOperatorHi		= 3;
		pAi->valueHi			= 600;			// [°C]
		pAi->msgTooHi			= 5;	//21;
		pAi->msgOperatorTooHi	= 3;
		pAi->valueTooHi			= 900;			// [°C]

		pAi->lastEnableFunction	= !pAi->enableFunction;

		snprintf (pAi->scaleUnit, C_db_dataStringSize, "°C");
	} // if (channel < C_ai_channelCount)
} /* ai_configOverride */


/**************************************************************************//**
 *
 *  \brief		ai_getBinary
 *
 *  \details	rechnet die übergebene Spannung mit Berücksichtigung der
 *				verwendeten Hardware in den Binärwert um.
 *
 *	\param		voltage		zu konvertierende Spannung
 *
 *	\return		Binärwert
 *
 ******************************************************************************/
int16_t ai_getBinary (int16_t voltage)
{
	return (int16_t)(( (int32_t)voltage * C_ads1115_resolution) / C_ads1115_maxVoltage);
} /* ai_getBinary */


/**************************************************************************//**
 *
 *  \brief		ai_getCurrent
 *
 *  \details	rechnet den übergebenen Binärwert mit Berücksichtigung der
 *				verwendeten Hardware in Strom [uA] um.
 *
 *	\param		binary		zu konvertierende Binärwert
 *
 *	\return		Stromwert [uA]
 *
 ******************************************************************************/
int16_t ai_getCurrent (int16_t binary)
{
	if (binary < 0)
	{
		return (0);
	}
	else
	{
		return (int16_t)(( (int32_t)binary * C_ads1115_maxCurrent) / C_ads1115_resolution);
	} // else if (binary < 0)
} /* ai_getCurrent */


/**************************************************************************//**
 *
 *  \brief		ai_getVoltage
 *
 *  \details	rechnet den übergebenen Binärwert mit Berücksichtigung der
 *				verwendeten Hardware in Spannung [mV] um.
 *
 *	\param		binary		zu konvertierende Binärwert
 *
 *	\return		Spannungswert [mV]
 *
 ******************************************************************************/
int16_t ai_getVoltage (int16_t binary)
{
	if (binary < 0)
	{
		return (0);
	}
	else
	{
		return (int16_t) (( (int32_t)binary * C_ads1115_maxVoltage) / C_ads1115_resolution);
	} // else if (binary < 0)
} /* ai_getVoltage */


/**************************************************************************//**
 *
 *	\brief		ai_initMean
 *
 *  \details	initialisiert die Daten in der Struktur ts_ai_mean.
 *
 *	\param		*pMean		Pointer auf Struktur ts_ai_mean
 *	\param		meanCount	Anzahl Datenpunkte für Mittelwertbildung
 *
 *	\return		-
 *
 ******************************************************************************/
void ai_initMean (ts_ai_mean	*pMean,
				  uint16_t		meanCount)
{
	int i;


	pMean->sum		 = 0;
	pMean->posNewest = 0;
	pMean->posOldest = 1;
	pMean->posMax	 = meanCount - 1;
	pMean->posCount	 = 0;

	for (i = 0; i < meanCount; i++)
	{
		pMean->data [i] = 0;
	}
} /* ai_initMean */


/**************************************************************************//**
 *
 *  \brief		ai_processQuick
 *
 *  \details	wertet das Signal des analogen Eingangs ohne Datenbankeinträge
 *				aus um eine schnelle Hardwarereaktion zu ermöglichen.
 *
 *	\param		*pAi		Pointer auf Struktur ts_ai
 *	\param		channel		Kannalnummer des analogen Eingangs 
 *
 *	\return		-
 *
 ******************************************************************************/
void ai_processQuick (ts_ai	*pAi,
					  int	channel)
{
} /* ai_processQuick */


/**************************************************************************//**
 *
 *  \brief		ai_process
 *
 *  \details	wertet das Signal des analogen Eingangs aus.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		*pAi		Pointer auf Struktur ts_ai
 *	\param		channel		Kannalnummer des analogen Eingangs 
 *	\param		*pMail		Pointer auf Struktur ts_mail
 *
 *	\return		-
 *
 ******************************************************************************/
int ai_process (ts_db_connect	*pConnect,
                ts_ai			*pAi,
                int				channel,
				ts_mail			*pMail)
{
	static ts_db_ai	dbAI;
	static ts_db_va	dbVA;

 
	if (!pAi->readError)
	{
		if (pAi->enableFunction)
		{
			dbAI.id = channel + 1;
			snprintf (dbAI.strValue, C_db_dataStringSize, "%d", pAi->binary); 

			if (db_rowUpdate (pConnect, E_db_tableTypeAI, &dbAI))
			{
				db_rowAdd (pConnect, E_db_tableTypeAI, E_db_listTypeNone, dbAI.id, &dbAI);
			}

			// Mittelwert bilden
			if (pAi->meanSampleCount > 0)
			{
				pAi->binaryMean = ai_getMean (&pAi->mean, pAi->binary);
			}
			else
			{
				pAi->binaryMean = pAi->binary;
			} // else if (pAi->meanSampleCount > 0)


			if (pAi->unitVoltage)
			{
				// Mittelwert in Spannung [mV] umrechnen 
				pAi->voltage = ai_getVoltage (pAi->binaryMean);
				pAi->current = 0;
				
				pAi->meanValue = (int32_t)pAi->voltage;

				#ifdef __ai_showDebugStrings
					fprintf (stderr, "ai_process: ai%d: %5d, %5d, %5d[mV]", channel, pAi->binary, pAi->binaryMean, pAi->voltage); 
				#endif
			}
			else
			{
				// Mittelwert in Strom [uA] umrechnen 
				pAi->current = ai_getCurrent (pAi->binaryMean);
				pAi->voltage = 0;

				pAi->meanValue = (int32_t)pAi->current;

				#ifdef __ai_showDebugStrings
					fprintf (stderr, "ai_process: ai%d: %5d, %5d, %5d[uA]", channel, pAi->binary, pAi->binaryMean, pAi->current); 
				#endif
			} // else if (pAi->unitVoltage)

			// Mittelwert skalieren
// +-		pAi->scaledValue = (((pAi->meanValue - pAi->limitLo) * pAi->scaleElements) / (pAi->limitHi - pAi->limitLo)) - pAi->scaleOffset;
			pAi->scaledValue = (((pAi->meanValue - pAi->limitLo) * pAi->scaleElements) / (pAi->limitHi - pAi->limitLo)) + pAi->scaleOffset;

			#ifdef __ai_showDebugStrings
				fprintf (stderr, ", scaled: %5d\n", pAi->scaledValue);
			#endif

			// Grenzwerte Prüfen
			ai_checkLimits (pConnect, pAi, channel, pMail);

			// skalierter Wert in VA Tabelle schreiben
			dbVA.id = E_db_VAidAi1Scaled + channel;
			snprintf (dbVA.strDescription, C_db_dataStringSize, "%d", pAi->scaledValue);

			if (db_rowUpdate (pConnect, E_db_tableTypeVA, &dbVA))
			{
				db_rowAdd (pConnect, E_db_tableTypeVA, E_db_listTypeNone, dbVA.id, &dbVA);
			}
		}
		else
		{
			#ifdef __ai_showDebugStrings
				fprintf (stderr, "ai_process: ai%d disabled\n", channel);
			#endif

			if (pAi->lastEnableFunction)
			{
				pAi->lastEnableFunction = 0;

				dbAI.id = channel + 1;
				snprintf (dbAI.strValue, C_db_dataStringSize, "off"); 

				if (db_rowUpdate (pConnect, E_db_tableTypeAI, &dbAI))
				{
					db_rowAdd (pConnect, E_db_tableTypeAI, E_db_listTypeNone, dbAI.id, &dbAI);
				}

				// skalierter Wert in VA Tabelle schreiben
				dbVA.id = E_db_VAidAi1Scaled + channel;
				snprintf (dbVA.strDescription, C_db_dataStringSize, "off");

				if (db_rowUpdate (pConnect, E_db_tableTypeVA, &dbVA))
				{
					db_rowAdd (pConnect, E_db_tableTypeVA, E_db_listTypeNone, dbVA.id, &dbVA);
				}
			} // if (pAi.lastEnableFunction)
		} // else if (pAi->enableFunction)
	} // if (!pAi->readError)
} /* ai_process */


/**************************************************************************//**
 *
 *	\brief		ai_logScaled
 *
 *  \details	schreibt, falls freigegeben, den Mittelwert und den skalierten
 *				Wert des analogen Eingangs channel in die entsprechenden Daten-
 *				listen der Datenbank.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		*pAi		Pointer auf Struktur ts_ai
 *	\param		channel		Kannalnummer des analog Eingang
 *
 *	\return		-
 *
 ******************************************************************************/
void ai_logScaled (ts_db_connect *pConnect,
				   ts_ai		 *pAi,
				   int			 channel)
{
	static ts_db_dl	dbDL;


	if (pAi->enableScaledLog)
	{
		// Mittelwert in DLAIx Tabelle schreiben (hat autoincrement)
		snprintf (dbDL.strValue, C_db_dataStringSize, "%d", pAi->binaryMean);
		db_rowAdd (pConnect, E_db_tableTypeDL, E_db_listTypeAI, channel + 1, &dbDL);

		// skalierter Wert in DLVAx Tabelle schreiben (hat autoincrement)
		snprintf (dbDL.strValue, C_db_dataStringSize, "%d", pAi->scaledValue);
		db_rowAdd (pConnect, E_db_tableTypeDL, E_db_listTypeVA, E_db_VAidAi1Scaled + channel, &dbDL);

		#ifdef __ai_showDebugStrings
			fprintf (stderr, "ai_logScaled: ai%d scaled value:%d\n", channel, pAi->scaledValue);
		#endif
	} // if (pAi->enableScaledLog)
 } /* ai_logScaled */
 
 
/* ai.c */

