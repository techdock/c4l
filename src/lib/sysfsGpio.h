/**************************************************************************//**
 *
 *	\file		sysfsGpio.h
 *
 *	\brief		Headerfile der Funktionen für den GPIO Zugriff über sysfs.
 *
 *	\details	-
 *
 *	\date		04.09.2020
 *	\author		Marcel Ming
 *	\copyright	TRiMADA AG   CH-5610 Wohlen
 *	\remark		Compiler- / IDE-Version
 *
 *	\version	04.09.20 mm
 *					- Erstellt.
 *
 ******************************************************************************/
#ifndef __sysfsGpio_H
    #define __sysfsGpio_H


/******************************************************************************
 * Modules                                                                    *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Definitions and Macros                                                     *
 *                                                                            *
 ******************************************************************************/
#define C_sysfsGpio_dirIn			0
#define C_sysfsGpio_dirOut			1

#define C_sysfsGpio_bufferLenInt	12		// Platz für int32 mit \0
#define C_sysfsGpio_bufferLenPath	64

/******************************************************************************
 * Typedefs and Structures                                                    *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Exported Variables                                                         *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Exported Functions                                                         *
 *                                                                            *
 ******************************************************************************/
int sysfsGpio_export (int gpioNumber);
int sysfsGpio_unexport (int gpioNumber);
int sysfsGpio_direction (int gpioNumber, int dir);
int sysfsGpio_write (int gpioNumber, int value);
int sysfsGpio_read (int gpioNumber);


#endif //__sysfsGpio_H

/* End sysfsGpio.h */
