/**************************************************************************//**
 *
 *	\file		do.c
 *
 *	\brief		Stellt die Funktionen für die Verwaltung der digitalen Ausgänge
 *				zur Verfügung.
 *
 *	\details	-
 *
 *	\date		08.04.2021
 *	\author		Marcel Ming
 *	\copyright	TRiMADA AG   CH-5610 Wohlen
 *	\remark		Compiler- / IDE-Version
 *
 *	\version	08.04.21 mm
 *					- Erstellt.
 *	\version	20.05.21 mm
 *					- Anpassungen an die Änderungen der Tabellenstrukturelemente
 *					  in ts_db_xy.
 *	\version	03.08.21 mm
 *					- Debugmeldungen angepasst.
 *	\version	05.08.21 mm
 *					- do_configOverride, do_lock und do_unlock impementiert.
 *	\version	24.08.21 mm
 *					- Mapping Hardware AO [0..1] zu Datenbank AO [1..2] korrigiert.
 *					- logging nur bei Zustandsänderungen.
 *	\version	01.09.21 mm
 *					- Funktion do_logEvent implementiert.
 *					- ai_processQuick implementiert.
 *	\version	06.09.21 mm
 *					- Compilerschalter __do_showDebugStrings definiert.
 *	\version	17.11.21 mm
 *					- Zustand von DO's auch im Servicemodus loggen.
 *	\version	26.11.21 mm
 *					- in Funktion do_processQuick in case E_do_fktImpulse nur
 *					  neu triggern, wenn der zugewiesene Eingang noch aktiv ist.
 *	\version	30.11.21 mm
 *					- Ausgang bei E_do_followOff in seinem aktuellen Zustand
 *					  belassen, nicht löschen.
 *	\version	08.11.21 mm
 *					- in Funktion do_process den Wert von lastFunction nachgeführt
 *					  um auch bei dynamischen Konfigurationen richtig reagieren zu
 *					  können.
 *	\version	18.02.22 mm
 *					- Kommentar ergänzt / korrigiert.
 *	\version	22.09.23 mm
 *					- in Funktion do_configOverride den nicht benötigten Parameter
 * 					  *pConnect gelöscht.
 *	\version	17.10.23 mm
 *					- Funktion do_mailEvent implementiert und damit Fehler für
 *					  Mailkriterium behoben.
 *
 *
 ******************************************************************************/


/******************************************************************************
 * Modules                                                                    *
 *                                                                            *
 ******************************************************************************/
#include <mysql/mysql.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "csw.h"
#include "db.h"
#include "do.h"
#include "ioa.h"


/******************************************************************************
 * Definitions and Macros                                                     *
 *                                                                            *
 ******************************************************************************/
#define __GNU_SOURCE


/******************************************************************************
 * Typedefs and Structures                                                    *
 *                                                                            *
 ******************************************************************************/
 
/******************************************************************************
 * Exported Variables                                                         *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Global Variables                                                           *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Local Functions                                                            *
 *                                                                            *
 ******************************************************************************/

/**************************************************************************//**
 *
 *	\brief		do_getDbValue
 *
 *  \details	liest den Konfigurationswert dbIdOffset für den Kanal channel
 *				aus der Datenbanktabelle PA.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		channel		Kannalnummer des digital Ausgangs [0..3]
 *	\param		dbIdOffset	Datenbank id Offset zur Basis id des Eintrags.
 *	\param		*pFail		Resultat Flag, wird bei einem Fehler auf 1 gesetzt
 *
 *	\return		0: Lesen erfolgreich
 *
 ******************************************************************************/
static int do_getDbValue (ts_db_connect		 *pConnect,
						  int				 channel,
						  te_db_PAidOffsetDo dbIdOffset,
						  int				 *pFail)
{
	static ts_db_pa dbPA;


	dbPA.id = C_db_paChannelBaseDo + (channel * C_db_paChannelOffsetDo) + dbIdOffset;

	if (db_rowGet (pConnect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA) == 0)
	{
		return (atoi (dbPA.strValue));
	}
	else
	{
		*pFail = 1;
		return (0);
	} // else if (db_rowGet (pConnect, E_db_tableTypePA, &dbPA) == 0)
} /* do_getDbValue */


/**************************************************************************//**
 *
 *	\brief		do_logEvent
 *
 *	\details	schreibt die Eventnummer, welche dem PA Index entspricht in die
 *				EV Tabelle.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		channel		Kannalnummer des digitalen Ausgangs 
 *	\param		idOffset	Offset für Event Wert
 *
 *	\return		-
 *
 ******************************************************************************/
static void do_logEvent (ts_db_connect		*pConnect,
						 int				channel,
						 te_db_PAidOffsetDi	idOffset)
{
	static ts_db_ev	dbEV;


	// PA Index ermitteln und in Eventliste eintragen
	snprintf (dbEV.strValue, C_db_dataStringSize, "%d", C_db_paChannelBaseDo + channel * C_db_paChannelOffsetDo + idOffset);
	snprintf (dbEV.strDateTime, C_db_dataStringSize, "%s",  "");
	db_rowAdd (pConnect, E_db_tableTypeEV, E_db_listTypeNone, 0, &dbEV);

	#ifdef __do_showDebugStrings
		fprintf (stderr, ", do_logEvent: do%d EV:%s\n", channel, dbEV.strValue);
	#endif
} /* do_logEvent */


/**************************************************************************//**
 *
 *	\brief		do_mailEvent
 *
 *	\details	setzt den Mailtext zusammen und sendet ein Mail an den Empfänger
 *				gemäss ts_mail.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		channel		Kannalnummer des digitalen Eingangs 
 *	\param		PAidOffset	Parameter Offset für Event Wert
 *	\param		TPidOffset	Textparameter Offset für Event Wert
 *	\param		*pMail		Pointer auf Mail
 *
 *	\return		-
 *
 ******************************************************************************/
static void do_mailEvent (ts_db_connect		 *pConnect,
						  int				 channel,
						  te_db_PAidOffsetDo PAidOffset,
						  te_db_TPidOffsetDo TPidOffset,
						  ts_mail			 *pMail)
{
	static ts_db_pa	dbPA;
	static ts_db_tp	dbTP;


	// Mailtext zusammensetzen und senden
	dbPA.id = C_db_paChannelBaseDo + channel * C_db_paChannelOffsetDo + PAidOffset;
	db_rowGet (pConnect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA);

	dbTP.id = pMail->languageOffset + C_db_tpChannelBaseDo + channel * C_db_tpChannelOffsetDo + TPidOffset;
	db_rowGet (pConnect, E_db_tableTypeTP, E_db_listTypeNone, 0, &dbTP);

	snprintf (pMail->message, C_db_dataStringSize, "%s", dbTP.strText);
	mail_send (pMail->from, pMail->to, pMail->subject, pMail->message);

	#ifdef __do_showDebugStrings
		fprintf (stderr, ", mail from:%s, to:%s, sbj:%s, msg:%s",
				 pMail->from, pMail->to, pMail->subject, pMail->message);
	#endif
} /* do_mailEvent */


/******************************************************************************
 * Exported Functions                                                         *
 *                                                                            *
 ******************************************************************************/

/**************************************************************************//**
 *
 *	\brief		do_configDebug
 *
 *	\details	schreibt die Werte der übergebenen ts_do Struktur auf stderr
 *				aus.
 *
 *	\param		*pDo	Pointer auf Struktur ts_do
 *	\param		channel		Kannalnummer des digital Ausgangs [0..3] 
 *
 *	\return		-
 *
 ******************************************************************************/
void do_configDebug (ts_do	*pDo,
					 int	channel)
{
	fprintf (stderr, "\033[1mdo_configDebug: do%d\033[0m\n", channel);
	fprintf (stderr, "function  : %d\n", pDo->function);
	fprintf (stderr, "enableLog : %d\n", pDo->enableLog);
	fprintf (stderr, "activeTime: %d\n", pDo->activeTime);
	fprintf (stderr, "follow    : %d\n", pDo->follow);
	fprintf (stderr, "msgLoHi   : %d\n", pDo->msgLoHi);
	fprintf (stderr, "msgHiLo   : %d\n", pDo->msgHiLo);
} /* do_configDebug */


/**************************************************************************//**
 *
 *  \brief		do_configRead
 *
 *  \details	liest die Konfiguration für den Kanal channel aus der Datenbank-
 *				tabelle PA.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		*pDo		Pointer auf Struktur ts_do
 *	\param		channel		Kannalnummer des digital Ausgangs [0..3] 
 *
 *	\return		0: Lesen erfolgreich
 *
 ******************************************************************************/
int do_configRead (ts_db_connect *pConnect,
                   ts_do		 *pDo,
                   int			 channel)
{
	int	fail = 0;


	if (channel < C_do_channelCount)
	{
		pDo->function	= do_getDbValue (pConnect, channel, E_db_PAoDoFunction, &fail);
		pDo->enableLog	= do_getDbValue (pConnect, channel, E_db_PAoDoEnableLog, &fail);
		pDo->activeTime	= do_getDbValue (pConnect, channel, E_db_PAoDoActiveTime, &fail);
		pDo->follow		= do_getDbValue (pConnect, channel, E_db_PAoDoFollow, &fail);
		pDo->msgLoHi	= do_getDbValue (pConnect, channel, E_db_PAoDoMsgLoHi, &fail);
		pDo->msgHiLo	= do_getDbValue (pConnect, channel, E_db_PAoDoMsgHiLo, &fail);

		pDo->lastFunction = E_do_fktCount;
	}
	else
	{
		fail = 1;
	} // else if (channel < C_do_channelCount)
	
	return (fail);
} /* do_configRead */


/**************************************************************************//**
 *
 *  \brief		do_configOverride
 *
 *  \details	überschreibt die Konfiguration mit Testwerten.
 *
 *	\param		*pDo		Pointer auf Struktur ts_do
 *	\param		channel		Kannalnummer des digital Ausgangs [0..3]
 *
 *	\return		-
 *
 ******************************************************************************/
void do_configOverride (ts_do	*pDo,
						int		channel)
{
	if (channel < C_do_channelCount)
	{
		#ifdef __do_showDebugStrings
			fprintf (stderr, "do_configOverride: do%d\n", channel);
		#endif

		pDo->function	= E_do_fktSwitchNormallyOpen;
/*
		pDo->function	= E_do_fktSwitchNormallyClose;
		pDo->function	= E_do_fktButtonNormallyOpen;
		pDo->function	= E_do_fktButtonNormallyClose;
		pDo->function	= E_do_fktImpulse;
*/
		pDo->enableLog	= 1;
		pDo->activeTime	= 5;				// [s]
		pDo->follow		= channel + 1;		// E_do_followOff;
		pDo->msgLoHi	= 1;				// ein
		pDo->msgHiLo	= 1;				// ein
	} // if (channel < C_do_channelCount)
} /* do_configOverride */


/**************************************************************************//**
 *
 *  \brief		do_lock
 *
 *  \details	schaltet die Versorgungsspannung der Relaisspulen aus.
 *
 *	\param		*pHandleDoLock	Pointer auf Struktur gpiod_line
 *
 *	\return		-
 *
 ******************************************************************************/
void do_lock (struct gpiod_line *pHandleDoLock,
			  struct gpiod_line *pHandleDoLockFB)
{
	#ifdef __do_hwWithLockFeedback
		while (gpiod_line_get_value (pHandleDoLockFB))
		{
			gpiod_line_set_value (pHandleDoLock, 1);
			gpiod_line_set_value (pHandleDoLock, 0);
		} // while (!gpiod_line_get_value (pHandleDoLockFB)
	#else
		uint i;


		for (i = 0; i < 8; i++)
		{
			gpiod_line_set_value (pHandleDoLock, 1);
			gpiod_line_set_value (pHandleDoLock, 0);
		}
	#endif
} /* do_lock */


/**************************************************************************//**
 *
 *  \brief		do_unlock
 *
 *  \details	schaltet die Versorgungsspannung der Relaisspulen ein.
 *
 *	\param		*pHandleDoLock		Pointer auf HandleDoLock
 *	\param		*pHandleDoLockFB	Pointer auf HandleDoLockFB
 *
 *	\return		-
 *
 ******************************************************************************/
void do_unlock (struct gpiod_line *pHandleDoLock,
				struct gpiod_line *pHandleDoLockFB)
{
	#ifdef __do_hwWithLockFeedback
		while (!gpiod_line_get_value (pHandleDoLockFB))
		{
			gpiod_line_set_value (pHandleDoLock, 1);
			gpiod_line_set_value (pHandleDoLock, 0);
		} // while (!gpiod_line_get_value (pHandleDoLockFB)
	#else
		do_lock (pHandleDoLock);
	#endif
} /* do_unlock */


/**************************************************************************//**
 *
 *  \brief		do_processQuick
 *
 *  \details	wertet das Signal des digitalen Ausgangs ohne Datenbankeinträge
 *				aus um eine schnelle Hardwarereaktion zu ermöglichen.
 *
 *	\param		*pDo		Pointer auf Struktur ts_do
 *	\param		channel		Kannalnummer des digital Ausgang
 *
 *	\attention	damit die Zeit-Funktionen richtig bearbeitet werden, muss diese
 *				Funktion im 100 ms Takt aufgerufen werden.
 *
 *	\return		-
 *
 ******************************************************************************/
void do_processQuick (void	*pVoid,
					  int	channel)
{
	ts_ioa			*pIoa;
	ts_di			*pDi;
	ts_do			*pDo;


	pIoa	= pVoid;
	pDo		= &pIoa->DO [channel];
	pDi		= &pIoa->DI [pDo->follow - 1];

	snprintf (pDo->message, C_db_dataStringSize, "");

	if (!pIoa->serviceMode)
	{
		switch (pDo->function)
		{
			case E_do_fktSwitchNormallyOpen:
				pDo->impulseRunning = 0;

				switch (pDo->follow)
				{
					case E_do_followOff:
						// Ausgang muss im aktuellen Zustand belassen werden
						break;

					case E_do_followDi1:
					case E_do_followDi2:
					case E_do_followDi3:
					case E_do_followDi4:
						// nur bei Änderung reagieren
						if (pDo->coil != pDi->stateActual)
						{
							pDo->coil = pDi->stateActual;

							snprintf (pDo->message, C_db_dataStringSize, "%d", pDi->stateActual);
						} // if (pDo->coil != pDi->stateActual)
						break;

					default:
						break;

				} // switch (pDo->follow)

				#ifdef __do_showDebugStrings
					fprintf (stderr, "do_processQuick: do%d: switch no, coil:%d\n", channel, pDo->coil);
				#endif
				break;

			case E_do_fktSwitchNormallyClose:
				pDo->impulseRunning	= 0;

				switch (pDo->follow)
				{
					case E_do_followOff:
						// Ausgang muss im aktuellen Zustand belassen werden
						break;

					case E_do_followDi1:
					case E_do_followDi2:
					case E_do_followDi3:
					case E_do_followDi4:
						// nur bei Änderung reagieren
						if (pDo->coil == pDi->stateActual)
						{
							pDo->coil = !pDi->stateActual;

							snprintf (pDo->message, C_db_dataStringSize, "%d", !pDi->stateActual);
						} // if (pDo->coil == pDi->stateActual)
						break;

					default:
						break;

				} // switch (pDo->follow)

				#ifdef __do_showDebugStrings
					fprintf (stderr, "do_processQuick: do%d: switch nc, coil:%d\n", channel, pDo->coil);
				#endif
				break;

			case E_do_fktButtonNormallyOpen:
				pDo->impulseRunning	= 0;

				switch (pDo->follow)
				{
					case E_do_followOff:
						// Ausgang muss im aktuellen Zustand belassen werden
						break;

					case E_do_followDi1:
					case E_do_followDi2:
					case E_do_followDi3:
					case E_do_followDi4:
						if (pDi->trigger)
						{
							pDi->trigger = 0;
							pDo->remainingTime = pDo->activeTime * 10;		// 100 ms -> 1 s
							pDo->coil = 1;

							snprintf (pDo->message, C_db_dataStringSize, "%d", 1); 
						} // if (pDi->trigger)

						if (pDo->remainingTime > 0)
						{
							pDo->remainingTime--;
						}
						else
						{
							// nur bei Änderung reagieren
							if (pDo->coil == 1)
							{
								pDo->coil = 0;

								snprintf (pDo->message, C_db_dataStringSize, "%d", 0);
							} // if (pDo->coil == 1)
						} // else if (pDo->remainingTime > 0)
						break;

					default:
						break;

				} // switch (pDo->follow)

				#ifdef __do_showDebugStrings
					fprintf (stderr, "do_processQuick: do%d: button no, coil:%d\n", channel, pDo->coil);
				#endif
				break;

			case E_do_fktButtonNormallyClose:
				pDo->impulseRunning	= 0;

				switch (pDo->follow)
				{
					case E_do_followOff:
						// Ausgang muss im aktuellen Zustand belassen werden
						break;

					case E_do_followDi1:
					case E_do_followDi2:
					case E_do_followDi3:
					case E_do_followDi4:
						if (pDi->trigger)
						{
							pDi->trigger = 0;
							pDo->remainingTime = pDo->activeTime * 10;		// 100 ms -> 1 s;
							pDo->coil = 0;

							snprintf (pDo->message, C_db_dataStringSize, "%d", 0); 
						} // if (pDi->trigger)

						if (pDo->remainingTime > 0)
						{
							pDo->remainingTime--;
						}
						else
						{
							// nur bei Änderung reagieren
							if (pDo->coil == 0)
							{
								pDo->coil = 1;

								snprintf (pDo->message, C_db_dataStringSize, "%d", 1);
							} // if (pDo->coil == 0)
						} // else if (pDo->remainingTime > 0)
						break;

					default:
						break;

				} // switch (pDo->follow)

				#ifdef __do_showDebugStrings
					fprintf (stderr, "do_processQuick: do%d: button nc, coil:%d\n", channel, pDo->coil);
				#endif
				break;

			case E_do_fktImpulse:
				switch (pDo->follow)
				{
					case E_do_followOff:
						// Ausgang muss im aktuellen Zustand belassen werden
						break;

					case E_do_followDi1:
					case E_do_followDi2:
					case E_do_followDi3:
					case E_do_followDi4:
						if (pDo->impulseRunning)
						{
							if (pDo->remainingTime > 0)
							{
								pDo->remainingTime--;
							}
							else
							{
								// nur neu triggern, wenn Eingang aktiv
								if (pDi->stateActual)
								{
									pDo->remainingTime	= pDo->activeTime * 10;		// 100 ms -> 1 s;
									pDo->coil			= !pDo->coil;

									snprintf (pDo->message, C_db_dataStringSize, "%d", pDo->coil);
								} // if (pDi->stateActual)
							} // else if (pDo->remainingTime > 0)
						}
						else
						{
							// nur neu triggern, wenn Eingang aktiv
							if (pDi->stateActual)
							{
								pDo->impulseRunning	= 1;
								pDo->remainingTime	= pDo->activeTime * 10;		// 100 ms -> 1 s;
							} // if (pDi->stateActual)
						} // else if (pDo->impulseRunning)
						break;

					default:
						break;

				} // switch (pDo->follow)

				#ifdef __do_showDebugStrings
					fprintf (stderr, "do_processQuick: do%d: impulse, coil:%d\n", channel, pDo->coil);
				#endif
				break;

			default:
				break;

		} // switch (pDo->function)
	} // if (!pIoa->serviceMode)
} /* do_processQuick */


/**************************************************************************//**
 *
 *  \brief		do_process
 *
 *  \details	wertet das Signal des digitalen Ausgangs aus.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		*pVoid		Pointer auf Struktur ts_ioa
 *	\param		channel		Kannalnummer des digital Ausgang [0..3]
 *	\param		*pMail		Pointer auf Struktur ts_mail
 *
 *	\attention	damit die Zeit-Funktionen richtig bearbeitet werden, muss diese
 *				Funktion im 100 ms Takt aufgerufen werden.
 *
 *	\return		-
 *
 ******************************************************************************/
int do_process (ts_db_connect	*pConnect,
				void			*pVoid,
				int				channel,
				ts_mail			*pMail)
{
	ts_ioa			*pIoa;
	ts_di			*pDi;
	ts_do			*pDo;
	static ts_db_do	dbDO;
	static ts_db_dl	dbDL;
	static ts_db_pa	dbPA;
	static ts_db_tp	dbTP;
	uint			coilStateDb;


	pIoa	= pVoid;
	pDo		= &pIoa->DO [channel];
	pDi		= &pIoa->DI [pDo->follow - 1];
	dbDO.id	= channel + 1;

//	snprintf (dbDO.strValue, C_db_dataStringSize, "");

	if (pIoa->serviceMode)
	{
		// DO aus der Datenbank lesen
		db_rowGet (pConnect, E_db_tableTypeDO, E_db_listTypeNone, dbDO.id, &dbDO);
		coilStateDb = atoi (dbDO.strValue);

		// nur bei Änderung reagieren
		if (pDo->coil != coilStateDb)
		{
			pDo->coil = coilStateDb;

			snprintf (pDo->message, C_db_dataStringSize, "%d", coilStateDb);
		} // if (pDo->coil != pDi->stateActual)

		#ifdef __do_showDebugStrings
			fprintf (stderr, "do_process: do%d: servicemode, %d\n", channel, pDo->coil);
		#endif
	}
	else
	{
		switch (pDo->function)
		{
			case E_do_fktOff:
				if (pDo->lastFunction != E_do_fktOff)
				{
					pDo->lastFunction	= E_do_fktOff;
					pDo->impulseRunning	= 0;
					pDo->coil			= 0;

					snprintf (pDo->message, C_db_dataStringSize, "off");

					// ohne Auswertung des Rückgabewertes
					fprintf (stderr, "do_process: do%d: %d, db_rowUpdate: %d\n", channel,
								 pDo->coil,
								 db_rowUpdate (pConnect, E_db_tableTypeDO, &dbDO));
				} // if (pDo->lastfunction != E_do_fktOff)

				#ifdef __do_showDebugStrings
					fprintf (stderr, "do_process: do%d: off\n", channel);
				#endif
				break;

			case E_do_fktSwitchNormallyOpen:
				pDo->lastFunction = E_do_fktSwitchNormallyOpen;
				// Aufgaben in do_processQuick erledigt
				break;

			case E_do_fktSwitchNormallyClose:
				pDo->lastFunction = E_do_fktSwitchNormallyClose;
				// Aufgaben in do_processQuick erledigt
				break;

			case E_do_fktButtonNormallyOpen:
				pDo->lastFunction = E_do_fktButtonNormallyOpen;
				// Aufgaben in do_processQuick erledigt
				break;

			case E_do_fktButtonNormallyClose:
				pDo->lastFunction = E_do_fktButtonNormallyClose;
				// Aufgaben in do_processQuick erledigt
				break;

			case E_do_fktImpulse:
				pDo->lastFunction = E_do_fktImpulse;
				// Aufgaben in do_processQuick erledigt
				break;

			default:
				pDo->lastFunction = E_do_fktCount;
				break;

		} // switch (pDo->function)
	} // else if (pIoa->serviceMode)

	if (strlen (pDo->message) != 0)
	{
		snprintf (dbDO.strValue, C_db_dataStringSize, "%s", pDo->message);

		if (db_rowUpdate (pConnect, E_db_tableTypeDO, &dbDO))
		{
			db_rowAdd (pConnect, E_db_tableTypeDO, E_db_listTypeNone, dbDO.id, &dbDO);
		}

		if (pDo->enableLog)
		{
			// aktueller Wert in DLDOx Tabelle schreiben
			dbDL.id = channel + 1;
			snprintf (dbDL.strValue, C_db_dataStringSize, "%d", pDo->coil);
			db_rowAdd (pConnect, E_db_tableTypeDL, E_db_listTypeDO, dbDL.id, &dbDL);

			#ifdef __do_showDebugStrings
				fprintf (stderr, "dbDO.strValue logged: %s\n", dbDO.strValue);
			#endif
		} // if (pDo->enableLog)

		if (pDo->coil)
		{
			do_logEvent (pConnect, channel, E_db_PAoDoMsgLoHi);

			if ((pMail->mode != E_mail_modeOff) && (pDo->msgLoHi > 0))
			{
				do_mailEvent (pConnect, channel, E_db_PAoDoMsgLoHi, E_db_TPoDoMsgChangeFalseTrue, pMail);

				/* Mailtext zusammensetzen und senden
				dbPA.id = C_db_paChannelBaseDo + channel * C_db_paChannelOffsetDo + E_db_TPoDoMsgChangeFalseTrue;
				db_rowGet (pConnect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA);

				dbTP.id	= pMail->languageOffset + C_db_tpChannelBaseDo + channel * C_db_tpChannelOffsetDo + E_db_TPoDoMsgChangeFalseTrue;
				db_rowGet (pConnect, E_db_tableTypeTP, E_db_listTypeNone, 0, &dbTP);

				snprintf (pMail->message, C_db_dataStringSize, "%s", dbTP.strText);
				mail_send (pMail->from, pMail->to, pMail->subject, pMail->message);

				#ifdef __do_showDebugStrings
					fprintf (stderr, ", mail from:%s, to:%s, sbj:%s, msg:%s",
									pMail->from, pMail->to, pMail->subject, pMail->message);
				#endif
				*/
			} // if ((pMail->mode != E_mail_modeOff) && (pDo.msgLoHi > 0))
		}
		else
		{
			do_logEvent (pConnect, channel, E_db_PAoDoMsgHiLo);

			if ((pMail->mode != E_mail_modeOff) && (pDo->msgHiLo > 0))
			{
				do_mailEvent (pConnect, channel, E_db_PAoDoMsgHiLo, E_db_TPoDoMsgChangeTrueFalse, pMail);

				/* Mailtext zusammensetzen und senden
				dbPA.id = C_db_paChannelBaseDo + channel * C_db_paChannelOffsetDo + E_db_TPoDoMsgChangeTrueFalse;
				db_rowGet (pConnect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA);

				dbTP.id	= pMail->languageOffset + C_db_tpChannelBaseDo + channel * C_db_tpChannelOffsetDo + E_db_TPoDoMsgChangeTrueFalse;
				db_rowGet (pConnect, E_db_tableTypeTP, E_db_listTypeNone, 0, &dbTP);

				snprintf (pMail->message, C_db_dataStringSize, "%s", dbTP.strText);
				mail_send (pMail->from, pMail->to, pMail->subject, pMail->message);

				#ifdef __do_showDebugStrings
					fprintf (stderr, ", mail from:%s, to:%s, sbj:%s, msg:%s",
									pMail->from, pMail->to, pMail->subject, pMail->message);
				#endif
				*/
			} // if ((pMail->mode != E_mail_modeOff) && (pDo.msgLoHi > 0))
		} // else if (pDo->coil)
	} // if (dbDO.strValue	!= "")
} /* do_process */


/* do.c */

