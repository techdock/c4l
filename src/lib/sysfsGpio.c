/**************************************************************************//**
 *
 *	\file		sysfsGpio.c
 *
 *	\brief		stellt Funktionen für den GPIO Zugriff über sysfs zur Verfügung.
 *
 *	\details	-
 *
 *	\date		04.09.2020
 *	\author		Marcel Ming
 *	\copyright	TRiMADA AG   CH-5610 Wohlen
 *	\remark		Compiler- / IDE-Version
 *
 *	\version	04.09.20 mm
 *					- Erstellt.
 *
 ******************************************************************************/


/******************************************************************************
 * Compilerswitches
 *
 ******************************************************************************/
#define __sysfsGpio_printDebugMsg


/******************************************************************************
 * Modules
 *
 ******************************************************************************/
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "sysfsGpio.h"


/******************************************************************************
 * Definitions and Macros
 *
 ******************************************************************************/

/******************************************************************************
 * Typedefs and Structures                                                    *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Exported Variables                                                         *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Global Variables                                                           *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Local Functions                                                            *
 *                                                                            *
 ******************************************************************************/

/**************************************************************************//**
 *
 *  \brief		sysfsGpio_export
 *
 *  \details	exportiert den GPIO gpioNumber.
 *
 *	\param		gpioNumber
 *
 *	\return		0 = ok, negativ = errorcode der open- oder write- Funktion
 *
 ******************************************************************************/
int sysfsGpio_export (int gpioNumber)
{
	char	buffer [C_sysfsGpio_bufferLenInt];
	int		len;
	int		fd;

	fd = open ("/sys/class/gpio/export", O_WRONLY);

	if (fd < 0)
	{
		#ifdef __sysfsGpio_printDebugMsg
			printf ("failed export GPIO %d\n", gpioNumber);
		#endif
		
		return (fd);
	} // if (fd < 0)

	len = snprintf (buffer, sizeof (buffer), "%d", gpioNumber);

	if (write (fd, buffer, len) < 0)
	{
		#ifdef __sysfsGpio_printDebugMsg
			printf ("failed write GPIO %d\n", gpioNumber);
		#endif
		
		return (fd);
	} // if (write (fd, buffer, len) < 0)

	// keine Funktion fehlgeschlagen
	close (fd);

	return (0);
} /* sysfsGpio_export */


/**************************************************************************//**
 *
 *  \brief		sysfsGpio_unexport
 *
 *  \details	un-exportiert den GPIO gpioNumber.
 *
 *	\param		gpioNumber
 *
 *	\return		0 = ok, negativ = errorcode der open Funktion
 *
 ******************************************************************************/
int sysfsGpio_unexport (int gpioNumber)
{
	char	buffer [C_sysfsGpio_bufferLenInt];
	int		len;
	int		fd;

	fd = open ("/sys/class/gpio/unexport", O_WRONLY);

	if (fd < 0)
	{
		#ifdef __sysfsGpio_printDebugMsg
			printf ("failed unexport GPIO %d\n", gpioNumber);
		#endif
		
		return (fd);
	} // if (fd < 0)

	len = snprintf (buffer, sizeof (buffer), "%d", gpioNumber);

	if (write (fd, buffer, len) < 0)
	{
		#ifdef __sysfsGpio_printDebugMsg
			printf ("failed write GPIO %d\n", gpioNumber);
		#endif
		
		return (fd);
	} // if (write (fd, buffer, len) < 0)

	// keine Funktion fehlgeschlagen
	close (fd);

	return (0);
} /* sysfsGpio_unexport */


/**************************************************************************//**
 *
 *  \brief		sysfsGpio_direction
 *
 *  \details	definiert den GPIO gpioNumber als Ein- oder Ausgang.
 * 					- 0: Eingang
 * 					- 1: Ausgang
 *
 *	\param		gpioNumber
 *	\param		direction
 *
 *	\return		0 = ok, negativ = errorcode der open Funktion
 *
 ******************************************************************************/
int sysfsGpio_direction (int gpioNumber, int direction)
{
	static const char	dir_str [] = "in\0out";
	char				path [C_sysfsGpio_bufferLenPath];
	int					fd;

	snprintf (path, sizeof (path), "/sys/class/gpio/gpio%d/direction", gpioNumber);

	fd = open (path, O_WRONLY);

	if (fd < 0) return (fd);
	if (write (fd, &dir_str [direction == 0 ? 0 : 3], direction == 0 ? 2 : 3) < 0) return (fd);

	// keine Funktion fehlgeschlagen
	close (fd);

	return (0);
} /* sysfsGpio_direction */


/**************************************************************************//**
 *
 *  \brief		sysfsGpio_write
 *
 *  \details	schreibt zum Ausgang GPIO gpioNumber den Wert value.
 * 					- 0: low
 * 					- 1: high
 *
 *	\param		gpioNumber
 *	\param		direction
 *
 *	\return		0 = ok, negativ = errorcode der open Funktion
 *
 ******************************************************************************/
int sysfsGpio_write (int gpioNumber, int value)
{
	static const char	values_str [] = "01";
	char				path [C_sysfsGpio_bufferLenPath];
	int					fd;

	snprintf(path, sizeof(path), "/sys/class/gpio/gpio%d/value", gpioNumber);

	fd = open (path, O_WRONLY);

	if (fd < 0)
	{
		#ifdef __sysfsGpio_printDebugMsg
			printf ("failed open GPIO %d\n", gpioNumber);
		#endif
		
		return (fd);
	} // if (fd < 0)
	
	if (write (fd, &values_str [value == 0 ? 0 : 1], 1) < 0)
	{
		#ifdef __sysfsGpio_printDebugMsg
			printf ("failed write GPIO %d\n", gpioNumber);
		#endif
		
		return (fd);
	} // if (write (fd, &values_str [value == 0 ? 0 : 1], 1) < 0)

	// keine Funktion fehlgeschlagen
	close (fd);

	#ifdef __sysfsGpio_printDebugMsg
		printf ("success write GPIO %d\n", gpioNumber);
	#endif
		
	return (0);
} /* sysfsGpio_write */


/**************************************************************************//**
 *
 *  \brief		sysfsGpio_read
 *
 *  \details	liest vom Eingang GPIO gpioNumber.
 *
 *	\param		gpioNumber
 *
 *	\return		Eingangspegel 0: low
 * 							  1: high
 *
 ******************************************************************************/
int sysfsGpio_read (int gpioNumber)
{
	char	path [C_sysfsGpio_bufferLenPath];
	char	value_str [3];
	int		fd;

	snprintf (path, sizeof (path), "/sys/class/gpio/gpio%d/value", gpioNumber);

	fd = open (path, O_RDONLY);

	if (fd < 0) return (fd);
	if (read (fd, value_str, 3) < 0) return (fd);

	// keine Funktion fehlgeschlagen
	close (fd);

	return (atoi(value_str));
} /* sysfsGpio_read */


/**************************************************************************//**
 *
 *  \brief		sysfsGpio_edge
 *
 *  \details	setzt das Interrupt-Verhalten des Eingangs GPIO gpioNumber.
 * 					- 0: none
 * 					- 1: rising
 * 					- 2: falling
 * 					- 3: both
 *
 *	\param		gpioNumber
 *	\param		edge
 *
 *	\return		0 = ok, negativ = errorcode der open Funktion
 *
 ******************************************************************************/
int sysfsGpio_edge (int gpioNumber, int edge)
{
	const char	dir_str [] = "none\0rising\0falling\0both";
	char		ptr;
	char		path [C_sysfsGpio_bufferLenPath];
	int			fd;

	switch (edge)
	{
		case 0:
			ptr = 0;
			break;

		case 1:
			ptr = 5;
			break;

		case 2:
			ptr = 12;
			break;

		case 3:
			ptr = 20;
			break;

		default:
			ptr = 0;
	} // switch (edge)

	snprintf (path, sizeof (path), "/sys/class/gpio/gpio%d/edge", gpioNumber);

	fd = open(path, O_WRONLY);

	if (fd < 0)
	{
		return (fd);
	}

	if (write (fd, &dir_str [ptr], strlen (&dir_str [ptr])) < 0)
	{
		return (fd);
	}

	close(fd);

	return (0);
} /* sysfsGpio_edge */


/* end sysfsGpio.c */
