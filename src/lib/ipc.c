/**************************************************************************//**
 *
 *	\file		ipc.c
 *
 *	\brief		Stellt Funktionen für die inter process communication auf der
 *				Basis von FiFo's zur Verfügung.
 *
 *	\details		-
 *
 *	\date		17.06.2015
 *	\author		Marcel Ming
 *	\copyright	TRiMADA AG   CH-5610 Wohlen
 *	\remark		GCC Embedded
 *
 *	\version		17.06.15 mm
 *						- Erstellt.
 *	\version		11.06.21 mm
 *						- Funktionen universeller implementiert.
 *
 *
 ******************************************************************************/
#ifdef __cplusplus
    extern "C" {
#endif // __cplusplus


/******************************************************************************
 * Modules                                                                    *
 *                                                                            *
 ******************************************************************************/
#include <errno.h>

#include "ipc.h"


/******************************************************************************
 * Definitions and Macros                                                     *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Typedefs and Structures                                                    *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Prototypes and Local Functions                                             *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Exported Variables                                                         *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Global Variables                                                           *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Local Functions                                                            *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Exported Functions                                                         *
 *                                                                            *
 ******************************************************************************/


/**************************************************************************//**
 *
 *  \brief		ipc_createFiFo
 *
 *  \details    erstellt ein benanntes FiFo für die inter process communication.
 *
 *	\param      pFiFoName	Pointer auf den Namen String für die 'named pipe'
 *
 *	\return		 0  FiFo erstellt
 *				-1	Fehler beim erstellen des FiFo
 *
 ******************************************************************************/
int	ipc_createFiFo (const char *pFiFoName)
{
    int retVal = 0;
/*
    if (access (pFiFoName, F_OK) != -1)
    {
        // FiFo für vorhanden -> ungültiger Inhalt entfernen durch löschen
        retVal = unlink (pFiFoName);

		fprintf (stderr, "retval unlink: %d\n", retVal);
    }

    if (retVal == 0)
    {
    */ 
        // FiFo erstellen mit Schreib- und Leseberechtigung für alle, allerdings umask beachten!
        retVal = mkfifo  (pFiFoName, 0666);

		fprintf (stderr, "retval mkfifo: %d\n", retVal);
		/*
    } // if (retVal == 0)
*/
    return (retVal);
} /* ipc_createFiFo */


/**************************************************************************//**
 *
 *  \brief		ipc_deleteFiFo
 *
 *  \details    löscht ein benanntes FiFo für die inter process communication.
 *
 *	\param      pFiFoName	Pointer auf den Namen String für die 'named pipe'
 *
 *	\return		 0  FiFo erstellt
 *				-1	Fehler beim erstellen des FiFo
 *
 ******************************************************************************/
int	ipc_deleteFiFo (const char *pFiFoName)
{
	return (unlink (pFiFoName));
} /* ipc_deleteFiFo */


/**************************************************************************//**
 *
 *  \brief		ipc_writeFiFo
 *
 *  \details    schreibt die Daten in DataString ins FiFo FiFoName.
 *
 *	\param      pFiFoName		Pointer auf den Namen String
 *              pDataString		Pointer auf den Daten String
 *
 *	\return		 0  DataString in FiFo geschrieben
 *				-1	Fehler beim schreiben des FiFo's
 *
 ******************************************************************************/
int ipc_writeFiFo (const char *pFiFoName, const char *pDataString)
{
    int fdFiFo;
    int bytesWritten;
    int retVal = -1;


	fprintf (stderr, "ipc_writeFiFo open");

    // Resultat mittels ipc via FiFo senden
    fdFiFo = open (pFiFoName, O_WRONLY | O_NDELAY);

    if (fdFiFo < 0)
    {
		fprintf (stderr, " failed, error: %d, %s\n", errno, strerror (errno));
    }
    else
    {
		fprintf (stderr, ", name: %s, data: %sretval mkfifo: %d\n", pFiFoName, pDataString, fdFiFo);

        bytesWritten = write (fdFiFo, pDataString, strlen(pDataString));
		fprintf (stderr, "ipc_writeFiFo write, bytes written: %d\n", bytesWritten);

        // ACHTUNG ! FiFo wird ev. mit close zerstört !
        close (fdFiFo);
        retVal = 0;
	} // else if (fdFiFo > 0)

    return (retVal);
} /* ipc_writeFiFo */


/**************************************************************************//**
 *
 *  \brief		ipc_readFiFo
 *
 *  \details    liest die Daten im FiFo FiFoName in den DataString.
 *
 *	\param      pFiFoName		Pointer auf den Namen String
 *              pDataString		Pointer auf den Daten String
 *
 *	\return		>0  FiFo gelesen (Anzahl Bytes)
 *				-1	Fehler beim lesen des FiFo's
 *
 ******************************************************************************/
int ipc_readFiFo (const char *pFiFoName, char *pDataString)
{
    int fdFiFo;
    int bytesRead;
    int retVal = -1;

    char debugString [C_ipc_bufferSize];

    // FiFo öffnen, lesen und wieder schliessen
    fdFiFo = open (pFiFoName, O_RDONLY | O_NDELAY);

//  printf ("ipc_ReadFiFo: Filename (%s), Descriptor (%d)\n", pFiFoName, fdFiFo);

    if (fdFiFo > 0)
    {
        bytesRead = read (fdFiFo, pDataString, C_ipc_bufferSize);

        if (bytesRead > 0)
        {
            printf ("ipc_ReadFiFo: Data (%s), Bytes (%d)\n", pDataString, bytesRead);
        }
        else
        {
//          printf("ipc_ReadFiFo: no data\n");
        }

        retVal = bytesRead;

        // Wiederholung Debug
        bytesRead = read (fdFiFo, debugString, C_ipc_bufferSize);

        if (bytesRead > 0)
        {
            printf("ipc_ReadFiFo: DebugData (%s), Bytes (%d)\n", debugString, bytesRead);
        }
    } // if (fdFiFo > 0)

    close (fdFiFo);

    return (retVal);
} /* ipc_readFiFo */


#ifdef __cplusplus
    } // extern "C"
#endif // __cplusplus


/* End ipc.c */
