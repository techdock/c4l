/**************************************************************************//**
 *
 *	\file		csw.h
 *
 *	\brief		enthält die globalen compiler switches des Projekts.
 *
 *	\details	-
 *
 *	\date		06.09.2021
 *	\author		Marcel Ming
 *	\copyright	TRiMADA AG   CH-5610 Wohlen
 *	\remark		-
 *
 *	\version	06.09.21 mm
 *					- Erstellt.
 *	\version	29.10.21 mm
 *					- Compilerschalter __csw_configAiCurrent und __csw_configAoCurrent
 *					  definiert.
 *	\version	30.11.21 mm
 *					- Compilerschalter __csw_hardwareTest, __csw_configOverride,
 *					  __csw_configAiCurrent und __csw_configAoCurrent entfernt.
 *	\version	22.09.23 mm
 *					- Struktur ts_csw_flag mit simpleTest erweitert.
 *
 *
 ******************************************************************************/
#ifndef __csw_H
    #define __csw_H

#ifdef __cplusplus
    extern "C" {
#endif // __cplusplus


/******************************************************************************
 * Compiler Switches                                                          *
 *                                                                            *
 ******************************************************************************/
//	#define __csw_slowdown
//	#define __csw_mail_useSendmail
	#define __csw_mail_useMailx


/******************************************************************************
 * Modules                                                                    *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Definitions and Macros                                                     *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Typedefs and Structures                                                    *
 *                                                                            *
 ******************************************************************************/
typedef struct {
	uint	configOverride	: 1;	//!< Konfiguration aus Datenbank wird überschrieben
	uint	hardwareTest	: 1;	//!< Hardware Testmodus aktiv
	uint	unitVoltage		: 1;	//!< Hardware Testmodus 0: Strommessung, 1: Spannungsmessung
	uint	verbose			: 1;	//!< detaillierte Ausgabe auf Konsole
	uint	simpleTest		: 1;	//!< einfacher Hardwaretest, keine Signalverarbeitung, keine Datenbank
} ts_csw_flag;


/******************************************************************************
 * Exported Variables                                                         *
 *                                                                            *
 ******************************************************************************/
extern ts_csw_flag	csw_flag;


/******************************************************************************
 * Exported Functions                                                         *
 *                                                                            *
 ******************************************************************************/


#ifdef __cplusplus
    } // extern "C"
#endif // __cplusplus


#endif // __csw_H


/* End csw.h */
