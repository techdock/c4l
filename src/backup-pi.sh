#! /bin/bash

# file          backup-pi.sh
# brief         script for project control4log
# details       backup the full raspberry pi OS
# date          04.06.2023
# author        Michael Mislin
# copyright     techdock GmbH, 4102 Binningen, Switzerland
# license       The MIT License (MIT)
#
# version       04.06.2023      mimi    - creating
#

# declare script variables and dierctories
project="control4log"
name="c4l"
filename=$(basename $0)
user=$(whoami)
todfile=$(date +'%d.%m.%Y_%H:%M:%S')
tod=$(date)

# enter password at CLI
pw=""
printf "\n\nInput password for device DATABASE and push ENTER:"
stty -echo # deactivate echo, password is not shown
read pw
stty echo # activate echo
printf "\n\n"

#printf "pw: ${pw}\n"

# create instdir = /opt/<name>/
instdir="/opt/${name}"
printf "Install directory is:\t\t${instdir}\n"

# create srcdir = /home/<user>/projects/<project>/src/
srcdir="$(cd -P -- "$(dirname -- "$0")" && pwd -P)" # will get back current directory where this skript is running
printf "Source directory is:\t\t${srcdir}\n"

# navigate to prodir /home/<user>/projects/<project>/
cd srcdir > /dev/null 2>&1 # redirect output for make cmd silent
cd ../ > /dev/null 2>&1 # redirect output for make cmd silent

# create prodir = /home/<user>/projects/<project>/
prodir="$(cd -P -- "$(dirname -- "$0")" && pwd -P)" # will get back current directory where this skript is running
printf "Project directory is:\t\t${prodir}\n"

# create bindir = /home/<user>/projects/<project>/opt/<name>/
bindir="$(cd -P -- "$(dirname -- "$0")" && pwd -P)${instdir}" # will get back current directory where this skript is running
printf "Binary directory is:\t\t${bindir}\n"

# navigate to homedir /home/<user>/
cd prodir > /dev/null 2>&1 # redirect output for make cmd silent
cd ../../ > /dev/null 2>&1 # redirect output for make cmd silent

# create homedir = /home/<user>/
homedir="$(cd -P -- "$(dirname -- "$0")" && pwd -P)" # will get back current directory where this skript is running
homedir="${homedir}/${name}"
printf "Home directory is:\t\t${homedir}\n"

# create docdir = /home/<user>/Documents/
docdir="${homedir}/Documents"
printf "Documents directory is:\t\t${docdir}\n"

# get project version from database
ver=$(mysql --database=testBase --user=trimada --password=$pw -s -N --execute="SELECT value FROM PA WHERE id = 0;")

# get device SN from database
sn=$(mysql --database=testBase --user=trimada --password=$pw -s -N --execute="SELECT value FROM PA WHERE id = 9;")

# create backdir = /home/<user>/Documents/<ver>-backup/
backdir="${docdir}/${name}-img"
printf "Backup directory is:\t\t${backdir}\n"

imgname=$sn-v$ver.img

# overwrite password string with NULL
pw=""

printf "\n### Running ${filename} to start project ${project}, v${ver} services as user ${user} at ${tod}\n"


# starting process
printf "\n### starting setup for ${project}\n\n"


## create backup directory if not existing
sudo mkdir $backdir > /dev/null 2>&1 # redirect output for make cmd silent

## show all partitions
printf "\n"
lsblk -f
printf "\n"

## define partition
partition=""
printf "Input the partiotion name for backup:"
printf "\nChoose the MAIN name like e.g. sda or mmcblk0\n"
read partition
printf "\n\n"

## define size of image in MB
imagesize=""
printf "Input MB size of the image from microSD:"
printf "Info: has to be more as partition size!"
printf "\n1GB = 1000MB\n"
read imagesize
printf "\n"

## create image file.img
printf "Create IMG file"
printf "\nPlease take a seat, this will take a while!"
printf "\nif you wanna check ¨the progress go to </home/<user>/Documents/<project>-img> and check file size with CMD <ls -la>\n"
sudo dd if=/dev/$partition of=$backdir/$imgname bs=1M count=$imagesize

## copy image file.img
printf "\Copy file.img to ${docdir}"
sudo cp $backdir/$imgname /$docdir/$imgname

## shrink image file.img to file.img.gz with script
printf "\nShrink IMG file"
printf "\nAnd again: Please take a seat, this will take again a while!"
printf "\nif you wanna check ¨the progress go to </home/<user>/Documents/<project>-img> and check file size with CMD <ls -la>\n"
cd $backdir
sudo bash $homedir/projects/img/pishrink.sh -z $imgname

## show img file
printf "\n\nimage can be found here:"
cd $backdir
ls -la

## ask to copy image.img.gz to external disk
printf "\nWanna copy image <${imgname}.gz> to USB-storage?\n y = yes: "
   read inputcp
   #printf "\n"

if [ "$inputcp" = "y" ]; then

   printf "\nIs USB-storage connected?\nIf not, connect it now and push y for yes: "
   read inputdevice
   #printf "\n"

      if [ "$inputdevice" = "y" ]; then

         printf "\n"
         lsblk -f
         printf "\n\n"

         ## input partition of USB-storage
         storage=""
         printf "Input the partiotion name for backup:"
         printf "\nChoose the SUB name like e.g. sda1 or mmcblk0p1\n"
         read storage
         printf "\n"

         printf "\nNeed to mount USB-storage ?"
         printf "\nfor use image at windows format <ntfs> and linux <ext4>"
         printf "\ny = yes: "

         read mountdevice
         #printf "\n"

             if [ "$mountdevice" = "y" ]; then

                sudo mkdir /dev/$name-img > /dev/null 2>&1 # redirect output for make cmd silent
                sudo mount /dev/$storage /dev/$name-img > /dev/null 2>&1 # redirect output for make cmd silent

             fi

         sudo cp $backdir/$imgname.gz /dev/$name-img/$imgname.gz

      fi

fi

## ask to delete image.img and image.img.gz
printf "\nWanna delete the new image <${imgname}> AND <${imgname}.gz>?\n y = yes: "
   read input2
   #printf "\n"

if [ "$input2" = "y" ]; then

    printf "Delete now <${imgname}> and <${imgname}.gz>\n"
    sudo rm $docdir/$imgname > /dev/null 2>&1 # redirect output for make cmd silent
    sudo rm $backdir/$imgname.gz > /dev/null 2>&1 # redirect output for make cmd silent

fi


# end skript
printf "### end ${filename}\n\n"
