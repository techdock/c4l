#! /bin/bash

# file          reboot-c4l.sh
# brief         script for project control4log
# details       reboot device with flags FC32, FC33, FC34 and FC99
# date          11.05.2023
# author        Michael Mislin
# copyright     techdock GmbH, 4102 Binningen, Switzerland
# license       The MIT License (MIT)
#
# version       11.05.2023      mimi    - creating
#

# declare script variables and dierctories
project="control4log"
name="c4l"
filename=$(basename $0)
user=$(whoami)
todfile=$(date +'%d.%m.%Y_%H:%M:%S')
tod=$(date)

printf "\n### Running ${filename} to start project ${project} services as user ${user} at ${tod}\n"

# create instdir = /opt/<name>/
instdir="/opt/${name}"
printf "Install directory is:   ${instdir}\n"

# create srcdir = /home/<user>/projects/<project>/src/
srcdir="$(cd -P -- "$(dirname -- "$0")" && pwd -P)" # will get back current directory where this skript is running
printf "Source directory is:    ${srcdir}\n"

# navigate to prodir /home/<user>/projects/<project>/
cd srcdir > /dev/null 2>&1 # redirect output for make cmd silent
cd ../ > /dev/null 2>&1 # redirect output for make cmd silent

# create prodir = /home/<user>/projects/<project>/
prodir="$(cd -P -- "$(dirname -- "$0")" && pwd -P)" # will get back current directory
printf "Project directory is:    ${prodir}\n"

# create bindir = /home/<user>/projects/<project>/opt/<name>/
bindir="$(cd -P -- "$(dirname -- "$0")" && pwd -P)${instdir}" # will get back current directory
printf "Binary directory is:    ${bindir}\n"

# navigate to homedir /home/<user>/
cd prodir > /dev/null 2>&1 # redirect output for make cmd silent
cd ../../ > /dev/null 2>&1 # redirect output for make cmd silent

# create homedir = /home/<user>/
homedir="$(cd -P -- "$(dirname -- "$0")" && pwd -P)" # will get back current directory where this skript is running
homedir="${homedir}/${name}"
printf "Home directory is:\t\t${homedir}\n"

# create docdir = /home/<user>/Documents/
docdir="${homedir}/Documents" # will get back current directory
printf "Documents directory is:    ${srcdir}\n"


# starting process
printf "\n### starting setup for ${project}\n\n"


## enter password device

pw=""

### enter password at CLI

printf "Input password for device DATABASE and push ENTER:"
stty -echo # deactivate echo, password is not shown
read pw
stty echo # activate echo
printf "\n"

#printf "pw: ${pw}\n"
 

## restart  device

printf "\n### FLAG current:\n"
mysql --database=testBase --user=trimada --password=$pw --execute="SELECT * FROM FLAG;"

printf "\n### set FC32: set standard PA's"
mysql --database=testBase --user=trimada --password=$pw --execute="UPDATE FLAG SET value = 1 WHERE id = 2;"

printf "\n### set FC33: set new ip's"
mysql --database=testBase --user=trimada --password=$pw --execute="UPDATE FLAG SET value = 1 WHERE id = 3;"

printf "\n### set FC34: set new ntp host"
mysql --database=testBase --user=trimada --password=$pw --execute="UPDATE FLAG SET value = 1 WHERE id = 4;"

printf "\n### FLAG new:\n"
mysql --database=testBase --user=trimada --password=$pw --execute="SELECT * FROM FLAG;"
printf "\n\n"

### ... and now reboot device!
printf "\n### set FC99: rebooting and execute PA changes"
mysql --database=testBase --user=trimada --password=$pw --execute="UPDATE FLAG SET value = 1 WHERE id = 5;"
printf "\n\n"


# end skript
printf "### end ${filename}\n\n"
