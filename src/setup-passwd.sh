#! /bin/bash

# file          setup-passwd.sh
# brief         script for project control4log
# details       setup all passwords used in project
# date          18.05.2023
# author        Michael Mislin
# copyright     techdock GmbH, 4102 Binningen, Switzerland
# license       The MIT License (MIT)
#
# version       18.05.2023      mimi    - creating
#

# declare script variables and dierctories
project="control4log"
name="c4l"
filename=$(basename $0)
user=$(whoami)
todfile=$(date +'%d.%m.%Y_%H:%M:%S')
tod=$(date)
pwos="master-5610"
pwdb="Master-5610"
pwvnc="456852"

printf "\n### Running ${filename} to start project ${project}, v${ver} services as user ${user} at ${tod}\n"

# create instdir = /opt/<name>/
instdir="/opt/${name}"
printf "Install directory is:\t\t${instdir}\n"

# create srcdir = /home/<user>/projects/<project>/src/
srcdir="$(cd -P -- "$(dirname -- "$0")" && pwd -P)" # will get back current directory where this skript is running
printf "Source directory is:\t\t${srcdir}\n"

# navigate to prodir /home/<user>/projects/<project>/
cd srcdir > /dev/null 2>&1 # redirect output for make cmd silent
cd ../ > /dev/null 2>&1 # redirect output for make cmd silent

# create prodir = /home/<user>/projects/<project>/
prodir="$(cd -P -- "$(dirname -- "$0")" && pwd -P)" # will get back current directory where this skript is running
printf "Project directory is:\t\t${prodir}\n"

# create bindir = /home/<user>/projects/<project>/opt/<name>/
bindir="$(cd -P -- "$(dirname -- "$0")" && pwd -P)${instdir}" # will get back current directory where this skript is running
printf "Binary directory is:\t\t${bindir}\n"

# navigate to homedir /home/<user>/
cd prodir > /dev/null 2>&1 # redirect output for make cmd silent
cd ../../ > /dev/null 2>&1 # redirect output for make cmd silent

# create homedir = /home/<user>/
homedir="$(cd -P -- "$(dirname -- "$0")" && pwd -P)" # will get back current directory where this skript is running
homedir="${homedir}/${name}"
printf "Home directory is:\t\t${homedir}\n"

# create docdir = /home/<user>/Documents/
docdir="${homedir}/Documents"
printf "Documents directory is:\t\t${docdir}\n"

# create backdir = /home/<user>/Documents/<ver>-backup/
backdir="${docdir}/${ver}-backup"
printf "Backup directory is:    ${backdir}\n"


# starting process
printf "\n### starting setup passwords for ${project}\n\n"


## set password for os user <c4l>
printf "# Set standard password for device for user <c4l>:"
sudo passwd $name <<- !
$pwos
$pwos
!
printf "\n\n"


## set password for os user <root
printf "# Input new password for device for user <root>:"
sudo passwd root <<- !
$pwos
$pwos
!
printf "\n\n"


### set password for x11vnc
printf "# Input new password for x11vnc\n"
sudo x11vnc -storepasswd $homedir/.vnc/passwd <<- !
$pwvnc
$pwvnc
y
!
!
printf "\n\n"


## show MariaDB version
printf "# Show MariaDB version:\n"
mysql -V --database=testBase --user=trimada --password=Master-5610
 # different way ith more details
 #mysql --database=testBase --user=trimada --password=Master-5610 --execute="SHOW VARIABLES LIKE \"%version%\";"

printf "# Input current password for MariaDB user <trimada> and <root>\n"
read pwold

## show existing accounts in MariaDB
printf "# Show all accounts:\n"
mysql --database=testBase --user=trimada --password=$pwold --execute="SELECT host, user, password FROM mysql.user;"

printf "# Set password for MariaDB user <root>\n"
mysql --database=testBase --user=trimada --password=$pwold --execute="SET PASSWORD FOR \"trimada\"@localhost = PASSWORD(\"${pwdb}\");"
 # different way
 #mysql --database=testBase --user=trimada --password=$pwold --execute="UPDATE mysql.user SET Password = PASSWORD(\"${pw1}\") WHERE User = \"trimada\";"

printf "# Set password for MariaDB user <root>\n"
mysql --database=testBase --user=root --password=$pwold --execute="SET PASSWORD FOR \"root\"@localhost = PASSWORD(\"${pwdb}\");"


# end skript
printf "### end ${filename}\n\n"
