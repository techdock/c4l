/**************************************************************************//**
 *
 *	\file		tcpss.c
 *
 *	\brief		TCP/IP socket server zum lesen und schreiben der Datenbank
 *
 *	\details	Dieses Programm agiert als TCP/IP Socket Server. Ein einziger 
 * 				Client kann eine Verbindung zum Server herstellung mit diesem
 * 				kommunizieren. Die Aufrufe "listen" und "read" sind blockierend.
 * 				Das heisst, dass das Programm so lange in der Funktion hängen
 * 				bleibt, bis sich ein Client anmeldet oder Daten im Buffer sind.
 *
 *	\date		07.07.2021
 *	\author		Claudio Häusermann
 *	\copyright	TRiMADA AG   CH-5610 Wohlen
 *	\remark		Compiler- / IDE-Version
 *
 *	\version	07.07.21 ch
 *					- Erstellt.
 * 	\version	19.07.21 ch
 * 					- TCP/IP Port wird aus DB gelesen
 * 	\version	19.08.21 ch
 * 					- Signalisierung, dass Netwerkeinstellungen geändert haben via
 * 					  FLAG
 * 					- Unterbindung, dass NULL als Zahlenwert geschrieben werden 
 * 					  kann. 
 * 	\version	03.11.21 mm
 *					- detaillierte Fehlerausgabe bei bind Fehlern.
 *	\version	18.11.21 mm
 *					- C_appVersion über builDate.h implementiert.
 *					- detailiertere Ausgaben eingefügt.
 *	\version	22.11.21 mm
 *					- main muss als Parameter die eth Nummer übergeben werden.
 *	\version	25.11.21 mm
 *					- Zugriffsrechte der einzelnen eth's implementiert.
 *	\version	16.12.21 mm
 *					- Socketoptionen SO_REUSEADDR und SO_REUSEPORT implementiert.
 *	\version	21.12.21 mm
 *					- Im State bind socket "soFlag = 1" eingefügt.
 *					- Im State close socket "close (connfd)" eingefügt.
 *	\version	23.12.21 mm
 *					- Im State readData bei einem Lesefehler die Verbindung mit
 *					  "close (connfd)" schliessen bevor zum State listen client
 *					  gwechselt wird.
 *	\version	17.02.22 mm
 *					- detailiertere Fehlerausgaben in main eingefügt.
 *	\version	24.02.22 mm
 *					- bei einem Fehler im State E_tcpss_mainState_readData wird
 *					  als nächtser State E_tcpss_mainState_closeSocket aufgerufen.
 *					- neuer Parameter -v verbose
 *	\version	17.05.22 mm
 *					- Wartezeit von 100 ms vor einem Retry eingefügt. (Issue #98)
 *	\version	20.10.23 mm
 *					- #include "db.h" eingefügt.
 *
 *
 ******************************************************************************/

/******************************************************************************
 * Compilerswitches                                                           *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Modules                                                                    *
 *                                                                            *
 ******************************************************************************/
#include <arpa/inet.h>
#include <errno.h>
#include <stdio.h>
#include <netdb.h>
#include <netinet/in.h>
#include <mysql/mysql.h>
#include <stdlib.h> 
#include <string.h> 
#include <sys/socket.h> 
#include <sys/types.h> 
#include <unistd.h> 

#include "buildDate.h"
#include "db.h"
#include "tcpss.h"
#include "ptkC4l.h"


/******************************************************************************
 * Definitions and Macros                                                     *
 *                                                                            *
 ******************************************************************************/
#define	C_appName		"tcpss"

const uint8_t C_appVersion [] = {
   C_buildDate_year_CH0,
   C_buildDate_year_CH1,
   C_buildDate_month_CH0,
   C_buildDate_month_CH1,
   C_buildDate_day_CH0,
   C_buildDate_day_CH1,
   '-',
   C_buildDate_hour_CH0,
   C_buildDate_hour_CH1,
   C_buildDate_min_CH0,
   C_buildDate_min_CH1
};


/******************************************************************************
 * Typedefs and Structures                                                    *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Exported Variables                                                         *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Global Variables                                                           *
 *                                                                            *
 ******************************************************************************/
int	verbose	= 0;	//!< detaillierte Ausgabe auf Konsole


/******************************************************************************
 * Local Functions                                                            *
 *                                                                            *
 ******************************************************************************/
 
 /**************************************************************************//**
 *
 *  \brief		tcpss_findIndexOfLf
 *
 *  \details	sucht im String nach dem ersten \n.
 *
 *	\param		*pStr		Pointer auf den String
 *
 *	\return		index von 
 *
 ******************************************************************************/
int tcpss_findIndexOfLf (char *pStr)
{
	char lf = '\n';
	int index;
	const char *ptr = strchr(pStr, lf);

	if(ptr) 
	{
		index = ptr - pStr;
	} // if(ptr)
	else
	{
		index = - 1;
	} // else if(ptr)

	return (index);
} /* tcpss_findIndexOfLf */


/**************************************************************************//**
 *
 *  \brief		tcpss_processParam
 *
 *  \details	wertet eventuell übergebenen Programm Parameter aus.
 *
 *	\param		argc	Argument Conter
 *	\param		*argv	Argument Vector
 *	\param		*pDbPA	Pointer auf Struktur ts_db_pa
 *
 *	\return		-
 *
 ******************************************************************************/
static void tcpss_processParam (int		 argc,
								char	 *argv [],
								int		 *pEthNumber,
								int		 *pIdxEthAddr,
								ts_db_pa *pDbPA)
{
	int	option;


	while ((option = getopt (argc, argv, "012v")) != -1)
	{
		switch (option)
		{
			case '0':
				*pEthNumber	 = 0;
				*pIdxEthAddr = C_db_paBaseEth0;
				pDbPA->id	 = C_db_paBaseApp0;
				break;

			case '1':
				*pEthNumber	 = 1;
				*pIdxEthAddr = C_db_paBaseEth1;
				pDbPA->id	 = C_db_paBaseApp1;
				break;

			case '2':
				*pEthNumber	 = 2;
				*pIdxEthAddr = C_db_paBaseEth2;
				pDbPA->id	 = C_db_paBaseApp2;
				break;

			case 'v':
				verbose = 1;
				fprintf (stderr, "Meldungen eingeschaltet\n");
				break;

			default:
				break;
		} // switch (option)
	} // while ((option = getopt (argc, argv, "012")) != -1)
} /* tcpss_processParam */


/******************************************************************************
 * Exported Functions                                                         *
 *                                                                            *
 ******************************************************************************/

/**************************************************************************//**
 *
 *  \brief		main
 *
 *  \details	Hauptprogramm vom TCP/IP Socket Server
 *
 *
 *	\return		-
 *
 ******************************************************************************/
int main (int	argc,
		  char*	argv [])
{ 
	te_tcpss_mainState	mainState = E_tcpss_mainState_createSocket;
	char				rxBuffer[C_tcpss_bufferLength];
	char				txBuffer[C_tcpss_bufferLength];
	int					ethNumber;
	int					idxEthAddr;
	int					bytesRead = 0;
	int					bytesWrite = 0;
    int					sockfd;
	int					connfd;
	int					tcpPort;
	int					soFlag = 1;
	socklen_t			len; 
    struct sockaddr_in	servaddr;
	struct sockaddr_in	cli;
	struct timeval		timerValue; 
	int					retValue;
	ts_db_connect		connect;
	ts_db_pa			dbPA;

	
	// immer auf Konsole ausgeben
	fprintf (stderr, "start %s version %s, as user %s\n", C_appName, C_appVersion, getlogin ());

	dbPA.id = 0;
	timerValue.tv_sec  = C_tcpss_waitRead;
	timerValue.tv_usec = 0;

	// Paramater auswerten
	tcpss_processParam (argc, argv, &ethNumber, &idxEthAddr, &dbPA);

	if (dbPA.id != 0)
	{
		if (verbose == 0)
		{
			fprintf (stderr, "Meldungen ausgeschaltet\n");
			freopen ("dev/null", "w", stderr);
		} // if (csw_flag.verbose == 0)

		fprintf (stderr, "preparing for eth%d\n", ethNumber);
		
		retValue = db_connectInit (&connect,
								   C_db_dataBaseName,
								   C_db_dataBaseHost,
								   C_db_dataBaseUser,
								   C_db_dataBasePassword);
								   
		retValue = retValue | db_rowGet (&connect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA);
								   
		if (retValue == 0)
		{
			fprintf (stderr,"mysql library and the access structure to the database initialized...\n");
			tcpPort = atoi(dbPA.strValue);
		
			while(1)
			{
				switch(mainState)
				{
					case E_tcpss_mainState_createSocket:
     					fprintf (stderr,"eth:%d, enter socket create\n", ethNumber); 

						// Socket erstellen und prüfen
						sockfd = socket (AF_INET, SOCK_STREAM, 0);

						if (sockfd == -1)
						{ 
							// Socket schliessen und nach Wartezeit wieder versuchen 
							fprintf (stderr,"eth:%d, Socket creation failed, error %d, %s\n", ethNumber, errno, strerror (errno)); 

							mainState = E_tcpss_mainState_closeSocket;
						} // if (sockfd == -1) 
						else
						{
							fprintf (stderr,"eth:%d, Socket successfully created, sockfd:%d\n", ethNumber, sockfd);

							mainState = E_tcpss_mainState_bindSocket;
						} // else if (sockfd == -1)
						break;

					case E_tcpss_mainState_bindSocket:
     					fprintf (stderr,"eth:%d, enter socket bind\n", ethNumber); 

						dbPA.id	= idxEthAddr;
						db_rowGet (&connect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA);

						memset (&servaddr, '\0', sizeof (servaddr)); 
		  
						// IP und Port zuweisen
						servaddr.sin_family = AF_INET; 
//						servaddr.sin_addr.s_addr = htonl (INADDR_ANY); 
						servaddr.sin_addr.s_addr = inet_addr (dbPA.strValue); 
						servaddr.sin_port = htons(tcpPort);

						soFlag = 1;	// darf beim Aufruf von setsockopt nicht 0 sein

						if (setsockopt (sockfd, SOL_SOCKET, SO_REUSEADDR, &soFlag, sizeof (soFlag)) == -1)
						{
							fprintf (stderr, "eth:%d, Socket %s:%d set option reuse addr failed, error %d, %s\n",
										ethNumber, dbPA.strValue, tcpPort, errno, strerror (errno));
						} // if (setsockopt (sockfd, SOL_SOCKET, SO_REUSEADDR, &soFlag, sizeof (soFlag)) == -1)

						soFlag = 1;	// darf beim Aufruf von setsockopt nicht 0 sein

						if (setsockopt (sockfd, SOL_SOCKET, SO_REUSEPORT, &soFlag, sizeof (soFlag)) == -1)
						{
							fprintf (stderr, "eth:%d, Socket %s:%d set option reuse port failed, error %d, %s\n",
										ethNumber, dbPA.strValue, tcpPort, errno, strerror (errno));
						} // if (setsockopt (sockfd, SOL_SOCKET, SO_REUSEPORT, &soFlag, sizeof (soFlag)) == -1)

						soFlag = 1;	// darf beim Aufruf von setsockopt nicht 0 sein

						if (setsockopt (sockfd, SOL_SOCKET, SO_RCVTIMEO, &timerValue, sizeof (struct timeval)) == -1)
						{
							fprintf (stderr, "eth:%d, Socket %s:%d set option reuse port failed, error %d, %s\n",
										ethNumber, dbPA.strValue, tcpPort, errno, strerror (errno));
						} // if (setsockopt (sockfd, SOL_SOCKET, SO_REUSEPORT, &soFlag, sizeof (soFlag)) == -1)

						// Bindung des Sockets an die zugewiesene IP und Überprüfung
						if ((bind (sockfd, (C_tcpss_sockaddr*)&servaddr, sizeof (servaddr))) != 0)
						{ 
							fprintf (stderr, "eth:%d, Socket %s:%d bind failed, error %d, %s\n",
										ethNumber, dbPA.strValue, tcpPort, errno, strerror (errno)); 

							mainState = E_tcpss_mainState_closeSocket; 
						} // if ((bind (sockfd, (C_tcpss_sockaddr*)&servaddr, sizeof (servaddr))) != 0) 
						else
						{
							fprintf (stderr,"eth:%d, Socket %s:%d successfully binded\n", ethNumber, dbPA.strValue, tcpPort);

							mainState = E_tcpss_mainState_listenClient;
						} // else if ((bind (sockfd, (C_tcpss_sockaddr*)&servaddr, sizeof (servaddr))) != 0)
						break;

					case E_tcpss_mainState_listenClient:
     					fprintf (stderr,"eth:%d, enter server listening\n", ethNumber); 

						// Der Server ist nun bereit und wartet auf einen Client (Verification inklusive)
						if ((listen (sockfd, C_tcpss_backlogLength)) != 0)
						{ 
							fprintf (stderr, "eth:%d, Listen failed, error %d, %s\n", ethNumber, errno, strerror (errno));

							mainState = E_tcpss_mainState_closeSocket;
						} // if ((listen (sockfd, C_tcpss_backlogLength)) != 0) 
						else
						{
							fprintf (stderr,"eth:%d, server listening...\n", ethNumber);

							mainState = E_tcpss_mainState_acceptClient;
						} // else if ((listen (sockfd, C_tcpss_backlogLength)) != 0)
						break;

					case E_tcpss_mainState_acceptClient:
     					fprintf (stderr,"eth:%d, enter server acccept\n", ethNumber); 
						len = sizeof (cli);

						// Aktzeptieren und prüfen der Anfrage vom Client 
						connfd = accept (sockfd, (C_tcpss_sockaddr*)&cli, &len); 

						if (connfd < 0)
						{ 
							fprintf (stderr,"eth:%d, server acccept failed, error %d, %s\n", ethNumber, errno, strerror (errno)); 

							mainState = E_tcpss_mainState_closeSocket;
						} // if (connfd < 0) 
						else
						{
							fprintf (stderr,"eth:%d, server acccept the client, connfd:%d\n", ethNumber, connfd);

							mainState = E_tcpss_mainState_readData;
						} // else if (connfd < 0)
						break;

					case E_tcpss_mainState_readData:
     					fprintf (stderr,"eth:%d, enter read data\n", ethNumber); 

						memset (rxBuffer, '\0', C_tcpss_bufferLength);
						memset (txBuffer, '\0', C_tcpss_bufferLength);

						bytesRead = read (connfd, rxBuffer, sizeof(rxBuffer)); 

//						if (bytesRead < 0)
						if (bytesRead <= 0)
						{
							fprintf (stderr,"eth:%d, read data failed, error %d, %s\n", ethNumber, errno, strerror (errno)); 

//							close (connfd);
//							mainState = E_tcpss_mainState_listenClient;
							mainState = E_tcpss_mainState_closeSocket; // Client hat Port geschlossen -> komplett neu anfangen
						}
						else
						{
							fprintf (stderr,"eth:%d, rxBuffer: %s", ethNumber, rxBuffer);

							mainState = E_tcpss_mainState_evaluatePtk;
						} // else if (bytesRead < 0)
						break;

					case E_tcpss_mainState_evaluatePtk:
     					fprintf (stderr,"eth:%d, enter evaluate protocol\n", ethNumber); 

						ptkC4l_evaluateCmd (&connect, rxBuffer, txBuffer, ethNumber);

						mainState = E_tcpss_mainState_writeData;
						break;

					case E_tcpss_mainState_writeData:
     					fprintf (stderr,"eth:%d, enter write data\n", ethNumber); 

						bytesWrite = tcpss_findIndexOfLf(txBuffer) + 1;

						if (bytesWrite == 0)
						{
							// Unbekanntes Kommando: rxBuffer mit errMsg zusammenhängen
							ptkC4l_replaceChar1withChar2 (rxBuffer, '\r', '\0');
							snprintf (txBuffer, C_db_dataStringSize, "%s%s\r\n",
									  rxBuffer, C_ptkC4l_errMsgCmdInvalid); 
							bytesWrite = tcpss_findIndexOfLf(txBuffer) + 1;
						} // if (bytesWrite == 0)

						write(connfd, txBuffer, bytesWrite);
						fprintf (stderr,"eth:%d, txBuffer: %s", ethNumber, txBuffer);

						mainState = E_tcpss_mainState_readData;
						break;

					case E_tcpss_mainState_closeSocket:
						fprintf (stderr,"eth:%d, close connfd:%d, sockfd:%d\n", ethNumber, connfd, sockfd); 
						close (connfd);
						close (sockfd);
						usleep (C_tcpss_waitBeforeRetry);

						mainState = E_tcpss_mainState_createSocket;
						break;

					default:
						break;
				} // switch(mainState)
			} // while(1)
		} // if (retValue == 0)
		else
		{
			fprintf (stderr,"Initialization mysql library and the access structure to the database failed...\n");
		} // else if (retValue == 0)

		mysql_library_end ();
	}
	else
	{
		fprintf (stderr,"error: no eth number specified\n");
		fprintf (stderr,"usage: tcpss -1 (for eth1)\n");
	} // else if (dbPA.id =! 0)

	return (0);
} /* main */

/* tcpss.c */
