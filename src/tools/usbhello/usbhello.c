 /**************************************************************************//**
 *
 *	\file		usbhello.c
 *
 *	\brief		führt die Aufgaben beim Einstecken eines USB Memorysticks aus.
 *
 *	\details	Zusatzprogramm für den I/O-Logger des techdock Projekts control4log.
 *				Folgende Aufgaben werden ausgeführt:
 *				- öffnen der Datenbank.
 *				- setzen des Flags E_db_flagMemoryStickPlugged. Damit wird der
 *				  Trigger für das Tool usbevent gesetzt.
 *
 *	\attention	Das Programm überwacht die USB schnittstelle nicht. Es muss vom
 *				System beim Erkennen eines Plug In Ereignisses aufgerufen werden.
 *				Dazu kann in /etc/udev/rules.d ein File custom_usb.rules mit
 *				folgendem Inhalt erstellt werden:
 *				- ACTION=="add", SBSYSTEM=="usb", ATTRS{bInterfaceClass}=="08", PROGRAM="/opt/11007/usbhello"
 *
 *	\date		12.11.2021
 *	\author		Marcel Ming
 *	\copyright	TRiMADA AG   CH-5610 Wohlen
 *	\remark		Compiler- / IDE-Version
 *
 *	\version	12.11.21 mm
 *					- Erstellt.
 *	\version	24.11.21 mm
 *					- C_appVersion über builDate.h implementiert.
 *
 *
 ******************************************************************************/


/******************************************************************************
 * Compilerswitches                                                           *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Modules                                                                    *
 *                                                                            *
 ******************************************************************************/
#include <mysql/mysql.h>
#include <stdint.h>
#include <stdio.h>
#include <unistd.h>

#include "buildDate.h"
#include "db.h"


/******************************************************************************
 * Definitions and Macros                                                     *
 *                                                                            *
 ******************************************************************************/
const uint8_t C_appVersion [] = {
   C_buildDate_year_CH0,
   C_buildDate_year_CH1,
   C_buildDate_month_CH0,
   C_buildDate_month_CH1,
   C_buildDate_day_CH0,
   C_buildDate_day_CH1,
   '-',
   C_buildDate_hour_CH0,
   C_buildDate_hour_CH1,
   C_buildDate_min_CH0,
   C_buildDate_min_CH1
};


/******************************************************************************
 * Typedefs and Structures                                                    *
 *                                                                            *
 ******************************************************************************/
 
/******************************************************************************
 * External Variables                                                         *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Global Variables                                                           *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Local Functions                                                            *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Exported Functions                                                         *
 *                                                                            *
 ******************************************************************************/

/**************************************************************************//**
 *
 *	\brief		main
 *
 *	\param		-
 *	\return		-
 *
 ******************************************************************************/
int main (void)
{
	ts_db_connect	connect;
	int				retValue = db_connectInit (&connect,
								   C_db_dataBaseName,
								   C_db_dataBaseHost,
								   C_db_dataBaseUser,
								   C_db_dataBasePassword);


	// stderr auf /tmp/usbhello.err umleiten
	// freopen ("/tmp/usbhello.err", "w", stderr);

	fprintf (stderr, "start usbhello %s as getlogin() user %s\n", C_appVersion, getlogin ());

	if (retValue == 0)
	{
		db_flagSet (&connect, E_db_flagMemoryStickPlugged);
		mysql_library_end ();
	} // if (retValue == 0)

	fprintf (stderr, "end usbhello\n");
	fclose (stderr);

	return (0);
} /* main */


/* usbhello */
