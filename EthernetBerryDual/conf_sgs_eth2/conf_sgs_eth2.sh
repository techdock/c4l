#!/bin/bash
# Configuration script for EthernetBerry board
# Originally designed by Salvatore Guccione - SG Electronic Systems - Italy
# Modified by TECHDOCK GmbH

echo "Start Configuration..."

# Function to set the configuration file path
set_config_path() {
    CONFIG_PATH="/boot/firmware/config.txt"
    if [[ ! -f "$CONFIG_PATH" ]]; then
        CONFIG_PATH="/boot/config.txt"
    fi
    echo "Using configuration file at: $CONFIG_PATH"
}

# Function to determine and set the module path based on architecture
set_module_path() {
    ARCH=$(uname -m)
    MODULES_PATH="/lib/modules/$(uname -r)/kernel/drivers/net/ethernet/microchip"
    if [[ $ARCH != "armv7l" ]]; then
        MODULES_PATH="/usr/lib/modules/$(uname -r)/kernel/drivers/net/ethernet/microchip"
    fi
    echo "Module path set to: $MODULES_PATH"
}

# Function to prepare the kernel module
prepare_kernel_module() {
    # Check and decompress the kernel module if it exists compressed
    if [[ -f "$MODULES_PATH/enc28j60.ko.xz" ]]; then
        sudo xz -d "$MODULES_PATH/enc28j60.ko.xz"
    fi

    # Ensure the module file exists
    if [[ ! -f "$MODULES_PATH/enc28j60.ko" ]]; then
        echo "Error: ENC28J60 kernel module not found."
        exit 1
    fi

    # Copy and rename the kernel module
    echo "Copying and renaming kernel module..."
    sudo cp "$MODULES_PATH/enc28j60.ko" "$MODULES_PATH/enc28j61.ko"
    sudo sed -i.bckp -e "s/\x65\x6E\x63\x32\x38\x6A\x36\x30/\x65\x6E\x63\x32\x38\x6A\x36\x31/g" "$MODULES_PATH/enc28j61.ko"
}

# Function to update configuration files
update_config_files() {
    echo "Updating device tree overlays and modules..."
    echo 'enc28j61' | sudo tee -a /etc/modules
    sudo depmod
    cat <<EOF | sudo tee -a "$CONFIG_PATH"
# Configuration file for EthernetBerry board
# Designed by Salvatore Guccione - SG Electronic Systems - Italy
# Device tree overlays configuration
dtoverlay=spi1-1cs,cs0_pin=18
dtoverlay=sgs_enc28j60-spi1.dtbo,int_pin=25
dtoverlay=enc28j60,int_pin=22
EOF
}

# Main execution flow
set_config_path
set_module_path
prepare_kernel_module
update_config_files

# Copy device tree overlay file
sudo cp sgs_enc28j60-spi1.dtbo /boot/overlays/

echo "Configuration complete. End! Enjoy."
