##### Index

[TOC]

<!---new page--->
<div style="page-break-before:always"></div>

##### Documentation

The follow additional software are neccessary to open and edit documentation files:

|Software|Path file(-s)|
|--------|--------|
|LibreOffice |.\c4l\doc\software |
|FreeCAD |.\c4l\doc\mechanics |
|Siemens PADS Logic VX.2.10 |.\c4l\doc\hardware |
|Siemens PADS Layout VX.2.10 |.\c4l\doc\hardware |
|Geany |.\c4l\src |

### Install Raspberry Pi OS

Install the `Raspberry Pi OS Full (32-bit), version 11 a port of Debain Bullseye` or `Raspberry Pi OS (Legacy), version 10 a port of Debain Buster` on a MicroSD accorrding https://www.raspberrypi.com/software/. Preferable the MicroSD Card is designed for extended writing cycles, 8GB will be enough without desktop, 32GB are recommended when install with desktop.

This project is only working with sudoer user `c4l`.

If using [Raspberry Pi Imager](https://www.raspberrypi.com/software/) for install the OS on MicroSD then it is possible to define the user under the gear wheel as `c4l` instead `pi`.

If using an OS with user `pi` follow the wiki [Change user account pi to c4l](https://gitlab.com/techdock/c4l/-/wikis/Change-user-account-pi-to-c4l).

To be sure the project c4l is running after first installation the IPv4 address from eth0 need to be changed to static IP 192.168.19.77 according section **Set IP**.

### Set IP

> Open dhcpcd.conf
> ```
> sudo nano /etc/dhcpcd.conf
> ```

> Change below the index 'interface eth0' the settings as follow:
> ```
> static ip_address=192.168.19.77/24
> static routers=192.168.19.1
> static domain_name_servers=192.168.19.1
> ```
> ... or use Desktop-GUI if Raspberry Pi OS **Desktop** is installed.


### Set root password

> ```
> sudo -i
> passwd
> exit
> ```

### User

> ```
> Info: User needs to be sudoer!
> ```



### Setup Raspberry Pi OS

##### Activate i2c driver

> ```
> sudo raspi-config 
> ```
> Navigate → `3 Interfaces Options` → `I5 I2C` and activate option


##### Prepare the OS

> ```
> sudo apt-get update
> ```


##### Install neccessary packet

> The follow additional packet are neccessary to operate this project:

> MariaDB
> ```
> sudo apt-get install default-mysql-server
> ```

> Metapaket MySQL
> ```
> sudo apt-get install default-libmysqlclient-dev
> ```

> I2C programming library
> ```
> sudo apt-get install libi2c-dev
> ```

> C library for GPIO
> ```
> sudo apt-get install libgpiod-dev
> ```

> C library for GPIO tools
> ```
> sudo apt-get install libgpiod-doc
> ```

> C library for GPIO and GPIO tools (is probably allready installed!)
> ```
> sudo apt-get install libgpiod2
> ```

> Comman line tools for interact with GPIO
> ```
> sudo apt-get install gpiod
> ```

> Create and manage bridge devices
> ```
> sudo apt-get install bridge-utils
> ```

> SMTP client
> ```
> sudo apt-get install bsd-mailx msmtp msmtp-mta
> ```

> GIT
> ```
> sudo apt install git
> ```


### Install repository c4l

> Info: do not use `sudo` for command **mkdir** and **git** because then it belongs to root!

> ```
> mkdir /home/c4l/projects/
> cd /home/c4l/projects/
> git clone https://gitlab.com/techdock/c4l.git
> ```


### Prepare database

> Info: user and password are hard coded in the follow file and can be customized if wished

> > db.h row 93
> > ```
> > sudo nano /home/c4l/projects/c4l/src/db/db.h
> > ```


> Install security update for database

> > ```
> > sudo mysql_secure_installation
> > ```

> Answer questions as follow:
> > Enter current password for root: **input root passsword from section **Set root password**
> > 
> > Switch to unix_socket authentication: **n**
> > 
> > Change the root password? **y**
> > 
> > Remove anonymous users? **y**
> > 
> > Disallow root login remotely? **y**
> > 
> > Remove 'test' database? **y**
> > 
> > Reload privilege tables now? **y**


> Create user `trimada` in database

> > Log in database
> > ```
> > sudo mysql -u root
> > ```

> > Create user `trimada` and set password
> > ```
> > create user trimada@localhost;
> > grant all privileges on *.* to 'trimada'@'localhost' identified by 'welcome';
> > SET PASSWORD FOR 'trimada'@'localhost' = PASSWORD('Master-5610');
> > exit
> > ```


### Install driver for eth1 & -2 driver located at HAT

> ```
> cd /home/c4l/projects/c4l/EthernetBerryDual/conf_sgs_eth2
> sudo bash conf_sgs_eth2.sh
> ```


### Enter projects source directory

> > ```
> > cd /home/c4l/projects/c4l/src
> > ```


### Stop services and build binaries 

> Info 1: delete and build binaries new is just neccessary if user and password of database was customized

> Info 2: service usbevent and tcpss need to be owned by current sudoer user and not root, check out variable `user` in script

> > Build binaries
> >
> > ```
> > sudo bash project-build.sh
> > ```


### Install project

> Info: copies all needed projects file to the neccesary destinations

> >
> > Install project 
> > ```
> > sudo bash project-install.sh
> > ```


### Clean project

> Info: clean project is just 'nice to have'. The script will remove all files, which are not mandatory to run the project.

> > Clean project
> >
> > ```
> > sudo bash project-clean.sh
> > ```


### Start project

> Info: start project will add needed c4l services to systemctl list and start all of them.

> > Clean project
> >
> > ```
> > sudo bash project-start.sh
> > ```


### Restart and write eth standard settings

Info: standard IPv4 settings are comming from `/home/c4l/projects/c4l/src/db/11007-PA-default.csv` in column `value`. Based at `11007-PA-default.csv` the table `PA` in database is build and can be access by query `SELECT * FROM PA;`

|PA|standard value|meaning|
|--------|--------|--------|
|101|192.168.19.77|IPv4 for eth0|
|201|192.168.78.78|IPv4 for eth1|
|251|192.168.92.92|IPv4 for eth2|

_More information about parameters at https://techdock.ch/de/downloads/control4log_


> Prepare database for restart
> ```
> mysql -u trimada -p
> ```
> Passwort: `Master-5610`

> Choose database
> ```
> use testBase
> ```

> Set flag for `FC33`
> ```
> UPDATE FLAG SET value = 1 WHERE id = 3;
> ```

> Set flag for `FC34`
> ```
> UPDATE FLAG SET value = 1 WHERE id = 4;
> ```

> Restart with `FC99`
> ```
> UPDATE FLAG SET value = 1 WHERE id = 5;
> ```

> The Raspiberry Pi will start several times to write all data 


### Check status of services after rebooting

> **tcpss**
> ```
> sudo systemctl status tcpss.service
> ```
> Result:
> 
> > _Loaded: loaded (/home/c4l/projects/c4l/src/tcpss/tcpss.service; enabled; vendor preset: enabled)_
> 
> > _Active: inactive (dead) ..._


> **usbevent**
> ```
>  sudo systemctl status usbevent.service
> ```
> Result:
> 
> > _Loaded: loaded (/home/c4l/projects/c4l/src/tools/usbevent/usbevent.service; enabled; vendor preset: enabled)_
> 
> > _Active: active (running) ..._


> **ioLog**
> ```
>  sudo systemctl status ioLog.service
> ```
> Result:
> 
> > _Loaded: loaded (/home/c4l/projects/c4l/src/iolog/ioLog.service; enabled; vendor preset: enabled)_
> 
> > _Active: active (running) ..._


> **Alternative** 
> 
> all running services by root can be displayed with ...
> 
> ```
> ps -U root -u root -N
> ```
>
> all services as tree can be displayed with ...
>
> ```
> ps axjf
> ```
>
> all running services by user can be displayed with ...
>
> ```
> ps -eo euser,ruser,suser,fuser,f,comm,label
> ```
>
> The list should show:
> 
> _tcpss (three instances, one for each eth port)_
> 
> _ioLog_
> 
> _usbevent_


### c4l-Wikis

[Config device with USB flash drive](https://gitlab.com/techdock/c4l/-/wikis/Config-device-with-USB-storage)

[Config device device with ethernet](https://gitlab.com/techdock/c4l/-/wikis/Config-device-with-ETH-interface)

[Encrypt SMTP password](https://gitlab.com/techdock/c4l/-/wikis/Encrypt-SMTP-password)



### Uninstall project

> > **Enter projects source directory**
> > ```
> > cd ./projects/c4l/src
> > ```

> > **Stop services**
> >
> > ```
> > sudo bash project-stop.sh
> > ```

> > **Delete all build files in project**
> >
> > ```
> > sudo bash project-clean.sh
> > ```

> > **Delete all build binaries**
> >
> > ```
> > sudo bash project-uninstall.sh
> > ```

